package com.partisiablockchain.fee;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.KeyPair;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ServicePayerTest {

  @Test
  public void payServiceFees() {
    List<BlockchainAddress> observedNodes = new ArrayList<>();
    List<Long> observedFees = new ArrayList<>();
    ServicePayer servicePayer =
        (gas, target) -> {
          observedFees.add(gas);
          observedNodes.add(target);
        };

    FixedList<BlockchainAddress> nodes = generateNodes();
    FixedList<Integer> weights = FixedList.create(List.of(1, 1, 2, 1));

    servicePayer.payServiceFees(1_000L, nodes, weights);
    Assertions.assertThat(observedNodes).containsExactlyElementsOf(nodes);
    Assertions.assertThat(observedFees).containsExactly(200L, 200L, 400L, 200L);
  }

  static FixedList<BlockchainAddress> generateNodes() {
    Random rng = new SecureRandom();

    FixedList<BlockchainAddress> result = FixedList.create();
    for (int i = 0; i < 4; i++) {
      KeyPair keyPair = new KeyPair(BigInteger.probablePrime(32, rng));
      result = result.addElement(keyPair.getPublic().createAddress());
    }
    return result;
  }
}
