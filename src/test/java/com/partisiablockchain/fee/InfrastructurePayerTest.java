package com.partisiablockchain.fee;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class InfrastructurePayerTest {

  @Test
  public void payInfrastructureFees() {
    List<BlockchainAddress> observedNodes = new ArrayList<>();
    List<Long> observedFees = new ArrayList<>();
    InfrastructurePayer infrastructurePayer =
        (gas, target) -> {
          observedFees.add(gas);
          observedNodes.add(target);
        };

    FixedList<BlockchainAddress> nodes = ServicePayerTest.generateNodes();
    FixedList<Integer> weights = FixedList.create(List.of(1, 1, 2, 1));

    infrastructurePayer.payInfrastructureFees(1_000L, nodes, weights);
    Assertions.assertThat(observedNodes).containsExactlyElementsOf(nodes);
    Assertions.assertThat(observedFees).containsExactly(200L, 200L, 400L, 200L);
  }
}
