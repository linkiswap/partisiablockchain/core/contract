package com.partisiablockchain.fee;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.security.SecureRandom;
import java.util.List;
import java.util.Random;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class FeeDistributorTest {

  private final Random rng = new SecureRandom();

  @Test
  public void distributeThrowsExceptionIfWeightsAndNodesAreNotEqualLength() {
    Assertions.assertThatThrownBy(
            () ->
                FeeDistributor.distribute(
                    0L,
                    FixedList.create(),
                    FixedList.create(List.of(1)),
                    FunctionUtility.noOpBiConsumer()))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void gasIsDistributedAccordingToWeights() {
    FixedList<Integer> weights = FixedList.create(List.of(1, 3, 4, 2));
    FixedList<Long> distributions = FeeDistributor.distributeGas(1_000L, weights);
    Assertions.assertThat(distributions).containsExactly(100L, 300L, 400L, 200L);
  }

  @Test
  public void gasAmountThatIsNotDivisibleByWeightIsUnequallyDistributed() {
    FixedList<Integer> weights = FixedList.create(List.of(1, 1, 1));
    FixedList<Long> distributions = FeeDistributor.distributeGas(1_000L, weights);
    Assertions.assertThat(distributions).containsExactlyInAnyOrder(333L, 333L, 334L);
  }

  @Test
  public void distributedGasSumsToTotalAmountOfGas() {
    for (int i = 0; i < 100; i++) {
      long gas = rng.longs(0, Long.MAX_VALUE).findAny().orElseThrow() + 1;
      int noOfWeights = rng.nextInt(10) + 1;

      FixedList<Integer> weights = generateWeights(noOfWeights);
      FixedList<Long> distributions = FeeDistributor.distributeGas(gas, weights);

      Long totalDistributedGas = distributions.stream().reduce(0L, Long::sum);
      Assertions.assertThat(totalDistributedGas).isEqualTo(gas);
    }
  }

  private FixedList<Integer> generateWeights(int length) {
    FixedList<Integer> result = FixedList.create();
    for (int i = 0; i < length; i++) {
      result = result.addElement(rng.nextInt(10) + 1);
    }
    return result;
  }
}
