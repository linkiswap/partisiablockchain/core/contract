package com.partisiablockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import java.util.List;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BlockchainAddressTest {

  @Test
  public void equals() {
    EqualsVerifier.forClass(BlockchainAddress.class).usingGetClass().verify();
  }

  @Test
  public void toStringTest() {
    KeyPair keyPair = new KeyPair();
    BlockchainAddress account = keyPair.getPublic().createAddress();

    Assertions.assertThat(account.toString()).contains(account.writeAsString());
  }

  @Test
  public void fromStringAccount() {
    BlockchainAddress address =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    byte[] expected = new byte[20];
    expected[19] = 1;
    Assertions.assertThat(address.getIdentifier()).isEqualTo(expected);
    Assertions.assertThat(address.getType()).isEqualTo(BlockchainAddress.Type.ACCOUNT);
  }

  @Test
  public void fromStringContract() {
    BlockchainAddress address =
        BlockchainAddress.fromString("010000000000000000000000000000000000000001");
    byte[] expected = new byte[20];
    expected[19] = 1;
    Assertions.assertThat(address.getIdentifier()).isEqualTo(expected);
    Assertions.assertThat(address.getType()).isEqualTo(BlockchainAddress.Type.CONTRACT_SYSTEM);
  }

  @Test
  public void fromStringContractPublic() {
    BlockchainAddress address =
        BlockchainAddress.fromString("020000000000000000000000000000000000000001");
    byte[] expected = new byte[20];
    expected[19] = 1;
    Assertions.assertThat(address.getIdentifier()).isEqualTo(expected);
    Assertions.assertThat(address.getType()).isEqualTo(BlockchainAddress.Type.CONTRACT_PUBLIC);
  }

  @Test
  public void fromStringContractZk() {
    BlockchainAddress address =
        BlockchainAddress.fromString("030000000000000000000000000000000000000001");
    byte[] expected = new byte[20];
    expected[19] = 1;
    Assertions.assertThat(address.getIdentifier()).isEqualTo(expected);
    Assertions.assertThat(address.getType()).isEqualTo(BlockchainAddress.Type.CONTRACT_ZK);
  }

  @Test
  public void fromStringTooShort() {
    Assertions.assertThatThrownBy(
            () -> BlockchainAddress.fromString("0000000000000000000000000000000000000001"))
        .isInstanceOf(ArrayIndexOutOfBoundsException.class);
  }

  @Test
  public void fromPublicKey() {
    KeyPair keyPair = new KeyPair(BigInteger.TWO);
    BlockchainAddress blockchainAddress = keyPair.getPublic().createAddress();

    Assertions.assertThat(blockchainAddress.getType()).isEqualTo(BlockchainAddress.Type.ACCOUNT);
    Assertions.assertThat(blockchainAddress.writeAsString())
        .isEqualTo("00b2e734b5d8da089318d0d2b076c19f59c450855a");
  }

  @Test
  public void compare() {
    BlockchainAddress first =
        BlockchainAddress.fromString("000000000000000000000000000000000000000000");
    BlockchainAddress second =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    BlockchainAddress contract =
        BlockchainAddress.fromString("010000000000000000000000000000000000000001");
    KeyPair keyPair = new KeyPair(BigInteger.TEN);
    BlockchainAddress third = keyPair.getPublic().createAddress();

    Assertions.assertThat(first).usingDefaultComparator().isEqualByComparingTo(first);
    Assertions.assertThat(first).usingDefaultComparator().isLessThan(second);
    Assertions.assertThat(first).usingDefaultComparator().isGreaterThan(third);
    Assertions.assertThat(first).usingDefaultComparator().isLessThan(contract);

    Assertions.assertThat(second).usingDefaultComparator().isGreaterThan(first);
    Assertions.assertThat(second).usingDefaultComparator().isEqualByComparingTo(second);
    Assertions.assertThat(second).usingDefaultComparator().isGreaterThan(third);
    Assertions.assertThat(second).usingDefaultComparator().isLessThan(contract);

    Assertions.assertThat(third).usingDefaultComparator().isLessThan(first);
    Assertions.assertThat(third).usingDefaultComparator().isLessThan(second);
    Assertions.assertThat(third).usingDefaultComparator().isEqualByComparingTo(third);
    Assertions.assertThat(third).usingDefaultComparator().isLessThan(contract);

    Assertions.assertThat(contract).usingDefaultComparator().isGreaterThan(first);
    Assertions.assertThat(contract).usingDefaultComparator().isGreaterThan(second);
    Assertions.assertThat(contract).usingDefaultComparator().isGreaterThan(third);
    Assertions.assertThat(contract).usingDefaultComparator().isEqualByComparingTo(contract);
  }

  @Test
  public void hash() {
    BlockchainAddress account =
        BlockchainAddress.fromString("000000000000000000000000000000000000000000");
    BlockchainAddress contract =
        BlockchainAddress.fromString("010000000000000000000000000000000000000000");

    Assertions.assertThat(hash(account)).isNotEqualTo(hash(contract));
  }

  private Hash hash(BlockchainAddress contract) {
    return Hash.create(contract);
  }

  @Test
  public void listSerializer() {
    List<BlockchainAddress> addresses =
        List.of(
            BlockchainAddress.fromString("00339896577caf2fc1cdcb1b144683e1312b791c24"),
            BlockchainAddress.fromString("00103cd829090ac5fafbbb0abd7c732aaf7e35c1f2"),
            BlockchainAddress.fromString("01103cd829090ac5fafbbb0abd7c732aaf7e35c1f2"));
    List<BlockchainAddress> deserialized =
        SafeDataInputStream.deserialize(
            stream -> BlockchainAddress.LIST_SERIALIZER.readFixed(stream, addresses.size()),
            SafeDataOutputStream.serialize(
                stream -> BlockchainAddress.LIST_SERIALIZER.writeFixed(stream, addresses)));
    Assertions.assertThat(deserialized).isEqualTo(addresses);
  }
}
