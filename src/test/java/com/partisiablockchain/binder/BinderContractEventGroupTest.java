package com.partisiablockchain.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test of the data transfer object. */
public final class BinderContractEventGroupTest {

  private final BinderEvent e1;
  private final BinderEventGroup<BinderEvent> binderEventGroup1;
  private final BinderEventGroup<BinderEvent> binderEventGroup2;

  /** Creates a new test. */
  public BinderContractEventGroupTest() {
    e1 = new BinderEvent() {};
    binderEventGroup1 = new BinderEventGroup<>(new byte[] {1, 2}, 1234L, true, List.of(e1));
    binderEventGroup2 = new BinderEventGroup<>(List.of(e1));
  }

  @Test
  public void getCallback() {
    Assertions.assertThat(binderEventGroup1.getCallbackRpc()).isEqualTo(new byte[] {1, 2});
    Assertions.assertThat(binderEventGroup2.getCallbackRpc()).isNull();
  }

  @Test
  public void getCallbackCost() {
    Assertions.assertThat(binderEventGroup1.getCallbackCost()).isEqualTo(1234L);
    Assertions.assertThat(binderEventGroup2.getCallbackCost()).isNull();
  }

  @Test
  public void getCallbackCostFromContract() {
    Assertions.assertThat(binderEventGroup1.isCallbackCostFromContract()).isTrue();
    Assertions.assertThat(binderEventGroup2.isCallbackCostFromContract()).isFalse();
  }

  @Test
  public void getEvents() {
    Assertions.assertThat(binderEventGroup1.getEvents()).hasSize(1).contains(e1);
    Assertions.assertThat(binderEventGroup2.getEvents()).hasSize(1).contains(e1);
  }
}
