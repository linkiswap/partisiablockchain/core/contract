package com.partisiablockchain.binder.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.crypto.Hash;
import com.secata.tools.coverage.FunctionUtility;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BinderEventTest {

  private static final Hash EMPTY_HASH = Hash.create(FunctionUtility.noOpConsumer());
  private static final BlockchainAddress CONTRACT_ADDRESS =
      BlockchainAddress.fromHash(BlockchainAddress.Type.CONTRACT_PUBLIC, EMPTY_HASH);
  private static final BlockchainAddress ACCOUNT =
      BlockchainAddress.fromHash(BlockchainAddress.Type.ACCOUNT, EMPTY_HASH);
  private static final String SHARD = "Test";
  private static final byte[] RPC = new byte[0];

  @Test
  public void interaction() {
    BinderInteraction interaction =
        new BinderInteraction(CONTRACT_ADDRESS, RPC, true, 12354, false);
    Assertions.assertThat(interaction.contract).isEqualTo(CONTRACT_ADDRESS);
    Assertions.assertThat(interaction.rpc).isEmpty();
    Assertions.assertThat(interaction.originalSender).isTrue();
    Assertions.assertThat(interaction.effectiveCost).isEqualTo(12354);
  }

  @Test
  public void interactionCostFromContract() {
    BinderInteraction interaction =
        new BinderInteraction(CONTRACT_ADDRESS, RPC, false, 54321, true);
    Assertions.assertThat(interaction.contract).isEqualTo(CONTRACT_ADDRESS);
    Assertions.assertThat(interaction.rpc).isEmpty();
    Assertions.assertThat(interaction.originalSender).isFalse();
    Assertions.assertThat(interaction.effectiveCost).isEqualTo(54321);
    Assertions.assertThat(interaction.costFromContract).isTrue();
  }

  @Test
  public void deploy() {
    BinderDeploy deploy = new BinderDeploy(CONTRACT_ADDRESS, RPC, RPC, RPC, RPC, true, 12354);
    Assertions.assertThat(deploy.contract).isEqualTo(CONTRACT_ADDRESS);
    Assertions.assertThat(deploy.binderJar).isEmpty();
    Assertions.assertThat(deploy.abi).isEmpty();
    Assertions.assertThat(deploy.rpc).isEmpty();
    Assertions.assertThat(deploy.contractJar).isEmpty();
    Assertions.assertThat(deploy.originalSender).isTrue();
    Assertions.assertThat(deploy.effectiveCost).isEqualTo(12354);
  }

  @Test
  public void testCreateAccount() {
    SystemInteraction.CreateAccount createAccount = new SystemInteraction.CreateAccount(ACCOUNT);
    Assertions.assertThat(createAccount.address).isEqualTo(ACCOUNT);
  }

  @Test
  public void testCreateShard() {
    SystemInteraction.CreateShard createShard = new SystemInteraction.CreateShard(SHARD);
    Assertions.assertThat(createShard.shardId).isEqualTo(SHARD);
  }

  @Test
  public void testRemoveShard() {
    SystemInteraction.RemoveShard removeShard = new SystemInteraction.RemoveShard(SHARD);
    Assertions.assertThat(removeShard.shardId).isEqualTo(SHARD);
  }

  @Test
  public void testUpdateLocalAccountPluginState() {
    SystemInteraction.UpdateLocalAccountPluginState updateLocalAccountPluginState =
        new SystemInteraction.UpdateLocalAccountPluginState(
            LocalPluginStateUpdate.create(ACCOUNT, RPC));
    Assertions.assertThat(updateLocalAccountPluginState.update.getRpc()).isEmpty();
    Assertions.assertThat(updateLocalAccountPluginState.update.getContext()).isEqualTo(ACCOUNT);
  }

  @Test
  public void testUpdateContextFreeAccountPluginState() {
    SystemInteraction.UpdateContextFreeAccountPluginState updateContextFreeAccountPluginState =
        new SystemInteraction.UpdateContextFreeAccountPluginState(SHARD, RPC);
    Assertions.assertThat(updateContextFreeAccountPluginState.rpc).isEmpty();
    Assertions.assertThat(updateContextFreeAccountPluginState.shardId).isEqualTo(SHARD);
  }

  @Test
  public void testUpdateGlobalAccountPluginState() {
    SystemInteraction.UpdateGlobalAccountPluginState updateGlobalAccountPluginState =
        new SystemInteraction.UpdateGlobalAccountPluginState(GlobalPluginStateUpdate.create(RPC));
    Assertions.assertThat(updateGlobalAccountPluginState.update.getRpc()).isEmpty();
  }

  @Test
  public void testUpdateAccountPlugin() {
    SystemInteraction.UpdateAccountPlugin updateAccountPlugin =
        new SystemInteraction.UpdateAccountPlugin(RPC, RPC);
    Assertions.assertThat(updateAccountPlugin.pluginJar).isEmpty();
    Assertions.assertThat(updateAccountPlugin.rpc).isEmpty();
  }

  @Test
  public void testUpdateLocalConsensusPluginState() {
    SystemInteraction.UpdateLocalConsensusPluginState updateLocalConsensusPluginState =
        new SystemInteraction.UpdateLocalConsensusPluginState(
            LocalPluginStateUpdate.create(CONTRACT_ADDRESS, RPC));
    Assertions.assertThat(updateLocalConsensusPluginState.update.getRpc()).isEmpty();
    Assertions.assertThat(updateLocalConsensusPluginState.update.getContext())
        .isEqualTo(CONTRACT_ADDRESS);
  }

  @Test
  public void testUpdateGlobalConsensusPluginState() {
    SystemInteraction.UpdateGlobalConsensusPluginState updateGlobalConsensusPluginState =
        new SystemInteraction.UpdateGlobalConsensusPluginState(GlobalPluginStateUpdate.create(RPC));
    Assertions.assertThat(updateGlobalConsensusPluginState.update.getRpc()).isEmpty();
  }

  @Test
  public void testUpdateConsensusPlugin() {
    SystemInteraction.UpdateConsensusPlugin updateConsensusPlugin =
        new SystemInteraction.UpdateConsensusPlugin(RPC, RPC);
    Assertions.assertThat(updateConsensusPlugin.pluginJar).isEmpty();
    Assertions.assertThat(updateConsensusPlugin.rpc).isEmpty();
  }

  @Test
  public void testUpdateRoutingPlugin() {
    SystemInteraction.UpdateRoutingPlugin updateRoutingPlugin =
        new SystemInteraction.UpdateRoutingPlugin(RPC, RPC);
    Assertions.assertThat(updateRoutingPlugin.pluginJar).isEmpty();
    Assertions.assertThat(updateRoutingPlugin.rpc).isEmpty();
  }

  @Test
  public void testRemoveContract() {
    SystemInteraction.RemoveContract removeContract =
        new SystemInteraction.RemoveContract(CONTRACT_ADDRESS);
    Assertions.assertThat(removeContract.contract).isEqualTo(CONTRACT_ADDRESS);
  }

  @Test
  public void testExists() {
    SystemInteraction.Exists exists = new SystemInteraction.Exists(ACCOUNT);
    Assertions.assertThat(exists.address).isEqualTo(ACCOUNT);
  }

  @Test
  public void testSetFeature() {
    SystemInteraction.SetFeature setFeature = new SystemInteraction.SetFeature("Test", "Value");
    Assertions.assertThat(setFeature.key).isEqualTo("Test");
    Assertions.assertThat(setFeature.value).isEqualTo("Value");
  }

  @Test
  public void testUpgradeSystemContract() {
    SystemInteraction.UpgradeSystemContract upgradeSystemContract =
        new SystemInteraction.UpgradeSystemContract(RPC, RPC, RPC, RPC, CONTRACT_ADDRESS);
    Assertions.assertThat(upgradeSystemContract.binderJar).isEmpty();
    Assertions.assertThat(upgradeSystemContract.contractJar).isEmpty();
    Assertions.assertThat(upgradeSystemContract.rpc).isEmpty();
    Assertions.assertThat(upgradeSystemContract.abi).isEmpty();
    Assertions.assertThat(upgradeSystemContract.contractAddress).isEqualTo(CONTRACT_ADDRESS);
  }
}
