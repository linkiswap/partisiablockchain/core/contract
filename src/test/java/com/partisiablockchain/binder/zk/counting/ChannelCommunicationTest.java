package com.partisiablockchain.binder.zk.counting;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.serialization.MemoryStorage;
import com.partisiablockchain.serialization.SerializationResult;
import com.partisiablockchain.serialization.StateSerializer;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ChannelCommunicationTest {

  private final ChannelCommunication channelCommunication;
  private final List<Long> sent;
  private final List<Long> received;

  /** Creates a mew test instance. */
  public ChannelCommunicationTest() {
    sent = List.of(123L, 432L, 111L);
    received = List.of(432L, 124L, 111L);
    channelCommunication = ChannelCommunication.create(sent, received);
  }

  @Test
  public void getBytesSent() {
    assertEqualList(channelCommunication.getBytesSent(), sent);
  }

  @Test
  public void getBytesReceived() {
    assertEqualList(channelCommunication.getBytesReceived(), received);
  }

  @Test
  public void readAndWriteByteStream() {
    byte[] serialize = SafeDataOutputStream.serialize(channelCommunication::write);
    ChannelCommunication fromBytes =
        SafeDataInputStream.deserialize(ChannelCommunication::read, serialize);
    assertEqualList(fromBytes.getBytesSent(), sent);
    assertEqualList(fromBytes.getBytesReceived(), received);
  }

  @Test
  public void readAndWriteStateSerializable() {
    final boolean supportRecordDeserialization = false; // Not required for this functionality
    StateSerializer serializer =
        new StateSerializer(new MemoryStorage(), supportRecordDeserialization);
    SerializationResult write = serializer.write(channelCommunication);
    ChannelCommunication read = serializer.read(write.hash(), ChannelCommunication.class);
    assertEqualList(read.getBytesSent(), sent);
    assertEqualList(read.getBytesReceived(), received);
  }

  private void assertEqualList(FixedList<Long> actual, List<Long> expected) {
    Assertions.assertThat(actual).containsExactlyElementsOf(expected);
  }

  @Test
  public void create() {
    ChannelCommunication channelCommunication =
        ChannelCommunication.create(new long[] {123, 432, 111}, new long[] {432, 124, 111});
    assertEqualList(channelCommunication.getBytesSent(), sent);
    assertEqualList(channelCommunication.getBytesReceived(), received);
  }
}
