package com.partisiablockchain.tree;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.serialization.StateLong;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class AvlCompositeTest {

  @Test
  public void heightAndBalance() {
    AvlComposite<Integer, StateLong> avlComposite =
        new AvlComposite<>(new AvlLeaf<>(1, new StateLong(1)), new AvlLeaf<>(2, new StateLong(2)));

    Assertions.assertThat(avlComposite.height()).isEqualTo((byte) 1);
    Assertions.assertThat(avlComposite.balance()).isEqualTo(0);
  }
}
