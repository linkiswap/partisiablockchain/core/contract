package com.partisiablockchain.tree;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.HashHistoryStorage;
import com.partisiablockchain.serialization.MemoryStorage;
import com.partisiablockchain.serialization.StateLong;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateSerializer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class AvlTreeTest {

  private final Random random = new Random(1475);

  @Test
  public void randomInsertionsAndRemoveAll() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    HashSet<Integer> keys = new HashSet<>();
    for (int i = 0; i < 1000; i++) {
      int next = random.nextInt(2000);
      boolean alreadyPresent = keys.contains(next);
      Assertions.assertThat(tree.containsKey(next)).isEqualTo(alreadyPresent);
      if (!alreadyPresent) {
        Assertions.assertThat(tree.getValue(next)).isNull();
      } else {
        Assertions.assertThat(tree.getValue(next).value()).isEqualTo(next);
      }
      keys.add(next);
      tree = tree.set(next, new StateLong(next));
      Assertions.assertThat(testTree(tree)).isTrue();
      Assertions.assertThat(tree.size()).isEqualTo(keys.size());
    }
    for (Integer key : keys) {
      Assertions.assertThat(tree.getValue(key).value()).isEqualTo(Long.valueOf(key));
    }
    System.out.println(tree(0, tree));
    System.out.println();

    for (Integer key : keys) {
      tree = tree.remove(key);
      Assertions.assertThat(testTree(tree)).isTrue();
    }

    Assertions.assertThat(tree.size()).isEqualTo(0);
  }

  @Test
  public void randomInsertionsAndRemoveInRandomOrder() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    HashSet<Integer> keys = new HashSet<>();
    for (int i = 0; i < 1000; i++) {
      int next = random.nextInt(3000);
      keys.add(next);
      tree = tree.set(next, new StateLong(next));
      Assertions.assertThat(testTree(tree)).isTrue();
      Assertions.assertThat(tree.size()).isEqualTo(keys.size());
    }
    for (Integer key : keys) {
      Assertions.assertThat(tree.getValue(key).value()).isEqualTo(Long.valueOf(key));
    }

    List<Integer> integers = new ArrayList<>(keys);
    Collections.shuffle(integers, random);
    for (Integer key : integers) {
      tree = tree.remove(key);
      Assertions.assertThat(testTree(tree)).isTrue();
    }

    Assertions.assertThat(tree.size()).isEqualTo(0);
  }

  @Test
  public void piBoundaryInBalance() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    tree = tree.set(121, new StateLong(1));
    tree = tree.set(222, new StateLong(1));
    tree = tree.set(929, new StateLong(1));
    testTree(tree);
    tree = tree.set(1000, new StateLong(1));
    tree = tree.set(1001, new StateLong(1));
    testTree(tree);
    tree = tree.remove(929);
    testTree(tree);
  }

  @Test
  public void removeFromEmpty() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    tree = tree.remove(0);
    Assertions.assertThat(tree.size()).isEqualTo(0);
  }

  @Test
  public void getFromEmpty() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    Assertions.assertThat(tree.getValue(0)).isNull();
  }

  @Test
  public void removeNonExisting() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    tree = tree.set(1, new StateLong(2)).remove(2);

    Assertions.assertThat(tree.size()).isEqualTo(1);
  }

  @Test
  public void removeRandomOrder() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    List<Integer> values = IntStream.range(0, 100).boxed().collect(Collectors.toList());
    for (int i = 0; i < values.size(); i++) {
      Assertions.assertThat(tree.size()).isEqualTo(i);
      Integer value = values.get(i);
      tree = tree.set(value, new StateLong(value));
      Assertions.assertThat(testTree(tree)).isTrue();
    }

    ArrayList<Integer> shuffled = new ArrayList<>(values);
    Collections.shuffle(shuffled, random);
    for (int i = shuffled.size() - 1; i >= 0; i--) {
      Integer key = shuffled.get(i);
      tree = tree.remove(key);
      Assertions.assertThat(testTree(tree)).isTrue();
      Assertions.assertThat(tree.size()).isEqualTo(i);
      // Safe to remove twice
      tree = tree.remove(key);
    }
    Assertions.assertThat(tree.size()).isEqualTo(0);
  }

  @Test
  public void keySet() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    Assertions.assertThat(tree.keySet()).isEmpty();
    tree = tree.set(11, new StateLong(22));
    Assertions.assertThat(tree.keySet()).containsExactlyInAnyOrder(11);
    tree = tree.set(99, new StateLong(22));
    Assertions.assertThat(tree.keySet()).containsExactlyInAnyOrder(11, 99);
  }

  @Test
  public void values() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    Assertions.assertThat(tree.values()).isEmpty();
    tree = tree.set(11, new StateLong(22));
    Assertions.assertThat(tree.values()).hasSize(1);
    Assertions.assertThat(tree.values().get(0).value()).isEqualTo(22);
    tree = tree.set(99, new StateLong(22));
    Assertions.assertThat(tree.values()).hasSize(2);
    Assertions.assertThat(tree.values().get(0).value()).isEqualTo(22);
    Assertions.assertThat(tree.values().get(1).value()).isEqualTo(22);
  }

  @Test
  public void fromMapShouldBeStable() {
    LinkedHashMap<Integer, StateLong> first = new LinkedHashMap<>();
    first.put(1, new StateLong(1));
    first.put(2, new StateLong(2));
    first.put(3, new StateLong(3));

    LinkedHashMap<Integer, StateLong> second = new LinkedHashMap<>();
    second.put(3, new StateLong(3));
    second.put(2, new StateLong(2));
    second.put(1, new StateLong(1));

    AvlTree<Integer, StateLong> firstAvl = AvlTree.create(first);
    AvlTree<Integer, StateLong> secondAvl = AvlTree.create(second);

    MemoryStorage storage = new MemoryStorage();
    final boolean supportDeserializeRecords = false; // Not needed for this functionality
    StateSerializer populate =
        new StateSerializer(new HashHistoryStorage(storage), supportDeserializeRecords);
    WithAvlTree firstSerializable = new WithAvlTree(firstAvl);
    Assertions.assertThat(firstSerializable.getTree()).isEqualTo(firstAvl);
    Hash firstHash = populate.createHash(firstSerializable);
    Hash secondHash = populate.createHash(new WithAvlTree(secondAvl));
    Assertions.assertThat(firstHash).isEqualTo(secondHash);
  }

  private String tree(int i, AvlTree<Integer, StateLong> node) {
    if (node == null) {
      return "Empty";
    }
    StringBuilder r = new StringBuilder();
    String indent = "-".repeat(i);
    r.append(indent);
    r.append(node.getKey());
    r.append("\n");
    if (node instanceof AvlComposite) {
      r.append(tree(i + 1, ((AvlComposite<Integer, StateLong>) node).getLeft()));
      r.append(tree(i + 1, ((AvlComposite<Integer, StateLong>) node).getRight()));
    }
    return r.toString();
  }

  private boolean testTree(AvlTree<Integer, StateLong> node) {
    if (node instanceof AvlComposite) {
      AvlTree<Integer, StateLong> left = ((AvlComposite<Integer, StateLong>) node).getLeft();
      AvlTree<Integer, StateLong> right = ((AvlComposite<Integer, StateLong>) node).getRight();
      Integer leftMax = left.maxKeyInTree();
      Integer rightMin = minKeyInTree(right);
      boolean leftOk = leftMax.compareTo(node.getKey()) == 0;
      boolean rightOk = rightMin.compareTo(node.getKey()) > 0;
      boolean keyOk = leftOk && rightOk;
      if (!keyOk) {
        System.out.println("Error at " + node.getKey());
        System.out.println(this);
        System.out.println("------------------");
      }
      return keyOk && testTree(left) && testTree(right) && Math.abs(node.balance()) <= 1;
    }
    return true;
  }

  private Integer minKeyInTree(AvlTree<Integer, StateLong> node) {
    if (node instanceof AvlComposite) {
      AvlTree<Integer, StateLong> left = ((AvlComposite<Integer, StateLong>) node).getLeft();
      AvlTree<Integer, StateLong> right = ((AvlComposite<Integer, StateLong>) node).getRight();
      return min(minKeyInTree(left), minKeyInTree(right));
    } else {
      return node.getKey();
    }
  }

  private Integer min(Integer left, Integer right) {
    return left.compareTo(right) < 0 ? left : right;
  }

  @Override
  public String toString() {
    return "AvlTreeTest{" + "random=" + random + '}';
  }

  @Immutable
  static final class WithAvlTree implements StateSerializable {

    private final AvlTree<Integer, StateLong> tree;

    WithAvlTree() {
      tree = null;
    }

    WithAvlTree(AvlTree<Integer, StateLong> tree) {
      this.tree = tree;
    }

    public AvlTree<Integer, StateLong> getTree() {
      return tree;
    }
  }
}
