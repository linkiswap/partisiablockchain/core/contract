package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class EventManagerTest extends EventCreatorAbstract {

  public EventManagerTest() {
    super(new ContractEventGroup(), EventManagerImpl::new);
  }

  @Test
  public void settingCallbackRpc() {
    ContractEventGroup contractEventGroup = new ContractEventGroup();
    EventManager eventManager = new EventManagerImpl(contractEventGroup);
    Assertions.assertThatThrownBy(() -> eventManager.registerCallback(EventCreator.EMPTY_RPC, -1))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cost must be 0 or larger: -1");
    eventManager.registerCallback(EventCreator.EMPTY_RPC, 0);
    Assertions.assertThat(contractEventGroup.callbackRpc).isEqualTo(new byte[0]);
    Assertions.assertThat(contractEventGroup.callbackCost).isEqualTo(0);
    Assertions.assertThat(contractEventGroup.callbackCostFromContract).isFalse();

    Assertions.assertThatThrownBy(
            () -> eventManager.registerCallbackWithCostFromContract(EventCreator.EMPTY_RPC, -1))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cost must be 0 or larger: -1");
    eventManager.registerCallbackWithCostFromContract(s -> s.writeInt(1), 24);
    Assertions.assertThat(contractEventGroup.callbackRpc).isEqualTo(new byte[] {0, 0, 0, 1});
    Assertions.assertThat(contractEventGroup.callbackCost).isEqualTo(24);
    Assertions.assertThat(contractEventGroup.callbackCostFromContract).isTrue();

    eventManager.registerCallbackWithCostFromRemaining(s -> s.writeInt(22));
    Assertions.assertThat(contractEventGroup.callbackRpc).isEqualTo(new byte[] {0, 0, 0, 22});
    Assertions.assertThat(contractEventGroup.callbackCost).isNull();
    Assertions.assertThat(contractEventGroup.callbackCostFromContract).isFalse();
  }
}
