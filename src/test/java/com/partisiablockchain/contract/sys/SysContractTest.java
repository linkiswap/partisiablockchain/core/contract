package com.partisiablockchain.contract.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateString;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class SysContractTest {

  private final TestSysContract handler = new TestSysContract();

  @Test
  public void onInvoke() {
    Assertions.assertThat(handler.onInvoke(null, new StateString(), null)).isNotNull();
    check(false, true, false, false, false);
  }

  @Test
  public void onCallback() {
    Hash hash = Hash.create(FunctionUtility.noOpConsumer());
    CallbackContext.ExecutionResult executionResult =
        CallbackContext.createResult(hash, false, null);
    CallbackContext callbackContext =
        CallbackContext.create(FixedList.create(List.of(executionResult)));
    Assertions.assertThat(handler.onCallback(null, new StateString(), callbackContext, null))
        .isNotNull();
    check(false, false, true, false, false);
  }

  @Test
  public void onCreate() {
    Assertions.assertThat(handler.onCreate(null, null)).isNull();
    check(true, false, false, false, false);
  }

  @Test
  public void onUpgrade() {
    Assertions.assertThat(handler.onUpgrade(null, null)).isNull();
    check(false, false, false, false, true);
  }

  private void check(
      boolean create, boolean invoke, boolean callback, boolean destroy, boolean upgrade) {
    Assertions.assertThat(handler.create).isEqualTo(create);
    Assertions.assertThat(handler.invoke).isEqualTo(invoke);
    Assertions.assertThat(handler.callback).isEqualTo(callback);
    Assertions.assertThat(handler.destroy).isEqualTo(destroy);
    Assertions.assertThat(handler.upgrade).isEqualTo(upgrade);
  }

  private static final class TestSysContract extends SysContract<StateString> {

    private boolean create;
    private boolean invoke;
    private boolean callback;
    private boolean destroy;
    private boolean upgrade;

    @Override
    public StateString onCreate(SysContractContext context, SafeDataInputStream rpc) {
      StateString create = super.onCreate(context, rpc);
      this.create = true;
      check(context, create, rpc, true);
      return create;
    }

    @Override
    public StateString onInvoke(
        SysContractContext context, StateString state, SafeDataInputStream rpc) {
      StateString invoke = super.onInvoke(context, state, rpc);
      this.invoke = true;
      check(context, invoke, rpc, false);
      return invoke;
    }

    @Override
    public StateString onCallback(
        SysContractContext context,
        StateString state,
        CallbackContext callbackContext,
        SafeDataInputStream rpc) {
      final StateString invoke = super.onCallback(context, state, callbackContext, rpc);
      Assertions.assertThat(callbackContext.isSuccess()).isFalse();
      callbackContext.setSuccess(true);
      Assertions.assertThat(callbackContext.isSuccess()).isTrue();
      this.callback = true;
      check(context, invoke, rpc, false);
      return invoke;
    }

    @Override
    public StateString onUpgrade(StateAccessor oldState, SafeDataInputStream rpc) {
      StateString stateString = super.onUpgrade(oldState, rpc);
      this.upgrade = true;
      check(null, stateString, rpc, true);
      return stateString;
    }

    private void check(
        SysContractContext context,
        StateString state,
        SafeDataInputStream rpc,
        boolean isStateNull) {
      Assertions.assertThat(context).isNull();
      if (isStateNull) {
        Assertions.assertThat(state).isNull();
      } else {
        Assertions.assertThat(state.value()).isNull();
      }
      Assertions.assertThat(rpc).isNull();
    }
  }
}
