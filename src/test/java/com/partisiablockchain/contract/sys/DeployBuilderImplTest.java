package com.partisiablockchain.contract.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.ContractEventGroup;
import com.partisiablockchain.contract.EventCreator;
import com.secata.stream.SafeDataOutputStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class DeployBuilderImplTest {

  private final ContractEventGroup sender = new ContractEventGroup();
  private final BlockchainAddress contract =
      BlockchainAddress.fromString("01A000000000000000000000000000000000000001");

  @Test
  public void sendDeploy() {
    new DeployBuilderImpl(sender::add, contract)
        .withBinderJar(new byte[33])
        .withContractJar(new byte[13])
        .withAbi(new byte[52])
        .allocateCost(1234)
        .withPayload(EventCreator.EMPTY_RPC)
        .send();

    assertSentDeployEvent(1234L, false);
  }

  @Test
  public void sendDeployWithRemaining() {
    new DeployBuilderImpl(sender::add, contract)
        .withBinderJar(new byte[33])
        .withContractJar(new byte[13])
        .withAbi(new byte[52])
        .allocateRemainingCost()
        .withPayload(EventCreator.EMPTY_RPC)
        .send();

    assertSentDeployEvent(null, false);
  }

  @Test
  public void sendDeployWithContractPays() {
    new DeployBuilderImpl(sender::add, contract)
        .withBinderJar(new byte[33])
        .withContractJar(new byte[13])
        .withAbi(new byte[52])
        .allocateCostFromContract(12354)
        .withPayload(EventCreator.EMPTY_RPC)
        .send();

    assertSentDeployEvent(12354L, true);
  }

  @Test
  public void sendFromContractDeploy() {
    new DeployBuilderImpl(sender::add, contract)
        .withBinderJar(new byte[33])
        .withContractJar(new byte[13])
        .withAbi(new byte[52])
        .allocateCost(1234)
        .withPayload(EventCreator.EMPTY_RPC)
        .sendFromContract();

    assertSentDeployEvent(1234L, false);
  }

  private void assertSentDeployEvent(Long expectedCost, boolean contractPays) {
    ContractEventDeploy contractEvent = (ContractEventDeploy) sender.contractEvents.get(0);
    Deploy event = contractEvent.deploy;
    Assertions.assertThat(event.contract).isEqualTo(contract);
    Assertions.assertThat(event.binderJar).hasSize(33);
    Assertions.assertThat(event.contractJar).hasSize(13);
    Assertions.assertThat(event.abi).hasSize(52);
    Assertions.assertThat(contractEvent.allocatedCost).isEqualTo(expectedCost);
    Assertions.assertThat(contractEvent.costFromContract).isEqualTo(contractPays);
    Assertions.assertThat(SafeDataOutputStream.serialize(event.rpc)).hasSize(0);
  }

  @Test
  public void nullChecks() {
    Assertions.assertThatThrownBy(() -> new DeployBuilderImpl(sender::add, null));

    Assertions.assertThatThrownBy(
            () -> new DeployBuilderImpl(sender::add, contract).withBinderJar(new byte[33]).send())
        .isInstanceOf(NullPointerException.class);
    Assertions.assertThatThrownBy(
            () -> new DeployBuilderImpl(sender::add, contract).withContractJar(new byte[13]).send())
        .isInstanceOf(NullPointerException.class);

    Assertions.assertThatThrownBy(
            () ->
                new DeployBuilderImpl(sender::add, contract)
                    .withBinderJar(new byte[33])
                    .sendFromContract())
        .isInstanceOf(NullPointerException.class);
    Assertions.assertThatThrownBy(
            () ->
                new DeployBuilderImpl(sender::add, contract)
                    .withContractJar(new byte[13])
                    .sendFromContract())
        .isInstanceOf(NullPointerException.class);
  }
}
