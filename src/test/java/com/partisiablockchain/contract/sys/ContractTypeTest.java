package com.partisiablockchain.contract.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.ContractType;
import com.partisiablockchain.crypto.Hash;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ContractTypeTest {

  @Test
  public void ordinals() {
    ContractType[] values = ContractType.values();
    Assertions.assertThat(values).hasSize(4);
    Assertions.assertThat(values[0]).isEqualTo(ContractType.SYSTEM);
    Assertions.assertThat(values[1]).isEqualTo(ContractType.PUBLIC);
    Assertions.assertThat(values[2]).isEqualTo(ContractType.ZERO_KNOWLEDGE);
    Assertions.assertThat(values[3]).isEqualTo(ContractType.GOVERNANCE);
  }

  @Test
  public void fromAddress() {
    Hash hash = Hash.create(s -> s.writeInt(1));
    Assertions.assertThat(
            ContractType.fromAddress(
                BlockchainAddress.fromHash(BlockchainAddress.Type.CONTRACT_SYSTEM, hash)))
        .isEqualTo(ContractType.SYSTEM);
    Assertions.assertThat(
            ContractType.fromAddress(
                BlockchainAddress.fromHash(BlockchainAddress.Type.CONTRACT_PUBLIC, hash)))
        .isEqualTo(ContractType.PUBLIC);
    Assertions.assertThat(
            ContractType.fromAddress(
                BlockchainAddress.fromHash(BlockchainAddress.Type.CONTRACT_ZK, hash)))
        .isEqualTo(ContractType.ZERO_KNOWLEDGE);
    Assertions.assertThat(
            ContractType.fromAddress(
                BlockchainAddress.fromHash(BlockchainAddress.Type.CONTRACT_GOV, hash)))
        .isEqualTo(ContractType.GOVERNANCE);
  }

  @Test
  public void contractTypeForAccount() {
    Hash hash = Hash.create(s -> s.writeInt(1));
    Assertions.assertThatThrownBy(
            () ->
                ContractType.fromAddress(
                    BlockchainAddress.fromHash(BlockchainAddress.Type.ACCOUNT, hash)))
        .isInstanceOf(IllegalArgumentException.class);
  }
}
