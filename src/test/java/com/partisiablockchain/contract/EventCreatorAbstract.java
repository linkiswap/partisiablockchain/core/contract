package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import java.util.function.Function;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Abstract for shared test on event builder. */
public abstract class EventCreatorAbstract {

  private static final Hash EMPTY_HASH = Hash.create(FunctionUtility.noOpConsumer());
  private static final BlockchainAddress CONTRACT_ADDRESS =
      BlockchainAddress.fromHash(BlockchainAddress.Type.CONTRACT_PUBLIC, EMPTY_HASH);
  private final ContractEventGroup contractEventGroup;
  private final EventCreator eventCreator;

  protected EventCreatorAbstract(
      ContractEventGroup contractEventGroup,
      Function<ContractEventGroup, EventCreator> eventCreator) {
    this.contractEventGroup = contractEventGroup;
    this.eventCreator = eventCreator.apply(contractEventGroup);
  }

  @Test
  public void testCreateFromSender() {

    eventCreator
        .invoke(CONTRACT_ADDRESS)
        .withPayload(stream -> stream.writeInt(4321))
        .allocateCost(123)
        .allocateRemainingCost()
        .send();
    Assertions.assertThatThrownBy(
            () ->
                eventCreator
                    .invoke(CONTRACT_ADDRESS)
                    .withPayload(stream -> stream.writeInt(4321))
                    .allocateCost(-1)
                    .send())
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cost must be 0 or larger: -1");

    Assertions.assertThat(contractEventGroup.contractEvents).hasSize(1);
    ContractEventInteraction sender =
        (ContractEventInteraction) contractEventGroup.contractEvents.get(0);
    Assertions.assertThat(sender.contract).isEqualTo(CONTRACT_ADDRESS);
    Assertions.assertThat(bytes(sender.rpc)).isEqualTo(new byte[] {0, 0, 16, -31});
    Assertions.assertThat(sender.allocatedCost).isNull();
    Assertions.assertThat(sender.costFromContract).isEqualTo(false);
  }

  @Test
  public void testCreateFromContract() {
    ContractEventGroup contractEventGroup = new ContractEventGroup();
    EventCreatorImpl eventCreator = new EventCreatorImpl(contractEventGroup::add);

    eventCreator
        .invoke(CONTRACT_ADDRESS)
        .withPayload(stream -> stream.writeInt(1234))
        .allocateCost(4321)
        .sendFromContract();

    Assertions.assertThat(contractEventGroup.contractEvents).hasSize(1);
    ContractEventInteraction sender =
        (ContractEventInteraction) contractEventGroup.contractEvents.get(0);
    Assertions.assertThat(sender.contract).isEqualTo(CONTRACT_ADDRESS);
    Assertions.assertThat(bytes(sender.rpc)).isEqualTo(new byte[] {0, 0, 4, -46});
    Assertions.assertThat(sender.allocatedCost).isEqualTo(4321);
    Assertions.assertThat(sender.costFromContract).isEqualTo(false);
  }

  @Test
  public void testCostFromContract() {
    ContractEventGroup contractEventGroup = new ContractEventGroup();
    EventCreatorImpl eventCreator = new EventCreatorImpl(contractEventGroup::add);

    eventCreator
        .invoke(CONTRACT_ADDRESS)
        .withPayload(stream -> stream.writeInt(98765))
        .allocateCostFromContract(7654)
        .send();

    Assertions.assertThat(contractEventGroup.contractEvents).hasSize(1);
    ContractEventInteraction sender =
        (ContractEventInteraction) contractEventGroup.contractEvents.get(0);
    Assertions.assertThat(sender.contract).isEqualTo(CONTRACT_ADDRESS);
    Assertions.assertThat(bytes(sender.rpc)).isEqualTo(new byte[] {0, 1, -127, -51});
    Assertions.assertThat(sender.allocatedCost).isEqualTo(7654);
    Assertions.assertThat(sender.costFromContract).isEqualTo(true);
  }

  @Test
  public void testNegativeCostFromContract() {
    EventCreatorImpl eventCreator = new EventCreatorImpl(FunctionUtility.noOpConsumer());

    Assertions.assertThatThrownBy(
            () ->
                eventCreator
                    .invoke(CONTRACT_ADDRESS)
                    .withPayload(stream -> stream.writeInt(98765))
                    .allocateCostFromContract(-1000)
                    .send())
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Cost must be 0 or larger: -1000");
  }

  @Test
  public void testEmptyPayload() {
    EventCreatorImpl eventCreator = new EventCreatorImpl(FunctionUtility.noOpConsumer());

    Assertions.assertThatThrownBy(
            () -> eventCreator.invoke(CONTRACT_ADDRESS).allocateCost(10_000).sendFromContract())
        .isInstanceOf(NullPointerException.class);
  }

  private byte[] bytes(DataStreamSerializable serializable) {
    if (serializable != null) {
      return SafeDataOutputStream.serialize(serializable);
    } else {
      return null;
    }
  }
}
