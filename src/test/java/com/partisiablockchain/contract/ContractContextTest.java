package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.DataStreamSerializable;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ContractContextTest {

  private final EventManagerImpl eventManager = new EventManagerImpl(new ContractEventGroup());

  @Test
  public void createEventManager() {
    Assertions.assertThat(new MyContractContext().getInvocationCreator()).isEqualTo(eventManager);
  }

  private final class MyContractContext implements ContractContext {

    @Override
    public BlockchainAddress getContractAddress() {
      return null;
    }

    @Override
    public long getBlockTime() {
      return 0;
    }

    @Override
    public long getBlockProductionTime() {
      return 0;
    }

    @Override
    public BlockchainAddress getFrom() {
      return null;
    }

    @Override
    public Hash getCurrentTransactionHash() {
      return null;
    }

    @Override
    public Hash getOriginalTransactionHash() {
      return null;
    }

    @Override
    public EventCreator getInvocationCreator() {
      return eventManager;
    }

    @Override
    public EventManager getRemoteCallsCreator() {
      return eventManager;
    }

    @Override
    public void setResult(DataStreamSerializable result) {}
  }
}
