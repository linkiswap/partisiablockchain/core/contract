package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.FunctionUtility;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class InteractionBuilderImplTest {

  @Test
  void allocateCost() {
    new InteractionBuilderImpl(
            event -> {
              Assertions.assertThat(event).isInstanceOf(ContractEventInteraction.class);
              Assertions.assertThat(((ContractEventInteraction) event).allocatedCost).isEqualTo(0);
            },
            null,
            EventCreator.EMPTY_RPC,
            123L,
            true)
        .allocateCost(0)
        .send();
    Assertions.assertThatThrownBy(
            () ->
                new InteractionBuilderImpl(
                        FunctionUtility.noOpConsumer(), null, EventCreator.EMPTY_RPC, 123L, true)
                    .allocateCost(-1)
                    .send())
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cost must be 0 or larger: -1");
  }
}
