package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import java.util.stream.IntStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class CallbackContextTest {

  private CallbackContext callbackContext;

  @BeforeEach
  public void setUp() {
    callbackContext =
        CallbackContext.create(FixedList.create(IntStream.range(0, 10).mapToObj(this::result)));
  }

  private CallbackContext.ExecutionResult result(int i) {
    return CallbackContext.createResult(
        hash(i),
        i != 5,
        SafeDataInputStream.createFromBytes(
            SafeDataOutputStream.serialize(stream -> stream.writeInt(i))));
  }

  @Test
  public void isSuccess() {
    Assertions.assertThat(callbackContext.isSuccess()).isFalse();
    callbackContext.setSuccess(true);
    Assertions.assertThat(callbackContext.isSuccess()).isTrue();
    Assertions.assertThat(CallbackContext.create(FixedList.create()).isSuccess()).isTrue();
    Assertions.assertThat(CallbackContext.create(FixedList.create(List.of(result(1)))).isSuccess())
        .isTrue();
  }

  @Test
  public void results() {
    FixedList<CallbackContext.ExecutionResult> results = callbackContext.results();
    for (int i = 0; i < results.size(); i++) {
      CallbackContext.ExecutionResult result = results.get(i);
      Assertions.assertThat(result.eventTransaction()).isEqualTo(hash(i));
      Assertions.assertThat(result.isSucceeded()).isEqualTo(i != 5);
      Assertions.assertThat(result.returnValue().readInt()).isEqualTo(i);
    }
  }

  private Hash hash(int i) {
    return Hash.create(s -> s.writeInt(i));
  }
}
