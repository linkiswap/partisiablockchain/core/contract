package com.partisiablockchain.contract.zk;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class CalculationStatusTest {

  @Test
  public void ordinals() {
    CalculationStatus[] values = CalculationStatus.values();
    Assertions.assertThat(values).hasSize(5);
    Assertions.assertThat(values[0]).isEqualTo(CalculationStatus.WAITING);
    Assertions.assertThat(values[1]).isEqualTo(CalculationStatus.CALCULATING);
    Assertions.assertThat(values[2]).isEqualTo(CalculationStatus.OUTPUT);
    Assertions.assertThat(values[3]).isEqualTo(CalculationStatus.MALICIOUS_BEHAVIOUR);
    Assertions.assertThat(values[4]).isEqualTo(CalculationStatus.DONE);
  }
}
