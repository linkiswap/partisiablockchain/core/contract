package com.partisiablockchain.contract.zk;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ShareWriterTest {

  private final ShareWriter shareWriter = ShareWriter.create();

  @Test
  public void testWrite() {
    shareWriter.write(0, 1);
    shareWriter.write(0, 64);
    Assertions.assertThat(shareWriter.toBytes())
        .hasSize(Long.BYTES + 1)
        .containsExactly(0, 0, 0, 0, 0, 0, 0, 0, 0);
  }

  @Test
  public void testWriteZeroBits() {
    Assertions.assertThatThrownBy(() -> shareWriter.write(0, 0))
        .hasMessage("Illegal bit length for long. Must be between 1 and 64.")
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void testWriteToManyBits() {
    Assertions.assertThatThrownBy(() -> shareWriter.write(0, 65))
        .hasMessage("Illegal bit length for long. Must be between 1 and 64.")
        .isInstanceOf(IllegalArgumentException.class);
  }
}
