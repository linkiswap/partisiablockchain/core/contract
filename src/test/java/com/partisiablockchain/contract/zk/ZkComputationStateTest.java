package com.partisiablockchain.contract.zk;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateVoid;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ZkComputationStateTest {

  @Test
  public void getVariables() {
    BlockchainAddress one =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    BlockchainAddress two =
        BlockchainAddress.fromString("000000000000000000000000000000000000000002");
    ZkComputationState<StateVoid, StateVoid, ZkClosed<StateVoid>> computationState =
        new ZkComputationState<>() {

          @Override
          public StateVoid getOpenState() {
            return null;
          }

          @Override
          public ZkClosed<StateVoid> getVariable(int id) {
            return null;
          }

          @Override
          public List<ZkClosed<StateVoid>> getVariables() {
            return List.of(new TestZkClosed(one), new TestZkClosed(one), new TestZkClosed(two));
          }
        };

    Assertions.assertThat(computationState.getVariables(one)).hasSize(2);
    Assertions.assertThat(computationState.getVariables(two)).hasSize(1);
    Assertions.assertThat(
            computationState.getVariables(
                BlockchainAddress.fromString("000000000000000000000000000000000000000054")))
        .hasSize(0);
  }
}
