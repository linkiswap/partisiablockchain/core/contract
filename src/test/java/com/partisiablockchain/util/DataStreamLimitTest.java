package com.partisiablockchain.util;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.SafeCollectionStream;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.stream.SafeListStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Tests for {@link DataStreamLimit}. */
final class DataStreamLimitTest {

  @Test
  public void readDynamicBytes() {
    byte[] bytes =
        DataStreamLimit.readDynamicBytes(
            SafeDataInputStream.createFromBytes(
                SafeDataOutputStream.serialize(
                    s -> s.writeDynamicBytes(new byte[DataStreamLimit.MAX_BYTES_LENGTH]))));
    Assertions.assertThat(bytes).hasSize(DataStreamLimit.MAX_BYTES_LENGTH);

    Assertions.assertThatThrownBy(
            () ->
                DataStreamLimit.readDynamicBytes(
                    SafeDataInputStream.createFromBytes(
                        SafeDataOutputStream.serialize(
                            s ->
                                s.writeDynamicBytes(
                                    new byte[DataStreamLimit.MAX_BYTES_LENGTH + 1])))))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void readString() {
    String largeString =
        new String(new byte[DataStreamLimit.MAX_BYTES_LENGTH], StandardCharsets.UTF_8);
    String readString =
        DataStreamLimit.readString(
            SafeDataInputStream.createFromBytes(
                SafeDataOutputStream.serialize(s -> s.writeString(largeString))));
    Assertions.assertThat(readString).hasSize(DataStreamLimit.MAX_BYTES_LENGTH);

    String tooLargeString =
        new String(new byte[DataStreamLimit.MAX_BYTES_LENGTH + 1], StandardCharsets.UTF_8);
    Assertions.assertThatThrownBy(
            () ->
                DataStreamLimit.readDynamicBytes(
                    SafeDataInputStream.createFromBytes(
                        SafeDataOutputStream.serialize(s -> s.writeString(tooLargeString)))))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void readDynamicList() {
    List<byte[]> testList = new ArrayList<>();
    testList.add(new byte[] {1, 2, 3});
    testList.add(new byte[DataStreamLimit.MAX_BYTES_LENGTH]);
    for (int i = 0; i < DataStreamLimit.MAX_LIST_SIZE - 2; i++) {
      testList.add(new byte[0]);
    }

    SafeListStream<byte[]> testBytesSerializer =
        SafeListStream.create(
            DataStreamLimit::readDynamicBytes,
            SafeCollectionStream.primitive(SafeDataOutputStream::writeDynamicBytes));

    List<byte[]> readList =
        DataStreamLimit.readDynamicList(
            SafeDataInputStream.createFromBytes(
                SafeDataOutputStream.serialize(s -> testBytesSerializer.writeDynamic(s, testList))),
            testBytesSerializer);
    Assertions.assertThat(readList).hasSize(DataStreamLimit.MAX_LIST_SIZE);

    testList.add(new byte[0]);
    Assertions.assertThatThrownBy(
            () ->
                DataStreamLimit.readDynamicList(
                    SafeDataInputStream.createFromBytes(
                        SafeDataOutputStream.serialize(
                            s -> testBytesSerializer.writeDynamic(s, testList))),
                    testBytesSerializer))
        .isInstanceOf(IllegalArgumentException.class);
  }
}
