package com.partisiablockchain.util;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.immutable.FixedList;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ListUtilTest {

  private final ListUtil<Integer> util = new ListUtil<>(() -> 0, l -> l == 0);

  @Test
  public void withEntry() {
    FixedList<Integer> initial = FixedList.create();
    FixedList<Integer> first = util.withEntry(initial, 2, 33);
    Assertions.assertThat(first).containsExactly(0, 0, 33);
    FixedList<Integer> second = util.withEntry(first, 3, 32);
    Assertions.assertThat(second).containsExactly(0, 0, 33, 32);
    FixedList<Integer> third = util.withEntry(second, 3, 0);
    Assertions.assertThat(third).containsExactly(0, 0, 33);
  }

  @Test
  public void withUpdatedEntry() {
    FixedList<Integer> initial = FixedList.create();
    FixedList<Integer> first = util.withUpdatedEntry(initial, 2, i -> 33);
    Assertions.assertThat(first).containsExactly(0, 0, 33);
    FixedList<Integer> second = util.withUpdatedEntry(first, 3, i -> i + 32);
    Assertions.assertThat(second).containsExactly(0, 0, 33, 32);
    FixedList<Integer> third = util.withUpdatedEntry(second, 3, i -> 32 - i);
    Assertions.assertThat(third).containsExactly(0, 0, 33);
  }

  @Test
  public void createNull() {
    ListUtil<Integer> nullList = ListUtil.createNull();

    FixedList<Integer> first = nullList.withEntry(FixedList.create(), 1, 3);
    Assertions.assertThat(first).containsExactly(null, 3);
    FixedList<Integer> second = nullList.withEntry(first, 1, null);
    Assertions.assertThat(second).isEmpty();
    FixedList<Integer> third = nullList.withEntry(second, 0, 7);
    Assertions.assertThat(third).containsExactly(7);
  }
}
