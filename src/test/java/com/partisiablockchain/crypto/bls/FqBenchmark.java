package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.util.NumericConversion;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Random;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.RunnerException;

/** Fq benchmark. Benchmarks implementation of custom limbs. */
public abstract class FqBenchmark {

  public static void main(String[] args) throws IOException, RunnerException {
    org.openjdk.jmh.Main.main(args);
  }

  /**
   * Benchmarks implementation of addition.
   *
   * @param plan an execution plan
   * @param blackhole a black hole
   */
  @Fork(value = 1, warmups = 1)
  @Benchmark
  public void add(ExecutionPlan plan, Blackhole blackhole) {
    for (int i = plan.iterations; i > 0; i--) {
      Fq left = select(i % 9, plan);
      Fq right = select(i % 34, plan);
      blackhole.consume(right.add(left));
    }
  }

  /**
   * Benchmarks implementation of modular exponentiation.
   *
   * @param plan an execution plan
   * @param blackhole a black hole
   */
  @Fork(value = 1, warmups = 1)
  @Benchmark
  public void modExp(ExecutionPlan plan, Blackhole blackhole) {
    for (int i = plan.iterations; i > 0; i--) {
      Fq left = select(i % 9, plan);
      long[] exponent = selectExponent(i % 34, plan);
      blackhole.consume(left.modExp(exponent));
    }
  }

  /**
   * Benchmarks implementation of multiplication.
   *
   * @param plan an execution plan
   * @param blackhole a black hole
   */
  @Fork(value = 1, warmups = 1)
  @Benchmark
  public void multiply(ExecutionPlan plan, Blackhole blackhole) {
    for (int i = plan.iterations; i > 0; i--) {
      Fq left = select(i % 9, plan);
      Fq right = select(i % 34, plan);
      blackhole.consume(right.multiply(left));
    }
  }

  /**
   * Selects the next Fq to be benchmarked.
   *
   * @param index index for lookup
   * @param plan an execution plan
   * @return Fq
   */
  private Fq select(int index, ExecutionPlan plan) {
    return plan.ops[index % ExecutionPlan.COUNT];
  }

  /**
   * Selects the next limbs exponent to be benchmarked.
   *
   * @param index index for lookup
   * @param plan an execution plan
   * @return BigInteger
   */
  private long[] selectExponent(int index, ExecutionPlan plan) {
    return plan.exponent[index % ExecutionPlan.COUNT];
  }

  /** ExecutionPlan class. */
  @State(Scope.Benchmark)
  public abstract static class ExecutionPlan {

    public static final int COUNT = 13;

    @Param({"10000"})
    private int iterations;

    @Param({"500"})
    private int bitLength;

    @Param({"300"})
    private int expLength;

    private final Fq[] ops = new Fq[COUNT];
    private final long[][] exponent = new long[COUNT][12];

    /** Setup method for benchmarks. */
    @Setup(Level.Invocation)
    public void setUp() {
      Random random = new Random(1234);
      for (int i = 0; i < COUNT; i++) {
        BigInteger bigInteger = new BigInteger(bitLength, random);
        BigInteger expBig = new BigInteger(expLength, random);
        ops[i] = Fq.create(bigInteger.toByteArray());
        exponent[i] = createLimbs(expBig);
      }
    }

    private long[] createLimbs(BigInteger exp) {
      byte[] bytes = Fq.padToElementByteSize(exp.toByteArray());
      long[] arr = new long[7];
      for (int i = 0; i < 7 - 1; i++) {
        arr[i] = NumericConversion.longFromBytes(bytes, (7 - i - 2) * 8);
      }
      return Fq.convertIntoSmallLimbs(arr);
    }
  }
}
