package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.math.BigInteger;
import java.util.Random;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class G1Test {

  private final Random rnd = new Random();

  @Test
  public void generator() {
    Assertions.assertThat(G1.G.scalePoint(Pairing.order)).isEqualTo(G1.POINT_AT_INFINITY);
  }

  @Test
  public void scalar() {
    Assertions.assertThat(G1.G.doublePoint())
        .isEqualTo(G1.G.scalePoint(NafEncoded.create(BigInteger.TWO)));
    BigInteger scalar0 = new BigInteger(380, rnd);
    BigInteger scalar1 = new BigInteger(380, rnd);

    // s1·(s0·G) == s0·(s1·G) == (s0·s1)·G
    G1 a = G1.G.scalePoint(NafEncoded.create(scalar0));
    G1 b = a.scalePoint(NafEncoded.create(scalar1));
    G1 c = G1.G.scalePoint(NafEncoded.create(scalar1));
    G1 d = c.scalePoint(NafEncoded.create(scalar0));
    G1 e = G1.G.scalePoint(NafEncoded.create(scalar0.multiply(scalar1)));

    Assertions.assertThat(b).isEqualTo(e);
    Assertions.assertThat(b).isEqualTo(d);
  }

  @Test
  public void associativity() {
    // (a + b) + c = a + (b + c)
    G1 a = randomPoint();
    G1 b = randomPoint();
    G1 c = randomPoint();
    Assertions.assertThat(a.addPoint(b).addPoint(c)).isEqualTo(a.addPoint(b.addPoint(c)));
  }

  @Test
  public void doublePoint() {
    Assertions.assertThat(G1.G.doublePoint()).isEqualTo(G1.G.addPoint(G1.G));
    Assertions.assertThat(G1.POINT_AT_INFINITY.doublePoint()).isEqualTo(G1.POINT_AT_INFINITY);
    Assertions.assertThat(G1.G.addPoint(G1.POINT_AT_INFINITY)).isEqualTo(G1.G);
  }

  @Test
  public void orderTwoArithmetic() {
    // this is a bogus point, but we wish to check that doubling a point of order 2 returns the
    // point at infinity.
    G1 bogus = new G1(Fq.createConstant(4), Fq.createZero(), Fq.createOne());
    Assertions.assertThat(bogus.doublePoint()).isEqualTo(G1.POINT_AT_INFINITY);
    Assertions.assertThat(bogus.negatePoint()).isEqualTo(G1.POINT_AT_INFINITY);
  }

  @Test
  public void negate() {
    G1 negated = G1.G.negatePoint();
    Assertions.assertThat(G1.G.addPoint(negated)).isEqualTo(G1.POINT_AT_INFINITY);
    Assertions.assertThat(G1.POINT_AT_INFINITY.negatePoint()).isEqualTo(G1.POINT_AT_INFINITY);
  }

  @Test
  public void serializeUnsafe() {
    byte[] poi = G1.POINT_AT_INFINITY.serialize(false);
    Assertions.assertThat(G1.create(poi)).isEqualTo(G1.POINT_AT_INFINITY);

    byte[] bytes = G1.G.serialize(false);
    Assertions.assertThat(G1.create(bytes)).isEqualTo(G1.G);

    byte[] compressed = G1.G.serialize(true);
    Assertions.assertThat(G1.create(compressed)).isEqualTo(G1.G);

    byte[] negated = G1.G.negatePoint().serialize(true);
    Assertions.assertThat(G1.create(negated)).isEqualTo(G1.G.negatePoint());
  }

  @Test
  public void serialize() {
    byte[] poi = G1.POINT_AT_INFINITY.serialize(false);
    Assertions.assertThat(G1.create(poi)).isEqualTo(G1.POINT_AT_INFINITY);

    byte[] poiCompressed = G1.POINT_AT_INFINITY.serialize(true);
    Assertions.assertThat(G1.create(poiCompressed)).isEqualTo(G1.POINT_AT_INFINITY);

    Assertions.assertThatThrownBy(() -> G1.create(new byte[0]))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot deserialize 0 bytes");

    Assertions.assertThatThrownBy(() -> G1.create(new byte[2 * Fq.BYTES - 1]))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Expected 96 bytes, but got 95");

    Assertions.assertThatThrownBy(
            () -> {
              byte[] dataBytes = new byte[Fq.BYTES - 1];
              // mark as a compressed point.
              dataBytes[0] = (byte) (dataBytes[0] | 1 << 7);
              G1.create(dataBytes);
            })
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Expected 48 bytes, but got 47");

    // invalid point at infinity
    Assertions.assertThatThrownBy(
            () -> {
              byte[] invalidPoi = new byte[Fq.BYTES];
              // mark as compressed and poi.
              invalidPoi[0] = (byte) (invalidPoi[0] | ((1 << 7) | (1 << 6)));
              invalidPoi[1] = 1;
              G1.create(invalidPoi);
            })
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Invalid serialized element");

    // another invalid point at infinity. This time with junk in the first byte.
    Assertions.assertThatThrownBy(
            () -> {
              byte[] invalidPoi = new byte[Fq.BYTES];
              // mark as compressed and poi.
              invalidPoi[0] = (byte) (invalidPoi[0] | ((1 << 7) | (1 << 6) | 4));
              G1.create(invalidPoi);
            })
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Invalid serialized element");

    // this is the point (0, 0), which does not satisfy y^2 = x^3 + 4.
    Assertions.assertThatThrownBy(() -> G1.create(new byte[2 * Fq.BYTES]))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Deserialized point is not on the curve");

    Assertions.assertThatThrownBy(
            () -> {
              byte[] invalid = new byte[Fq.BYTES];
              invalid[0] = (byte) (invalid[0] | 1 << 7);
              G1.create(invalid);
            })
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Deserialized point is in an invalid subgroup");

    Assertions.assertThatThrownBy(
            () -> {
              byte[] invalid = new byte[Fq.BYTES];
              invalid[0] = (byte) (invalid[0] | (1 << 7 | 1 << 5));
              G1.create(invalid);
            })
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Deserialized point is in an invalid subgroup");

    byte[] bytes = G1.G.serialize(false);
    Assertions.assertThat((bytes[0] >> 7) & 1).isEqualTo(0);
    Assertions.assertThat((bytes[0] >> 6) & 1).isEqualTo(0);
    Assertions.assertThat((bytes[0] >> 5) & 1).isEqualTo(0);
    Assertions.assertThat(G1.create(bytes)).isEqualTo(G1.G);

    byte[] compressed = G1.G.serialize(true);
    Assertions.assertThat((compressed[0] >> 7) & 1).isEqualTo(1);
    Assertions.assertThat((compressed[0] >> 6) & 1).isEqualTo(0);
    Assertions.assertThat((compressed[0] >> 5) & 1).isEqualTo(0);
    Assertions.assertThat(G1.create(compressed)).isEqualTo(G1.G);

    byte[] negated = G1.G.negatePoint().serialize(true);
    Assertions.assertThat((negated[0] >> 7) & 1).isEqualTo(1);
    Assertions.assertThat((negated[0] >> 6) & 1).isEqualTo(0);
    Assertions.assertThat((negated[0] >> 5) & 1).isEqualTo(1);
    Assertions.assertThat(G1.create(negated)).isEqualTo(G1.G.negatePoint());
  }

  @Test
  public void toStringTest() {
    Assertions.assertThat(G1.POINT_AT_INFINITY.toString()).isEqualTo("G1{POINT_AT_INFINITY}");
    Assertions.assertThat(G1.G.toString())
        .isEqualTo(
            "G1{X: "
                + "3685416753713387016781088315183077757961620795782546"
                + "409894578378688607592378376318836054947676345821548104185464507"
                + ", Y: "
                + "13395065449444764730204713799419212215849338759383496"
                + "20426543736416511423956333506472724655353366534992391756441569"
                + "}");
  }

  @Test
  public void equals() {
    Assertions.assertThat(G1.G).isEqualTo(G1.G);
    Assertions.assertThat(G1.POINT_AT_INFINITY).isNotEqualTo(G1.G);
    G1 negated = G1.G.negatePoint();
    Assertions.assertThat(negated).isNotEqualTo(G1.G);
    Assertions.assertThat(G1.G.equals(null)).isFalse();
    Assertions.assertThat(G1.G.equals(new Object())).isFalse();
    Assertions.assertThat(G1.G.scalePoint(NafEncoded.create(BigInteger.valueOf(42))))
        .isNotEqualTo(G1.G);
    Assertions.assertThat(G1.G.hashCode()).isEqualTo(G1.G.hashCode());
    Assertions.assertThat(negated.hashCode()).isNotEqualTo(G1.G.hashCode());
    Assertions.assertThat(G1.POINT_AT_INFINITY.hashCode()).isEqualTo(0);
  }

  @Test
  public void isGreaterTestInCurveHelper() {
    CurveHelper<Fq> helper = new CurveHelper<>(Fq::createConstant, Fq.createConstant(4));
    Assertions.assertThat(helper.isGreater(Fq.createOne(), Fq.createZero())).isTrue();
    Assertions.assertThat(helper.isGreater(Fq.createZero(), Fq.createOne())).isFalse();
    Assertions.assertThat(helper.isGreater(Fq.createZero(), Fq.createZero())).isFalse();
  }

  @Test
  void createUnsafeNotOnCurve() {
    byte[] serialized = randomPoint().serialize(false);
    serialized[0] += (byte) 1;
    serialized[serialized.length - 1] += (byte) 1;

    var unsafe = G1.createUnsafe(serialized);
    Assertions.assertThatThrownBy(unsafe::validatePoint)
        .hasMessage("Deserialized point is not on the curve");
  }

  @Test
  void createUnsafeWrongSubGroup() {
    byte[] invalid = new byte[Fq.BYTES];
    invalid[0] = (byte) (invalid[0] | 1 << 7);

    var point = G1.createUnsafe(invalid);

    byte[] serialized = point.serialize(false);
    var unsafe = G1.createUnsafe(serialized);
    Assertions.assertThatThrownBy(unsafe::validatePoint)
        .hasMessage("Deserialized point is in an invalid subgroup");
  }

  private G1 randomPoint() {
    NafEncoded scalar = NafEncoded.create(new BigInteger(380, rnd));
    return G1.G.scalePoint(scalar);
  }
}
