package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.math.BigInteger;
import java.util.Random;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

/** Test. */
public final class Fq2Test {

  private static final Random rnd = new Random();
  private static final int bits = 390;

  @Test
  public void create() {
    assertThatThrownBy(() -> Fq2.create(new byte[1]))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Cannot create Fq2 element from 1 bytes. Expected 96");
  }

  @Test
  public void serialize() {
    Fq2 element = randomFq2();
    byte[] bytes = element.serialize();
    assertThat(Fq2.create(bytes)).isEqualTo(element);
  }

  @Test
  public void add() {
    Fq2 element = randomFq2();
    Fq2 negate = element.negate();
    assertThat(element.add(negate)).isEqualTo(Fq2.createZero());
  }

  @Test
  public void subtract() {
    Fq2 element = randomFq2();
    assertThat(element.subtract(element)).isEqualTo(Fq2.createZero());
  }

  @Test
  public void multiply() {
    Fq2 element = randomFq2();
    Fq2 invert = element.invert();
    assertThat(element.multiply(invert)).isEqualTo(Fq2.createOne());
  }

  @Test
  public void equals() {
    EqualsVerifier.forClass(Fq2.class).withNonnullFields("a0", "a1").verify();
  }

  @Test
  public void toStringTest() {
    assertThat(Fq2.createOne().toString()).isEqualTo("Fq2{1 + 0 * X}");
  }

  @Test
  public void isZero() {
    Fq2 element = randomFq2();
    assertThat(element.isZero()).isFalse();
    assertThat(Fq2.createOne().isZero()).isFalse();
    byte[] leftZeroBytes = new byte[Fq2.BYTE_SIZE];
    leftZeroBytes[Fq2.BYTE_SIZE - 1] = 1;
    Fq2 leftZero = Fq2.create(leftZeroBytes);
    // leftZero = X
    assertThat(leftZero.isZero()).isFalse();
    assertThat(Fq2.createZero().isZero()).isTrue();
  }

  @Test
  public void sqrt() {
    Fq2 x = randomFq2();
    Fq2 someSquare = x.square();
    Fq2 y = someSquare.sqrt();

    // There is two roots. So it has to be equal one of them.
    assertThat(y).isIn(x, x.negate());

    // also trial and error -- sqrt(sqrt(1)) happens to hit the \alpha == -1 branch.
    Fq2 sqrtSqrtOne = Fq2.createOne().sqrt().sqrt();
    assertThat(sqrtSqrtOne.pow(BigInteger.valueOf(4))).isEqualTo(Fq2.createOne());
  }

  @Test
  public void notSquare() {
    Fq2 nonSquare =
        new Fq2(
            Fq.create(
                new BigInteger(
                        "3527486581511376997701091962903644655383185926621872090204736244572592"
                            + "745946146517512076182119349150696959861297254")
                    .toByteArray()),
            Fq.create(
                new BigInteger(
                        "126238006100776243823467952021819692041166551588479672987445003188790"
                            + "8669992714754695517537401637277774437245953424")
                    .toByteArray()));
    assertThatThrownBy(nonSquare::sqrt)
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Value is not a square");
  }

  @Test
  public void pow() {
    Fq2 element = randomFq2();
    Fq2 pow = element.pow(BigInteger.TWO);
    assertThat(pow).isEqualTo(element.square());

    Fq2 powPow = element.pow(BigInteger.valueOf(5));
    assertThat(powPow).isEqualTo(element.square().square().multiply(element));
  }

  @Test
  public void compare() {
    // a < c < b
    Fq2 a = Fq2.createZero();
    Fq2 b = new Fq2(Fq.createOne(), Fq.createZero());
    assertThat(a.compare(b)).isEqualTo(-1);
    assertThat(b.compare(a)).isEqualTo(1);
    Fq2 c = new Fq2(Fq.createZero(), Fq.createOne());
    assertThat(b.compare(c)).isEqualTo(1);
    assertThat(c.compare(a)).isEqualTo(1);
  }

  @Test
  public void frobenius() {
    Fq2 element = randomFq2();
    assertThat(element.frobenius().frobenius()).isEqualTo(element);
    assertThat(element.frobenius()).isNotEqualTo(element);
  }

  static Fq2 randomFq2() {
    return new Fq2(Fq.create(new BigInteger(bits, rnd)), Fq.create(new BigInteger(bits, rnd)));
  }
}
