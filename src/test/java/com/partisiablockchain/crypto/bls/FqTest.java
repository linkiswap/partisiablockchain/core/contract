package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Random;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

/** Tests. */
public final class FqTest {

  private static final int COUNT = 1000;
  private static final int bits = 390;

  private final Random rnd = new Random();

  /** Repeatedly tests add COUNT number of times on random values. */
  @RepeatedTest(COUNT)
  public void add() {
    BigInteger left = new BigInteger(bits, rnd);
    Fq leftNew = Fq.create(left.toByteArray());
    BigInteger right = new BigInteger(bits, rnd);
    Fq rightNew = Fq.create(right.toByteArray());

    assertThat(leftNew.add(rightNew).serialize())
        .isEqualTo(padBytes(left.add(right).mod(Fq.PRIME_BIGINTEGER).toByteArray()));
  }

  @Test
  public void addProperties() {
    // a+(b+c) = (a+b)+c
    Fq a = randomFq();
    Fq b = randomFq();
    Fq c = randomFq();
    assertThat(a.add(b.add(c))).isEqualTo(a.add(b).add(c));

    // a+0 = a
    assertThat(a.add(Fq.createZero())).isEqualTo(a);
  }

  @Test
  public void bitLength() {
    long[] oneLength = new long[7];
    oneLength[0] = 1;
    assertThat(Fq.bitLength(oneLength)).isEqualTo(1);
    long[] lengthOf59Bits = new long[7];
    lengthOf59Bits[1] = 1;
    assertThat(Fq.bitLength(lengthOf59Bits)).isEqualTo(59);
    assertThat(Fq.bitLength(new long[7])).isEqualTo(0);
  }

  @Test
  public void createUnsafe() {
    Fq unsafe =
        Fq.createUnsafe(
            new BigInteger(
                "1705910780087735827321293917491208152597683379752928848181365446003"
                    + "15634730376431796576566476604919566951998176597"));
    assertThat(Fq.createConstant(500)).isEqualTo(unsafe);
  }

  @Test
  public void reduce() {
    long[] biggerThanPrime =
        new long[] {
          143833713099123371L,
          216172422762594286L,
          83896495553790442L,
          149689799186160835L,
          163057217235613515L,
          171129804685765101L,
          6980443811L
        };
    biggerThanPrime[6] <<= 2;
    long[] onePrime = Fq.multiply(biggerThanPrime, biggerThanPrime);
    assertThat(Fq.reduce(onePrime))
        .isEqualTo(new long[] {0L, 0L, 0L, 0L, 0L, 140960060000270865L, 1521L});
    Fq prime = Fq.create(Fq.PRIME_BIGINTEGER);
    assertThat(prime.multiply(prime)).isEqualTo(Fq.createZero());
    assertThat(prime.multiply(prime).compare(prime)).isEqualTo(0);
    assertThat(
            Fq.reduce(
                new long[] {
                  Fq.PRIME[0],
                  Fq.PRIME[1],
                  Fq.PRIME[2],
                  Fq.PRIME[3],
                  Fq.PRIME[4],
                  Fq.PRIME[5],
                  Fq.PRIME[6],
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
                }))
        .isEqualTo(new long[7]);
  }

  /** Repeatedly tests creation COUNT number of times on random values. */
  @RepeatedTest(COUNT)
  public void lessThan384Bits() {
    BigInteger value = new BigInteger(384, rnd);
    if (value.bitLength() <= 383) {
      value = value.shiftLeft(1);
      assertThat(new BigInteger(Fq.create(value).serialize()))
          .isEqualTo(value.mod(Fq.PRIME_BIGINTEGER));
    }
    assertThat(new BigInteger(Fq.create(value).serialize()))
        .isEqualTo(value.mod(Fq.PRIME_BIGINTEGER));
  }

  /** Repeatedly tests creation COUNT number of times on random values. */
  @RepeatedTest(COUNT)
  public void bytesBeing48InSize() {
    BigInteger value = new BigInteger(384, rnd);
    assertThat(new BigInteger(Fq.create(value.toByteArray()).serialize()))
        .isEqualTo(value.mod(Fq.PRIME_BIGINTEGER));
  }

  /** Repeatedly tests invert COUNT number of times on random values. */
  @RepeatedTest(COUNT)
  public void invert() {
    Fq random = randomFq();
    assertThat(random.invert().multiply(random)).isEqualTo(Fq.createOne());
  }

  @Test
  public void invertZero() {
    Fq zero = Fq.createZero();
    assertThatThrownBy(zero::invert)
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("0 is not invertible in Fq");
  }

  /** Repeatedly tests multiply COUNT number of times on random values. */
  @RepeatedTest(COUNT)
  public void multiply() {
    BigInteger left = new BigInteger(bits, rnd);
    Fq leftFq = Fq.create(left.toByteArray());

    BigInteger right = new BigInteger(bits, rnd);
    Fq rightFq = Fq.create(right.toByteArray());

    assertThat(leftFq.multiply(rightFq).serialize())
        .isEqualTo(padBytes(left.multiply(right).mod(Fq.PRIME_BIGINTEGER).toByteArray()));
  }

  @Test
  public void multiplyProperties() {
    Fq a = randomFq();
    Fq b = randomFq();
    Fq c = randomFq();
    // a·(b·c) = (a·b)·c
    assertThat(a.multiply(b.multiply(c))).isEqualTo(a.multiply(b).multiply(c));
    // a · b = b · a
    assertThat(a.multiply(b)).isEqualTo(b.multiply(a));
    // a ·(b+c) = a·b+a·c
    assertThat(a.multiply(b.add(c))).isEqualTo(a.multiply(b).add(a.multiply(c)));
  }

  /** Repeatedly tests serialize COUNT number of times on random values. */
  @RepeatedTest(COUNT)
  public void serialize() {
    BigInteger bigInteger = new BigInteger(bits, rnd);
    Fq fq = Fq.create(bigInteger.toByteArray());
    assertThat(fq.serialize())
        .isEqualTo(padBytes(bigInteger.mod(Fq.PRIME_BIGINTEGER).toByteArray()));
  }

  /** Repeatedly tests sub COUNT number of times on random values. */
  @RepeatedTest(COUNT)
  public void sub() {
    BigInteger left = new BigInteger(bits, rnd);
    Fq leftNew = Fq.create(left.toByteArray());

    BigInteger right = new BigInteger(bits, rnd);
    Fq rightNew = Fq.create(right.toByteArray());

    assertThat(leftNew.subtract(rightNew).serialize())
        .isEqualTo(padBytes(left.subtract(right).mod(Fq.PRIME_BIGINTEGER).toByteArray()));
  }

  @Test
  public void subProperties() {
    // a - b = -(b-a)
    Fq a = randomFq();
    Fq b = randomFq();
    assertThat(a.subtract(b)).isEqualTo(b.subtract(a).negate());
  }

  @Test
  public void create() {
    Fq zero = Fq.createConstant(0);
    Fq otherZero = Fq.createZero();
    assertThat(zero).isEqualTo(otherZero);
    Fq notZero = Fq.createConstant(42);
    assertThat(zero).isNotEqualTo(notZero);
    Fq one = Fq.createConstant(1);
    assertThat(one).isNotEqualTo(zero);
    assertThat(one).isNotEqualTo(notZero);
    Fq yetAnotherZero = Fq.createConstant(0);
    assertThat(yetAnotherZero).isEqualTo(zero);
    Fq minusOne = Fq.createConstant(-1);
    Fq minusOneAlt = Fq.create(Fq.PRIME_BIGINTEGER.subtract(BigInteger.ONE).toByteArray());
    assertThat(minusOne).isEqualTo(minusOneAlt);
    assertThat(minusOne.add(Fq.createOne())).isEqualTo(Fq.createZero());
  }

  @Test
  public void negate() {
    // a + (-a) = 0
    Fq a = randomFq();
    assertThat(a.add(a.negate())).isEqualTo(Fq.createZero());
  }

  @Test
  public void equals() {
    EqualsVerifier.forClass(Fq.class).withNonnullFields("value").verify();
  }

  @Test
  public void sqrt() {
    Fq x = randomFq();
    Fq someSquare = x.square();
    Fq y = someSquare.sqrt();
    // There is two roots. So it has to be equal one of them.
    assertThat(y).isIn(x, x.negate());
    Fq nonSquare =
        Fq.create(
            new BigInteger(
                "122546351638186712901005780256006663856056777614417"
                    + "4385761098088628195378177500497282926635691018057188153519047202"));
    assertThatThrownBy(nonSquare::sqrt)
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Fq value is not a square");
    assertThat(someSquare.isSquare()).isTrue();
  }

  @Test
  public void sign() {
    Fq a =
        Fq.create(
            new BigInteger(
                "62194072918723367662308192695514977204692093424812896383"
                    + "66383246868036440350765598579672329426805700817453367418627"));
    Fq b =
        Fq.create(
            new BigInteger(
                "68694711255034197366701570382254992497276336239"
                    + "99747308764676559692350619661439479248571470150500767262303869643294"));
    assertThat(a.sign()).isFalse();
    assertThat(b.sign()).isTrue();
  }

  private static byte[] padBytes(byte[] bytesToPad) {
    ByteBuffer buffer = ByteBuffer.allocate(48).position(48 - bytesToPad.length);
    return buffer.put(bytesToPad).array();
  }

  private Fq randomFq() {
    BigInteger random = new BigInteger(bits, rnd);
    return Fq.create(random);
  }
}
