package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.math.BigInteger;
import java.util.Random;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class G2Test {

  private final Random rnd = new Random();
  private static final G2 POINT_AT_INFINITY =
      new G2(Fq2.createZero(), Fq2.createOne(), Fq2.createZero());

  @Test
  public void generator() {
    Assertions.assertThat(G2.G.scalePoint(Pairing.order)).isEqualTo(POINT_AT_INFINITY);
    Assertions.assertThat(G2.G.scalePoint(NafEncoded.create(BigInteger.valueOf(42))))
        .isNotEqualTo(POINT_AT_INFINITY);
  }

  @Test
  public void neutralElement() {
    Assertions.assertThat(G2.G.addPoint(POINT_AT_INFINITY)).isEqualTo(G2.G);
  }

  @Test
  public void serializeUnsafe() {
    byte[] poiCompressed = POINT_AT_INFINITY.serialize(true);
    Assertions.assertThat(G2.create(poiCompressed)).isEqualTo(POINT_AT_INFINITY);

    byte[] bytes = G2.G.serialize(false);
    Assertions.assertThat(G2.create(bytes)).isEqualTo(G2.G);

    byte[] compressed = G2.G.serialize(true);
    Assertions.assertThat(G2.create(compressed)).isEqualTo(G2.G);

    byte[] negated = G2.G.negatePoint().serialize(true);
    Assertions.assertThat(G2.create(negated)).isEqualTo(G2.G.negatePoint());
  }

  @Test
  public void scalar() {
    Assertions.assertThat(G2.G.doublePoint())
        .isEqualTo(G2.G.scalePoint(NafEncoded.create(BigInteger.TWO)));
    BigInteger scalar0 = new BigInteger(380, rnd);
    BigInteger scalar1 = new BigInteger(380, rnd);

    // s1·(s0·G) == s0·(s1·G) == (s0·s1)·G
    G2 a = G2.G.scalePoint(NafEncoded.create(scalar0));
    G2 b = a.scalePoint(NafEncoded.create(scalar1));
    G2 c = G2.G.scalePoint(NafEncoded.create(scalar1));
    G2 d = c.scalePoint(NafEncoded.create(scalar0));
    G2 e = G2.G.scalePoint(NafEncoded.create(scalar0.multiply(scalar1)));

    Assertions.assertThat(b).isEqualTo(e);
    Assertions.assertThat(b).isEqualTo(d);
  }

  @Test
  public void associativity() {
    // (a + b) + c = a + (b + c)
    G2 a = randomPoint();
    G2 b = randomPoint();
    G2 c = randomPoint();
    Assertions.assertThat(a.addPoint(b).addPoint(c)).isEqualTo(a.addPoint(b.addPoint(c)));
  }

  @Test
  public void serialize() {
    byte[] poi = POINT_AT_INFINITY.serialize(false);
    Assertions.assertThat(G2.create(poi)).isEqualTo(POINT_AT_INFINITY);

    byte[] poiCompressed = POINT_AT_INFINITY.serialize(true);
    Assertions.assertThat(G2.create(poiCompressed)).isEqualTo(POINT_AT_INFINITY);

    Assertions.assertThatThrownBy(() -> G2.create(new byte[0]))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot deserialize 0 bytes");

    Assertions.assertThatThrownBy(() -> G2.create(new byte[2 * Fq2.BYTE_SIZE - 1]))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Expected 192 bytes, but got 191");

    Assertions.assertThatThrownBy(
            () -> {
              byte[] dataBytes = new byte[Fq2.BYTE_SIZE - 1];
              // mark as a compressed point.
              dataBytes[0] = (byte) (dataBytes[0] | 1 << 7);
              G2.create(dataBytes);
            })
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Expected 96 bytes, but got 95");

    // invalid point at infinity
    Assertions.assertThatThrownBy(
            () -> {
              byte[] invalidPoi = new byte[Fq2.BYTE_SIZE];
              // mark as compressed and poi.
              invalidPoi[0] = (byte) (invalidPoi[0] | ((1 << 7) | (1 << 6)));
              invalidPoi[1] = 1;
              G2.create(invalidPoi);
            })
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Invalid serialized element");

    // another invalid point at infinity. This time with junk in the first byte.
    Assertions.assertThatThrownBy(
            () -> {
              byte[] invalidPoi = new byte[Fq2.BYTE_SIZE];
              // mark as compressed and poi.
              invalidPoi[0] = (byte) (invalidPoi[0] | ((1 << 7) | (1 << 6) | 4));
              G2.create(invalidPoi);
            })
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Invalid serialized element");

    // this is the point (0, 0), which does not satisfy y^2 = x^3 + 4.
    Assertions.assertThatThrownBy(() -> G2.create(new byte[2 * Fq2.BYTE_SIZE]))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Deserialized point is not on the curve");

    Assertions.assertThatThrownBy(
            () -> {
              byte[] invalid = new byte[Fq2.BYTE_SIZE];
              invalid[0] = (byte) (invalid[0] | 1 << 7);
              invalid[1] = 4;
              G2.create(invalid);
            })
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Deserialized point is in an invalid subgroup");

    Assertions.assertThatThrownBy(
            () -> {
              byte[] invalid = new byte[Fq2.BYTE_SIZE];
              invalid[0] = (byte) (invalid[0] | (1 << 7 | 1 << 5));
              invalid[1] = 4;
              G2.create(invalid);
            })
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Deserialized point is in an invalid subgroup");
  }

  @Test
  public void orderTwoArithmetic() {
    // this is a bogus point, but we wish to check that doubling a point of order 2 returns the
    // point at infinity.
    G2 bogus = new G2(Fq2.FOUR_I_1, Fq2.createZero(), Fq2.createOne());
    Assertions.assertThat(bogus.doublePoint()).isEqualTo(POINT_AT_INFINITY);
    Assertions.assertThat(bogus.negatePoint()).isEqualTo(POINT_AT_INFINITY);
  }

  @Test
  public void toStringTest() {
    Assertions.assertThat(POINT_AT_INFINITY.toString()).isEqualTo("G2{POINT_AT_INFINITY}");
    Assertions.assertThat(G2.G.toString())
        .isEqualTo(
            "G2{X: "
                + "Fq2{3527010695874666181871391160110601448900299527927752402199086442397937"
                + "85735715026873347600343865175952761926303160"
                + " + 305914434424421370997125981475378163698647032547664755865937320"
                + "6291635324768958432433509563104347017837885763365758"
                + " * X}, Y: "
                + "Fq2{1985150602287291935568054521177171638300868978215655"
                + "730859378665066344726373823718423869104263333984641494340347905"
                + " + 92755366549233245574720196577603788075774019345359297002502"
                + "7978793976877002675564980949289727957565575433344219582"
                + " * X}}");
  }

  @Test
  public void equals() {
    Assertions.assertThat(G2.G.equals(null)).isFalse();
    Assertions.assertThat(G2.G.equals(new Object())).isFalse();
    Assertions.assertThat(G2.G.hashCode()).isEqualTo(G2.G.hashCode());
    Assertions.assertThat(G2.G.negatePoint().hashCode()).isNotEqualTo(G2.G.hashCode());
    Assertions.assertThat(POINT_AT_INFINITY.hashCode()).isEqualTo(0);
  }

  @Test
  void createUnsafeNotOnCurve() {
    byte[] serialized = randomPoint().serialize(false);
    serialized[0] += (byte) 1;
    serialized[serialized.length - 1] += (byte) 1;

    var unsafe = G2.createUnsafe(serialized);
    Assertions.assertThatThrownBy(unsafe::validatePoint)
        .hasMessage("Deserialized point is not on the curve");
  }

  @Test
  void createUnsafeWrongSubGroup() {
    byte[] invalid = new byte[Fq2.BYTE_SIZE];
    invalid[0] = (byte) (invalid[0] | (1 << 7 | 1 << 5));
    invalid[1] = 4;
    var point = G2.createUnsafe(invalid);

    byte[] serialized = point.serialize(false);
    var unsafe = G2.createUnsafe(serialized);
    Assertions.assertThatThrownBy(unsafe::validatePoint)
        .hasMessage("Deserialized point is in an invalid subgroup");
  }

  private G2 randomPoint() {
    NafEncoded scalar = NafEncoded.create(new BigInteger(380, rnd));
    return G2.G.scalePoint(scalar);
  }
}
