package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import java.math.BigInteger;
import java.util.concurrent.atomic.AtomicInteger;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class G1HasherTest {

  static final Hash message0 = Hash.create(stream -> stream.writeString("aaa"));
  static final Hash message1 = Hash.create(stream -> stream.writeString("aaa"));
  static final Hash message2 = Hash.create(stream -> stream.writeString("bbb"));

  @Test
  public void hash() {

    G1 hash0 = G1.hash(message0);
    G1 hash1 = G1.hash(message1);
    G1 hash2 = G1.hash(message2);

    Assertions.assertThat(hash0.isPointAtInfinity()).isFalse();
    Assertions.assertThat(hash0).isEqualTo(hash1);
    Assertions.assertThat(hash0).isNotEqualTo(hash2);
    Assertions.assertThat(hash2.isPointAtInfinity()).isFalse();
    Assertions.assertThat(hash0.scalePoint(Pairing.order)).isEqualTo(G1.POINT_AT_INFINITY);
    Assertions.assertThat(hash1.scalePoint(Pairing.order)).isEqualTo(G1.POINT_AT_INFINITY);

    G1 expected =
        new G1(
            Fq.create(
                new BigInteger(
                    "281982563516489581198865368518449900544612998574318273"
                        + "1170791432314493902561667933260297073591123239412779508122931")),
            Fq.create(
                new BigInteger(
                    "2830623769025462928267161579003111174377117580004009037"
                        + "837589439131758773584431436698510859898961370516895211901715")),
            Fq.createOne());
    Assertions.assertThat(hash0).isEqualTo(expected);
  }

  @Test
  public void exceptional() {
    // test exceptional cases for some of the methods in G1Hasher. These cases should never be hit
    // in practice, because they arise when e.g., SHA256 outputs an all 0 string.
    Fq[] exceptionalIso =
        G1Hasher.isoMap(new Fq[] {Fq.createZero(), Fq.createZero()}, (b, c) -> Fq.createZero());
    Assertions.assertThat(exceptionalIso).isNull();

    AtomicInteger callCounter = new AtomicInteger();
    exceptionalIso =
        G1Hasher.isoMap(
            new Fq[] {Fq.createZero(), Fq.createZero()},
            (b, c) -> {
              callCounter.getAndIncrement();
              if (callCounter.get() == 2) {
                return Fq.createZero();
              } else {
                return Fq.createOne();
              }
            });
    Assertions.assertThat(exceptionalIso).isNull();

    Fq[] fqs = G1Hasher.mapToCurveSimpleSwu(Fq.createZero());
    CurveHelper<Fq> helper = new CurveHelper<>(Fq::createConstant, G1Hasher.swuB, G1Hasher.swuA);
    Assertions.assertThat(helper.isOnCurve(fqs[0], fqs[1])).isTrue();

    Assertions.assertThat(G1Hasher.clearCofactor(null)).isEqualTo(G1.POINT_AT_INFINITY);
  }

  @Test
  public void expandMessage() {
    byte[] bytes = G1Hasher.expandMessage(message0, 64 * 2);
    Assertions.assertThat(bytes).hasSize(128);
  }

  @Test
  public void encodeSize() {
    byte[] encoded = G1Hasher.encodeSize(456);
    Assertions.assertThat(encoded).contains(0x01, 0xc8);
  }
}
