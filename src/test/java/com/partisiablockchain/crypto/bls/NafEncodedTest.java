package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class NafEncodedTest {

  @Test
  void checkRepresentation() {
    check(Short.MAX_VALUE, new int[] {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2});
    check(Short.MAX_VALUE + 2, new int[] {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1});
    check(
        Integer.MAX_VALUE / 2,
        new int[] {
          1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          2
        });
    check(0b010101010111000, new int[] {1, 0, 2, 0, 2, 0, 2, 0, 2, 0, 0, 2, 0, 0, 0});
  }

  private void check(long maxValue, int[] bytes) {
    NafEncoded values = NafEncoded.create(BigInteger.valueOf(maxValue));
    checkValues(values, bytes);
  }

  private void checkValues(NafEncoded values, int[] bytes) {
    List<Integer> list = new ArrayList<>();
    for (NafEncoded.Value value : values) {
      list.add(value.ordinal());
    }
    List<Integer> integers = Arrays.stream(bytes).boxed().toList();
    Assertions.assertThat(list).isEqualTo(integers);
  }
}
