package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.Hash;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Random;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.RunnerException;

/** Pairing benchmark. */
@SuppressWarnings("unused")
public abstract class PairingBenchmark {

  /**
   * Runs the pairing benchmark.
   *
   * @param args unused
   * @throws IOException exception
   */
  public static void main(String[] args) throws IOException, RunnerException {
    org.openjdk.jmh.Main.main(args);
  }

  /**
   * Benchmarks implementation of BlsKeyPair for creating with NAF encoding.
   *
   * @param plan an execution plan
   * @param blackhole a black hole
   */
  @Fork(value = 1, warmups = 1)
  @Benchmark
  public void blsKeyPairCreate(ExecutionPlan plan, Blackhole blackhole) {
    for (int i = plan.iterations; i > 0; i--) {
      BigInteger secretKey = selectSecretKey(i % 9, plan);
      blackhole.consume(new BlsKeyPair(secretKey));
    }
  }

  /**
   * Benchmarks implementation of BlsKeyPair for hashing with NAF encoding.
   *
   * @param plan an execution plan
   * @param blackhole a black hole
   */
  @Fork(value = 1, warmups = 1)
  @Benchmark
  public void blsKeyPairHash(ExecutionPlan plan, Blackhole blackhole) {
    for (int i = plan.iterations; i > 0; i--) {
      BlsKeyPair keyPair = selectBlsKeyPair(i % 9, plan);
      Hash message = selectHashMessage(i % 9, plan);
      blackhole.consume(keyPair.sign(message));
    }
  }

  /**
   * Benchmarks implementation of computePairing.
   *
   * @param plan an execution plan
   * @param blackhole a black hole
   */
  @Fork(value = 1, warmups = 1)
  @Benchmark
  public void computePairing(ExecutionPlan plan, Blackhole blackhole) {
    for (int i = plan.iterations; i > 0; i--) {
      G1 g1 = selectG1(i % 9, plan);
      G2 g2 = selectG2(i % 34, plan);
      blackhole.consume(Pairing.computePairing(g1, g2));
    }
  }

  /**
   * Benchmarks implementation of miller loop.
   *
   * @param plan an execution plan
   * @param blackhole a black hole
   */
  @Fork(value = 1, warmups = 1)
  @Benchmark
  public void millerLoop(ExecutionPlan plan, Blackhole blackhole) {
    for (int i = plan.iterations; i > 0; i--) {
      G1 g1 = selectG1(i % 9, plan);
      G2 g2 = selectG2(i % 34, plan);
      blackhole.consume(Pairing.millerLoop(g1, g2));
    }
  }

  /**
   * Benchmarks implementation of final exponentiation.
   *
   * @param plan an execution plan
   * @param blackhole a black hole
   */
  @Fork(value = 1, warmups = 1)
  @Benchmark
  public void finalExponentiation(ExecutionPlan plan, Blackhole blackhole) {
    for (int i = plan.iterations; i > 0; i--) {
      Fq12 fq12 = selectFq12(i % 9, plan);
      blackhole.consume(Pairing.finalExponentiation(fq12));
    }
  }

  /**
   * Selects the next Fq12 element to be benchmarked.
   *
   * @param index index for lookup
   * @param plan an execution plan
   * @return G1
   */
  private Fq12 selectFq12(int index, ExecutionPlan plan) {
    return plan.opsFq12[index % ExecutionPlan.COUNT];
  }

  /**
   * Selects the next G1 element to be benchmarked.
   *
   * @param index index for lookup
   * @param plan an execution plan
   * @return G1
   */
  private G1 selectG1(int index, ExecutionPlan plan) {
    return plan.opsG1[index % ExecutionPlan.COUNT];
  }

  /**
   * Selects the next G2 element to be benchmarked.
   *
   * @param index index for lookup
   * @param plan an execution plan
   * @return G2
   */
  private G2 selectG2(int index, ExecutionPlan plan) {
    return plan.opsG2[index % ExecutionPlan.COUNT];
  }

  /**
   * Selects the next BigInteger secret key to be benchmarked.
   *
   * @param index index for lookup
   * @param plan an execution plan
   * @return BigInteger
   */
  private BigInteger selectSecretKey(int index, ExecutionPlan plan) {
    return plan.secretKey[index % ExecutionPlan.COUNT];
  }

  /**
   * Selects the next BlsKeyPair to be benchmarked.
   *
   * @param index index for lookup
   * @param plan an execution plan
   * @return BlsKeyPairBig
   */
  private BlsKeyPair selectBlsKeyPair(int index, ExecutionPlan plan) {
    return plan.blsKeyPair[index % ExecutionPlan.COUNT];
  }

  /**
   * Selects the next hash message to be benchmarked.
   *
   * @param index index for lookup
   * @param plan an execution plan
   * @return BlsKeyPairBig
   */
  private Hash selectHashMessage(int index, ExecutionPlan plan) {
    return plan.hashes[index % ExecutionPlan.COUNT];
  }

  /** ExecutionPlan class. */
  @State(Scope.Benchmark)
  public abstract static class ExecutionPlan {

    public static final int COUNT = 13;

    @Param({"100"})
    private int iterations;

    @Param({"381"})
    private int bitLength;

    private final G1[] opsG1 = new G1[COUNT];
    private final G2[] opsG2 = new G2[COUNT];
    private final Fq12[] opsFq12 = new Fq12[COUNT];
    private final BigInteger[] secretKey = new BigInteger[COUNT];
    private final BlsKeyPair[] blsKeyPair = new BlsKeyPair[COUNT];
    private final Hash[] hashes = new Hash[COUNT];

    /** Setup method for benchmarks. */
    @Setup(Level.Invocation)
    public void setUp() {
      Random random = new Random(1234);
      for (int i = 0; i < COUNT; i++) {
        BigInteger bigInteger1 = new BigInteger(bitLength, random);
        BigInteger bigInteger2 = new BigInteger(bitLength, random);
        secretKey[i] = bigInteger1;
        blsKeyPair[i] = new BlsKeyPair(bigInteger2);
        hashes[i] = Hash.create(s -> s.writeDynamicBytes(bigInteger1.toByteArray()));
        opsG1[i] =
            new G1(
                Fq.create(bigInteger1.toByteArray()),
                Fq.create(bigInteger2.toByteArray()),
                Fq.createOne());
        BigInteger bigInteger3 = new BigInteger(bitLength, random);
        BigInteger bigInteger4 = new BigInteger(bitLength, random);
        BigInteger bigInteger5 = new BigInteger(bitLength, random);
        BigInteger bigInteger6 = new BigInteger(bitLength, random);
        opsG2[i] =
            new G2(
                new Fq2(Fq.create(bigInteger3.toByteArray()), Fq.create(bigInteger4.toByteArray())),
                new Fq2(Fq.create(bigInteger5.toByteArray()), Fq.create(bigInteger6.toByteArray())),
                Fq2.createOne());
        opsFq12[i] =
            new Fq12(
                new Fq6(
                    new Fq2(
                        Fq.create(bigInteger1.toByteArray()), Fq.create(bigInteger2.toByteArray())),
                    new Fq2(
                        Fq.create(bigInteger3.toByteArray()), Fq.create(bigInteger4.toByteArray())),
                    Fq2.createOne()),
                new Fq6(
                    new Fq2(
                        Fq.create(bigInteger5.toByteArray()), Fq.create(bigInteger6.toByteArray())),
                    new Fq2(
                        Fq.create(bigInteger1.toByteArray()), Fq.create(bigInteger2.toByteArray())),
                    Fq2.createOne()));
      }
    }
  }
}
