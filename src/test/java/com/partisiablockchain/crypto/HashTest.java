package com.partisiablockchain.crypto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

/** Test. */
public final class HashTest {

  @Test
  public void equals() {
    EqualsVerifier.forClass(Hash.class).verify();
  }

  @Test
  public void booleanTest() {
    Hash trueHash = Hash.create(s -> s.writeBoolean(true));
    Hash falseHash = Hash.create(s -> s.writeBoolean(false));

    assertThat(trueHash).isNotEqualTo(falseHash);
  }

  @Test
  public void hashFalse() {
    Hash asByte = Hash.create(s -> s.writeByte(0));
    Hash asBoolean = Hash.create(s -> s.writeBoolean(false));

    assertThat(asByte).isEqualTo(asBoolean);
  }

  @Test
  public void hashTrue() {
    Hash asByte = Hash.create(s -> s.writeByte(1));
    Hash asBoolean = Hash.create(s -> s.writeBoolean(true));

    assertThat(asByte).isEqualTo(asBoolean);
  }

  @Test
  public void hashInt() {
    Hash fromBytes = Hash.create(s -> s.write(new byte[] {-1, -1, -1, -1}));
    Hash fromInt = Hash.create(s -> s.writeInt(-1));

    assertThat(fromInt).isEqualTo(fromBytes);
  }

  @Test
  public void tooShortHash() {
    assertThatThrownBy(
            () -> Hash.fromString("9d562d6f7cbb41165407c22526b8cd859621f4b05226e9deed28b96f71f6fd"))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void tooLongHash() {
    assertThatThrownBy(
            () ->
                Hash.fromString(
                    "9d562d6f7cbb41165407c22526b8cd859621f4b05226e9deed28b96f71f6fdcb00"))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void hashBunchOfStuff() {
    final BlockchainAddress account =
        BlockchainAddress.fromString("000000000000000000000000000000000006666666");
    final Hash hash1 =
        Hash.fromString("9d562d6f7cbb41165407c22526b8cd859621f4b05226e9deed28b96f71f6fdcb");
    final Hash hash2 =
        Hash.fromString("9f1fd770c0d632318e6e99c0339896577caf2fc1cdcb1b144683e1312b791c24");
    final Hash hash3 =
        Hash.fromString("6918bf3aad86418825594976103cd829090ac5fafbbb0abd7c732aaf7e35c1f2");

    Hash hash =
        Hash.create(
            stream -> {
              stream.writeEnum(MyEnum.VALUE);
              stream.write(new byte[] {0x11, 0x22});
              hash1.write(stream);
              hash2.write(stream);
              hash3.write(stream);
              account.write(stream);
              stream.writeByte(8);
              stream.writeLong(Long.MAX_VALUE);
            });

    assertThat(hash.toString())
        .isEqualTo("f954a8d5427c3aac315d81433fb6ac7d4ee3ce363fb09086a3ffed037fb2a1de");
  }

  @Test
  public void listSerializer() {
    List<Hash> hashes =
        List.of(
            Hash.fromString("9f1fd770c0d632318e6e99c0339896577caf2fc1cdcb1b144683e1312b791c24"),
            Hash.fromString("6918bf3aad86418825594976103cd829090ac5fafbbb0abd7c732aaf7e35c1f2"));
    List<Hash> deserialized =
        SafeDataInputStream.deserialize(
            stream -> Hash.LIST_SERIALIZER.readFixed(stream, 2),
            SafeDataOutputStream.serialize(
                stream -> Hash.LIST_SERIALIZER.writeFixed(stream, hashes)));
    assertThat(deserialized).isEqualTo(hashes);
  }

  @Test
  public void fixedListSerializer() {
    Hash hash1 =
        Hash.fromString("9f1fd770c0d632318e6e99c0339896577caf2fc1cdcb1b144683e1312b791c24");
    Hash hash2 =
        Hash.fromString("6918bf3aad86418825594976103cd829090ac5fafbbb0abd7c732aaf7e35c1f2");
    FixedList<Hash> hashes = FixedList.create(List.of(hash1, hash2));
    FixedList<Hash> deserialized =
        SafeDataInputStream.deserialize(
            stream -> Hash.FIXED_LIST_SERIALIZER.readFixed(stream, 2),
            SafeDataOutputStream.serialize(
                stream -> Hash.FIXED_LIST_SERIALIZER.writeFixed(stream, hashes)));
    assertThat(deserialized).containsExactly(hash1, hash2);
  }

  @Test
  public void compareTo() {
    Hash a1 = Hash.fromString("a000000000000000000000000000000000000000000000000000000000000000");
    Hash a2 = Hash.fromString("a000000000000000000000000000000000000000000000000000000000000000");
    Hash b = Hash.fromString("b000000000000000000000000000000000000000000000000000000000000000");

    assertThat(a1.compareTo(a2)).isEqualTo(0);
    assertThat(a1.compareTo(b)).isLessThan(0);
    assertThat(b.compareTo(a1)).isEqualTo(b.compareTo(a2)).isGreaterThan(0);
  }

  private enum MyEnum {
    VALUE
  }
}
