package com.partisiablockchain.crypto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.bls.BlsVerifier;
import com.partisiablockchain.crypto.bls.G1;
import com.partisiablockchain.crypto.bls.G2;
import com.partisiablockchain.crypto.bls.NafEncoded;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import java.util.Random;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BlsSignatureTest {

  private static final BigInteger secretKey = BigInteger.valueOf(123453);

  private static final BlsKeyPair keyPair0 = new BlsKeyPair(BigInteger.valueOf(42));

  private static final NafEncoded order =
      NafEncoded.create(
          new BigInteger(
              "52435875175126190479447740508185965837690552500527637822603658699938581184513"));

  private static final Hash message =
      Hash.create(stream -> stream.writeString("hey hey, I'm a message"));

  @Test
  public void sign() {
    BlsKeyPair keyPair = new BlsKeyPair(secretKey);

    BlsSignature signature = keyPair.sign(message);
    Assertions.assertThat(BlsVerifier.verify(message, signature, keyPair.getPublicKey())).isTrue();

    BlsSignature sig0 = keyPair0.sign(message);

    // updatedSignature is now an aggregate signature of two signatures.
    // Similar holds for updatedPk.
    BlsSignature updatedSignature = signature.addSignature(sig0);
    BlsPublicKey updatedPk = keyPair.getPublicKey().addPublicKey(keyPair0.getPublicKey());

    Assertions.assertThat(BlsVerifier.verify(message, updatedSignature, updatedPk)).isTrue();
    Assertions.assertThat(BlsVerifier.verify(message, updatedSignature, keyPair.getPublicKey()))
        .isFalse();
  }

  @Test
  public void serialization() {
    BlsKeyPair keyPair = new BlsKeyPair(secretKey);
    BlsSignature signature = keyPair.sign(message);
    byte[] signatureBytes = SafeDataOutputStream.serialize(signature::write);
    BlsSignature signaturePrime =
        BlsSignature.read(SafeDataInputStream.createFromBytes(signatureBytes));
    Assertions.assertThat(signature).isEqualTo(signaturePrime);
    Assertions.assertThat(keyPair.getPrivateKey()).isEqualTo(secretKey);

    BlsPublicKey publicKey = keyPair.getPublicKey();
    byte[] pkBytes = SafeDataOutputStream.serialize(publicKey::write);
    BlsPublicKey publicKeyPrime = BlsPublicKey.read(SafeDataInputStream.createFromBytes(pkBytes));
    Assertions.assertThat(publicKey).isEqualTo(publicKeyPrime);
  }

  @Test
  public void equalsTest() {
    EqualsVerifier.forClass(BlsSignature.class)
        .withNonnullFields("signatureValue")
        .withPrefabValues(
            G1.class,
            G1.G.scalePoint(NafEncoded.create(BigInteger.TWO)),
            G1.G.scalePoint(NafEncoded.create(BigInteger.TEN)))
        .verify();

    EqualsVerifier.forClass(BlsPublicKey.class)
        .withNonnullFields("publicKeyValue")
        .withPrefabValues(
            G2.class,
            G2.G.scalePoint(NafEncoded.create(BigInteger.TWO)),
            G2.G.scalePoint(NafEncoded.create(BigInteger.TEN)))
        .verify();
  }

  @Test
  public void signatureIsCorrect() {
    G1 pointAtInfinity = G1.G.scalePoint(order);
    Assertions.assertThatThrownBy(() -> new BlsSignature(pointAtInfinity.serialize(true)))
        .hasMessage("Signature is zero");
  }

  @Test
  public void blsKeyPairIsNotPointAtInfinity() {
    BigInteger groupOrder =
        new BigInteger(
            "52435875175126190479447740508185965837690552500527637822603658699938581184513");
    Assertions.assertThatThrownBy(() -> new BlsKeyPair(groupOrder))
        .hasMessage("Public keys cannot point at infinity");
  }

  @Test
  void createUnsafe() {
    NafEncoded scalar = NafEncoded.create(new BigInteger(380, new Random()));
    G1 point = G1.G.scalePoint(scalar);

    byte[] serialized = point.serialize(true);

    var unsafe1 = BlsSignature.readUnsafe(SafeDataInputStream.createFromBytes(serialized));
    Assertions.assertThat(unsafe1.getSignatureValue()).isEqualTo(point);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(serialized);
    var unsafe2 = BlsSignature.readUnsafe(stream);
    Assertions.assertThat(unsafe1).isEqualTo(unsafe2);
  }
}
