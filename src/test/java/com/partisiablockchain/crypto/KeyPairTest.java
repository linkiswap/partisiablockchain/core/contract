package com.partisiablockchain.crypto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.crypto.Curve.CURVE;
import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.secata.tools.coverage.FunctionUtility;
import java.math.BigInteger;
import org.junit.jupiter.api.Test;

/** Test. */
public final class KeyPairTest {

  @Test
  public void signAndRecoverKeyOne() {
    signAndTestForKey(BigInteger.ONE);
  }

  @Test
  public void signAndRecoverKeyTwo() {
    signAndTestForKey(BigInteger.TWO);
  }

  private void signAndTestForKey(BigInteger privateKey) {
    KeyPair keyPair = new KeyPair(privateKey);
    byte[] message = new byte[32];
    Hash messageHash = Hash.create(out -> out.write(message));
    Signature signature = keyPair.sign(messageHash);

    assertThat(signature).isNotNull();

    BlockchainAddress sender = signature.recoverSender(messageHash);
    assertThat(sender).isEqualTo(keyPair.getPublic().createAddress());
  }

  @Test
  public void nonRecoverableSignature() {
    KeyPair keyPair = new KeyPair(BigInteger.TEN);
    Hash messageHash = Hash.create(FunctionUtility.noOpConsumer());
    Signature signature = keyPair.sign(messageHash);

    Signature modifiedSignature =
        new Signature(signature.getRecoveryId() + 1 % 4, signature.getR(), signature.getS());
    assertThat(modifiedSignature.recoverSender(messageHash)).isNull();
  }

  @Test
  public void unableToFindRecoveryId() {
    KeyPair keyPair = new KeyPair();
    byte[] message = new byte[32];
    Hash messageHash = Hash.create(out -> out.write(message));
    Signature signature = keyPair.sign(messageHash);

    Signature withRecoveryId =
        KeyPair.findRecoveryId(
            messageHash,
            signature.getR(),
            signature.getS(),
            new BlockchainPublicKey(Curve.CURVE.getG()));
    assertThat(withRecoveryId).isNull();
  }

  @Test
  public void publicPointFromPrivateWithLargeBitSize() {
    KeyPair keyPair = new KeyPair(CURVE.getN().multiply(BigInteger.TWO).add(BigInteger.TEN));
    assertThat(keyPair.getPrivateKey()).isEqualTo(BigInteger.TEN);
  }

  @Test
  public void publicPointFromPrivateWithExactBitSize() {
    BigInteger key = CURVE.getN().add(BigInteger.TEN);
    KeyPair keyPair = new KeyPair(key);
    assertThat(keyPair.getPrivateKey()).isEqualTo(key);
  }

  @Test
  public void shouldNotNormalizeHalfCurve() {
    BigInteger bigInteger = KeyPair.normalizeS(Curve.HALF_CURVE_ORDER);
    assertThat(bigInteger).isEqualTo(Curve.HALF_CURVE_ORDER);
  }

  @Test
  public void shouldNormalizeHalfCurvePlusOne() {
    BigInteger bigInteger = KeyPair.normalizeS(Curve.HALF_CURVE_ORDER.add(BigInteger.ONE));
    assertThat(bigInteger).isEqualTo(Curve.HALF_CURVE_ORDER);
  }
}
