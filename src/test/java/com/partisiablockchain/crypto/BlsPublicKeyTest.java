package com.partisiablockchain.crypto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.bls.G2;
import com.partisiablockchain.crypto.bls.NafEncoded;
import com.secata.stream.SafeDataInputStream;
import java.math.BigInteger;
import java.util.Random;
import org.assertj.core.api.Assertions;
import org.bouncycastle.util.encoders.Base64;
import org.junit.jupiter.api.Test;

/** Tests for {@link BlsPublicKey}. */
public final class BlsPublicKeyTest {

  private static final NafEncoded order =
      NafEncoded.create(
          new BigInteger(
              "52435875175126190479447740508185965837690552500527637822603658699938581184513"));

  @Test
  public void createFromBytesInvalidLength() {
    Assertions.assertThatThrownBy(() -> new BlsPublicKey(new byte[95]))
        .hasMessageContaining("Expected 192 bytes, but got 95");
  }

  @Test
  public void publicKeyPointsAtInfinity() {
    G2 pointAtInfinity = G2.G.scalePoint(order);
    Assertions.assertThatThrownBy(() -> new BlsPublicKey(pointAtInfinity.serialize(true)))
        .hasMessage("Public keys cannot point at infinity");
  }

  @Test
  public void uncompressed() {
    byte[] g2Bytes = G2.G.serialize(false);
    BlsPublicKey key = BlsPublicKey.readUncompressed(SafeDataInputStream.createFromBytes(g2Bytes));
    Assertions.assertThat(key.getPublicKeyValue()).isEqualTo(G2.G);
  }

  @Test
  void createUnsafe() {
    NafEncoded scalar = NafEncoded.create(new BigInteger(380, new Random()));
    G2 point = G2.G.scalePoint(scalar);

    byte[] serialized = point.serialize(true);

    var unsafe1 = BlsPublicKey.createUnsafe(serialized);
    Assertions.assertThat(unsafe1.getPublicKeyValue()).isEqualTo(point);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(serialized);
    var unsafe2 = BlsPublicKey.readUnsafe(stream);
    Assertions.assertThat(unsafe1).isEqualTo(unsafe2);
  }

  @Test
  public void toStringTest() {
    byte[] decode = Base64.decode("AiebLsq336r4rq2G/KR+RJsnxYsUQU9VXonHGwjYiIsB");
    BlockchainPublicKey blockchainPublicKey = BlockchainPublicKey.fromEncodedEcPoint(decode);
    Assertions.assertThat(blockchainPublicKey.toString())
        .isEqualTo("AiebLsq336r4rq2G/KR+RJsnxYsUQU9VXonHGwjYiIsB");
  }
}
