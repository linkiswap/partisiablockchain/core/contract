package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import java.util.Random;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class LargeByteArrayTest {

  @Test
  public void createAndUserLargeByteArray() {
    Random random = new Random(123);
    byte[] randomBytes = new byte[187951];

    random.nextBytes(randomBytes);
    testWithBytes(randomBytes, "277b0af16ab0ebfb91ec3fd97e190c3b966c764878349b14ca617a59f7151047");
    randomBytes = new byte[random.nextInt(100_000)];
    random.nextBytes(randomBytes);
    testWithBytes(randomBytes, "7addc006361924b4bec1c3722f382bc34ea10207f76c1e6828832d174288b215");
    randomBytes = new byte[random.nextInt(1_000)];
    random.nextBytes(randomBytes);
    testWithBytes(randomBytes, "50493c0b9e99a9f88bbcff62b84b71013dac0ea56291a294a267507808942119");
  }

  private void testWithBytes(byte[] randomBytes, String hash) {
    LargeByteArray largeByteArray = new LargeByteArray(randomBytes);
    Assertions.assertThat(largeByteArray.getLength()).isEqualTo(randomBytes.length);
    Assertions.assertThat(largeByteArray.getIdentifier()).isEqualTo(Hash.fromString(hash));
    Assertions.assertThat(largeByteArray.getData()).containsExactly(randomBytes);

    final boolean supportRecordDeserialization = false; // Not needed
    StateSerializer stateSerializer =
        new StateSerializer(new MemoryStorage(), supportRecordDeserialization);
    SerializationResult write = stateSerializer.write(largeByteArray);
    int sizeOfNullMask = 0;
    Assertions.assertThat(write.totalByteCount())
        .isEqualTo(randomBytes.length + Integer.BYTES + sizeOfNullMask);
    LargeByteArray read = stateSerializer.read(write.hash(), LargeByteArray.class);
    Assertions.assertThat(read).isNotNull();
    Assertions.assertThat(read.getLength()).isEqualTo(randomBytes.length);
    Assertions.assertThat(read.getData()).containsExactly(randomBytes);
    Assertions.assertThat(read.getIdentifier()).isEqualTo(largeByteArray.getIdentifier());
  }

  @Test
  public void serialize() {
    LargeByteArray largeByteArray = new LargeByteArray();
    Assertions.assertThatThrownBy(largeByteArray::getData).isInstanceOf(NullPointerException.class);
  }
}
