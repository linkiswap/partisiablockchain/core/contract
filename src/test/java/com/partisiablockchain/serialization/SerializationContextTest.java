package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.tree.AvlTree;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class SerializationContextTest {

  private final SerializationContext context = new SerializationContext(new MemoryStorage(), true);

  @Test
  void nullValue() {
    Assertions.assertThat(context.convertValue(null, Long.class)).isNull();
  }

  @Test
  void longPrimitive() {
    Assertions.assertThat(context.convertValue(1L, long.class)).isEqualTo(1L);
  }

  @SuppressWarnings("UnnecessaryBoxing")
  @Test
  void longSimpleType() {
    Assertions.assertThat(context.convertValue(1L, Long.class)).isEqualTo(Long.valueOf(1L));
  }

  @Test
  void longAvlTree() {
    Assertions.assertThat(context.convertValue(null, AvlTree.class)).isEqualTo(null);
  }

  @Test
  void mismatchingType() {
    Assertions.assertThatThrownBy(() -> context.convertValue(1L, SerializationContext.class))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(
            "Illegal object type for serialization. "
                + "Expected=[class com.partisiablockchain.serialization.SerializationContext], "
                + "Actual=[class java.lang.Long]");
  }
}
