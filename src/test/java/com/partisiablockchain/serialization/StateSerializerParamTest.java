package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.secata.tools.coverage.ExceptionConverter;
import java.util.Collection;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.recursive.comparison.RecursiveComparisonConfiguration;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/** Test. */
@SuppressWarnings({"unchecked", "rawtypes", "unused"})
public final class StateSerializerParamTest {

  static final RecursiveComparisonConfiguration cryptoAwareConfig =
      StateSerializerTest.cryptoAwareConfig;

  //// Generalized happy path cases

  static Collection<StateExamples.TestCase> provideRecordTestCases() {
    return StateExamples.provideRecordTestCases();
  }

  static Collection<StateExamples.TestCase> provideNonRecordTestCases() {
    return StateExamples.provideNonRecordTestCases();
  }

  static Collection<StateExamples.TestCase> provideAllTestCases() {
    return StateExamples.provideAllTestCases();
  }

  //// Tests

  @ParameterizedTest
  @MethodSource("provideNonRecordTestCases")
  void writeReadNonRecord(final StateExamples.TestCase testCase) {
    readWriteBytes(testCase, false);
  }

  @ParameterizedTest
  @MethodSource("provideRecordTestCases")
  void writeReadRecord(final StateExamples.TestCase testCase) {
    readWriteBytes(testCase, true);
  }

  @ParameterizedTest
  @MethodSource("provideRecordTestCases")
  void writeReadRecordFailure(final StateExamples.TestCase testCase) {
    final String rawClassName = testCase.state().getClass().toString().substring(6);

    // Ensure that serialization fails
    Assertions.assertThatCode(() -> readWriteBytes(testCase, false))
        .isInstanceOf(RuntimeException.class)
        .cause()
        .isInstanceOf(ReflectiveOperationException.class)
        .hasMessageContaining("" + rawClassName);
  }

  /** Test that each individual test case state can be serialized and deserialized. */
  private static void readWriteBytes(
      final StateExamples.TestCase testCase, final boolean supportRecordDeserialization) {
    // Setup
    System.out.println(testCase.name());
    final Class<? extends StateSerializable> clazz = testCase.state().getClass();
    final StateSerializable stateObject = testCase.state();
    final Class<?>[] parameters = testCase.typeArguments().toArray(new Class<?>[0]);

    final MemoryStorage storage = new MemoryStorage();
    final StateSerializer serializer =
        new StateSerializer(new HashHistoryStorage(storage), supportRecordDeserialization);

    // Serialize to bytes
    SerializationResult serializationResult = serializer.write(stateObject, parameters);
    Assertions.assertThat(serializer.createHash(stateObject, parameters))
        .isEqualTo(serializationResult.hash());

    // Deserialize from bytes
    StateSerializable read = serializer.read(serializationResult.hash(), clazz, parameters);

    // Verify that state is stable across serialization
    Assertions.assertThat(read).usingRecursiveComparison(cryptoAwareConfig).isEqualTo(stateObject);

    // Write object again
    int writtenBefore = storage.getObjectsWritten();
    SerializationResult result = serializer.write(stateObject, parameters);

    // Verify that change count did not change
    Assertions.assertThat(storage.getObjectsWritten()).isEqualTo(writtenBefore);
  }

  @ParameterizedTest
  @MethodSource("provideAllTestCases")
  void writeReadJson(final StateExamples.TestCase testCase) {
    // Setup
    System.out.println(testCase.name());
    final Class<? extends StateSerializable> clazz = testCase.state().getClass();
    final StateSerializable stateObject = testCase.state();
    final Class<?>[] parameters = testCase.typeArguments().toArray(new Class<?>[0]);
    final ObjectMapper jsonMapper = StateObjectMapper.createObjectMapper();

    // Write to json
    final String stateAsJson =
        ExceptionConverter.call(
            () -> jsonMapper.writeValueAsString(stateObject),
            testCase.name() + ": Unable to convert to json");

    // Read from json
    final Object stateReadFromJson =
        ExceptionConverter.call(
            () ->
                jsonMapper.readValue(
                    stateAsJson,
                    jsonMapper.getTypeFactory().constructParametricType(clazz, parameters)),
            testCase.name() + ": Unable to read from json");

    // Verify that state is stable across serialization
    Assertions.assertThat(stateObject)
        .as(testCase.name())
        .usingRecursiveComparison(cryptoAwareConfig)
        .isEqualTo(stateReadFromJson);
  }

  /** Test that each test case state is unique. */
  @Test
  public void allTestCasesDifferent() {
    final List<StateExamples.TestCase> testCases = List.copyOf(StateExamples.provideAllTestCases());
    for (int idx1 = 0; idx1 < testCases.size(); idx1++) {
      for (int idx2 = 0; idx2 < testCases.size(); idx2++) {
        if (idx1 == idx2) {
          continue;
        }
        final StateExamples.TestCase case1 = testCases.get(idx1);
        final StateExamples.TestCase case2 = testCases.get(idx2);
        Assertions.assertThat(case1.state())
            .as("%s vs %s".formatted(case1.name(), case2.name()))
            .usingRecursiveComparison(cryptoAwareConfig)
            .isNotEqualTo(case2.state());
      }
    }
  }
}
