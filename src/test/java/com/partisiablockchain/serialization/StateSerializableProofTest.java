package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.google.errorprone.annotations.ImmutableTypeParameter;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class StateSerializableProofTest {

  private static final boolean supportRecordDeserialization = false; // Not needed

  private final Hash subId = Hash.create(s -> s.writeInt(1111));
  private final byte[] data =
      SafeDataOutputStream.serialize(
          s -> {
            // Mask for nullable objects
            s.writeByte(0b11);
            s.writeInt(37);
            subId.write(s);
          });
  private final Hash expectedHash = Hash.create(s -> s.write(data));
  private final StateSerializableProof proof =
      StateSerializableProof.create(data, supportRecordDeserialization);

  @Test
  public void getRootHash() {
    Assertions.assertThat(proof.getIdentifier()).isEqualTo(expectedHash);
  }

  @Test
  public void reader() {
    StateSerializableProof.ValueReader reader = proof.reader(MyState.class, Integer.class);
    Assertions.assertThat(reader.getSubIdentifier("next")).isEqualTo(subId);
    Assertions.assertThat(reader.<Integer>getInlineValue("current")).isEqualTo(37);

    Assertions.assertThatThrownBy(() -> reader.<Hash>getInlineValue("next"))
        .hasMessageContaining("Trying to get value for field that is not serialized inline");
    Assertions.assertThatThrownBy(() -> reader.getSubIdentifier("current"))
        .hasMessageContaining("Trying to get identifier for field that is serialized inline");
    Assertions.assertThatThrownBy(() -> reader.getSubIdentifier("current1"))
        .hasMessageContaining("Type does not contain field with name current1");
  }

  @Test
  public void writeAndRead() {
    StateSerializableProof readProof =
        SafeDataInputStream.readFully(
            SafeDataOutputStream.serialize(proof),
            x -> StateSerializableProof.read(x, supportRecordDeserialization));
    Assertions.assertThat(readProof).usingRecursiveComparison().isEqualTo(proof);
  }

  @Immutable
  @SuppressWarnings({"unused", "FieldCanBeLocal"})
  private static final class MyState<@ImmutableTypeParameter FirstT> implements StateSerializable {

    private final FirstT current;
    private final MyState<FirstT> next;

    private MyState(FirstT current, MyState<FirstT> next) {
      this.next = next;
      this.current = current;
    }
  }
}
