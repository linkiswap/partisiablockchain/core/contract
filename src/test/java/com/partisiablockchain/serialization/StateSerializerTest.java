package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.immutable.FixedList;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.recursive.comparison.RecursiveComparisonConfiguration;
import org.bouncycastle.math.ec.ECPoint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
@SuppressWarnings({"unchecked", "rawtypes", "unused"})
public final class StateSerializerTest {

  static final RecursiveComparisonConfiguration cryptoAwareConfig =
      RecursiveComparisonConfiguration.builder()
          .withEqualsForType(ECPoint::equals, ECPoint.class)
          .withEqualsForType(BlsPublicKey::equals, BlsPublicKey.class)
          .withEqualsForType(BlsSignature::equals, BlsSignature.class)
          .withEqualsForType(StateSerializerTest::assertEqualAvlTrees, AvlTree.class)
          .withEqualsForType(StateSerializerTest::assertEqualFixedLists, FixedList.class)
          .withEqualsForType(StateSerializerTest::assertEqualLargeByteArray, LargeByteArray.class)
          .withStrictTypeChecking(true)
          .build();

  private StateSerializer populate;
  private ObjectMapper jsonMapper;
  private MemoryStorage storage;

  /** Setup serialization. */
  @BeforeEach
  public void setUp() {
    final boolean supportDeserializeRecords = false; // Not needed for this functionality
    storage = new MemoryStorage();
    populate = new StateSerializer(new HashHistoryStorage(storage), supportDeserializeRecords);
    jsonMapper = StateObjectMapper.createObjectMapper();
  }

  @Test
  public void handleNull() {
    SerializationResult result = populate.write(null);
    Assertions.assertThat(result.totalByteCount()).isEqualTo(0);
    StateExamples.WithFields read = read(StateExamples.WithFields.class, result);
    Assertions.assertThat(read).isNull();
  }

  @Test
  public void writeReadFreshWriteReadFreshWrite() {
    MemoryStorage storage1 = new MemoryStorage();
    MemoryStorage storage2 = new MemoryStorage();
    MemoryStorage storage3 = new MemoryStorage();

    final StateSerializer serializer1 = new StateSerializer(storage1, false, false);
    final StateSerializer serializer2 = new StateSerializer(storage2, false, true);
    final StateSerializer serializer3 = new StateSerializer(storage3, false, true);

    AvlTree<Integer, StateLong> tree = AvlTree.create();
    tree = tree.set(1, new StateLong(1));
    tree = tree.set(2, new StateLong(2));
    tree = tree.set(3, new StateLong(3));
    StateExamples.WithTree stateSerializable = new StateExamples.WithTree(123, tree);

    Hash write1Hash = serializer1.write(stateSerializable).hash();
    Map<Hash, byte[]> dataBefore = storage1.getSerialized();
    StateSerializable read1 = serializer1.read(write1Hash, StateExamples.WithTree.class); // lazy

    Hash write2Hash = serializer2.write(read1).hash(); // fresh
    StateSerializable read2 = serializer2.read(write2Hash, StateExamples.WithTree.class); // lazy

    Hash write3Hash = serializer3.write(read2).hash(); // fresh
    Map<Hash, byte[]> dataAfter = storage3.getSerialized();

    Assertions.assertThat(dataAfter).containsExactlyEntriesOf(dataBefore);
  }

  @Test
  public void freshWriteEmptyTree() {
    MemoryStorage storage1 = new MemoryStorage();
    MemoryStorage storage2 = new MemoryStorage();

    StateSerializer serializer1 = new StateSerializer(storage1, false, true);
    StateSerializer serializer2 = new StateSerializer(storage2, false, true);

    AvlTree<Integer, StateLong> tree = AvlTree.create();
    StateExamples.WithTree stateSerializable = new StateExamples.WithTree(1, tree);

    Hash write1Hash = serializer1.write(stateSerializable).hash();
    Map<Hash, byte[]> dataBefore = storage1.getSerialized();
    StateSerializable read1 = serializer1.read(write1Hash, StateExamples.WithTree.class); // lazy

    Hash write2Hash = serializer2.write(read1).hash(); // fresh
    Map<Hash, byte[]> dataAfter = storage2.getSerialized();

    Assertions.assertThat(dataAfter).containsExactlyEntriesOf(dataBefore);
  }

  @Test
  public void withoutNoArgs() {
    StateExamples.WithoutNoArgsConstuctor test = new StateExamples.WithoutNoArgsConstuctor("");
    Assertions.assertThatThrownBy(
            () -> writeReadAndVerifyBoth(StateExamples.WithoutNoArgsConstuctor.class, test))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Cannot instantiate "
                + StateExamples.WithoutNoArgsConstuctor.class
                + " - missing a default constructor");
  }

  @Test
  public void illegalSubType() {
    StateExamples.WithFieldsAbstract withFields =
        new StateExamples.WithFieldsAbstract(new StateExamples.AbstractStateImplementation());
    expectIllegalSubType(withFields);
  }

  @Test
  public void compositeTreeIllegalSub() {
    StateExamples.WithCompositeAbstractTree withTree =
        new StateExamples.WithCompositeAbstractTree(
            AvlTree.<Integer, StateExamples.AbstractState>create()
                .set(112, new StateExamples.AbstractStateImplementation()));
    expectIllegalSubType(withTree);
  }

  @Test
  public void withIllegalSubInList() {
    StateExamples.WithCompositeAbstractList withList =
        new StateExamples.WithCompositeAbstractList(
            FixedList.create(List.of(new StateExamples.AbstractStateImplementation())));

    expectIllegalSubType(withList);
  }

  private void expectIllegalSubType(StateSerializable toWrite) {
    Assertions.assertThatThrownBy(() -> populate.write(toWrite))
        .isInstanceOf(IllegalArgumentException.class)
        .hasStackTraceContaining("Illegal object type for serialization")
        .hasStackTraceContaining(
            "Expected=[class " + StateExamples.class.getName() + "$AbstractState]")
        .hasStackTraceContaining(
            "Actual=[class " + StateExamples.class.getName() + "$AbstractStateImplementation]");
  }

  @Test
  public void illegalType() {
    Assertions.assertThatThrownBy(() -> populate.write(new StateExamples.WithMap(Map.of())))
        .isInstanceOf(IllegalArgumentException.class)
        .hasStackTraceContaining(
            "Field \"map\" in "
                + StateExamples.WithMap.class
                + " cannot be serialized, due to "
                + java.util.HashMap.class
                + " not implementing StateSerializable.");
  }

  @Test
  public void illegalTypeParameters() {
    Assertions.assertThatThrownBy(
            () -> populate.write(new StateExamples.ListWithoutGenerics(FixedList.create())))
        .isInstanceOf(IllegalArgumentException.class)
        .hasStackTraceContaining("FixedList must be parameterized");
    Assertions.assertThatThrownBy(
            () -> populate.write(new StateExamples.ListWithParameterizedType(null)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasStackTraceContaining("Type arguments cannot be parameterized");
    Assertions.assertThatThrownBy(() -> populate.write(new ListWithInvalidType(null)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasStackTraceContaining(
            "Field \"list\" in "
                + ListWithInvalidType.class
                + " cannot be serialized, due to "
                + Short.class
                + " not implementing StateSerializable.");
    Assertions.assertThatThrownBy(() -> populate.write(new StateExamples.TreeWithoutGenerics(null)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasStackTraceContaining("AvlTree must be parameterized");
    Assertions.assertThatThrownBy(() -> populate.write(new TreeWithInvalidTypeFirst(null)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasStackTraceContaining(
            "Field \"tree\" in "
                + TreeWithInvalidTypeFirst.class
                + " cannot be serialized, due to "
                + ByteBuffer.class
                + " not implementing StateSerializable.");
    Assertions.assertThatThrownBy(() -> populate.write(new TreeWithInvalidTypeSecond(null)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasStackTraceContaining(
            "Field \"tree\" in "
                + TreeWithInvalidTypeSecond.class
                + " cannot be serialized, due to "
                + ByteBuffer.class
                + " not implementing StateSerializable.");
  }

  @Test
  public void updatedLeafWithSameValueShouldNotBeWrittenAgain() {
    StateExamples.WithTree withTree =
        new StateExamples.WithTree(1, AvlTree.create(Map.of(1, new StateLong(2L))));

    SerializationResult result1 = populate.write(withTree);
    Assertions.assertThat(result1.totalByteCount()).isEqualTo(54L);

    withTree = new StateExamples.WithTree(2, withTree.tree().set(1, new StateLong(2L)));
    SerializationResult result2 = writeWithChanged(1, withTree);
    Assertions.assertThat(result2.totalByteCount()).isEqualTo(54L);
  }

  @Test
  public void listWithSameHashShouldNotBeWrittenAgain() {
    StateExamples.WithList withList =
        new StateExamples.WithList(12, FixedList.create(List.of(1, 2, 3)));

    populate.write(withList);
    withList = new StateExamples.WithList(1, withList.list());
    writeWithChanged(1, withList);
  }

  @Test
  public void proof() {
    Hash key = Hash.create(s -> s.writeInt(0));
    AvlTree<Hash, Long> empty = AvlTree.create();
    Assertions.assertThatThrownBy(
            () ->
                populate.avlProof(
                    new StateExamples.WithHashTree<>(empty, null), "tree", key, Long.class))
        .hasStackTraceContaining("Key " + key + " does not exist");
    Assertions.assertThatThrownBy(
            () ->
                populate.avlProof(
                    new StateExamples.WithHashTree<>(
                        empty.set(Hash.create(s -> s.writeInt(1)), 10L), null),
                    "tree",
                    key,
                    Long.class))
        .hasStackTraceContaining("Key " + key + " does not exist");

    AvlTree<Hash, Long> tree = empty.set(key, 0L);
    for (int i = 1; i < 5; i++) {
      StateExamples.WithHashTree serializable = new StateExamples.WithHashTree(tree, null);
      Hash expectedRootHash = populate.createHash(serializable, Long.class);
      StateSerializableProof stateProof = populate.proof(serializable, Long.class);
      StateSerializableProof.ValueReader reader =
          stateProof.reader(StateExamples.WithHashTree.class, Long.class);
      AvlProof proof = populate.avlProof(serializable, "tree", key, Long.class);
      Assertions.assertThat(proof.getRootIdentifier()).isEqualTo(reader.getSubIdentifier("tree"));
      Hash readKey = proof.getKey();
      Assertions.assertThat(reader.<Long>getInlineValue("someValue")).isEqualTo(777L);
      Assertions.assertThat(readKey).isEqualTo(key);

      int finalI = i;
      tree = tree.set(Hash.create(s -> s.writeInt(finalI)), (long) i);
    }
  }

  private <T extends StateSerializable> void writeReadAndVerifyBoth(
      Class<? extends T> clazz, T withFields, Class<?>... parameters) {
    writeReadAndVerify(clazz, withFields, parameters);
    writeReadAndVerifyJson(clazz, withFields, parameters);
  }

  private <T extends StateSerializable> void writeReadAndVerify(
      Class<? extends T> clazz, T withFields, Class<?>... parameters) {
    T read = read(clazz, write(clazz, withFields, parameters), parameters);

    Assertions.assertThat(read).usingRecursiveComparison(cryptoAwareConfig).isEqualTo(withFields);

    writeWithChanged(0, withFields, parameters);
  }

  private <T extends StateSerializable> SerializationResult writeWithChanged(
      int changeCount, T withFields, Class<?>... parameters) {
    int writtenBefore = storage.getObjectsWritten();
    SerializationResult result = populate.write(withFields, parameters);
    Assertions.assertThat(storage.getObjectsWritten()).isEqualTo(writtenBefore + changeCount);
    return result;
  }

  private <T extends StateSerializable> void writeReadAndVerifyJson(
      Class<? extends T> clazz, T withFields, Class<?>... parameters) {
    T fromJson = writeAndReadJson(clazz, withFields, parameters);
    Assertions.assertThat(withFields)
        .usingRecursiveComparison(cryptoAwareConfig)
        .isEqualTo(fromJson);
  }

  private <T> T writeAndReadJson(Class<? extends T> clazz, T withFields, Class<?>... parameters) {
    String json =
        ExceptionConverter.call(
            () -> jsonMapper.writeValueAsString(withFields), "Unable to convert to json");
    return ExceptionConverter.call(
        () ->
            jsonMapper.readValue(
                json, jsonMapper.getTypeFactory().constructParametricType(clazz, parameters)),
        "Unable to read from json");
  }

  private <T extends StateSerializable> SerializationResult write(
      Class<? extends T> clazz, T withFields, Class<?>... parameters) {
    SerializationResult result = populate.write(withFields, parameters);
    Assertions.assertThat(populate.createHash(withFields, parameters)).isEqualTo(result.hash());
    return result;
  }

  private <T extends StateSerializable> T read(
      Class<? extends T> clazz, SerializationResult result, Class<?>... parameters) {
    return populate.read(result.hash(), clazz, parameters);
  }

  @Immutable
  static final class TreeWithInvalidTypeFirst implements StateSerializable {

    @SuppressWarnings("Immutable")
    public final AvlTree<ByteBuffer, StateLong> tree;

    public TreeWithInvalidTypeFirst() {
      tree = null;
    }

    private TreeWithInvalidTypeFirst(AvlTree<ByteBuffer, StateLong> tree) {
      this.tree = tree;
    }
  }

  @Immutable
  static final class TreeWithInvalidTypeSecond implements StateSerializable {

    @SuppressWarnings("Immutable")
    public final AvlTree<Long, ByteBuffer> tree;

    private TreeWithInvalidTypeSecond(AvlTree<Long, ByteBuffer> tree) {
      this.tree = tree;
    }
  }

  @Immutable
  static final class ListWithInvalidType implements StateSerializable {

    @SuppressWarnings("FieldCanBeLocal")
    private final FixedList<Short> list;

    private ListWithInvalidType(FixedList<Short> list) {
      this.list = list;
    }
  }

  /**
   * Custom equality between two AVL-trees.
   *
   * <p>They are equal if, they possess the same keyset, and are recursively equal.
   *
   * @param tree1 First tree to compare
   * @param tree2 Second tree to compare.
   * @return true if equal.
   */
  private static <K extends Comparable<K>, V> boolean assertEqualAvlTrees(
      final AvlTree<K, V> tree1, final AvlTree<K, V> tree2) {
    // Shortcut for different sizes
    if (tree1 == tree2) {
      return true;
    } else if (tree1 == null || tree2 == null) {
      return false;
    } else if (tree1.size() != tree2.size()) {
      return false;
    }

    final var keys = new LinkedHashSet<K>();
    keys.addAll(tree1.keySet());
    keys.addAll(tree2.keySet());
    try {
      for (final K key : keys) {
        Assertions.assertThat(tree1.getValue(key))
            .as(key.toString())
            .usingRecursiveComparison(cryptoAwareConfig)
            .isEqualTo(tree2.getValue(key));
      }
    } catch (AssertionError e) {
      return false;
    }
    return true;
  }

  /**
   * Custom equality between two fixed lists.
   *
   * <p>They are equal if, they are equally long, and are recursively equal.
   *
   * @param list1 First list to compare
   * @param list2 Second list to compare.
   * @return true if equal.
   */
  private static <V> boolean assertEqualFixedLists(
      final FixedList<V> list1, final FixedList<V> list2) {
    // Shortcut for different sizes
    if (list1 == null || list2 == null) {
      return false;
    } else if (list1.size() != list2.size()) {
      return false;
    }

    try {
      for (int idx = 0; idx < list1.size(); idx++) {
        Assertions.assertThat(list1.get(idx))
            .as("" + idx)
            .usingRecursiveComparison(cryptoAwareConfig)
            .isEqualTo(list2.get(idx));
      }
    } catch (AssertionError e) {
      return false;
    }
    return true;
  }

  /**
   * Custom between large byte arrays.
   *
   * <p>They are equal if, they are equally long, and each bytes is pairwise equal.
   *
   * @param array1 First array to compare
   * @param array2 Second array to compare.
   * @return true if equal.
   */
  private static boolean assertEqualLargeByteArray(
      final LargeByteArray array1, final LargeByteArray array2) {
    if (array1 == null || array2 == null) {
      return false;
    }
    return Arrays.equals(array1.getData(), array2.getData());
  }
}
