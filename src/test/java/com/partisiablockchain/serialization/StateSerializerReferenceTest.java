package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.WithResource;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/** Reference testing of serializer in order to detect possible regressions. */
@SuppressWarnings({"unchecked", "rawtypes", "unused"})
public final class StateSerializerReferenceTest {

  private static final Path REFERENCE_FILES_PATH =
      getResourceSourcePath(StateExamples.class, "ref");

  private static boolean REGENERATE_REF_FILES = false;

  @BeforeAll
  public static void checkShouldRegenerateFiles() {
    REGENERATE_REF_FILES = !REFERENCE_FILES_PATH.toFile().exists();
  }

  static Collection<StateExamples.TestCase> provideAllTestCases() {
    return StateExamples.provideAllTestCases();
  }

  static Collection<StateExamples.TestCase> provideRecordTestCasesOnly() {
    return StateExamples.provideRecordTestCases();
  }

  /**
   * Reference test that the given test case can be serialized to a well-defined binary form.
   *
   * @param testCase Test case.
   */
  @ParameterizedTest
  @MethodSource("provideAllTestCases")
  public void writeReferenceBinary(final StateExamples.TestCase testCase) {
    final boolean deserializeRecords = false; // Not needed for this functionality.
    final MemoryStorage storage = new MemoryStorage();
    final StateSerializer serializer =
        new StateSerializer(new HashHistoryStorage(storage), deserializeRecords);

    // Setup
    final Path referenceFilepath = REFERENCE_FILES_PATH.resolve(testCase.name() + ".bin");
    final StateSerializable stateObject = testCase.state();
    final Class<?>[] typeArguments = testCase.typeArguments().toArray(new Class<?>[0]);

    // Determine produced output
    final SerializationResult serializationResult =
        this.<StateSerializable>write(
            serializer, stateObject.getClass(), stateObject, typeArguments);
    final byte[] gottenBytes =
        storage.read(serializationResult.hash(), SafeDataInputStream::readAllBytes);
    final String gottenBytesAsString = Hex.toHexString(gottenBytes);
    final String gottenOutput =
        "%s\nBytes: %d total".formatted(gottenBytesAsString, serializationResult.totalByteCount());

    // Write reference if regenerating
    if (REGENERATE_REF_FILES) {
      writeText(referenceFilepath, gottenOutput);
      System.err.println("Regenerated " + referenceFilepath);
    }

    // Load reference text
    final String expectedOutput = loadText(referenceFilepath);

    // Check output is as expected
    Assertions.assertThat(gottenOutput)
        .as(testCase.name())
        .isEqualToIgnoringNewLines(expectedOutput);
  }

  /**
   * Reference test that the given test case can be serialized to a well-defined JSON form.
   *
   * @param testCase Test case.
   */
  @ParameterizedTest
  @MethodSource("provideAllTestCases")
  public void writeReferenceJson(final StateExamples.TestCase testCase) {
    // Setup
    final Path referenceFilepath = REFERENCE_FILES_PATH.resolve(testCase.name() + ".json");
    final StateSerializable stateObject = testCase.state();
    final ObjectMapper jsonMapper = StateObjectMapper.createObjectMapper();

    // Determine produced output
    String gottenOutput =
        ExceptionConverter.call(
            () -> jsonMapper.writeValueAsString(stateObject), "Unable to convert to json");

    // Write reference if regenerating
    if (REGENERATE_REF_FILES) {
      writeText(referenceFilepath, gottenOutput);
      System.err.println("Regenerated " + referenceFilepath);
    }

    // Load reference text
    final String expectedOutput = loadText(referenceFilepath);

    // Check output is as expected
    Assertions.assertThat(gottenOutput)
        .as(testCase.name())
        .isEqualToIgnoringNewLines(expectedOutput);
  }

  /**
   * Reference test that the given test case can write the full {@link StateSerializable} object
   * (with potentially lazy values) to a fresh storage.
   *
   * @param testCase Test case.
   */
  @ParameterizedTest
  @MethodSource("provideAllTestCases")
  public void freshWriteReference(final StateExamples.TestCase testCase) {
    MemoryStorage storage1 = new MemoryStorage();
    MemoryStorage storage2 = new MemoryStorage();

    StateSerializer serializer1 = new StateSerializer(storage1, true, true);
    StateSerializer serializer2 = new StateSerializer(storage2, true, true);

    StateSerializable stateObject = testCase.state();
    Class<?>[] typeArguments = testCase.typeArguments().toArray(new Class<?>[0]);

    Hash write1Hash = serializer1.write(stateObject, typeArguments).hash();
    Map<Hash, byte[]> dataBefore = storage1.getSerialized();

    // Read might be lazy
    StateSerializable read1 = serializer1.read(write1Hash, stateObject.getClass(), typeArguments);

    // Fresh write required
    Hash write2Hash = serializer2.write(read1, typeArguments).hash();
    Map<Hash, byte[]> dataAfter = storage2.getSerialized();

    Assertions.assertThat(dataAfter).containsExactlyEntriesOf(dataBefore);
  }

  private <T extends StateSerializable> SerializationResult write(
      final StateSerializer serializer,
      Class<? extends T> clazz,
      T withFields,
      Class<?>... parameters) {
    SerializationResult result = serializer.write(withFields, parameters);
    Assertions.assertThat(serializer.createHash(withFields, parameters)).isEqualTo(result.hash());
    return result;
  }

  /**
   * Produces path for the resource with the given filename relative to the given class.
   *
   * @param clazz Class to determine resource relative to.
   * @param filename Filename of resource.
   * @return Path of file.
   */
  static Path getResourceSourcePath(final Class<?> clazz, final String filename) {
    final String[] split = clazz.getPackageName().split("\\.");
    final Path referencePathDir = Paths.get("./src/test/resources/", split);
    return referencePathDir.resolve(filename);
  }

  /**
   * Writes text to the given file.
   *
   * @param filePath Path of file.
   * @param textWithUnixNewline Text contents of file, with unix newlines.
   */
  static void writeText(final Path filePath, final String textWithUnixNewline) {
    final String text = textWithUnixNewline.replace("\n", System.lineSeparator());

    WithResource.accept(
        () -> {
          // Create any directories required
          Files.createDirectories(filePath.getParent());

          // Write file itself
          return new java.io.PrintWriter(filePath.toFile(), StandardCharsets.UTF_8);
        },
        outputStream -> outputStream.write(text),
        "Could not write to file " + filePath);
  }

  /**
   * Loads text from the given file.
   *
   * @param filePath Path of file.
   * @return Text contents of file.
   */
  static String loadText(final Path filePath) {
    final byte[] fileBytes =
        WithResource.apply(
            () -> new FileInputStream(filePath.toFile()),
            InputStream::readAllBytes,
            "Could not load file: " + filePath);

    return new String(fileBytes, StandardCharsets.UTF_8);
  }
}
