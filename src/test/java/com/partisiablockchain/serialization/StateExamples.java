package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.google.errorprone.annotations.ImmutableTypeParameter;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.zk.counting.ChannelCommunication;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Curve;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateExamples.WithFields.Enums;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

/** Utility class containing example state classes. */
@SuppressWarnings({"unchecked", "rawtypes", "unused"})
public final class StateExamples {

  /**
   * Contains information about an individual test case.
   *
   * <ul>
   *   <li>name: Name of the test case.
   *   <li>state: The given state.
   *   <li>typeArguments: Types of generic arguments.
   *   <li>requireRecordDeserializationSupport: Whether this state can be deserialized without
   *       support for record deserialization.
   * </ul>
   */
  record TestCase(
      String name,
      StateSerializable state,
      boolean requireRecordDeserializationSupport,
      List<Class<?>> typeArguments) {

    /** Create new test case. */
    public TestCase {
      Objects.requireNonNull(name);
      Objects.requireNonNull(state);
      Objects.requireNonNull(typeArguments);
    }
  }

  /**
   * Unconditional inner blocks exhibits style conflicts between checkstyle and google format, which
   * this variable helps to circumvent. Rather than using actualy uncondition inner blocks {@code {
   * XYZ; }}, we write the equivalent of {@code if (true) { XYZ; }}, through the indirection of this
   * variable, in order to avoid warning about redundancy in if-statements.
   */
  private static final boolean TRUE = true;

  private static final BlockchainAddress ACCOUNT_ONE =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");

  private static final BlockchainAddress ACCOUNT_TWO =
      BlockchainAddress.fromString("000000000000000000000000000000000000000002");

  private static final byte[] ENCODED_EC_POINT =
      Curve.CURVE.getG().multiply(BigInteger.valueOf(777)).getEncoded(false);
  private static final BlsKeyPair BLS_KEY_PAIR = new BlsKeyPair(BigInteger.TEN);

  private static <T> Collection<T> concat(Collection<T>... collections) {
    final ArrayList<T> concat = new ArrayList<T>();
    for (final Collection<T> col : collections) {
      concat.addAll(col);
    }
    return concat;
  }

  /**
   * Produces a collection of example states of various classes.
   *
   * <p>Note that names of test cases may overlap between different classes, which means that these
   * classes should possess the same serialized form.
   *
   * @return Collection of test cases.
   */
  static Collection<TestCase> provideAllTestCases() {
    return concat(provideClassicClassTestCases(), provideRecordTestCases());
  }

  static Collection<StateExamples.TestCase> provideNonRecordTestCases() {
    return concat(
        StateExamples.provideClassicClassTestCases(),
        StateExamples.provideComplexSpecialTestCases());
  }

  private static TestCase classicClassCase(
      String name, StateSerializable state, Class<?>... parameters) {
    return new TestCase(name, state, false, Arrays.asList(parameters));
  }

  static Collection<TestCase> provideClassicClassTestCases() {
    // Various states
    final WithFields withFields = new WithFields(123, 113L, null, null);
    final Generic<BlockchainAddress> generic =
        new Generic<>(ACCOUNT_TWO, FixedList.create(List.of(ACCOUNT_ONE)), null);

    // Simple cases
    final ArrayList<TestCase> cases =
        new ArrayList<>(
            List.of(
                classicClassCase("writeWithoutSub", new WithFields(112L, 113L, null, Enums.ONE)),
                classicClassCase("withFieldsSub", withFields),
                classicClassCase(
                    "withEnumOverridingMethod",
                    new WithEnumOverridingMethod(OverridingEnums.FIRST)),
                classicClassCase(
                    "withEnumOverridingMethodNull", new WithEnumOverridingMethod(null)),
                classicClassCase(
                    "readWriteU256", new U256Serialize(Unsigned256.create(new BigInteger("100")))),
                classicClassCase(
                    "writeWithBlockchainPublicKey",
                    new WithCryptoTypes(
                        BlockchainPublicKey.fromEncodedEcPoint(ENCODED_EC_POINT),
                        Hash.create(stream -> stream.writeLong(112233L)),
                        new Signature(
                            2, BigInteger.valueOf(115599L), BigInteger.valueOf(779995566L)))),
                classicClassCase(
                    "withBlsTypes",
                    new WithBlsTypes(
                        BLS_KEY_PAIR.getPublicKey(),
                        BLS_KEY_PAIR.sign(Hash.create(s -> s.writeString("message"))))),
                classicClassCase(
                    "withFieldsWithFields", new WithFields(112L, 113L, withFields, Enums.THREE)),
                classicClassCase("nonNullable", new NoNullableFields(99)),
                classicClassCase("empty", new Empty()),
                classicClassCase("versionedString", new WithStringVersioned("A string")),
                classicClassCase("generic", generic, BlockchainAddress.class),
                classicClassCase("containsGeneric", new SubGeneric(generic)),
                classicClassCase("booleanTrue", new StateBoolean(true)),
                classicClassCase("booleanFalse", new StateBoolean(false)),
                classicClassCase(
                    "wrappedBooleanTrue",
                    new SimpleGeneric<>(new StateBoolean(true)),
                    StateBoolean.class),
                classicClassCase(
                    "wrappedBooleanFalse",
                    new SimpleGeneric<>(new StateBoolean(false)),
                    StateBoolean.class),
                classicClassCase(
                    "generic2",
                    new Generic<>(
                        145L,
                        FixedList.create(List.of(1L, 2L, 3L)),
                        AvlTree.create(Map.of(1L, new StateLong(2L)))),
                    Long.class),
                classicClassCase("simpleStateString", new StateString("Test")),
                classicClassCase(
                    "wrappedStateString",
                    new SimpleGeneric<>(new StateString("Test")),
                    StateString.class),
                classicClassCase(
                    "wrappedJavaUUID",
                    new Generic<>(
                        UUID.fromString("3181a066-276e-4b2b-bd7f-3e1f95e8d230"), null, null),
                    UUID.class),
                classicClassCase(
                    "genericAgain",
                    new GenericAgain<>(
                        new Generic<>(
                            145L,
                            FixedList.create(List.of(1L, 2L, 3L)),
                            AvlTree.create(Map.of(1L, new StateLong(2L))))),
                    Long.class)));

    // Immutable data structures
    if (TRUE) {
      AvlTree<Integer, StateLong> tree =
          AvlTree.<Integer, StateLong>create().set(1, new StateLong(2L)).set(7, new StateLong(8L));
      WithTree withTree = new WithTree(1337, tree);
      cases.add(classicClassCase("withTree", withTree));
    }
    if (TRUE) {
      final WithTree withTree = new WithTree(0, AvlTree.create());
      cases.add(classicClassCase("emptyTree", withTree));
    }
    if (TRUE) {
      WithFields expected = new WithFields();
      AvlTree<Integer, WithFields> tree = AvlTree.<Integer, WithFields>create().set(112, expected);
      WithCompositeTree withTree = new WithCompositeTree(tree);
      cases.add(classicClassCase("compositeTreeValue", withTree));
    }
    if (TRUE) {
      CompositeKey expected = new CompositeKey(44);
      WithCompositeKey withTree =
          new WithCompositeKey(
              AvlTree.<CompositeKey, StateLong>create().set(expected, new StateLong(132)));
      cases.add(classicClassCase("compositeTreeKey", withTree));
    }
    if (TRUE) {
      final WithList withList =
          new WithList(2, FixedList.create(Arrays.asList(1, null, 2, 3, null)));
      cases.add(classicClassCase("withList", withList));
    }
    if (TRUE) {
      WithFields expected = new WithFields();
      WithCompositeList withList =
          new WithCompositeList(FixedList.create(Arrays.asList(expected, null)));
      cases.add(classicClassCase("withCompositeList", withList));
    }
    if (TRUE) {
      WithFields expected = new WithFields(10, 11L, new WithFields(), Enums.THREE);
      WithCompositeList withList =
          new WithCompositeList(FixedList.create(Arrays.asList(expected, null)));
      cases.add(classicClassCase("withCompositeList2", withList));
    }
    if (TRUE) {
      WithList withList = new WithList(1, FixedList.create());
      Random random = new Random(22);
      for (int i = 0; i < 20; i++) {
        cases.add(classicClassCase("list_" + i, withList));

        withList =
            new WithList(
                withList.value,
                withList.list.addElement(random.nextBoolean() ? null : random.nextInt()));
      }
    }

    if (TRUE) {
      SubGenericInListAndMap sub =
          new SubGenericInListAndMap(
              FixedList.create(List.of(generic)), AvlTree.create(Map.of(ACCOUNT_ONE, generic)));
      cases.add(classicClassCase("subGenericListAndMap", sub));
    }

    if (TRUE) {
      cases.add(
          classicClassCase("shouldBeAbleToHandleInlineObjectWithLargeByteArrayInTree", new One()));
    }

    if (TRUE) {
      Inline inlineElement = new Inline(123, new WithFields(22, null, null, Enums.THREE), null);
      Inline inlineElement2 =
          new Inline(124, new WithFields(22, null, null, Enums.ONE), inlineElement);

      WithInline withoutCollections =
          new WithInline(
              inlineElement2, FixedList.create(List.of(inlineElement, inlineElement)), null);
      WithInline fullyPopulatedInline =
          new WithInline(
              inlineElement,
              FixedList.create(List.of(inlineElement, inlineElement)),
              AvlTree.create(Map.of(inlineElement, inlineElement, inlineElement2, inlineElement)));

      cases.add(classicClassCase("inlineElement1", inlineElement));
      cases.add(classicClassCase("inlineElement2", inlineElement2));
      cases.add(classicClassCase("inlineWithoutCollections", withoutCollections));
      cases.add(classicClassCase("inlineFullyPopulated", fullyPopulatedInline));
    }

    // Produce cases
    return cases;
  }

  private static TestCase recordCase(String name, StateSerializable state, Class<?>... parameters) {
    return new TestCase(name, state, true, Arrays.asList(parameters));
  }

  static Collection<TestCase> provideRecordTestCases() {
    // Various states
    final RecordGeneric<BlockchainAddress> generic =
        new RecordGeneric<BlockchainAddress>(
            ACCOUNT_TWO, FixedList.create(List.of(ACCOUNT_ONE)), null);

    // Simple cases
    final ArrayList<TestCase> cases =
        new ArrayList<>(
            List.of(
                recordCase(
                    "withEnumOverridingMethod",
                    new RecordWithEnumOverridingMethod(OverridingEnums.FIRST)),
                recordCase(
                    "withEnumOverridingMethodNull", new RecordWithEnumOverridingMethod(null)),
                recordCase(
                    "readWriteU256",
                    new RecordU256Serialize(Unsigned256.create(new BigInteger("100")))),
                recordCase(
                    "writeWithBlockchainPublicKey",
                    new RecordWithCryptoTypes(
                        BlockchainPublicKey.fromEncodedEcPoint(ENCODED_EC_POINT),
                        Hash.create(stream -> stream.writeLong(112233L)),
                        new Signature(
                            2, BigInteger.valueOf(115599L), BigInteger.valueOf(779995566L)))),
                recordCase(
                    "withBlsTypes",
                    new RecordWithBlsTypes(
                        BLS_KEY_PAIR.getPublicKey(),
                        BLS_KEY_PAIR.sign(Hash.create(s -> s.writeString("message"))))),
                recordCase("nonNullable", new RecordNoNullableFields(99)),
                recordCase("generic", generic, BlockchainAddress.class),
                recordCase("containsGeneric", new RecordSubGeneric(generic)),
                recordCase(
                    "wrappedBooleanTrue",
                    new RecordSimpleGeneric<>(new StateBoolean(true)),
                    StateBoolean.class),
                recordCase(
                    "wrappedBooleanFalse",
                    new RecordSimpleGeneric<>(new StateBoolean(false)),
                    StateBoolean.class),
                recordCase(
                    "generic2",
                    new RecordGeneric<Long>(
                        145L,
                        FixedList.create(List.of(1L, 2L, 3L)),
                        AvlTree.create(Map.of(1L, new StateLong(2L)))),
                    Long.class),
                recordCase(
                    "wrappedJavaUUID",
                    new RecordGeneric<UUID>(
                        UUID.fromString("3181a066-276e-4b2b-bd7f-3e1f95e8d230"), null, null),
                    UUID.class),
                recordCase(
                    "genericAgain",
                    new RecordGenericAgain<>(
                        new RecordGeneric<Long>(
                            145L,
                            FixedList.create(List.of(1L, 2L, 3L)),
                            AvlTree.create(Map.of(1L, new StateLong(2L))))),
                    Long.class)));

    // WithFields
    if (TRUE) {
      final RecordWithFields withFields = new RecordWithFields(123, 113L, null, null);
      cases.addAll(
          List.of(
              recordCase("writeWithoutSub", new RecordWithFields(112L, 113L, null, Enums.ONE)),
              recordCase("withFieldsSub", withFields),
              recordCase(
                  "withFieldsWithFields",
                  new RecordWithFields(112L, 113L, withFields, Enums.THREE))));
    }

    // Immutable data structures
    if (TRUE) {
      final var tree =
          AvlTree.<Integer, StateLong>create().set(1, new StateLong(2L)).set(7, new StateLong(8L));
      final var withTree = new RecordWithTree(1337, tree);
      cases.add(recordCase("withTree", withTree));
    }
    if (TRUE) {
      final var withTree = new RecordWithTree(0, AvlTree.create());
      cases.add(recordCase("emptyTree", withTree));
    }
    if (TRUE) {
      final var expected = new RecordCompositeKey(44);
      final var withTree =
          new RecordWithCompositeKey(
              AvlTree.<RecordCompositeKey, StateLong>create().set(expected, new StateLong(132)));
      cases.add(recordCase("compositeTreeKey", withTree));
    }
    if (TRUE) {
      final var withList =
          new RecordWithList(2, FixedList.create(Arrays.asList(1, null, 2, 3, null)));
      cases.add(recordCase("withList", withList));
    }
    if (TRUE) {
      RecordWithList withList = new RecordWithList(1, FixedList.create());
      final Random random = new Random(22);
      for (int i = 0; i < 20; i++) {
        cases.add(recordCase("list_" + i, withList));

        withList =
            new RecordWithList(
                withList.value,
                withList.list.addElement(random.nextBoolean() ? null : random.nextInt()));
      }
    }

    if (TRUE) {
      final var sub =
          new RecordSubGenericInListAndMap(
              FixedList.create(List.of(generic)), AvlTree.create(Map.of(ACCOUNT_ONE, generic)));
      cases.add(recordCase("subGenericListAndMap", sub));
    }

    if (TRUE) {
      final var twoInit = new LargeByteArray(new byte[10]);
      final var oneInit =
          AvlTree.create(
              Map.of(ACCOUNT_ONE, new RecordTwo(twoInit), ACCOUNT_TWO, new RecordTwo(twoInit)));
      cases.add(
          recordCase(
              "shouldBeAbleToHandleInlineObjectWithLargeByteArrayInTree", new RecordOne(oneInit)));
    }

    if (TRUE) {
      final RecordInline inlineElement =
          new RecordInline(123, new WithFields(22, null, null, Enums.THREE), null);
      final RecordInline inlineElement2 =
          new RecordInline(124, new WithFields(22, null, null, Enums.ONE), inlineElement);

      final RecordWithInline withoutCollections =
          new RecordWithInline(
              inlineElement2, FixedList.create(List.of(inlineElement, inlineElement)), null);
      final RecordWithInline fullyPopulatedInline =
          new RecordWithInline(
              inlineElement,
              FixedList.create(List.of(inlineElement, inlineElement)),
              AvlTree.create(Map.of(inlineElement, inlineElement, inlineElement2, inlineElement)));

      cases.add(recordCase("inlineElement1", inlineElement));
      cases.add(recordCase("inlineElement2", inlineElement2));
      cases.add(recordCase("inlineWithoutCollections", withoutCollections));
      cases.add(recordCase("inlineFullyPopulated", fullyPopulatedInline));
    }

    // Record with empty constructor
    if (TRUE) {
      final var withFields = new RecordWithFieldsEmptyConstructor(123, 113L, null, null);
      cases.addAll(
          List.of(
              recordCase("writeWithoutSubEmpty", new RecordWithFieldsEmptyConstructor()),
              recordCase(
                  "writeWithoutSub",
                  new RecordWithFieldsEmptyConstructor(112L, 113L, null, Enums.ONE)),
              recordCase("withFieldsSub", withFields),
              recordCase(
                  "withFieldsWithFields",
                  new RecordWithFieldsEmptyConstructor(112L, 113L, withFields, Enums.THREE))));
    }

    // Produce cases
    return cases;
  }

  static Collection<TestCase> provideComplexSpecialTestCases() {
    final ArrayList<TestCase> cases = new ArrayList<>();

    // Channel communication
    cases.add(
        new TestCase(
            "channelCommunication",
            ChannelCommunication.create(List.of(123L, 432L, 111L), List.of(432L, 124L, 111L)),
            false,
            List.of()));

    // Empty records: Can be deserialized even without record support
    cases.add(new TestCase("empty", new RecordEmpty(), false, List.of()));

    return cases;
  }

  @Immutable
  static final class One implements StateSerializable {

    private final AvlTree<BlockchainAddress, Two> two;

    One() {
      two = AvlTree.create(Map.of(ACCOUNT_ONE, new Two(), ACCOUNT_TWO, new Two()));
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class Two implements StateSerializableInline {

    private final LargeByteArray array;

    Two() {
      array = new LargeByteArray(new byte[10]);
    }
  }

  @Immutable
  static final class WithCryptoTypes implements StateSerializable {

    @SuppressWarnings("Immutable")
    private final BlockchainPublicKey key;

    private final Hash hash;
    private final Signature signature;

    private WithCryptoTypes() {
      signature = null;
      hash = null;
      key = null;
    }

    WithCryptoTypes(BlockchainPublicKey key, Hash hash, Signature signature) {
      this.key = key;
      this.hash = hash;
      this.signature = signature;
    }
  }

  @Immutable
  static final class U256Serialize implements StateSerializable {

    private final Unsigned256 number;

    private U256Serialize() {
      number = null;
    }

    U256Serialize(Unsigned256 number) {
      this.number = number;
    }
  }

  @Immutable
  static final class BigIntegerSerialize implements StateSerializable {

    private final BigInteger bigInteger;

    private BigIntegerSerialize() {
      bigInteger = null;
    }

    BigIntegerSerialize(BigInteger bigInteger) {
      this.bigInteger = bigInteger;
    }
  }

  @Immutable
  static final class WithBlsTypes implements StateSerializable {

    private final BlsPublicKey blsPublicKey;
    private final BlsSignature blsSignature;

    private WithBlsTypes() {
      blsPublicKey = null;
      blsSignature = null;
    }

    WithBlsTypes(BlsPublicKey blsPublicKey, BlsSignature blsSignature) {
      this.blsPublicKey = blsPublicKey;
      this.blsSignature = blsSignature;
    }
  }

  @Immutable
  static final class WithFields implements StateSerializable {

    static AtomicBoolean nonSerializable;
    private final long value;
    private final Long nullable;
    private final WithFields sub;
    private final Enums firstEnum;

    @SuppressWarnings("Immutable")
    transient AtomicBoolean nonSerializableTransient;

    public WithFields() {
      value = 0;
      nullable = null;
      sub = null;
      firstEnum = null;
    }

    WithFields(long value, Long nullable, WithFields sub, Enums firstEnum) {
      this.value = value;
      this.nullable = nullable;
      this.sub = sub;
      this.firstEnum = firstEnum;
    }

    enum Enums {
      ONE,
      TWO,
      THREE
    }
  }

  @Immutable
  static final class NoNullableFields implements StateSerializable {

    private final int field;

    public NoNullableFields() {
      field = 0;
    }

    public NoNullableFields(int field) {
      this.field = field;
    }
  }

  @Immutable
  static final class WithTree implements StateSerializable {

    private final long someValue;
    private final AvlTree<Integer, StateLong> tree;

    public WithTree() {
      tree = AvlTree.create();
      someValue = 0;
    }

    WithTree(long someValue, AvlTree<Integer, StateLong> tree) {
      this.someValue = someValue;
      this.tree = tree;
    }

    public AvlTree<Integer, StateLong> tree() {
      return tree;
    }
  }

  @Immutable
  static final class WithHashTree<@ImmutableTypeParameter ValueT> implements StateSerializable {

    private final long someValue;
    private final WithFields sub;
    private final AvlTree<Hash, ValueT> tree;

    public WithHashTree() {
      tree = AvlTree.create();
      sub = new WithFields();
      someValue = 0;
    }

    WithHashTree(AvlTree<Hash, ValueT> tree, WithFields sub) {
      this.someValue = 777L;
      this.sub = sub;
      this.tree = tree;
    }
  }

  @Immutable
  static final class WithCompositeTree implements StateSerializable {

    private final AvlTree<Integer, WithFields> tree;

    public WithCompositeTree() {
      tree = AvlTree.create();
    }

    WithCompositeTree(AvlTree<Integer, WithFields> tree) {
      this.tree = tree;
    }
  }

  @Immutable
  static final class WithCompositeKey implements StateSerializable {

    private final AvlTree<CompositeKey, StateLong> tree;

    public WithCompositeKey() {
      tree = AvlTree.create();
    }

    WithCompositeKey(AvlTree<CompositeKey, StateLong> tree) {
      this.tree = tree;
    }
  }

  @Immutable
  static final class WithList implements StateSerializable {

    private final int value;
    private final FixedList<Integer> list;

    public WithList() {
      value = 0;
      list = FixedList.create();
    }

    public WithList(int value, FixedList<Integer> list) {
      this.value = value;
      this.list = list;
    }

    public FixedList<Integer> list() {
      return list;
    }
  }

  @Immutable
  static final class WithCompositeList implements StateSerializable {

    private final FixedList<WithFields> list;

    public WithCompositeList() {
      list = FixedList.create();
    }

    WithCompositeList(FixedList<WithFields> list) {
      this.list = list;
    }
  }

  @Immutable
  static final class WithCompositeAbstractList implements StateSerializable {

    private final FixedList<AbstractState> list;

    public WithCompositeAbstractList() {
      list = FixedList.create();
    }

    WithCompositeAbstractList(FixedList<AbstractState> list) {
      this.list = list;
    }
  }

  @Immutable
  static final class WithCompositeAbstractTree implements StateSerializable {

    private final AvlTree<Integer, AbstractState> tree;

    public WithCompositeAbstractTree() {
      tree = AvlTree.create();
    }

    WithCompositeAbstractTree(AvlTree<Integer, AbstractState> tree) {
      this.tree = tree;
    }
  }

  @Immutable
  static final class WithFieldsAbstract implements StateSerializable {

    private final AbstractState field;

    public WithFieldsAbstract() {
      this.field = null;
    }

    public WithFieldsAbstract(AbstractState field) {
      this.field = field;
    }
  }

  @Immutable
  abstract static class AbstractState implements StateSerializable {

    public AbstractState() {}
  }

  @Immutable
  static final class AbstractStateImplementation extends AbstractState {

    AbstractStateImplementation() {
      super();
    }
  }

  @Immutable
  static final class WithStringVersioned implements StateSerializableVersioned {

    private final String string;

    public WithStringVersioned() {
      string = null;
    }

    WithStringVersioned(String string) {
      this.string = string;
    }
  }

  @Immutable
  static final class WithMap implements StateSerializable {

    @SuppressWarnings({"Immutable", "FieldCanBeLocal"})
    private final HashMap<Integer, Integer> map;

    WithMap(Map<Integer, Integer> map) {
      this.map = new HashMap<>(map);
    }
  }

  @Immutable
  static final class TreeWithoutGenerics implements StateSerializable {

    @SuppressWarnings({"Immutable", "FieldCanBeLocal"})
    private final AvlTree tree;

    TreeWithoutGenerics(AvlTree tree) {
      this.tree = tree;
    }
  }

  @Immutable
  static final class ListWithoutGenerics implements StateSerializable {

    @SuppressWarnings({"Immutable", "FieldCanBeLocal"})
    private final FixedList list;

    public ListWithoutGenerics(FixedList list) {
      this.list = list;
    }
  }

  @Immutable
  static final class ListWithParameterizedType implements StateSerializable {

    @SuppressWarnings("FieldCanBeLocal")
    private final FixedList<FixedList<Integer>> list;

    ListWithParameterizedType(FixedList<FixedList<Integer>> list) {
      this.list = list;
    }
  }

  @Immutable
  static final class CompositeKey implements Comparable<CompositeKey>, StateSerializable {

    private final int value;

    private CompositeKey() {
      value = 0;
    }

    CompositeKey(int value) {
      this.value = value;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      CompositeKey that = (CompositeKey) o;
      return value == that.value;
    }

    @Override
    public int hashCode() {
      return Objects.hash(value);
    }

    @Override
    public int compareTo(CompositeKey o) {
      return Integer.compare(value, o.value);
    }
  }

  @Immutable
  static final class SimpleGeneric<T extends StateSerializable> implements StateSerializable {

    final T field;

    public SimpleGeneric() {
      field = null;
    }

    SimpleGeneric(T field) {
      this.field = field;
    }
  }

  @Immutable
  static final class Generic<T extends Comparable<T>> implements StateSerializable {

    @SuppressWarnings("Immutable")
    private final T field;

    @SuppressWarnings("Immutable")
    private final FixedList<T> list;

    @SuppressWarnings("Immutable")
    private final AvlTree<T, StateLong> map;

    public Generic() {
      field = null;
      list = null;
      map = null;
    }

    Generic(T field, FixedList<T> list, AvlTree<T, StateLong> map) {
      this.field = field;
      this.list = list;
      this.map = map;
    }
  }

  @Immutable
  static final class GenericAgain<S extends Comparable<S>> implements StateSerializable {

    private final Generic<S> generics;

    public GenericAgain() {
      generics = null;
    }

    GenericAgain(Generic<S> generics) {
      this.generics = generics;
    }
  }

  @Immutable
  static final class SubGeneric implements StateSerializable {

    @SuppressWarnings("FieldCanBeLocal")
    private final Generic<BlockchainAddress> addresses;

    public SubGeneric() {
      addresses = null;
    }

    SubGeneric(Generic<BlockchainAddress> addresses) {
      this.addresses = addresses;
    }
  }

  @Immutable
  static final class SubGenericInListAndMap implements StateSerializable {

    private final FixedList<Generic<BlockchainAddress>> addressesList;
    private final AvlTree<BlockchainAddress, Generic<BlockchainAddress>> genericMap;

    public SubGenericInListAndMap() {
      addressesList = FixedList.create();
      genericMap = AvlTree.create();
    }

    SubGenericInListAndMap(
        FixedList<Generic<BlockchainAddress>> addressesList,
        AvlTree<BlockchainAddress, Generic<BlockchainAddress>> genericMap) {
      this.addressesList = addressesList;
      this.genericMap = genericMap;
    }
  }

  @Immutable
  static final class Empty implements StateSerializable {}

  @Immutable
  static final class WithoutNoArgsConstuctor implements StateSerializable {

    @SuppressWarnings({"FieldCanBeLocal"})
    private final String value;

    public WithoutNoArgsConstuctor(String value) {
      this.value = value;
    }
  }

  @Immutable
  static final class WithInline implements StateSerializable {

    private final Inline inline;
    private final FixedList<Inline> list;
    private final AvlTree<Inline, Inline> tree;

    WithInline(Inline inline, FixedList<Inline> list, AvlTree<Inline, Inline> tree) {
      this.inline = inline;
      this.list = list;
      this.tree = tree;
    }

    public WithInline() {
      inline = null;
      list = null;
      tree = null;
    }

    public FixedList<Inline> list() {
      return list;
    }

    public AvlTree<Inline, Inline> tree() {
      return tree;
    }
  }

  @Immutable
  static final class Inline implements StateSerializableInline, Comparable<Inline> {

    private final long value;
    private final WithFields fields;
    private final Inline inline;

    public Inline() {
      value = 0;
      fields = null;
      inline = null;
    }

    Inline(long value, WithFields fields, Inline inline) {
      this.value = value;
      this.fields = fields;
      this.inline = inline;
    }

    @Override
    public int compareTo(Inline o) {
      return Long.compare(this.value, o.value);
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o instanceof Inline inline) {
        return value == inline.value;
      } else {
        return false;
      }
    }

    @Override
    public int hashCode() {
      return Objects.hash(value);
    }
  }

  @Immutable
  static final class WithEnumOverridingMethod implements StateSerializable {

    private final OverridingEnums overridingEnum;

    WithEnumOverridingMethod(OverridingEnums overridingEnum) {
      this.overridingEnum = overridingEnum;
    }

    public WithEnumOverridingMethod() {
      overridingEnum = null;
    }
  }

  enum OverridingEnums {
    FIRST() {
      @Override
      void dummy() {}
    },
    SECOND() {
      @Override
      void dummy() {}
    };

    abstract void dummy();
  }

  @Immutable
  record RecordWithFields(
      long value, Long nullable, RecordWithFields sub, WithFields.Enums firstEnum)
      implements StateSerializable {

    static AtomicBoolean nonSerializable;
  }

  @Immutable
  record RecordWithCryptoTypes(BlockchainPublicKey key, Hash hash, Signature signature)
      implements StateSerializable {}

  @Immutable
  record RecordU256Serialize(Unsigned256 number) implements StateSerializable {}

  @Immutable
  record RecordBigIntegerSerialize(BigInteger bigInteger) implements StateSerializable {}

  @Immutable
  record RecordWithBlsTypes(BlsPublicKey blsPublicKey, BlsSignature blsSignature)
      implements StateSerializable {}

  @Immutable
  record RecordNoNullableFields(int field) implements StateSerializable {}

  private interface RecordWithTreeInterface {

    long someValue();

    AvlTree<Integer, StateLong> tree();
  }

  @Immutable
  record RecordWithTree(long someValue, AvlTree<Integer, StateLong> tree)
      implements StateSerializable, RecordWithTreeInterface {}

  @Immutable
  record RecordWithHashTree<@ImmutableTypeParameter ValueT>(
      long someValue, AvlTree<Hash, ValueT> tree, WithFields sub) implements StateSerializable {}

  @Immutable
  record RecordWithCompositeTree(@SuppressWarnings("Immutable") AvlTree<Integer, WithFields> tree)
      implements StateSerializable {}

  @Immutable
  record RecordWithCompositeKey(
      @SuppressWarnings("Immutable") AvlTree<RecordCompositeKey, StateLong> tree)
      implements StateSerializable {}

  @Immutable
  record RecordWithList(int value, FixedList<Integer> list) implements StateSerializable {}

  @Immutable
  record RecordWithCompositeList(FixedList<WithFields> list) implements StateSerializable {}

  @Immutable
  record RecordWithCompositeAbstractList(FixedList<AbstractState> list)
      implements StateSerializable {}

  @Immutable
  record RecordWithCompositeAbstractTree(
      @SuppressWarnings("Immutable") AvlTree<Integer, AbstractState> tree)
      implements StateSerializable {}

  @Immutable
  record RecordWithMap(
      @SuppressWarnings({"Immutable", "FieldCanBeLocal"}) HashMap<Integer, Integer> map)
      implements StateSerializable {}

  @Immutable
  record RecordTreeWithoutGenerics(@SuppressWarnings({"Immutable", "FieldCanBeLocal"}) AvlTree tree)
      implements StateSerializable {}

  @Immutable
  record RecordTreeWithInvalidTypeFirst(
      @SuppressWarnings("Immutable") AvlTree<ByteBuffer, StateLong> tree)
      implements StateSerializable {}

  @Immutable
  record RecordTreeWithInvalidTypeSecond(
      @SuppressWarnings("Immutable") AvlTree<Long, ByteBuffer> tree) implements StateSerializable {}

  @Immutable
  record RecordListWithoutGenerics(
      @SuppressWarnings({"Immutable", "FieldCanBeLocal"}) FixedList list)
      implements StateSerializable {}

  @Immutable
  record RecordListWithInvalidType(@SuppressWarnings("FieldCanBeLocal") FixedList<Short> list)
      implements StateSerializable {}

  @Immutable
  record RecordListWithParameterizedType(
      @SuppressWarnings("FieldCanBeLocal") FixedList<FixedList<Integer>> list)
      implements StateSerializable {}

  @Immutable
  record RecordCompositeKey(int value)
      implements Comparable<RecordCompositeKey>, StateSerializable {

    @Override
    public int compareTo(RecordCompositeKey o) {
      return Integer.compare(value, o.value);
    }
  }

  @Immutable
  record RecordSimpleGeneric<T extends StateSerializable>(T field) implements StateSerializable {}

  @Immutable
  record RecordGeneric<@ImmutableTypeParameter T extends Comparable<T>>(
      T field, FixedList<T> list, AvlTree<T, StateLong> map) implements StateSerializable {}

  @Immutable
  record RecordGenericAgain<@ImmutableTypeParameter S extends Comparable<S>>(
      RecordGeneric<S> generics) implements StateSerializable {}

  @Immutable
  record RecordSubGeneric(
      @SuppressWarnings("FieldCanBeLocal") RecordGeneric<BlockchainAddress> addresses)
      implements StateSerializable {}

  @Immutable
  record RecordSubGenericInListAndMap(
      FixedList<RecordGeneric<BlockchainAddress>> addressesList,
      AvlTree<BlockchainAddress, RecordGeneric<BlockchainAddress>> genericMap)
      implements StateSerializable {}

  @Immutable
  record RecordEmpty() implements StateSerializable {}

  @Immutable
  record RecordWithFieldsEmptyConstructor(
      long value, Long nullable, RecordWithFieldsEmptyConstructor sub, WithFields.Enums firstEnum)
      implements StateSerializable {

    static AtomicBoolean nonSerializable;

    public RecordWithFieldsEmptyConstructor() {
      this(0, null, null, null);
    }
  }

  @Immutable
  record RecordWithInline(
      RecordInline inline, FixedList<RecordInline> list, AvlTree<RecordInline, RecordInline> tree)
      implements StateSerializable {}

  @Immutable
  record RecordInline(long value, WithFields fields, RecordInline inline)
      implements StateSerializableInline, Comparable<RecordInline> {

    @Override
    public int compareTo(RecordInline o) {
      return Long.compare(this.value, o.value);
    }

    @Override
    public boolean equals(Object o) {
      return o instanceof RecordInline inline && value == inline.value;
    }

    @Override
    public int hashCode() {
      return Objects.hash(value);
    }
  }

  @Immutable
  record RecordWithEnumOverridingMethod(OverridingEnums overridingEnum)
      implements StateSerializable {}

  @Immutable
  private record RecordOne(@SuppressWarnings("Immutable") AvlTree<BlockchainAddress, RecordTwo> two)
      implements StateSerializable {}

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  private record RecordTwo(LargeByteArray array) implements StateSerializableInline {}
}
