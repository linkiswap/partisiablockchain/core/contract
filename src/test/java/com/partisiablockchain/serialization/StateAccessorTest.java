package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.tree.AvlLeaf;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class StateAccessorTest {

  @Test
  public void getString() {
    StateSerializable state = new StateString("string state 123");
    StateAccessor stateAccessor = StateAccessor.create(state);
    Assertions.assertThat(stateAccessor.get("value").stringValue()).isEqualTo("string state 123");
  }

  @Test
  public void getFieldWithinField() {
    StateString innerState = new StateString("string state 1234");
    SerializableTestClass serializableTestClass = new SerializableTestClass(innerState);
    StateAccessor stateAccessor = StateAccessor.create(serializableTestClass);
    Assertions.assertThat(stateAccessor.get("stringState").get("value").stringValue())
        .isEqualTo("string state 1234");
  }

  @Test
  public void getObject() {
    SerializableTestClass serializableTestClass =
        new SerializableTestClass(new StateString("string state 1234"));
    StateAccessor stateAccessor = StateAccessor.create(serializableTestClass);
    Assertions.assertThat(stateAccessor.get("stringState").typedValue(StateString.class))
        .isEqualTo(serializableTestClass.getStringState());
  }

  @Test
  public void getSimpleBasicTypes() {
    BasicSimpleTypes basicSimpleTypes = new BasicSimpleTypes(1, 2L, true, "waifu", (byte) 69);
    StateAccessor stateAccessor = StateAccessor.create(basicSimpleTypes);

    Assertions.assertThat(stateAccessor.get("middle").intValue())
        .isEqualTo(basicSimpleTypes.getMiddle());
    Assertions.assertThat(stateAccessor.get("big").longValue())
        .isEqualTo(basicSimpleTypes.getBig());
    Assertions.assertThat(stateAccessor.get("yes").booleanValue())
        .isEqualTo(basicSimpleTypes.isYes());
    Assertions.assertThat(stateAccessor.get("text").stringValue())
        .isEqualTo(basicSimpleTypes.getText());
    Assertions.assertThat(stateAccessor.get("byt").byteValue())
        .isEqualTo(basicSimpleTypes.getByt());
  }

  @Test
  public void getBasicTypes() {
    BasicTypes basicTypes = new BasicTypes();
    StateAccessor stateAccessor = StateAccessor.create(basicTypes);
    Assertions.assertThat(stateAccessor.get("stateBoolean").stateBooleanValue().value())
        .isEqualTo(false);
    Assertions.assertThat(stateAccessor.get("stateBoolean").get("value").booleanValue())
        .isEqualTo(false);
    Assertions.assertThat(stateAccessor.get("stateLong").stateLongValue().value()).isEqualTo(3L);
    Assertions.assertThat(stateAccessor.get("stateString").stateStringValue().value())
        .isEqualTo("state of string");
    Assertions.assertThat(stateAccessor.get("blockchainAddress").blockchainAddressValue())
        .isEqualTo(BlockchainAddress.fromString("000000000000000000000000000000000000000001"));
    Assertions.assertThat(stateAccessor.get("hash").hashValue())
        .isEqualTo(
            Hash.fromString("9d562d6f7cbb41165407c22526b8cd859621f4b05226e9deed28b96f71f6fdcb"));
    Assertions.assertThat(stateAccessor.get("ecPoint").blockchainPublicKeyValue())
        .isEqualTo(new KeyPair(BigInteger.ONE).getPublic());
    Assertions.assertThat(stateAccessor.get("signature").signatureValue())
        .isEqualTo(new Signature(2, BigInteger.ONE, BigInteger.TWO));
    Assertions.assertThat(stateAccessor.get("blsPublicKey").blsPublicKeyValue())
        .isEqualTo(new BlsKeyPair(BigInteger.valueOf(42)).getPublicKey());
    Assertions.assertThat(stateAccessor.get("blsSignature").blsSignatureValue())
        .isEqualTo(
            new BlsKeyPair(BigInteger.valueOf(42))
                .sign(Hash.create(stream -> stream.writeString("hey hey, I'm a message"))));
  }

  @Test
  public void getAvlTree() {
    AvlTreeTest avlTreeTest = new AvlTreeTest();
    StateAccessor stateAccessor = StateAccessor.create(avlTreeTest);

    AvlTree<Integer, StateLong> retrievedTree =
        stateAccessor.get("tree").typedAvlTree(Integer.class, StateLong.class);

    AvlTree<Integer, StateLong> originalTree = avlTreeTest.getTree();
    Assertions.assertThat(retrievedTree.getValue(1).value())
        .isEqualTo(originalTree.getValue(1).value());
    Assertions.assertThat(retrievedTree.getValue(2).value())
        .isEqualTo(originalTree.getValue(2).value());
  }

  @Test
  public void checkTreeType() {
    InnerAvlTreeTest innerAvlTreeTest = new InnerAvlTreeTest();
    StateAccessor stateAccessor = StateAccessor.create(innerAvlTreeTest);
    Assertions.assertThat(
            stateAccessor.get("avlTreeTest").isAvlTreeOfType(Integer.class, StateLong.class))
        .isFalse();
    Assertions.assertThat(
            stateAccessor
                .get("avlTreeTest")
                .get("tree")
                .isAvlTreeOfType(Integer.class, StateLong.class))
        .isTrue();
    Assertions.assertThat(
            stateAccessor
                .get("avlTreeTest")
                .get("leaf")
                .isAvlTreeOfType(Integer.class, StateLong.class))
        .isFalse();
    Assertions.assertThat(
            stateAccessor
                .get("avlTreeTest")
                .get("tree")
                .isAvlTreeOfType(Integer.class, Boolean.class))
        .isFalse();
    Assertions.assertThat(
            stateAccessor
                .get("avlTreeTest")
                .get("tree")
                .isAvlTreeOfType(Double.class, StateLong.class))
        .isFalse();
  }

  @Test
  void isNull() {
    StateAccessor stateAccessor =
        StateAccessor.create(new BasicSimpleTypes(0, 0, true, null, (byte) 0));
    Assertions.assertThat(stateAccessor.isNull()).isFalse();
    Assertions.assertThat(stateAccessor.get("text").isNull()).isTrue();
  }

  @Test
  void hasField() {
    StateAccessor stateAccessor =
        StateAccessor.create(new BasicSimpleTypes(0, 0, true, null, (byte) 0));
    Assertions.assertThat(stateAccessor.hasField("middle")).isTrue();
    Assertions.assertThat(stateAccessor.hasField("unknown")).isFalse();
  }

  @Test
  public void isType() {
    StateAccessor stateAccessor = StateAccessor.create(new StateString("waifu"));
    Assertions.assertThat(stateAccessor.isType(StateString.class)).isTrue();
    Assertions.assertThat(stateAccessor.isType(StateLong.class)).isFalse();
  }

  @Test
  public void checkSimpleBasicTypes() {
    BasicSimpleTypes basicSimpleTypes = new BasicSimpleTypes(1, 2L, true, "waifu", (byte) 69);
    StateAccessor stateAccessor = StateAccessor.create(basicSimpleTypes);

    Assertions.assertThat(stateAccessor.get("middle").isInt()).isTrue();
    Assertions.assertThat(stateAccessor.get("middle").isLong()).isFalse();

    Assertions.assertThat(stateAccessor.get("big").isInt()).isFalse();
    Assertions.assertThat(stateAccessor.get("big").isLong()).isTrue();

    Assertions.assertThat(stateAccessor.get("yes").isBoolean()).isTrue();
    Assertions.assertThat(stateAccessor.get("yes").isString()).isFalse();

    Assertions.assertThat(stateAccessor.get("text").isString()).isTrue();
    Assertions.assertThat(stateAccessor.get("text").isBoolean()).isFalse();

    Assertions.assertThat(stateAccessor.get("byt").isByte()).isTrue();
    Assertions.assertThat(stateAccessor.get("byt").isStateBoolean()).isFalse();
  }

  @Test
  public void checkBasicTypes() {
    BasicTypes basicTypes = new BasicTypes();
    StateAccessor stateAccessor = StateAccessor.create(basicTypes);

    Assertions.assertThat(stateAccessor.get("stateBoolean").isStateBoolean()).isTrue();
    Assertions.assertThat(stateAccessor.get("stateBoolean").isByte()).isFalse();

    Assertions.assertThat(stateAccessor.get("stateLong").isStateLong()).isTrue();
    Assertions.assertThat(stateAccessor.get("stateLong").isStateString()).isFalse();

    Assertions.assertThat(stateAccessor.get("stateString").isStateString()).isTrue();
    Assertions.assertThat(stateAccessor.get("stateString").isStateLong()).isFalse();

    Assertions.assertThat(stateAccessor.get("blockchainAddress").isBlockchainAddress()).isTrue();
    Assertions.assertThat(stateAccessor.get("blockchainAddress").isHash()).isFalse();

    Assertions.assertThat(stateAccessor.get("hash").isHash()).isTrue();
    Assertions.assertThat(stateAccessor.get("hash").isBlockchainAddress()).isFalse();

    Assertions.assertThat(stateAccessor.get("ecPoint").isBlockchainPublicKey()).isTrue();
    Assertions.assertThat(stateAccessor.get("ecPoint").isSignature()).isFalse();

    Assertions.assertThat(stateAccessor.get("signature").isSignature()).isTrue();
    Assertions.assertThat(stateAccessor.get("signature").isBlockchainPublicKey()).isFalse();

    Assertions.assertThat(stateAccessor.get("blsPublicKey").isBlsPublicKey()).isTrue();
    Assertions.assertThat(stateAccessor.get("blsPublicKey").isBlsSignature()).isFalse();

    Assertions.assertThat(stateAccessor.get("blsSignature").isBlsSignature()).isTrue();
    Assertions.assertThat(stateAccessor.get("blsSignature").isBlsPublicKey()).isFalse();
  }

  @Test
  public void treeTypes() {
    AvlTreeTest avlTreeTest = new AvlTreeTest();
    StateAccessor stateAccessor = StateAccessor.create(avlTreeTest).get("tree");
    List<StateAccessorAvlLeafNode> leaves = stateAccessor.getTreeLeaves();
    Assertions.assertThat(leaves.get(0).getKey().intValue()).isEqualTo(1);
    Assertions.assertThat(leaves.get(0).getValue().typedValue(StateLong.class).value())
        .isEqualTo(2);
    Assertions.assertThat(leaves.get(1).getKey().intValue()).isEqualTo(2);
    Assertions.assertThat(leaves.get(1).getValue().typedValue(StateLong.class).value())
        .isEqualTo(4);
    Assertions.assertThat(leaves.get(2).getKey().intValue()).isEqualTo(69);
    Assertions.assertThat(leaves.get(2).getValue().typedValue(StateLong.class).value())
        .isEqualTo(420);
  }

  @Test
  public void getTreeValue() {
    AvlTreeTest avlTreeTest = new AvlTreeTest();
    StateAccessor stateAccessor = StateAccessor.create(avlTreeTest).get("tree");
    Assertions.assertThat(stateAccessor.getTreeValue(69).get("value").longValue()).isEqualTo(420L);
    Assertions.assertThat(stateAccessor.getTreeValue(10)).isNull();
    Assertions.assertThat(
            stateAccessor.getTreeValue(69).isAvlTreeOfType(Integer.class, String.class))
        .isFalse();
  }

  @Test
  public void fixedList() {
    FixedListTest fixedListTest = new FixedListTest();
    StateAccessor stateAccessor = StateAccessor.create(fixedListTest);
    FixedList<Integer> result = stateAccessor.get("fixList").typedFixedList(Integer.class);

    Assertions.assertThat(result.size()).isEqualTo(3);
    Assertions.assertThat(result.get(1)).isEqualTo(420);
  }

  @Test
  public void fixedListType() {
    FixedListTest fixedListTest = new FixedListTest();
    StateAccessor stateAccessor = StateAccessor.create(fixedListTest);

    StateAccessor fixList = stateAccessor.get("fixList");
    Assertions.assertThat(fixList.isFixedListOfType(Integer.class)).isTrue();
    Assertions.assertThat(fixList.isFixedListOfType(String.class)).isFalse();
    Assertions.assertThat(fixList.isFixedListOfType(Long.class)).isFalse();

    StateAccessor integer = stateAccessor.get("integer");
    Assertions.assertThat(integer.isFixedListOfType(Integer.class)).isFalse();
  }

  @Test
  public void fixedListStateAccessors() {
    FixedListTest fixedListTest = new FixedListTest();
    StateAccessor stateAccessor = StateAccessor.create(fixedListTest);

    List<StateAccessor> stateAccessors = stateAccessor.get("listSerialize").getListElements();
    Assertions.assertThat(stateAccessors.get(0).get("key").intValue()).isEqualTo(10);
    Assertions.assertThat(stateAccessors.get(1).get("key").intValue()).isEqualTo(20);
  }

  @Immutable
  @SuppressWarnings("unused")
  private static final class FixedListTest implements StateSerializable {

    private final FixedList<Integer> fixList = FixedList.create(List.of(69, 420, 80085));
    private final Integer integer = 80085;
    private final FixedList<KeyClassTest> listSerialize =
        FixedList.create(List.of(new KeyClassTest(10), new KeyClassTest(20)));

    public FixedListTest() {}
  }

  @Immutable
  @SuppressWarnings("unused")
  private static final class KeyClassTest implements StateSerializable {

    private final Integer key;

    public KeyClassTest() {
      this.key = 1;
    }

    public KeyClassTest(Integer key) {
      this.key = key;
    }
  }

  @Immutable
  @SuppressWarnings("unused")
  private static final class ValueClassTest implements StateSerializable {

    private final Integer value = 1;

    public ValueClassTest() {}
  }

  @Immutable
  @SuppressWarnings("unused")
  private static final class InnerAvlTreeTest implements StateSerializable {

    private final AvlTreeTest avlTreeTest = new AvlTreeTest();

    public InnerAvlTreeTest() {}
  }

  @Immutable
  @SuppressWarnings("unused")
  private static final class AvlTreeTest implements StateSerializable {

    private final AvlTree<Integer, StateLong> tree =
        AvlTree.create(
            Map.of(
                1, new StateLong(2),
                2, new StateLong(4),
                69, new StateLong(420)));
    private final AvlLeaf<Integer, Boolean> leaf = new AvlLeaf<>(1, true);

    public AvlTreeTest() {}

    public AvlTree<Integer, StateLong> getTree() {
      return tree;
    }
  }

  @Immutable
  private static final class SerializableTestClass implements StateSerializable {

    private final StateString stringState;

    public SerializableTestClass(StateString stringState) {
      this.stringState = stringState;
    }

    public StateString getStringState() {
      return stringState;
    }
  }

  @Immutable
  @SuppressWarnings("unused")
  private static final class BasicTypes implements StateSerializable {

    private final StateBoolean stateBoolean = new StateBoolean(false);
    private final StateLong stateLong = new StateLong(3L);
    private final StateString stateString = new StateString("state of string");
    private final BlockchainAddress blockchainAddress =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    private final Hash hash =
        Hash.fromString("9d562d6f7cbb41165407c22526b8cd859621f4b05226e9deed28b96f71f6fdcb");

    private final BlockchainPublicKey ecPoint = new KeyPair(BigInteger.ONE).getPublic();

    private final Signature signature = new Signature(2, BigInteger.ONE, BigInteger.TWO);
    private final BlsPublicKey blsPublicKey = new BlsKeyPair(BigInteger.valueOf(42)).getPublicKey();
    private final BlsSignature blsSignature =
        new BlsKeyPair(BigInteger.valueOf(42))
            .sign(Hash.create(stream -> stream.writeString("hey hey, I'm a message")));

    public BasicTypes() {}
  }

  @Immutable
  private static final class BasicSimpleTypes implements StateSerializable {

    private final int middle;
    private final long big;
    private final boolean yes;
    private final String text;
    private final byte byt;

    public BasicSimpleTypes(int middle, long big, boolean yes, String text, byte byt) {
      this.middle = middle;
      this.big = big;
      this.yes = yes;
      this.text = text;
      this.byt = byt;
    }

    public int getMiddle() {
      return middle;
    }

    public long getBig() {
      return big;
    }

    public boolean isYes() {
      return yes;
    }

    public String getText() {
      return text;
    }

    public byte getByt() {
      return byt;
    }
  }
}
