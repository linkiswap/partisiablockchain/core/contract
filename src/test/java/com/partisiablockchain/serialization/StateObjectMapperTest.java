package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class StateObjectMapperTest {

  private static final ObjectMapper mapper = StateObjectMapper.createObjectMapper();

  @Test
  public void serializeStateLong() throws JsonProcessingException {
    WithLongs value = new WithLongs(13L, 14);
    JsonNode s = mapper.valueToTree(value);
    Assertions.assertThat(s.has("nullable")).isTrue();
    Assertions.assertThat(s.get("nullable").isTextual()).isTrue();
    Assertions.assertThat(s.has("nonNull")).isTrue();
    Assertions.assertThat(s.get("nonNull").isTextual()).isTrue();

    WithLongs withLongs = mapper.treeToValue(s, WithLongs.class);
    Assertions.assertThat(withLongs.nullable).isEqualTo(value.nullable);
    Assertions.assertThat(withLongs.nonNull).isEqualTo(value.nonNull);
  }

  private static final class WithLongs {

    final Long nullable;
    final long nonNull;

    @SuppressWarnings("unused")
    private WithLongs() {
      nullable = null;
      nonNull = 0;
    }

    private WithLongs(Long nullable, long nonNull) {
      this.nullable = nullable;
      this.nonNull = nonNull;
    }
  }
}
