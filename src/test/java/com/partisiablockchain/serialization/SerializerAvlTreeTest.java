package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Hash.HashAndSize;
import com.partisiablockchain.tree.AvlTree;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class SerializerAvlTreeTest {

  private static final boolean supportDeserializeRecords =
      false; // Not needed for this functionality

  private AvlTree<Integer, StateLong> tree =
      AvlTree.create(
          Map.of(
              1, new StateLong(2),
              2, new StateLong(4)));
  private AvlTree<Integer, LargeByteArray> byteTree = AvlTree.create();
  private final MemoryStorage storage;
  private final SerializationContext context;
  private final Type byteTreeType;
  private final Type treeType;

  /** Setup default storage. */
  public SerializerAvlTreeTest() throws NoSuchFieldException {
    storage = new MemoryStorage();
    context = new SerializationContext(storage, supportDeserializeRecords);
    treeType = getClass().getDeclaredField("tree").getGenericType();
    byteTreeType = getClass().getDeclaredField("byteTree").getGenericType();
  }

  @Test
  public void hash() {
    SerializerAvlTree serializer = new SerializerAvlTree(context, storage, tree, treeType);

    HashAndSize hash = serializer.hash();
    Assertions.assertThat(hash.hash().toString())
        .isEqualTo("aafdb791d31444ea8da1f6ba2c88e0ad2403108bf885cec427e262bc80ac8530");
    Assertions.assertThat(hash.size()).isEqualTo(100);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void largeTreeShouldRootLazy() {
    tree =
        AvlTree.create(
            IntStream.range(0, 100)
                .boxed()
                .collect(Collectors.toMap(Function.identity(), StateLong::new)));
    SerializerAvlTree writer = new SerializerAvlTree(context, storage, tree, treeType);
    writer.write();

    Hash hash = writer.hash().hash();
    SerializerAvlTree reader = new SerializerAvlTree(context, storage, hash, treeType);

    AvlTree<Integer, StateLong> read = (AvlTree<Integer, StateLong>) reader.read();
    Assertions.assertThat(read).isNotNull();
    Assertions.assertThat(storage.getObjectsRead()).isEqualTo(1);
    Assertions.assertThat(read.getValue(0).value()).isEqualTo(0);
    Assertions.assertThat(storage.getObjectsRead()).isEqualTo(29);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void leafShouldBeReadLazy() {
    byteTree =
        AvlTree.create(
            IntStream.range(0, 100)
                .boxed()
                .collect(
                    Collectors.toMap(Function.identity(), i -> new LargeByteArray(new byte[0]))));
    SerializerAvlTree writer = new SerializerAvlTree(context, storage, byteTree, byteTreeType);
    writer.write();

    Hash hash = writer.hash().hash();
    SerializerAvlTree reader = new SerializerAvlTree(context, storage, hash, byteTreeType);

    AvlTree<Integer, LargeByteArray> read = (AvlTree<Integer, LargeByteArray>) reader.read();
    Assertions.assertThat(read).isNotNull();
    Assertions.assertThat(storage.getObjectsRead()).isEqualTo(1);
    Assertions.assertThat(read.size()).isEqualTo(100);
    Assertions.assertThat(storage.getObjectsRead()).isEqualTo(1);
    Assertions.assertThat(read.getValue(99)).isNotNull();
    Assertions.assertThat(storage.getObjectsRead()).isEqualTo(34);
    Assertions.assertThat(read.getValue(99)).isNotNull();
    Assertions.assertThat(storage.getObjectsRead()).isEqualTo(35);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void largeTreeOnlyWritesOnce() {
    tree =
        AvlTree.create(
            IntStream.range(0, 100)
                .boxed()
                .collect(Collectors.toMap(Function.identity(), StateLong::new)));
    SerializerAvlTree writer = new SerializerAvlTree(context, storage, tree, treeType);
    writer.write();
    int objectsWritten = storage.getObjectsWritten();
    checkWriteOnce(objectsWritten);

    Hash hash = writer.hash().hash();
    SerializerAvlTree reader = new SerializerAvlTree(context, storage, hash, treeType);

    AvlTree<Integer, StateLong> read = (AvlTree<Integer, StateLong>) reader.read();
    checkWriteOnce(objectsWritten);

    writer = new SerializerAvlTree(context, storage, read, treeType);
    writer.write();
    checkWriteOnce(objectsWritten);

    Assertions.assertThat(read.getValue(0).value()).isEqualTo(0);
    checkWriteOnce(objectsWritten);
  }

  @Test
  public void shouldNotWriteSubWhenUnchanged() {
    tree = AvlTree.create(Map.of(1, new StateLong(1), 2, new StateLong(2)));
    SerializerAvlTree writer = new SerializerAvlTree(context, storage, tree, treeType);
    writer.write();
    Assertions.assertThat(storage.getObjectsWritten()).isEqualTo(3);

    // Only keep the composite in storage
    Hash hash = writer.hash().hash();
    storage.removeExcept(hash);

    writer.write();
    Assertions.assertThat(storage.getObjectsWritten()).isEqualTo(3);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void writeReadLeaf() {
    tree = AvlTree.create(Map.of(1, new StateLong(1)));
    SerializerAvlTree writer = new SerializerAvlTree(context, storage, tree, treeType);
    writer.write();
    Assertions.assertThat(storage.getObjectsWritten()).isEqualTo(1);

    Hash hash = writer.hash().hash();
    SerializerAvlTree reader = new SerializerAvlTree(context, storage, hash, treeType);

    AvlTree<Integer, StateLong> read = (AvlTree<Integer, StateLong>) reader.read();
    storage.clear();

    writer = new SerializerAvlTree(context, storage, read, treeType);
    writer.write();
    Assertions.assertThat(storage.getObjectsWritten()).isEqualTo(1);
  }

  private void checkWriteOnce(int objectsWritten) {
    Assertions.assertThat(storage.getObjectsWritten()).isEqualTo(objectsWritten);
  }
}
