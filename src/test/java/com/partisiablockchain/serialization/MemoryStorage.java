package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/** Test. */
public final class MemoryStorage implements StateStorage {

  private int objectsWritten = 0;
  private int objectsRead = 0;
  private final Map<Hash, byte[]> serialized = new HashMap<>();

  @Override
  public boolean write(Hash hash, Consumer<SafeDataOutputStream> writer) {
    if (serialized.containsKey(hash)) {
      return false;
    }
    objectsWritten = objectsWritten + 1;
    serialized.put(hash, SafeDataOutputStream.serialize(writer));
    return true;
  }

  @Override
  public <S> S read(Hash hash, Function<SafeDataInputStream, S> reader) {
    if (serialized.containsKey(hash)) {
      objectsRead++;
      SafeDataInputStream bytes = SafeDataInputStream.createFromBytes(serialized.get(hash));
      return reader.apply(bytes);
    } else {
      return null;
    }
  }

  int getObjectsWritten() {
    return objectsWritten;
  }

  public int getObjectsRead() {
    return objectsRead;
  }

  public Map<Hash, byte[]> getSerialized() {
    return serialized;
  }

  void removeExcept(Hash hash) {
    serialized.keySet().removeIf(Predicate.not(hash::equals));
  }

  void clear() {
    serialized.clear();
  }
}
