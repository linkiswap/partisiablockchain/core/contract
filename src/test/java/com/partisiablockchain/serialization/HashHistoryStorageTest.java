package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataOutputStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class HashHistoryStorageTest {
  @Test
  public void writeSameTwiceReturnsFalse() {
    StateStorage storage = new HashHistoryStorage(new MemoryStorage());
    Hash hash = Hash.create(this::testData);
    Assertions.assertThat(storage.write(hash, this::testData)).isTrue();
    Assertions.assertThat(storage.write(hash, this::testData)).isFalse();
  }

  private void testData(SafeDataOutputStream stream) {
    stream.writeString("Testdata");
  }
}
