package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class AvlProofTest {

  private final Hash key = Hash.create(s -> s.writeInt(4756));

  @Test
  public void verifyKey() {
    byte[] leaf = leaf(key);
    Hash someHash = Hash.create(s -> s.writeInt(0));
    byte[] composite = composite(toHash(leaf), someHash);
    AvlProof avlProof = AvlProof.create(List.of(composite), leaf);
    Assertions.assertThat(avlProof.getKey()).isEqualTo(key);
    Assertions.assertThat(avlProof.getRootIdentifier()).isEqualTo(toHash(composite));

    composite = composite(someHash, toHash(leaf));
    avlProof = AvlProof.create(List.of(composite), leaf);
    Assertions.assertThat(avlProof.getKey()).isEqualTo(key);
    Assertions.assertThat(avlProof.getRootIdentifier()).isEqualTo(toHash(composite));

    Assertions.assertThatThrownBy(
            () -> AvlProof.create(List.of(composite(someHash, someHash)), leaf))
        .hasMessageContaining("Child node was not present in supplied parent");
  }

  @Test
  public void illegalLeafHeader() {
    byte[] leaf = composite(key, key);
    Assertions.assertThatThrownBy(() -> AvlProof.create(List.of(), leaf))
        .hasMessageContaining("Wrong header for leaf. Expected 0 got 1");
  }

  @Test
  public void illegalCompositeHeader() {
    byte[] leaf = leaf(key);
    byte[] composite = composite(toHash(leaf), key);
    composite[0] = 0;

    Assertions.assertThatThrownBy(() -> AvlProof.create(List.of(composite), leaf))
        .hasMessageContaining("Wrong header for composite. Expected 1 got 0");
  }

  private Hash toHash(byte[] composite) {
    return Hash.create(s -> s.write(composite));
  }

  private byte[] composite(Hash left, Hash right) {
    return SafeDataOutputStream.serialize(
        s -> {
          s.writeByte(1);
          left.write(s);
          right.write(s);
        });
  }

  private byte[] leaf(Hash key) {
    return SafeDataOutputStream.serialize(
        s -> {
          s.writeByte(0);
          key.write(s);
        });
  }

  @Test
  public void writeAndRead() {
    byte[] leaf = leaf(Hash.create(s -> s.writeInt(1)));
    AvlProof proof = AvlProof.create(List.of(composite(toHash(leaf), Hash.create(key))), leaf);

    AvlProof readProof =
        SafeDataInputStream.readFully(SafeDataOutputStream.serialize(proof), AvlProof::read);
    Assertions.assertThat(readProof).usingRecursiveComparison().isEqualTo(proof);
  }
}
