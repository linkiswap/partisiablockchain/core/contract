package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Curve;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.assertj.core.api.Assertions;
import org.bouncycastle.math.ec.ECPoint;
import org.junit.jupiter.api.Test;

final class BlockchainPublicKeyTest {

  @Test
  void readwrite() {
    BlockchainPublicKey blockchainPublicKey = new KeyPair(BigInteger.ONE).getPublic();
    Assertions.assertThat(blockchainPublicKey).isNotNull();

    byte[] bytes = SafeDataOutputStream.serialize(blockchainPublicKey::write);
    BlockchainPublicKey blockchainPublicKey1 =
        BlockchainPublicKey.read(SafeDataInputStream.createFromBytes(bytes));

    Assertions.assertThat(blockchainPublicKey1).isEqualTo(blockchainPublicKey);
    Assertions.assertThat(blockchainPublicKey1.getEcPointBytes())
        .isEqualTo(blockchainPublicKey.getEcPointBytes());
  }

  @Test
  void deriveSharedKey() {
    KeyPair k0 = new KeyPair(BigInteger.valueOf(42));
    KeyPair k1 = new KeyPair(BigInteger.valueOf(11));
    KeyPair k2 = new KeyPair(BigInteger.valueOf(22));
    BlockchainPublicKey blockchainPublicKey1 = k1.getPublic();
    BlockchainPublicKey blockchainPublicKey2 = k2.getPublic();
    Hash shared01 = blockchainPublicKey1.deriveSharedKey(k0.getPrivateKey());
    Hash shared12 = blockchainPublicKey2.deriveSharedKey(k1.getPrivateKey());
    Assertions.assertThat(shared01).isNotEqualTo(shared12);

    byte[] extraData = {1, 2};
    Hash shared01Extra = blockchainPublicKey1.deriveSharedKey(k0.getPrivateKey(), extraData);
    Hash shared12Extra = blockchainPublicKey2.deriveSharedKey(k1.getPrivateKey(), extraData);
    Assertions.assertThat(shared01Extra).isNotEqualTo(shared12Extra);
  }

  @Test
  void testDeriveSharedKey_ExtraData() {
    KeyPair k0 = new KeyPair(BigInteger.valueOf(42));
    KeyPair k1 = new KeyPair(BigInteger.valueOf(22));
    BlockchainPublicKey blockchainPublicKey0 = k0.getPublic();
    BlockchainPublicKey blockchainPublicKey1 = k1.getPublic();
    byte[] extraData = {1, 2};
    Hash shared0 = blockchainPublicKey1.deriveSharedKey(k0.getPrivateKey(), extraData);
    Hash shared1 = blockchainPublicKey0.deriveSharedKey(k1.getPrivateKey(), extraData);
    Assertions.assertThat(shared0).isEqualTo(shared1);

    Hash sharedExtra0 = blockchainPublicKey1.deriveSharedKey(k0.getPrivateKey(), extraData);
    Hash sharedExtra1 = blockchainPublicKey0.deriveSharedKey(k1.getPrivateKey(), new byte[] {2, 1});
    Assertions.assertThat(sharedExtra0).isNotEqualTo(sharedExtra1);
  }

  @Test
  public void equals() {
    byte[] ecPointRed = new KeyPair(BigInteger.ONE).getPublic().getEcPointBytes();
    byte[] ecPointBlue = new KeyPair(BigInteger.TWO).getPublic().getEcPointBytes();
    EqualsVerifier.simple()
        .forClass(BlockchainPublicKey.class)
        .withNonnullFields("ecPoint")
        .withPrefabValues(
            ECPoint.class,
            Curve.CURVE.getCurve().decodePoint(ecPointRed),
            Curve.CURVE.getCurve().decodePoint(ecPointBlue))
        .verify();
  }
}
