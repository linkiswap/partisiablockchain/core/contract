package com.partisiablockchain.util;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeListStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

/** Utility class for reading from a stream with a limit on what is read. */
public final class DataStreamLimit {

  /** The maximum number of bytes allowed. */
  public static final int MAX_BYTES_LENGTH = 10_000_000;

  /** The maximum size of a list. */
  public static final int MAX_LIST_SIZE = 1000;

  private DataStreamLimit() {}

  /**
   * Read a limited RPC from the stream. Maximum allowed bytes is {@link
   * DataStreamLimit#MAX_BYTES_LENGTH}.
   *
   * @param stream to read from
   * @return the read bytes
   */
  public static byte[] readDynamicBytes(SafeDataInputStream stream) {
    int size = stream.readInt();
    if (size > DataStreamLimit.MAX_BYTES_LENGTH) {
      throw new IllegalArgumentException(
          "Unable to read "
              + size
              + " bytes from stream. Maximum allowed is "
              + DataStreamLimit.MAX_BYTES_LENGTH);
    }
    return stream.readBytes(size);
  }

  /**
   * Read a limited string from the stream. Maximum allowed bytes is {@link
   * DataStreamLimit#MAX_BYTES_LENGTH}.
   *
   * @param stream to read from
   * @return the read string
   */
  public static String readString(SafeDataInputStream stream) {
    byte[] bytes = readDynamicBytes(stream);
    return new String(bytes, StandardCharsets.UTF_8);
  }

  /**
   * Read a limited list from the stream. Maximum allowed size is {@link
   * DataStreamLimit#MAX_LIST_SIZE}.
   *
   * @param stream to read from
   * @param listSerializer serializer for the list
   * @param <E> type of the elements in the list
   * @return the read list
   */
  public static <E> List<E> readDynamicList(
      SafeDataInputStream stream, SafeListStream<E> listSerializer) {
    int size = stream.readInt();
    if (size > MAX_LIST_SIZE) {
      throw new IllegalArgumentException(
          "Unable to read list of size "
              + size
              + " from stream. Maximum allowed is "
              + MAX_LIST_SIZE);
    }
    return listSerializer.readFixed(stream, size);
  }
}
