package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.CheckReturnValue;
import com.partisiablockchain.BlockchainAddress;
import com.secata.stream.DataStreamSerializable;
import java.util.Objects;
import java.util.function.Consumer;

/** Builder for an interaction to a contract. */
public final class InteractionBuilderImpl implements InteractionBuilder {

  /** Target contract address. */
  private final BlockchainAddress contract;
  /** Rpc for the contract. */
  private final DataStreamSerializable rpc;

  private final Consumer<ContractEvent> eventConsumer;
  private final Long cost;
  /** True if the cost should be paid by the current contract. */
  private final boolean costFromContract;

  /**
   * Creates a new builder for an interaction to a contract.
   *
   * @param contractEvents container for the resulting event.
   * @param contract the target contract destination
   * @param rpc the rpc payload
   * @param cost transaction cose
   * @param costFromContract true if cost is deducted from contract
   */
  public InteractionBuilderImpl(
      Consumer<ContractEvent> contractEvents,
      BlockchainAddress contract,
      DataStreamSerializable rpc,
      Long cost,
      boolean costFromContract) {
    this.eventConsumer = contractEvents;
    this.contract = contract;
    this.rpc = rpc;
    this.cost = cost;
    this.costFromContract = costFromContract;
  }

  @Override
  @CheckReturnValue
  public InteractionBuilder withPayload(DataStreamSerializable contractRpc) {
    return new InteractionBuilderImpl(eventConsumer, contract, contractRpc, cost, costFromContract);
  }

  @Override
  @CheckReturnValue
  public InteractionBuilder allocateCost(long cost) {
    ensurePositive(cost);
    return new InteractionBuilderImpl(eventConsumer, contract, rpc, cost, false);
  }

  @Override
  @CheckReturnValue
  public InteractionBuilder allocateCostFromContract(long cost) {
    ensurePositive(cost);
    return new InteractionBuilderImpl(eventConsumer, contract, rpc, cost, true);
  }

  @Override
  @CheckReturnValue
  public InteractionBuilder allocateRemainingCost() {
    return new InteractionBuilderImpl(eventConsumer, contract, rpc, null, false);
  }

  @Override
  public void sendFromContract() {
    Objects.requireNonNull(rpc, "Payload must be supplied using withPayload");
    eventConsumer.accept(
        new ContractEventInteraction(contract, rpc, cost, false, costFromContract));
  }

  @Override
  public void send() {
    Objects.requireNonNull(rpc, "Payload must be supplied using withPayload");
    eventConsumer.accept(new ContractEventInteraction(contract, rpc, cost, true, costFromContract));
  }

  private static void ensurePositive(long value) {
    if (value < 0) {
      throw new IllegalArgumentException("Cost must be 0 or larger: " + value);
    }
  }
}
