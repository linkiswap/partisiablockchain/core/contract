package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.CheckReturnValue;
import com.secata.stream.DataStreamSerializable;

/** Builder used to create a contract interaction. */
public interface InteractionBuilder {

  /**
   * Sets the rpc payload for the event, must be non-null.
   *
   * @param contractRpc the rpc payload
   * @return the builder
   */
  @CheckReturnValue
  InteractionBuilder withPayload(DataStreamSerializable contractRpc);

  /**
   * Sets the cost that is allocated for the event.
   *
   * @param cost the cost of the event
   * @return the builder
   */
  @CheckReturnValue
  InteractionBuilder allocateCost(long cost);

  /**
   * The current transaction should pay the cost for the interaction.
   *
   * @param cost The cost of the event
   * @return the builder
   */
  @CheckReturnValue
  InteractionBuilder allocateCostFromContract(long cost);

  /**
   * Sets the cost that is allocated for the event to be the remaining gas.
   *
   * @return the builder
   */
  @CheckReturnValue
  InteractionBuilder allocateRemainingCost();

  /** Sends the build interaction as the current contract. */
  void sendFromContract();

  /**
   * Sends the build interaction as an extension of the existing event, that is, it is the same
   * sender.
   */
  void send();
}
