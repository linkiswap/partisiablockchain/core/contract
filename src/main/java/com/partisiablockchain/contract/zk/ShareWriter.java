package com.partisiablockchain.contract.zk;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.Arrays;
import java.util.BitSet;

/**
 * Utility class creates a byte array representation of a list of values. E.g. two integers followed
 * by five booleans and then a long is packed efficiently into a byte array.
 *
 * @see ShareReader
 */
public final class ShareWriter {

  private final BitSet bitSet;
  private int index;

  private ShareWriter() {
    this.bitSet = new BitSet();
  }

  /**
   * Create share writer.
   *
   * @return share writer
   */
  public static ShareWriter create() {
    return new ShareWriter();
  }

  /**
   * Writes a long of the supplied bit length from the open value.
   *
   * @param value the value to write
   * @param bitLength the bit length of the long to read
   */
  public void write(long value, int bitLength) {
    if (bitLength < 1 || bitLength > 64) {
      throw new IllegalArgumentException("Illegal bit length for long. Must be between 1 and 64.");
    }
    for (int i = 0; i < bitLength; i++) {
      long bitMask = 1L << i;
      write((value & bitMask) != 0);
    }
  }

  /**
   * Writes a boolean from the open value.
   *
   * @param value the value to write
   */
  public void write(boolean value) {
    this.bitSet.set(index, value);
    this.index++;
  }

  /**
   * Gets the created byte array from the open value.
   *
   * @return the written bytes
   */
  public byte[] toBytes() {
    byte[] value = bitSet.toByteArray();
    int bytesNeeded = ZkClosed.getByteLength(index);
    byte[] bytes = Arrays.copyOfRange(value, 0, bytesNeeded);
    return bytes;
  }
}
