package com.partisiablockchain.contract.zk;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Signature;

/** Represents an attestation on a piece of data. */
public interface DataAttestation {

  /**
   * Get the identifier of this attestation.
   *
   * @return the attestation ID.
   */
  int getId();

  /**
   * Get the attestation on this data supplied by a particular node. An attestation by a node is a
   * signature on the data being attested.
   *
   * @param nodeIndex the index of the node.
   * @return a signature, or null if the node has not provided an attestation yet.
   */
  Signature getSignature(int nodeIndex);

  /**
   * Get the data that this attestation is concerning.
   *
   * @return a list of bytes.
   */
  byte[] getData();
}
