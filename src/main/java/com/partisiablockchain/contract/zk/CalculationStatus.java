package com.partisiablockchain.contract.zk;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * The current status of the ZK computation. Life cycle diagram can be found here:
 * https://drive.google.com/file/d/14vxJCf32Vs4-36mXfu3Nn4Z5uSIUvhOu/view?usp=sharing
 */
public enum CalculationStatus {
  /**
   * The initial state where the nodes await the call to {@link ZkState#startComputation start
   * computation}.
   */
  WAITING,
  /**
   * Result of the call to {@link ZkState#startComputation start computation}. When completed, the
   * contract enters the commitment phase.
   *
   * <p>The commitment phase enables fair open, i.e. all nodes agree on the output since their
   * commitments match (in a replicated sharing).
   *
   * <p>After checking the commitments the contract enters the output phase where the nodes send
   * their output.
   *
   * <p>This happens behind the scene so contract developers can assume the state will become either
   * OUTPUT or MALICIOUS_BEHAVIOUR once {@link ZkState#startComputation} has been called.
   */
  CALCULATING,
  /**
   * The computing nodes have committed to the computation output on chain, and this phase is where
   * opening of variables on chain is allowed. Nodes will signal when done via the smart contract.
   */
  OUTPUT,
  /**
   * If the contract detects some part of the protocol is not done according to specification, then
   * this state is the resulting consequence.
   */
  MALICIOUS_BEHAVIOUR,
  /**
   * The state entered after the call to {@link ZkState#contractDone()}. This contract is done and
   * the nodes can remove any secret shared data.
   */
  DONE
}
