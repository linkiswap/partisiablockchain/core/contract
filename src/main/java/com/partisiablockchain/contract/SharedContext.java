package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;

/** Shared interface for the context of a contract call. */
public interface SharedContext {

  /**
   * Address of this contract.
   *
   * @return Gets the address
   */
  BlockchainAddress getContractAddress();

  /**
   * Gets the current block time.
   *
   * @return the block time.
   */
  long getBlockTime();

  /**
   * Get the production time of the current block.
   *
   * @return the time of production for the current block
   */
  long getBlockProductionTime();

  /**
   * Gets the sender of the current evaluation.
   *
   * @return the sender
   */
  BlockchainAddress getFrom();

  /**
   * Get the hash of the transaction being executed.
   *
   * @return the transaction hash.
   */
  Hash getCurrentTransactionHash();

  /**
   * Get the hash of the original transaction in this execution stack - useful as a client
   * identifier.
   *
   * @return the transaction hash.
   */
  Hash getOriginalTransactionHash();
}
