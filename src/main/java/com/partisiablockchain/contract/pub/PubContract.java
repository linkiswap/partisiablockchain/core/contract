package com.partisiablockchain.contract.pub;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.Contract;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.SafeDataInputStream;

/**
 * A classic smart contract with create, interact and destroy interactions. Implementors extends
 * this class and overwrite relevant methods.
 *
 * @param <OpenT> the type for the internal state for the smart contract.
 */
public abstract class PubContract<OpenT extends StateSerializable> implements Contract {

  /**
   * Called whenever this contract is deployed, overwrite to change behaviour. The current user is
   * the owner of the contract.
   *
   * @param context context for the current execution
   * @param rpc the invocation data
   * @return state for the contract
   */
  public OpenT onCreate(PubContractContext context, SafeDataInputStream rpc) {
    return null;
  }

  /**
   * Any invocation to this contract will be handled here, overwrite to change behaviour.
   *
   * @param context context for the current invocation
   * @param state state for the contract
   * @param rpc the invocation data
   * @return updated state for the contract
   */
  public OpenT onInvoke(PubContractContext context, OpenT state, SafeDataInputStream rpc) {
    // NOOP
    return state;
  }

  /**
   * Callback registered in a previous event created in an invocation will be handled here.
   *
   * @param context context for the current invocation
   * @param state state for the contract
   * @param callbackContext the spawned event transactions and their execution result
   * @param rpc the invocation data registered with the callback
   * @return updated state for the contract
   */
  public OpenT onCallback(
      PubContractContext context,
      OpenT state,
      CallbackContext callbackContext,
      SafeDataInputStream rpc) {
    // NOOP
    return state;
  }
}
