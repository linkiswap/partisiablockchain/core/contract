package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataOutputStream;

/**
 * Default implementation of {@link EventManager} used to build interactions and callbacks for
 * events.
 */
public final class EventManagerImpl implements EventManager {

  private final ContractEventGroup contractEventGroup;

  /**
   * Creates a new EventCreator which manages the event send to other contracts during execution of
   * interactions.
   *
   * @param contractEventGroup the group to collect the events in.
   */
  public EventManagerImpl(ContractEventGroup contractEventGroup) {
    this.contractEventGroup = contractEventGroup;
  }

  @Override
  public InteractionBuilder invoke(BlockchainAddress address) {
    return new InteractionBuilderImpl(contractEventGroup::add, address, null, null, false);
  }

  @Override
  public void registerCallback(DataStreamSerializable rpc, long allocatedCost) {
    ensurePositive(allocatedCost);
    contractEventGroup.callbackCost = allocatedCost;
    contractEventGroup.callbackRpc = SafeDataOutputStream.serialize(rpc);
    contractEventGroup.callbackCostFromContract = false;
  }

  @Override
  public void registerCallbackWithCostFromRemaining(DataStreamSerializable rpc) {
    contractEventGroup.callbackCost = null;
    contractEventGroup.callbackRpc = SafeDataOutputStream.serialize(rpc);
    contractEventGroup.callbackCostFromContract = false;
  }

  @Override
  public void registerCallbackWithCostFromContract(DataStreamSerializable rpc, long allocatedCost) {
    ensurePositive(allocatedCost);
    contractEventGroup.callbackCost = allocatedCost;
    contractEventGroup.callbackRpc = SafeDataOutputStream.serialize(rpc);
    contractEventGroup.callbackCostFromContract = true;
  }

  private static void ensurePositive(long value) {
    if (value < 0) {
      throw new IllegalArgumentException("Cost must be 0 or larger: " + value);
    }
  }
}
