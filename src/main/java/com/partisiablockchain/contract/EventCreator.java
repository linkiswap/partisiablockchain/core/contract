package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.secata.stream.DataStreamSerializable;

/** EventManager used to create event transactions. */
public interface EventCreator {

  /** Empty invocation data stream. */
  DataStreamSerializable EMPTY_RPC = safeDataOutputStream -> {};

  /**
   * Creates a new InteractionBuilder which is invoking to the specified contract.
   *
   * @param contract the contract to invoke
   * @return the interaction builder
   */
  InteractionBuilder invoke(BlockchainAddress contract);
}
