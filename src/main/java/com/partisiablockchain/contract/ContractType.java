package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;

/** Different contract types. */
public enum ContractType {
  /** System contracts are the public part of the operating system on the blockchain. */
  SYSTEM,
  /** Public contracts are regular smart contract on the blockchain. */
  PUBLIC,
  /**
   * Zero knowledge contracts are the more complicated contracts that are similar to public
   * contracts, but have confidential state as well.
   */
  ZERO_KNOWLEDGE,
  /**
   * Governance contracts are a special type of system contract that is deemed necessary for the
   * system to run and therefore are (mostly) free to interact with.
   */
  GOVERNANCE;

  /**
   * Get the type of contract for a blockchain address.
   *
   * @param address the address to get contract type of
   * @return the determined contract type
   * @throws IllegalArgumentException if the address is not a contract address
   */
  public static ContractType fromAddress(BlockchainAddress address) {
    if (address.getType() == BlockchainAddress.Type.CONTRACT_SYSTEM) {
      return ContractType.SYSTEM;
    } else if (address.getType() == BlockchainAddress.Type.CONTRACT_PUBLIC) {
      return ContractType.PUBLIC;
    } else if (address.getType() == BlockchainAddress.Type.CONTRACT_ZK) {
      return ContractType.ZERO_KNOWLEDGE;
    } else if (address.getType() == BlockchainAddress.Type.CONTRACT_GOV) {
      return ContractType.GOVERNANCE;
    } else {
      throw new IllegalArgumentException("Address does not correspond to a contract " + address);
    }
  }
}
