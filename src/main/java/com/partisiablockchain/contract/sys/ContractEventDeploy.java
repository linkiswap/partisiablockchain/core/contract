package com.partisiablockchain.contract.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.ContractEvent;

/** A single complete event launched from the event manager. */
public final class ContractEventDeploy implements ContractEvent {

  /** Deploy interaction. */
  public final Deploy deploy;
  /** True if the sender is the same as the transaction. */
  public final boolean originalSender;
  /** True if the cost should come from this contract. */
  public final boolean costFromContract;
  /** The allocated cost. */
  public Long allocatedCost;

  /**
   * Creates a new contract event.
   *
   * @param deploy deploy interaction
   * @param allocatedCost the allocated cost, can be null
   * @param originalSender true if send as the original transaction sender
   * @param costFromContract true if contract will pay of own pocket for event
   */
  public ContractEventDeploy(
      Deploy deploy, Long allocatedCost, boolean originalSender, boolean costFromContract) {
    this.deploy = deploy;
    this.originalSender = originalSender;
    this.allocatedCost = allocatedCost;
    this.costFromContract = costFromContract;
  }
}
