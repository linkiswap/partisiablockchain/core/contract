package com.partisiablockchain.contract.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;

/** The definition of the system events. */
public interface SystemEvents {

  /**
   * Creates an account on the blockchain.
   *
   * @param address the address to create
   */
  void createAccount(BlockchainAddress address);

  /**
   * Adds a shard to the list of active shards.
   *
   * @param shardId id of the shard
   */
  void createShard(String shardId);

  /**
   * Removes a shard from the list of active shards.
   *
   * @param shardId id of the shard
   */
  void removeShard(String shardId);

  /**
   * Update the local state of the plugin responsible for handling accounts.
   *
   * @param update the update to execute
   */
  void updateLocalAccountPluginState(LocalPluginStateUpdate update);

  /**
   * Update the context free state of the account plugin on a named shard.
   *
   * @param shardId the shard on which to update the plugin
   * @param rpc rpc for local state update
   */
  void updateContextFreeAccountPluginState(String shardId, byte[] rpc);

  /**
   * Update the global state of the plugin responsible for handling accounts.
   *
   * @param update the update to execute
   */
  void updateGlobalAccountPluginState(GlobalPluginStateUpdate update);

  /**
   * Update the account plugin to the code in the supplied jar using the supplied migration rpc.
   *
   * @param pluginJar jar containing the actual plugin
   * @param rpc rpc for global state migration
   */
  void updateAccountPlugin(byte[] pluginJar, byte[] rpc);

  /**
   * Update the local state of the plugin responsible for handling consensus.
   *
   * @param update the update to execute
   */
  void updateLocalConsensusPluginState(LocalPluginStateUpdate update);

  /**
   * Update the global state of the plugin responsible for handling consensus.
   *
   * @param update the update to execute
   */
  void updateGlobalConsensusPluginState(GlobalPluginStateUpdate update);

  /**
   * Update the consensus plugin to the code in the supplied jar using the supplied migration rpc.
   *
   * @param pluginJar jar containing the actual plugin
   * @param rpc rpc for global state migration
   */
  void updateConsensusPlugin(byte[] pluginJar, byte[] rpc);

  /**
   * Update the routing plugin to the code in the supplied jar using the supplied migration rpc.
   *
   * @param pluginJar jar containing the actual plugin
   * @param rpc rpc for global state migration
   */
  void updateRoutingPlugin(byte[] pluginJar, byte[] rpc);

  /**
   * Forcibly remove an existing contract.
   *
   * @param contract the contract to remove
   */
  void removeContract(BlockchainAddress contract);

  /**
   * Identify if a contract/account exists on the blockchain.
   *
   * @param address the address of the contract or account
   */
  void exists(BlockchainAddress address);

  /**
   * Set a feature flag in the blockchain.
   *
   * @param key the key of the feature
   * @param value the value of the feature
   */
  void setFeature(String key, String value);

  /**
   * Upgrade a system contract.
   *
   * @param contractJar the contract.
   * @param binderJar the binder.
   * @param rpc the invocation.
   * @param contractAddress the address of the contract.
   */
  void upgradeSystemContract(
      byte[] contractJar, byte[] binderJar, byte[] rpc, BlockchainAddress contractAddress);

  /**
   * Upgrade a system contract.
   *
   * @param contractJar the contract
   * @param binderJar the binder
   * @param abi the abi
   * @param rpc the invocation
   * @param contractAddress the address of the contract
   */
  void upgradeSystemContract(
      byte[] contractJar,
      byte[] binderJar,
      byte[] abi,
      byte[] rpc,
      BlockchainAddress contractAddress);
}
