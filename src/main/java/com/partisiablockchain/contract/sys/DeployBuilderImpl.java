package com.partisiablockchain.contract.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.ContractEvent;
import com.secata.stream.DataStreamSerializable;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * Default implementation of {@link DeployBuilder} used to configure binder, contract and
 * initialization rpc.
 */
public final class DeployBuilderImpl implements DeployBuilder {

  private final Consumer<ContractEvent> eventGroup;
  private final Long cost;
  private final Deploy deploy;
  private final boolean costFromContract;

  private DeployBuilderImpl(
      Consumer<ContractEvent> eventGroup, Long cost, boolean costFromContract, Deploy deploy) {
    this.eventGroup = eventGroup;
    this.cost = cost;
    this.costFromContract = costFromContract;
    this.deploy = deploy;
  }

  /**
   * Creates a new DeployBuilderImpl.
   *
   * @param contractEvents the event group that stores the event
   * @param contract the contract address
   */
  public DeployBuilderImpl(Consumer<ContractEvent> contractEvents, BlockchainAddress contract) {
    this(
        contractEvents,
        null,
        false,
        new Deploy(
            Objects.requireNonNull(contract, "Cannot deploy to a null contract"),
            null,
            null,
            null,
            null));
  }

  @Override
  public DeployBuilder withBinderJar(byte[] binderJar) {
    return new DeployBuilderImpl(
        eventGroup,
        cost,
        costFromContract,
        new Deploy(deploy.contract, binderJar, deploy.contractJar, deploy.abi, deploy.rpc));
  }

  @Override
  public DeployBuilder withContractJar(byte[] contractJar) {
    return new DeployBuilderImpl(
        eventGroup,
        cost,
        costFromContract,
        new Deploy(deploy.contract, deploy.binderJar, contractJar, deploy.abi, deploy.rpc));
  }

  @Override
  public DeployBuilder withAbi(byte[] abi) {
    return new DeployBuilderImpl(
        eventGroup,
        cost,
        costFromContract,
        new Deploy(deploy.contract, deploy.binderJar, deploy.contractJar, abi, deploy.rpc));
  }

  @Override
  public DeployBuilder withPayload(DataStreamSerializable rpc) {
    return new DeployBuilderImpl(
        eventGroup,
        cost,
        costFromContract,
        new Deploy(deploy.contract, deploy.binderJar, deploy.contractJar, deploy.abi, rpc));
  }

  @Override
  public DeployBuilder allocateCost(long cost) {
    return new DeployBuilderImpl(
        eventGroup,
        cost,
        false,
        new Deploy(deploy.contract, deploy.binderJar, deploy.contractJar, deploy.abi, deploy.rpc));
  }

  @Override
  public DeployBuilder allocateRemainingCost() {
    return new DeployBuilderImpl(
        eventGroup,
        null,
        false,
        new Deploy(deploy.contract, deploy.binderJar, deploy.contractJar, deploy.abi, deploy.rpc));
  }

  @Override
  public DeployBuilder allocateCostFromContract(long cost) {
    return new DeployBuilderImpl(
        eventGroup,
        cost,
        true,
        new Deploy(deploy.contract, deploy.binderJar, deploy.contractJar, deploy.abi, deploy.rpc));
  }

  @Override
  public void sendFromContract() {
    Objects.requireNonNull(deploy.binderJar);
    Objects.requireNonNull(deploy.contractJar);
    eventGroup.accept(new ContractEventDeploy(deploy, cost, false, costFromContract));
  }

  @Override
  public void send() {
    Objects.requireNonNull(deploy.binderJar);
    Objects.requireNonNull(deploy.contractJar);
    eventGroup.accept(new ContractEventDeploy(deploy, cost, true, costFromContract));
  }
}
