package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayList;
import java.util.List;

/** The group used for collecting events. */
public final class ContractEventGroup {

  /** The list of the contract events in this group. */
  public final List<ContractEvent> contractEvents;
  /** True if the callback is paid by the Contract. */
  public boolean callbackCostFromContract;
  /** The allocated cost for the callback. */
  public Long callbackCost;
  /** The rpc for the callback, null means no callback. */
  public byte[] callbackRpc;

  /** Creates a new event group. */
  public ContractEventGroup() {
    contractEvents = new ArrayList<>();
  }

  /**
   * Adds a new event to this group.
   *
   * @param contractEvent the event to add.
   */
  public void add(ContractEvent contractEvent) {
    contractEvents.add(contractEvent);
  }
}
