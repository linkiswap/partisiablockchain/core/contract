package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.DataStreamSerializable;

/**
 * Holds the execution context for each interaction on a smart contract, this points to the
 * surrounding blockchain, current block and transaction being executed.
 */
public interface ContractContext extends SharedContext {

  /**
   * Gets the event creator for this contract evaluation context. Every invoke registered here will
   * be fire-and-forget.
   *
   * @return the event creator
   */
  EventCreator getInvocationCreator();

  /**
   * Gets the event manager for this contract evaluation context. Every invoke registered here will
   * be awaited for the execution of the supplied callback.
   *
   * @return the event creator
   */
  EventManager getRemoteCallsCreator();

  /**
   * Sets the result of a call to the smart contract.
   *
   * @param result the result.
   */
  void setResult(DataStreamSerializable result);
}
