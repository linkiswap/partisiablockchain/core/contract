package com.partisiablockchain.contract.reflect;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a method as the contract upgrade method. The parameters are read automatically from the RPC
 * stream and this method is called.
 *
 * <p>The list of allowed types is the same as for {@link Action} with a couple of modifications:
 *
 * <ul>
 *   <li>{@link com.partisiablockchain.serialization.StateAccessor} must be the first parameter if
 *       present
 *   <li>{@link com.partisiablockchain.contract.sys.SysContractContext} is not allowed
 *   <li>The contract state is not allowed
 * </ul>
 */
@Documented
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.METHOD)
public @interface Upgrade {}
