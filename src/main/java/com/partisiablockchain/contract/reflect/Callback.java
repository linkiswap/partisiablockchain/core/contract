package com.partisiablockchain.contract.reflect;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a method as a contract callback with a shortname. The parameters are read automatically
 * from the RPC stream and this method is called.
 *
 * <p>The annotated method must adhere to the following:
 *
 * <ul>
 *   <li>The method must return the contract state
 *   <li>The method must be non-static and public
 *   <li>The method can't declare a throws clause with checked exceptions
 * </ul>
 *
 * <p>The list of allowed types is the same as for {@link Action} with a single addition:
 *
 * <ul>
 *   <li>{@link com.partisiablockchain.contract.CallbackContext} if present it must be the first
 *       parameter after the state
 * </ul>
 */
@Documented
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.METHOD)
public @interface Callback {

  /**
   * The shortname of the callback. The following restrictions are applied at compile time:
   *
   * <ul>
   *   <li>The shortname value must be within the positive range of a byte
   *   <li>The shortname can only occur once in a callback per contract
   * </ul>
   *
   * <p>Note: The name 'value' is Java naming convention for the default argument of the annotation
   * allowing the user to write <code>
   * &#064;Action(1)</code>.
   *
   * @return the shortname.
   */
  int value();
}
