package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.secata.stream.DataStreamSerializable;

/** A single complete event launched from the event manager. */
public final class ContractEventInteraction implements ContractEvent {

  /** Target contract address of the other contract. */
  public final BlockchainAddress contract;
  /** Rpc for other contract interaction. */
  public final DataStreamSerializable rpc;
  /** True if the sender is the same as the transaction. */
  public final boolean originalSender;
  /** True if the cost should come from this contract. */
  public final boolean costFromContract;
  /** The allocated cost. */
  public Long allocatedCost;

  /**
   * Creates a new contract event.
   *
   * @param contract the target address of the other contract
   * @param rpc the rpc to the other contract
   * @param allocatedCost the allocated cost, can be null
   * @param originalSender true if send as the original transaction sender
   * @param costFromContract true if contract will pay of own pocket for event
   */
  ContractEventInteraction(
      BlockchainAddress contract,
      DataStreamSerializable rpc,
      Long allocatedCost,
      boolean originalSender,
      boolean costFromContract) {
    this.originalSender = originalSender;
    this.rpc = rpc;
    this.contract = contract;
    this.allocatedCost = allocatedCost;
    this.costFromContract = costFromContract;
  }
}
