package com.partisiablockchain.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.immutable.FixedList;

/** Context containing execution results for the sent events. */
public final class CallbackContext {

  private final FixedList<ExecutionResult> results;
  private boolean success;

  private CallbackContext(FixedList<ExecutionResult> results, Boolean reduce) {
    this.results = results;
    this.success = reduce;
  }

  /**
   * Creates a CallbackContext from a supplied list of results.
   *
   * @param results the starting point for this callback context
   * @return the created callback context
   */
  public static CallbackContext create(FixedList<ExecutionResult> results) {
    return new CallbackContext(results, results.stream().allMatch(ExecutionResult::isSucceeded));
  }

  /**
   * Create result of execution.
   *
   * @param eventTransaction hash of event transaction
   * @param success true if execution succeeded, false otherwise
   * @param returnValue stream providing return value
   * @return result of execution
   */
  public static ExecutionResult createResult(
      Hash eventTransaction, boolean success, SafeDataInputStream returnValue) {
    return new ExecutionResult(eventTransaction, success, returnValue);
  }

  /**
   * Check if this has succeeded.
   *
   * @return true if succeeded, false otherwise
   */
  public boolean isSuccess() {
    return success;
  }

  /**
   * Set if succeeded.
   *
   * @param success true if succeeded, false otherwise
   */
  public void setSuccess(boolean success) {
    this.success = success;
  }

  /**
   * Get the results.
   *
   * @return list of execution results
   */
  public FixedList<ExecutionResult> results() {
    return results;
  }

  /** The execution status of a single event. */
  public static final class ExecutionResult {

    private final Hash eventTransaction;
    private final boolean succeeded;
    private final SafeDataInputStream returnValue;

    private ExecutionResult(
        Hash eventTransaction, boolean succeeded, SafeDataInputStream returnValue) {
      this.eventTransaction = eventTransaction;
      this.succeeded = succeeded;
      this.returnValue = returnValue;
    }

    /**
     * Get the event transaction hash.
     *
     * @return event transaction hash
     */
    public Hash eventTransaction() {
      return eventTransaction;
    }

    /**
     * Check if this has succeeded.
     *
     * @return true if succeeded, false otherwise
     */
    public boolean isSucceeded() {
      return succeeded;
    }

    /**
     * Get the return value stream.
     *
     * @return return value stream
     */
    public SafeDataInputStream returnValue() {
      return returnValue;
    }
  }
}
