package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Hash.HashAndSize;

/**
 * When saving objects to disc, they have a total size and a count of the actual written number of
 * bytes.
 */
public final class SerializationResult {

  private final HashAndSize hash;

  /**
   * Create serialization result.
   *
   * @param hash hash and size
   */
  public SerializationResult(HashAndSize hash) {
    this.hash = hash;
  }

  /**
   * Generate hash.
   *
   * @return hash
   */
  public Hash hash() {
    return hash.hash();
  }

  /**
   * Get the full byte length of content in storage.
   *
   * @return full byte length of content in storage
   */
  public long totalByteCount() {
    return hash.size();
  }
}
