package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Hash.HashAndSize;
import com.partisiablockchain.serialization.StateSerializer.SerializationHandler;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.HashMap;
import java.util.Map;

final class SerializationContext {

  private final StateStorage storage;
  private final TypeParameterCache types;

  /**
   * Feature toggle for deserialization of records.
   *
   * @see StateSerializable
   */
  private final boolean supportRecordDeserialization;

  /**
   * Assume the storage is not the same as the object was read from, i.e. make no assumptions on
   * what data is already present when writing.
   */
  private final boolean freshStorage;

  SerializationContext(StateStorage storage, boolean supportRecordDeserialization) {
    this(storage, supportRecordDeserialization, false);
  }

  SerializationContext(
      StateStorage storage, boolean supportRecordDeserialization, boolean freshStorage) {
    this.storage = storage;
    this.types = new TypeParameterCache();
    this.supportRecordDeserialization = supportRecordDeserialization;
    this.freshStorage = freshStorage;
  }

  /**
   * Feature toggle for deserialization of records.
   *
   * @see StateSerializable
   */
  boolean supportRecordDeserialization() {
    return supportRecordDeserialization;
  }

  /**
   * Whether the storage is fresh or not.
   *
   * @return true if the storage is fresh otherwise false.
   */
  public boolean isFreshStorage() {
    return freshStorage;
  }

  private void write(Object object, Type genericType) {
    getSerializer(object, genericType).write();
  }

  SerializationResult writeStateSerializable(StateSerializable value) {
    Serializer<?> serializer = getSerializer(value, null);
    serializer.write();
    HashAndSize hash = serializer.hash();
    return new SerializationResult(hash);
  }

  Class<?> getActualType(Type actualTypeArgument) {
    if (actualTypeArgument instanceof TypeVariable<?>) {
      return types.lookupTypeVariable(actualTypeArgument);
    } else if (actualTypeArgument instanceof ParameterizedType) {
      return (Class<?>) ((ParameterizedType) actualTypeArgument).getRawType();
    } else {
      return (Class<?>) actualTypeArgument;
    }
  }

  HashAndSize createHash(Object value, Type genericType) {
    return getSerializer(value, genericType).hash();
  }

  private Serializer<?> getSerializer(Object value, Type genericType) {
    if (value instanceof AvlTree) {
      return new SerializerAvlTree(this, storage, (AvlTree<?, ?>) value, genericType);
    } else if (value instanceof FixedList) {
      return new SerializerFixedList(this, storage, (FixedList<?>) value, genericType);
    } else if (value instanceof LargeByteArray) {
      return new SerializerLargeByteArray(storage, (LargeByteArray) value);
    } else /*if (object instanceof StateSerializable)*/ {
      return new SerializerStateSerializable<>(this, storage, (StateSerializable) value);
    }
  }

  @SuppressWarnings("unchecked")
  private <T> Serializer<?> getSerializer(Class<T> clazz, Type genericType, Hash hash) {
    if (clazz.equals(AvlTree.class)) {
      return new SerializerAvlTree(this, storage, hash, genericType);
    } else if (clazz.equals(FixedList.class)) {
      return new SerializerFixedList(this, storage, hash, genericType);
    } else if (clazz.equals(LargeByteArray.class)) {
      return new SerializerLargeByteArray(storage, hash);
    } else /*if (object instanceof StateSerializable)*/ {
      return new SerializerStateSerializable<>(
          this, storage, hash, (Class<? extends StateSerializable>) clazz);
    }
  }

  void writeIfSub(Object childValue, Class<?> type, Type genericType) {
    if (childValue != null) {
      if (isInlineSubObject(type)) {
        SerializerStateSerializableInline.writeSubs(this, type, childValue);
      } else if (isSubObject(type)) {
        write(childValue, genericType);
      }
    }
  }

  int sizeIfSub(Object childValue, Class<?> type, Type genericType) {
    if (childValue != null) {
      if (isInlineSubObject(type)) {
        return SerializerStateSerializableInline.subSize(this, type, childValue);
      } else if (isSubObject(type)) {
        return createHash(childValue, genericType).size();
      }
    }
    return 0;
  }

  Object convertValue(Object value, Class<?> type) {
    return convertValue(value, type, type);
  }

  Object convertValue(Object value, Class<?> type, Type genericType) {
    verifyValueType(type, value);
    if (value != null) {
      if (isInlineSubObject(type)) {
        return value;
      } else if (isSubObject(type)) {
        return createHash(value, genericType);
      }
    }
    return value;
  }

  private void verifyValueType(Class<?> type, Object value) {
    if (value == null
        || type.isPrimitive()
        || StateSerializer.SIMPLE_TYPES.containsKey(type)
        || type.equals(AvlTree.class)
        || type.isEnum()) {
      return;
    }

    if (!type.equals(value.getClass())) {
      throw new IllegalArgumentException(
          "Illegal object type for serialization. "
              + "Expected=["
              + type
              + "], "
              + "Actual=["
              + value.getClass()
              + "]");
    }
  }

  void writeIfNotNullForType(SafeDataOutputStream stream, Class<?> type, Object value) {
    if (value != null) {
      if (isInlineSubObject(type)) {
        SerializerStateSerializableInline.write(this, stream, value);
      } else if (isSubObject(type)) {
        ((HashAndSize) value).hash().write(stream);
      } else {
        writeValue(stream, type, value);
      }
    }
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  private void writeValue(SafeDataOutputStream stream, Class<?> type, Object value) {
    SerializationHandler<?> serializationHandler = StateSerializer.SIMPLE_TYPES.get(type);
    if (serializationHandler != null) {
      serializationHandler.write(value, stream);
    } else /*if (type.isEnum())*/ {
      stream.writeEnum((Enum) value);
    }
  }

  @SuppressWarnings("unchecked")
  <T> T read(Class<T> clazz, Type genericType, Hash hash) {
    return (T) getSerializer(clazz, genericType, hash).read();
  }

  @SuppressWarnings("unchecked")
  <T> T readIfSub(Object value, Class<T> type, Type genericType) {
    if (value != null) {
      if (isInlineSubObject(type)) {
        return (T) ((SerializerStateSerializableInline<?>) value).instantiateWithSubs(this);
      } else if (isSubObject(type)) {
        return read(type, genericType, (Hash) value);
      } else {
        return (T) value;
      }
    } else {
      return null;
    }
  }

  @SuppressWarnings("unchecked")
  static Object readNonNullForType(
      SafeDataInputStream stream,
      Class<?> type,
      TypeParameterCache typeParameters,
      boolean supportRecordDeserialization) {
    if (isInlineSubObject(type)) {
      return SerializerStateSerializableInline.initialize(
          stream,
          (Class<? extends StateSerializableInline>) type,
          typeParameters,
          supportRecordDeserialization);
    } else if (isSubObject(type)) {
      return Hash.read(stream);
    } else {
      return readValue(stream, type);
    }
  }

  private static Object readValue(SafeDataInputStream stream, Class<?> type) {
    SerializationHandler<?> handler = StateSerializer.SIMPLE_TYPES.get(type);
    if (handler != null) {
      return handler.read(stream);
    } else /*if (type.isEnum())*/ {
      return stream.readEnum(type.getEnumConstants());
    }
  }

  static boolean isSubObject(Class<?> type) {
    return isParameterizedSubObject(type) || isSimpleSubObject(type);
  }

  static boolean isSimpleSubObject(Class<?> type) {
    return !StateSerializer.SIMPLE_TYPES.containsKey(type)
        && StateSerializable.class.isAssignableFrom(type);
  }

  static boolean isInlineSubObject(Class<?> type) {
    return StateSerializableInline.class.isAssignableFrom(type);
  }

  private static boolean isParameterizedSubObject(Class<?> type) {
    return type.equals(AvlTree.class) || type.equals(FixedList.class);
  }

  void addTypeParameters(Class<? extends StateSerializable> clazz, Class<?>... parameters) {
    types.addTypeParameters(clazz, parameters);
  }

  TypeParameterCache getTypeParameters() {
    return types;
  }

  static final class TypeParameterCache {

    private final Map<TypeVariable<?>, Class<?>> types = new HashMap<>();

    void addTypeParameters(Class<? extends StateSerializable> clazz, Class<?>... parameters) {
      TypeVariable<?>[] typeParameters = clazz.getTypeParameters();
      for (int i = 0; i < typeParameters.length; i++) {
        this.types.put(typeParameters[i], parameters[i]);
      }
    }

    @SuppressWarnings("SuspiciousMethodCalls")
    Class<?> lookupTypeVariable(Type genericType) {
      return types.get(genericType);
    }
  }

  interface Serializer<T> {

    HashAndSize hash();

    void write();

    T read();
  }
}
