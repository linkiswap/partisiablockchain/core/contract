package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Hash.HashAndSize;
import com.partisiablockchain.tree.AvlComposite;
import com.partisiablockchain.tree.AvlLeaf;
import com.partisiablockchain.tree.AvlTree;
import com.partisiablockchain.tree.EmptyTree;
import com.partisiablockchain.tree.TreeReader;
import com.secata.stream.SafeDataInputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.function.Supplier;

final class StateTreeReader<S extends Comparable<S>, T> implements TreeReader<S, T> {

  private final SerializationContext context;
  private final StateStorage storage;
  private final Class<S> keyType;
  private final Class<T> valueType;

  @SuppressWarnings("unchecked")
  StateTreeReader(SerializationContext context, StateStorage storage, Type genericType) {
    this.context = context;
    this.storage = storage;
    Type[] arguments = ((ParameterizedType) genericType).getActualTypeArguments();
    keyType = (Class<S>) context.getActualType(arguments[0]);
    valueType = (Class<T>) context.getActualType(arguments[1]);
  }

  @Override
  public AvlTree<S, T> readNode(Hash hash) {
    Supplier<AvlTree<S, T>> nodeReader =
        storage.read(
            hash,
            stream -> {
              byte b = stream.readSignedByte();
              if (b == 0) {
                return readLeaf(hash.withZeroSize(), stream);
              } else if (b == 1) {
                return readComposite(hash, stream);
              } else {
                return EmptyTree::new;
              }
            });
    return nodeReader.get();
  }

  @Override
  @SuppressWarnings("Immutable")
  public Supplier<AvlTree<S, T>> readComposite(Hash hash, SafeDataInputStream stream) {
    Hash leftHash = Hash.read(stream);
    Hash rightHash = Hash.read(stream);
    int treeSize = stream.readInt();
    byte height = stream.readSignedByte();
    int storageSize = stream.readInt();
    AvlComposite<S, T> tree =
        new AvlComposite<>(this, hash.withSize(storageSize), leftHash, rightHash, treeSize, height);
    return () -> tree;
  }

  @Override
  @SuppressWarnings("Immutable")
  public Supplier<AvlTree<S, T>> readLeaf(HashAndSize hash, SafeDataInputStream stream) {
    Object keyValue =
        SerializationContext.readNonNullForType(
            stream, keyType, context.getTypeParameters(), context.supportRecordDeserialization());
    Object valueValue =
        SerializationContext.readNonNullForType(
            stream, valueType, context.getTypeParameters(), context.supportRecordDeserialization());

    return () ->
        new AvlLeaf<>(
            context.readIfSub(keyValue, keyType, null),
            () -> context.readIfSub(valueValue, valueType, null));
  }
}
