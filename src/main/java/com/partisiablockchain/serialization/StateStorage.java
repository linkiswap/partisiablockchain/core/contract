package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Can save storage based on hash values. Users are responsible for making sure they can
 * recall/reproduce the hash values.
 */
public interface StateStorage {

  /**
   * Write to storage.
   *
   * @param hash identifies node to write to.
   * @param writer writing function
   * @return true if successful, false otherwise
   */
  boolean write(Hash hash, Consumer<SafeDataOutputStream> writer);

  /**
   * Read from storage.
   *
   * @param hash identifies node to read.
   * @param reader reading function
   * @param <S> result type of reading function
   * @return value read from storage
   */
  <S> S read(Hash hash, Function<SafeDataInputStream, S> reader);
}
