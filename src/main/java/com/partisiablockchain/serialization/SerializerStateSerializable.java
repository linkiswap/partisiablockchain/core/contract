package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Hash.HashAndSize;
import com.partisiablockchain.serialization.SerializationContext.Serializer;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.immutable.FixedList;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.RecordComponent;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

final class SerializerStateSerializable<T extends StateSerializable> implements Serializer<T> {

  private final SerializationContext context;
  private final StateStorage storage;
  private final T value;
  private final HashAndSize hash;
  private final Class<T> clazz;

  @SuppressWarnings("unchecked")
  SerializerStateSerializable(SerializationContext context, StateStorage storage, T value) {
    this.context = context;
    this.storage = storage;
    this.value = value;
    this.clazz = (Class<T>) value.getClass();
    this.hash = calculateHash();
  }

  SerializerStateSerializable(
      SerializationContext context, StateStorage storage, Hash hash, Class<T> clazz) {
    this.context = context;
    this.storage = storage;
    this.value = null;
    this.clazz = clazz;
    this.hash = hash.withZeroSize();
  }

  private HashAndSize calculateHash() {
    HashAndSize hash = Hash.createSize(this::writeObject);
    int subSize = subSize(context, clazz, value);
    return hash.withSubSize(subSize);
  }

  @Override
  public HashAndSize hash() {
    return hash;
  }

  @Override
  public void write() {
    boolean updated = storage.write(hash.hash(), this::writeObject);
    if (updated) {
      writeSubs(context, clazz, value);
    }
  }

  static <T> int subSize(SerializationContext context, Class<T> clazz, T value) {
    return ExceptionConverter.call(
        () -> {
          int result = 0;
          Field[] fields = getFields(clazz, context.getTypeParameters());
          for (Field field : fields) {
            Object childValue = field.get(value);
            Class<?> type = getFieldType(context.getTypeParameters(), field);
            result += context.sizeIfSub(childValue, type, field.getGenericType());
          }
          return result;
        },
        "Could not calculate subSize");
  }

  static <T> void writeSubs(SerializationContext context, Class<T> clazz, T value) {
    ExceptionConverter.run(
        () -> {
          Field[] fields = getFields(clazz, context.getTypeParameters());
          for (Field field : fields) {
            Object childValue = field.get(value);
            Class<?> type = getFieldType(context.getTypeParameters(), field);
            context.writeIfSub(childValue, type, field.getGenericType());
          }
        },
        "Unable to write children");
  }

  void writeObject(SafeDataOutputStream stream) {
    if (value instanceof StateSerializableVersioned) {
      stream.writeByte(1);
    }
    writeNonNull(context, stream, value);
  }

  static void writeNonNull(
      SerializationContext context, SafeDataOutputStream stream, Object value) {
    List<FieldAndValue> fieldAndValues =
        Arrays.stream(getFields(value.getClass(), context.getTypeParameters()))
            .map(
                field -> {
                  Object fieldValue =
                      ExceptionConverter.call(
                          () -> field.get(value), "Unable to get value for field");
                  Class<?> type = getFieldType(context.getTypeParameters(), field);
                  Object convertedValue =
                      context.convertValue(fieldValue, type, field.getGenericType());
                  return new FieldAndValue(field, type, convertedValue);
                })
            .collect(Collectors.toList());
    stream.write(createBitMask(fieldAndValues));
    fieldAndValues.forEach(
        fieldAndValue ->
            context.writeIfNotNullForType(stream, fieldAndValue.type, fieldAndValue.value));
  }

  private static byte[] createBitMask(List<FieldAndValue> fieldAndValues) {
    BitMask bitMask = new BitMask();
    for (FieldAndValue fieldAndValue : fieldAndValues) {
      if (!fieldAndValue.type.isPrimitive()) {
        bitMask.write(fieldAndValue.value != null);
      }
    }
    return bitMask.asBytes();
  }

  @Override
  public T read() {
    List<FieldAndValue> fieldAndValues =
        Objects.requireNonNull(
            storage.read(
                hash.hash(),
                stream ->
                    readFromStream(
                        clazz,
                        stream,
                        context.getTypeParameters(),
                        context.supportRecordDeserialization())));

    return readSubsAndInstantiate(context, clazz, fieldAndValues);
  }

  static <T> T readSubsAndInstantiate(
      SerializationContext context, Class<T> clazz, List<FieldAndValue> fieldAndValues) {
    // Special case for records, as they do not support default fields
    // If record deserialization support is disabled, use the old behaviour,
    // including the same reflection exceptions.
    if (context.supportRecordDeserialization() && clazz.isRecord()) {
      return instantiateRecord(context, clazz, fieldAndValues);
    }

    // Case for standard classes
    T instance = instantiate(clazz);
    fieldAndValues.forEach(fieldAndValue -> fieldAndValue.setField(context, instance));
    return instance;
  }

  private static <T> T instantiateRecord(
      SerializationContext context, Class<T> clazz, List<FieldAndValue> fieldAndValues) {

    // Determine components, parameter types, and constructor.
    final var components = clazz.getRecordComponents();
    final Class<?>[] parameterTypes =
        Stream.of(components).map(RecordComponent::getType).toArray(Class<?>[]::new);

    final Constructor<T> constructor =
        ExceptionConverter.call(
            () -> clazz.getDeclaredConstructor(parameterTypes),
            "Could not determine constructor for class");
    constructor.setAccessible(true);

    // Create parameter list by reordering loaded fields into order expected by
    // constructor.
    final HashMap<String, Object> parametersByFieldName = new HashMap<>();
    for (final FieldAndValue fieldAndValue : fieldAndValues) {
      parametersByFieldName.put(
          fieldAndValue.getField().getName(), fieldAndValue.getActualValue(context));
    }

    final Object[] parameters =
        Stream.of(components)
            .map(RecordComponent::getName)
            .map(parametersByFieldName::get)
            .toArray(Object[]::new);

    // Instantiate class
    T instance =
        ExceptionConverter.call(
            () -> constructor.newInstance(parameters),
            "Exception thrown when creating new instance, see below");
    return instance;
  }

  private static <T> T instantiate(Class<T> clazz) {
    T instance;
    try {
      Constructor<T> constructor = clazz.getDeclaredConstructor();
      constructor.setAccessible(true);
      instance =
          ExceptionConverter.call(
              constructor::newInstance, "Exception thrown when creating new instance, see below");
    } catch (NoSuchMethodException e) {
      throw new RuntimeException(
          "Cannot instantiate " + clazz + " - missing a default constructor", e);
    }
    return instance;
  }

  static <T> List<FieldAndValue> readFromStream(
      Class<T> clazz,
      SafeDataInputStream stream,
      SerializationContext.TypeParameterCache typeParameters,
      boolean supportRecordDeserialization) {
    Field[] fields;
    if (StateSerializableVersioned.class.isAssignableFrom(clazz)) {
      stream.readUnsignedByte(); // Assuming we always read 1...
    }
    fields = getFields(clazz, typeParameters);
    int maskSize =
        (int) Arrays.stream(fields).filter(field -> !field.getType().isPrimitive()).count();
    MaskReader maskReader = new MaskReader(stream.readBytes(BitMask.bytesNeededForBits(maskSize)));
    return Arrays.stream(fields)
        .map(
            field ->
                createFieldAndValue(
                    maskReader, field, stream, typeParameters, supportRecordDeserialization))
        .collect(Collectors.toList());
  }

  private static FieldAndValue createFieldAndValue(
      MaskReader maskReader,
      Field field,
      SafeDataInputStream stream,
      SerializationContext.TypeParameterCache typeParameters,
      boolean supportRecordDeserialization) {
    Class<?> type = getFieldType(typeParameters, field);
    Object value;
    if (type.isPrimitive() || maskReader.next()) {
      value =
          SerializationContext.readNonNullForType(
              stream, type, typeParameters, supportRecordDeserialization);
    } else {
      value = null;
    }
    return new FieldAndValue(field, type, value);
  }

  @SuppressWarnings("unchecked")
  private static Field[] getFields(
      Class<?> clazz, SerializationContext.TypeParameterCache typeParameters) {
    Field[] fields = clazz.getDeclaredFields();
    for (Field field : fields) {
      field.setAccessible(true);
    }
    fields =
        Arrays.stream(fields)
            .filter(field -> !Modifier.isTransient(field.getModifiers()))
            .filter(field -> !Modifier.isStatic(field.getModifiers()))
            .sorted(Comparator.comparing(Field::getName))
            .toArray(Field[]::new);
    for (Field field : fields) {
      Class<?> type = field.getType();
      if (StateSerializable.class.isAssignableFrom(type) && type.getTypeParameters().length != 0) {
        Class<?>[] parameters =
            Arrays.stream(((ParameterizedType) field.getGenericType()).getActualTypeArguments())
                .map((Type t) -> parameterToClass(t, typeParameters))
                .toArray(Class<?>[]::new);
        typeParameters.addTypeParameters((Class<? extends StateSerializable>) type, parameters);
      }
      verifyFieldType(clazz, field, typeParameters);
    }
    return fields;
  }

  private static void verifyFieldType(
      Class<?> clazz, Field field, SerializationContext.TypeParameterCache typeParameters) {
    Class<?> type = getFieldType(typeParameters, field);
    if (type.equals(AvlTree.class)) {
      Type genericType = field.getGenericType();
      if (genericType instanceof ParameterizedType) {
        Type[] arguments = ((ParameterizedType) genericType).getActualTypeArguments();
        ensureClassType(field, arguments[0], typeParameters);
        ensureClassType(field, arguments[1], typeParameters);
      } else {
        throw new IllegalArgumentException("AvlTree must be parameterized");
      }
    } else if (type.equals(FixedList.class)) {
      Type genericType = field.getGenericType();
      if (genericType instanceof ParameterizedType) {
        Type[] arguments = ((ParameterizedType) genericType).getActualTypeArguments();
        ensureClassType(field, arguments[0], typeParameters);
      } else {
        throw new IllegalArgumentException("FixedList must be parameterized");
      }
    } else {
      verifyClassType(clazz, field, type);
    }
  }

  @SuppressWarnings("unchecked")
  private static void ensureClassType(
      Field field, Type argument, SerializationContext.TypeParameterCache typeParameters) {
    if (argument instanceof ParameterizedType parameterized) {
      Class<?> type = (Class<?>) parameterized.getRawType();
      if (StateSerializable.class.isAssignableFrom(type)) {
        Class<?>[] parameters =
            Arrays.stream(parameterized.getActualTypeArguments())
                .map((Type t) -> parameterToClass(t, typeParameters))
                .toArray(Class<?>[]::new);
        typeParameters.addTypeParameters((Class<? extends StateSerializable>) type, parameters);
        return;
      }
    } else if (argument instanceof TypeVariable<?>) {
      argument = typeParameters.lookupTypeVariable(argument);
    }

    if (!(argument instanceof Class)) {
      throw new IllegalArgumentException("Type arguments cannot be parameterized");
    }
    verifyClassType(field.getDeclaringClass(), field, (Class<?>) argument);
  }

  private static void verifyClassType(Class<?> clazz, Field field, Class<?> type) {
    if (!isSupportedGenericType(type)) {
      throw new IllegalArgumentException(
          "Field \"%s\" in %s cannot be serialized, due to %s not implementing StateSerializable."
              .formatted(field.getName(), clazz, type));
    }
  }

  private static boolean isSupportedGenericType(Class<?> type) {
    return SerializationContext.isSimpleSubObject(type)
        || StateSerializer.SIMPLE_TYPES.containsKey(type)
        || type.isEnum();
  }

  private static Class<?> parameterToClass(
      Type t, SerializationContext.TypeParameterCache typeParameters) {
    if (t instanceof TypeVariable) {
      return typeParameters.lookupTypeVariable(t);
    } else {
      return (Class<?>) t;
    }
  }

  private static Class<?> getFieldType(
      SerializationContext.TypeParameterCache typeParameters, Field field) {
    if (field.getGenericType() instanceof TypeVariable<?>) {
      return typeParameters.lookupTypeVariable(field.getGenericType());
    } else {
      return field.getType();
    }
  }

  /** Container for individually parsed field value assignments. */
  static final class FieldAndValue {

    private final Field field;
    private final Class<?> type;
    private final Object value;

    /**
     * Create new FieldAndValue.
     *
     * @param field Field instance, for the parent type. Never null.
     * @param type Parsed type of the field. Never null.
     * @param value Parsed value. Null allowed.
     */
    private FieldAndValue(Field field, Class<?> type, Object value) {
      this.field = Objects.requireNonNull(field);
      this.type = Objects.requireNonNull(type);
      this.value = value;
    }

    boolean isSub() {
      return SerializationContext.isSubObject(type);
    }

    Object getActualValue(SerializationContext context) {
      return context.readIfSub(value, type, field.getGenericType());
    }

    Object getValue() {
      return value;
    }

    Field getField() {
      return field;
    }

    <S> void setField(SerializationContext context, S t) {
      Object instantiatedValue = getActualValue(context);
      ExceptionConverter.run(() -> field.set(t, instantiatedValue), "Unable to set field value");
    }
  }
}
