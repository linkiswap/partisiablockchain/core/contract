package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Hash.HashAndSize;

/** Wraps a (large) byte array into something more manageable for serialization. */
@Immutable
@SuppressWarnings("WeakerAccess")
public final class LargeByteArray implements StateSerializable {

  @SuppressWarnings("Immutable")
  private final byte[] data;

  @SuppressWarnings("Immutable")
  private transient HashAndSize hash;

  @SuppressWarnings("unused")
  LargeByteArray() {
    this.data = null;
    this.hash = null;
  }

  /**
   * Create large byte array.
   *
   * @param data inner byte array
   */
  public LargeByteArray(byte[] data) {
    this.data = data.clone();
    this.hash = null;
  }

  /**
   * Get the data as bytes.
   *
   * @return data as bytes
   */
  public byte[] getData() {
    return data.clone();
  }

  /**
   * Get the byte length of data.
   *
   * @return byte length of data
   */
  public int getLength() {
    return data.length;
  }

  /**
   * Produces an identifier for this byte array.
   *
   * @return the hash identification.
   */
  public Hash getIdentifier() {
    return hashWithSize().hash();
  }

  HashAndSize hashWithSize() {
    if (hash == null) {
      hash = Hash.createSize(stream -> stream.writeDynamicBytes(data));
    }
    return hash;
  }
}
