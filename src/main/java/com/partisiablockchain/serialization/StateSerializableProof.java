package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.util.DataStreamLimit;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;

/**
 * A proof for the values of the fields of a {@link StateSerializable}. Only contains the hashes of
 * any childrens.
 */
public final class StateSerializableProof implements DataStreamSerializable {

  private final byte[] root;
  private final Hash identifier;
  private final boolean supportRecordDeserialization;

  private StateSerializableProof(byte[] root, boolean supportRecordDeserialization) {
    this.root = root;
    this.identifier = Hash.create(s -> s.write(root));
    this.supportRecordDeserialization = supportRecordDeserialization;
  }

  /**
   * Create serializable proof.
   *
   * @param root raw proof as bytes
   * @param supportRecordDeserialization whether {@link StateSerializable record deserialization
   *     support} should be enabled
   * @return serializable proof.
   */
  public static StateSerializableProof create(byte[] root, boolean supportRecordDeserialization) {
    return new StateSerializableProof(root.clone(), supportRecordDeserialization);
  }

  /**
   * Get a reader to access the fields of the serialized object.
   *
   * @param clazz the type of this object
   * @param parameters the type parameters of this object
   * @param <T> the type of this object
   * @return a reader allowing to access the fields in the serialized object
   */
  public <T extends StateSerializable> ValueReader reader(Class<T> clazz, Class<?>... parameters) {
    SerializationContext.TypeParameterCache typeParameters =
        new SerializationContext.TypeParameterCache();
    typeParameters.addTypeParameters(clazz, parameters);
    List<SerializerStateSerializable.FieldAndValue> fieldAndValues =
        SerializerStateSerializable.readFromStream(
            clazz,
            SafeDataInputStream.createFromBytes(root),
            typeParameters,
            supportRecordDeserialization);
    return new ValueReader(fieldAndValues);
  }

  /**
   * Get the identifying hash.
   *
   * @return identifying hash
   */
  public Hash getIdentifier() {
    return identifier;
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.writeDynamicBytes(root);
  }

  /**
   * Read proof from stream.
   *
   * @param stream to read from
   * @param supportRecordDeserialization whether {@link StateSerializable record deserialization
   *     support} should be enabled
   * @return read proof
   */
  public static StateSerializableProof read(
      SafeDataInputStream stream, boolean supportRecordDeserialization) {
    return new StateSerializableProof(
        DataStreamLimit.readDynamicBytes(stream), supportRecordDeserialization);
  }

  /** A reader that allows getting the values that a proof attests to. */
  public static final class ValueReader {

    private final List<SerializerStateSerializable.FieldAndValue> fieldAndValues;

    private ValueReader(List<SerializerStateSerializable.FieldAndValue> fieldAndValues) {
      this.fieldAndValues = fieldAndValues;
    }

    /**
     * Read a identifier of a field value from the object.
     *
     * @param fieldName the field to read the identifier for
     * @return the identifier of the object assigned to the specified field
     */
    public Hash getSubIdentifier(String fieldName) {
      SerializerStateSerializable.FieldAndValue field = findField(fieldName);
      if (!field.isSub()) {
        throw new IllegalStateException(
            "Trying to get identifier for field that is serialized inline");
      }
      return (Hash) field.getValue();
    }

    /**
     * Read a value from the object that exists inline in the serialized object.
     *
     * @param fieldName the field to read the value from
     * @param <T> the expected type of the field
     * @return the value assigned to the specified field
     */
    @SuppressWarnings({"unchecked", "TypeParameterUnusedInFormals"})
    public <T> T getInlineValue(String fieldName) {
      SerializerStateSerializable.FieldAndValue field = findField(fieldName);
      if (field.isSub()) {
        throw new IllegalStateException(
            "Trying to get value for field that is not serialized inline");
      }
      return (T) field.getValue();
    }

    private SerializerStateSerializable.FieldAndValue findField(String fieldName) {
      for (SerializerStateSerializable.FieldAndValue fieldAndValue : fieldAndValues) {
        if (fieldAndValue.getField().getName().equals(fieldName)) {
          return fieldAndValue;
        }
      }
      throw new IllegalStateException("Type does not contain field with name " + fieldName);
    }
  }
}
