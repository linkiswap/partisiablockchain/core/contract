package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.io.IOException;
import java.util.List;

/** Maps from json to state objects. */
public final class StateObjectMapper {

  private StateObjectMapper() {}

  /**
   * Construct a new object mapper configured to be able to read and write contracts.
   *
   * @return a newly configured object mapper
   */
  public static ObjectMapper createObjectMapper() {
    SimpleModule testModule = new SimpleModule("MyModule", new Version(1, 0, 0, null, null, null));
    testModule
        .addSerializer(
            AvlTree.class,
            new JsonSerializer<>() {
              @Override
              @SuppressWarnings({"rawtypes", "unchecked"})
              public void serialize(
                  AvlTree avlTree,
                  JsonGenerator jsonGenerator,
                  SerializerProvider serializerProvider)
                  throws IOException {
                jsonGenerator.writeStartArray();
                for (Object key : avlTree.keySet()) {
                  jsonGenerator.writeStartObject();
                  jsonGenerator.writeObjectField("key", key);
                  jsonGenerator.writeObjectField("value", avlTree.getValue((Comparable) key));
                  jsonGenerator.writeEndObject();
                }
                jsonGenerator.writeEndArray();
              }
            })
        .addSerializer(
            FixedList.class,
            new JsonSerializer<>() {
              @Override
              @SuppressWarnings({"rawtypes", "unchecked"})
              public void serialize(
                  FixedList fixedList,
                  JsonGenerator jsonGenerator,
                  SerializerProvider serializerProvider)
                  throws IOException {
                jsonGenerator.writeStartArray();
                for (Object element : fixedList) {
                  jsonGenerator.writeObject(element);
                }
                jsonGenerator.writeEndArray();
              }
            })
        .addSerializer(
            BlockchainAddress.class,
            new JsonSerializer<>() {
              @Override
              public void serialize(
                  BlockchainAddress address,
                  JsonGenerator jsonGenerator,
                  SerializerProvider serializerProvider)
                  throws IOException {
                jsonGenerator.writeString(address.writeAsString());
              }
            })
        .addSerializer(
            Hash.class,
            new JsonSerializer<>() {
              @Override
              public void serialize(
                  Hash address, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
                  throws IOException {
                jsonGenerator.writeString(address.toString());
              }
            })
        .addSerializer(
            BlockchainPublicKey.class,
            new JsonSerializer<>() {
              @Override
              public void serialize(
                  BlockchainPublicKey key,
                  JsonGenerator jsonGenerator,
                  SerializerProvider serializerProvider)
                  throws IOException {
                jsonGenerator.writeBinary(key.asBytes());
              }
            })
        .addSerializer(
            Signature.class,
            new JsonSerializer<>() {
              @Override
              public void serialize(
                  Signature address,
                  JsonGenerator jsonGenerator,
                  SerializerProvider serializerProvider)
                  throws IOException {
                jsonGenerator.writeString(address.writeAsString());
              }
            })
        .addSerializer(
            BlsPublicKey.class,
            new JsonSerializer<>() {
              @Override
              public void serialize(
                  BlsPublicKey blsPublicKey,
                  JsonGenerator jsonGenerator,
                  SerializerProvider serializerProvider)
                  throws IOException {
                jsonGenerator.writeBinary(blsPublicKey.getPublicKeyValue().serialize(true));
              }
            })
        .addSerializer(
            BlsSignature.class,
            new JsonSerializer<>() {
              @Override
              public void serialize(
                  BlsSignature blsSignature,
                  JsonGenerator jsonGenerator,
                  SerializerProvider serializerProvider)
                  throws IOException {
                jsonGenerator.writeBinary(blsSignature.getSignatureValue().serialize(true));
              }
            })
        .addSerializer(
            Unsigned256.class,
            new JsonSerializer<>() {
              @Override
              public void serialize(
                  Unsigned256 unsigned256,
                  JsonGenerator jsonGenerator,
                  SerializerProvider serializerProvider)
                  throws IOException {
                jsonGenerator.writeString(unsigned256.toString());
              }
            })
        .addDeserializer(
            BlockchainAddress.class,
            new JsonDeserializer<>() {

              @Override
              public BlockchainAddress deserialize(JsonParser p, DeserializationContext ctxt)
                  throws IOException {
                return BlockchainAddress.fromString(p.getValueAsString());
              }
            })
        .addDeserializer(
            Hash.class,
            new JsonDeserializer<>() {

              @Override
              public Hash deserialize(JsonParser p, DeserializationContext ctxt)
                  throws IOException {
                return Hash.fromString(p.getValueAsString());
              }
            })
        .addDeserializer(
            BlockchainPublicKey.class,
            new JsonDeserializer<>() {
              @Override
              public BlockchainPublicKey deserialize(JsonParser p, DeserializationContext ctxt)
                  throws IOException {
                return BlockchainPublicKey.fromEncodedEcPoint(p.getBinaryValue());
              }
            })
        .addDeserializer(
            Signature.class,
            new JsonDeserializer<>() {
              @Override
              public Signature deserialize(JsonParser p, DeserializationContext ctxt)
                  throws IOException {
                return Signature.fromString(p.getValueAsString());
              }
            })
        .addDeserializer(
            BlsPublicKey.class,
            new JsonDeserializer<>() {
              @Override
              public BlsPublicKey deserialize(JsonParser p, DeserializationContext ctxt)
                  throws IOException {
                return new BlsPublicKey(p.getBinaryValue());
              }
            })
        .addDeserializer(
            BlsSignature.class,
            new JsonDeserializer<>() {
              @Override
              public BlsSignature deserialize(JsonParser p, DeserializationContext ctxt)
                  throws IOException {
                return new BlsSignature(p.getBinaryValue());
              }
            })
        .addDeserializer(
            Unsigned256.class,
            new JsonDeserializer<>() {
              @Override
              public Unsigned256 deserialize(
                  JsonParser jsonParser, DeserializationContext deserializationContext)
                  throws IOException {
                return Unsigned256.create(jsonParser.getValueAsString());
              }
            })
        .addDeserializer(FixedList.class, new FixedListDeserializer(null))
        .addDeserializer(AvlTree.class, new AvlTreeDeserializer(null, null))
        .addSerializer(Long.class, ToStringSerializer.instance)
        .addSerializer(long.class, ToStringSerializer.instance);

    return JsonMapper.builder()
        .configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true)
        .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
        .build()
        .registerModule(testModule)
        .setVisibility(PropertyAccessor.ALL, Visibility.NONE)
        .setVisibility(PropertyAccessor.FIELD, Visibility.ANY)
        .setVisibility(PropertyAccessor.CREATOR, Visibility.ANY)
        .setSerializationInclusion(Include.NON_NULL)
        .registerModule(new JavaTimeModule());
  }

  static final class FixedListDeserializer extends JsonDeserializer<FixedList<?>>
      implements ContextualDeserializer {

    private final JavaType elementType;

    private FixedListDeserializer(JavaType elementType) {
      this.elementType = elementType;
    }

    @Override
    public FixedList<?> deserialize(JsonParser parser, DeserializationContext ctxt)
        throws IOException {
      CollectionType collectionType =
          ctxt.getTypeFactory().constructCollectionType(List.class, elementType);
      List<?> o = ctxt.readValue(parser, collectionType);
      return FixedList.create(o);
    }

    @Override
    public JsonDeserializer<?> createContextual(
        DeserializationContext ctxt, BeanProperty property) {
      JavaType listType = property.getType();
      JavaType elementType = listType.containedType(0);
      return new FixedListDeserializer(elementType);
    }
  }

  static final class AvlTreeDeserializer extends JsonDeserializer<AvlTree<?, ?>>
      implements ContextualDeserializer {

    private final JavaType keyType;
    private final JavaType valueType;

    private AvlTreeDeserializer(JavaType keyType, JavaType valueType) {
      this.keyType = keyType;
      this.valueType = valueType;
    }

    @Override
    @SuppressWarnings("Immutable")
    public AvlTree<?, ?> deserialize(JsonParser parser, DeserializationContext ctxt)
        throws IOException {
      AvlTree<?, ?> tree = AvlTree.create();
      TreeNode treeNode = parser.readValueAsTree();
      for (int i = 0; i < treeNode.size(); i++) {
        ObjectCodec codec = parser.getCodec();
        TreeNode node = treeNode.get(i);
        TreeNode key = node.get("key");
        TreeNode value = node.get("value");
        JsonParser traverse = key.traverse(codec);
        traverse.nextToken();
        JsonParser traverse1 = value.traverse(codec);
        traverse1.nextToken();
        tree = tree.set(ctxt.readValue(traverse, keyType), ctxt.readValue(traverse1, valueType));
      }
      return tree;
    }

    @Override
    public JsonDeserializer<?> createContextual(
        DeserializationContext ctxt, BeanProperty property) {
      JavaType avlType = property.getType();
      JavaType keyType = avlType.containedType(0);
      JavaType valueType = avlType.containedType(1);
      return new AvlTreeDeserializer(keyType, valueType);
    }
  }
}
