package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.immutable.FixedList;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiConsumer;

/** Accessor of state. */
public final class StateAccessor {

  private final Object state;

  private StateAccessor(Object state) {
    this.state = state;
  }

  /**
   * Create state accessor.
   *
   * @param state state to access
   * @return created state accessor
   */
  public static StateAccessor create(StateSerializable state) {
    return new StateAccessor(state);
  }

  /**
   * Retrieves the specified field of a {@link StateSerializable} object and wraps it in a new
   * {@link StateAccessor}.
   *
   * @param fieldName the field to access.
   * @return a {@link StateAccessor} containing the retrieved field.
   */
  public StateAccessor get(String fieldName) {
    Field retrievedField = Objects.requireNonNull(getField(fieldName), "Field does not exist");
    retrievedField.setAccessible(true);
    Object retrievedState =
        ExceptionConverter.call(() -> retrievedField.get(state), "Illegal access of field.");
    return new StateAccessor(retrievedState);
  }

  /**
   * Check if the current object has a named field.
   *
   * @param fieldName the field to access
   * @return true if the field exists
   */
  public boolean hasField(String fieldName) {
    return getField(fieldName) != null;
  }

  private Field getField(String fieldName) {
    try {
      return state.getClass().getDeclaredField(fieldName);
    } catch (NoSuchFieldException e) {
      return null;
    }
  }

  /**
   * Get the state as typed value.
   *
   * @param type value type
   * @param <S> type parameter for value type
   * @return state as typed value
   */
  public <S extends StateSerializable> S typedValue(Class<S> type) {
    return cast(type);
  }

  /**
   * Cast the state to the supplied value.
   *
   * @param type value type
   * @param <S> type parameter for value type
   * @return state as type
   */
  public <S> S cast(Class<S> type) {
    return type.cast(state);
  }

  /**
   * Check if the current value is null.
   *
   * @return true if the value is null
   */
  public boolean isNull() {
    return state == null;
  }

  /**
   * Get the state as typed avl tree.
   *
   * @param keyClass key type for avl tree
   * @param valueClass value type for avl tree
   * @param <K> type parameter for key type
   * @param <V> type parameter for value type
   * @return state as typed avl tree
   */
  @SuppressWarnings("unchecked")
  public <K extends Comparable<K>, V> AvlTree<K, V> typedAvlTree(
      Class<K> keyClass, Class<V> valueClass) {
    return (AvlTree<K, V>) state;
  }

  /**
   * Get the state as typed list.
   *
   * @param elementClass value type for list
   * @param <E> type parameter for value type
   * @return state as typed list
   */
  @SuppressWarnings("unchecked")
  public <E> FixedList<E> typedFixedList(Class<E> elementClass) {
    return (FixedList<E>) state;
  }

  /**
   * Get the value as {@link String}.
   *
   * @return value as {@link String}
   */
  public String stringValue() {
    return (String) state;
  }

  /**
   * Get the value as {@link int}.
   *
   * @return value as {@link int}
   */
  public int intValue() {
    return (int) state;
  }

  /**
   * Get the value as {@link long}.
   *
   * @return value as {@link long}
   */
  public long longValue() {
    return (long) state;
  }

  /**
   * Get the value as {@link boolean}.
   *
   * @return value as {@link boolean}
   */
  public boolean booleanValue() {
    return (boolean) state;
  }

  /**
   * Get the value as {@link byte}.
   *
   * @return value as {@link byte}
   */
  public byte byteValue() {
    return (byte) state;
  }

  /**
   * Get the value as {@link StateBoolean}.
   *
   * @return value as {@link StateBoolean}
   */
  public StateBoolean stateBooleanValue() {
    return (StateBoolean) state;
  }

  /**
   * Get the value as {@link StateLong}.
   *
   * @return value as {@link StateLong}
   */
  public StateLong stateLongValue() {
    return (StateLong) state;
  }

  /**
   * Get the value as {@link StateString}.
   *
   * @return value as {@link StateString}
   */
  public StateString stateStringValue() {
    return (StateString) state;
  }

  /**
   * Get the value as {@link BlockchainAddress}.
   *
   * @return value as {@link BlockchainAddress}
   */
  public BlockchainAddress blockchainAddressValue() {
    return (BlockchainAddress) state;
  }

  /**
   * Get the value as {@link Hash}.
   *
   * @return value as {@link Hash}
   */
  public Hash hashValue() {
    return (Hash) state;
  }

  /**
   * Get the value as {@link BlockchainPublicKey}.
   *
   * @return value as {@link BlockchainPublicKey}
   */
  public BlockchainPublicKey blockchainPublicKeyValue() {
    return (BlockchainPublicKey) state;
  }

  /**
   * Get the value as {@link Signature}.
   *
   * @return value as {@link Signature}
   */
  public Signature signatureValue() {
    return (Signature) state;
  }

  /**
   * Get the value as {@link BlsPublicKey}.
   *
   * @return value as {@link BlsPublicKey}
   */
  public BlsPublicKey blsPublicKeyValue() {
    return (BlsPublicKey) state;
  }

  /**
   * Get the value as {@link BlsSignature}.
   *
   * @return value as {@link BlsSignature}
   */
  public BlsSignature blsSignatureValue() {
    return (BlsSignature) state;
  }

  /**
   * Check if state has provided type.
   *
   * @param type to compare to state type
   * @param <T> type parameter for comparison
   * @return true if state has provided type, false otherwise
   */
  public <T> boolean isType(Class<T> type) {
    return objectHasType(state, type);
  }

  private <T> boolean objectHasType(Object object, Class<T> type) {
    return object.getClass().isAssignableFrom(type);
  }

  /**
   * Lookup in the tree for the value belonging to the given key.
   *
   * @param key the key to look up in the tree.
   * @param <K> the type of the key.
   * @return the value to the key wrapped in a {@link StateAccessor} or null if the tree does not
   *     contain the key.
   */
  @SuppressWarnings("unchecked")
  public <K extends Comparable<K>> StateAccessor getTreeValue(K key) {
    AvlTree<K, Object> avlTree = typedAvlTree(key.getClass(), Object.class);
    Object value = avlTree.getValue(key);
    return value != null ? new StateAccessor(value) : null;
  }

  /**
   * Get a list of all key value pairs in tree wrapped in {@link StateAccessor}. Key and value are
   * stored together in a {@link StateAccessorAvlLeafNode}.
   *
   * @return a list where all key value pairs are bound together in a {@link
   *     StateAccessorAvlLeafNode}.
   */
  public List<StateAccessorAvlLeafNode> getTreeLeaves() {
    ArrayList<StateAccessorAvlLeafNode> stateAccessors = new ArrayList<>();
    traverseTree(
        state,
        (leafKey, leafValue) ->
            stateAccessors.add(
                new StateAccessorAvlLeafNode(
                    new StateAccessor(leafKey), new StateAccessor(leafValue))));
    return stateAccessors;
  }

  /**
   * Get a {@link StateAccessor} for each element in the list. Assumes that the state is a {@link
   * FixedList} of {@link StateSerializable}.
   *
   * @return a {@link StateAccessor} for each element.
   */
  @SuppressWarnings("unchecked")
  public List<StateAccessor> getListElements() {
    List<StateAccessor> stateAccessors = new ArrayList<>();
    for (StateSerializable listElement : (FixedList<StateSerializable>) state) {
      stateAccessors.add(new StateAccessor(listElement));
    }
    return stateAccessors;
  }

  /**
   * Checks if the state is an {@link AvlTree} of the given type.
   *
   * @param keyClass the key type.
   * @param valueClass the value type.
   * @param <K> the key type.
   * @param <V> the value type.
   * @return true if the state is an {@link AvlTree} of the given key and value type.
   */
  public <K extends Comparable<K>, V> boolean isAvlTreeOfType(
      Class<K> keyClass, Class<V> valueClass) {
    if (!(state instanceof AvlTree)) {
      return false;
    }
    AtomicBoolean isCorrectType = new AtomicBoolean(true);
    traverseTree(
        state,
        (leafKey, leafValue) -> {
          boolean isCorrect =
              objectHasType(leafKey, keyClass) && objectHasType(leafValue, valueClass);
          if (!isCorrect) {
            isCorrectType.set(false);
          }
        });
    return isCorrectType.get();
  }

  /**
   * Checks if the current state is a {@link FixedList} of the given type.
   *
   * @param elementClass the class of the type
   * @param <E> the type
   * @return true if the state is a {@link FixedList} of the given type and false otherwise
   */
  @SuppressWarnings("unchecked")
  public <E> boolean isFixedListOfType(Class<E> elementClass) {
    if (!(state instanceof FixedList)) {
      return false;
    }
    for (Object listElement : (FixedList<Object>) state) {
      if (!objectHasType(listElement, elementClass)) {
        return false;
      }
    }
    return true;
  }

  /**
   * Check if instance of {@link String}.
   *
   * @return true if instance of {@link String}, false otherwise
   */
  public boolean isString() {
    return state instanceof String;
  }

  /**
   * Check if instance of {@link Integer}.
   *
   * @return true if instance of {@link Integer}, false otherwise
   */
  public boolean isInt() {
    return state instanceof Integer;
  }

  /**
   * Check if instance of {@link Long}.
   *
   * @return true if instance of {@link Long}, false otherwise
   */
  public boolean isLong() {
    return state instanceof Long;
  }

  /**
   * Check if instance of {@link Boolean}.
   *
   * @return true if instance of {@link Boolean}, false otherwise
   */
  public boolean isBoolean() {
    return state instanceof Boolean;
  }

  /**
   * Check if instance of {@link Byte}.
   *
   * @return true if instance of {@link Byte}, false otherwise
   */
  public boolean isByte() {
    return state instanceof Byte;
  }

  /**
   * Check if instance of {@link StateBoolean}.
   *
   * @return true if instance of {@link StateBoolean}, false otherwise
   */
  public boolean isStateBoolean() {
    return state instanceof StateBoolean;
  }

  /**
   * Check if instance of {@link StateLong}.
   *
   * @return true if instance of {@link StateLong}, false otherwise
   */
  public boolean isStateLong() {
    return state instanceof StateLong;
  }

  /**
   * Check if instance of {@link StateString}.
   *
   * @return true if instance of {@link StateString}, false otherwise
   */
  public boolean isStateString() {
    return state instanceof StateString;
  }

  /**
   * Check if instance of {@link BlockchainAddress}.
   *
   * @return true if instance of {@link BlockchainAddress}, false otherwise
   */
  public boolean isBlockchainAddress() {
    return state instanceof BlockchainAddress;
  }

  /**
   * Check if instance of {@link Hash}.
   *
   * @return true if instance of {@link Hash}, false otherwise
   */
  public boolean isHash() {
    return state instanceof Hash;
  }

  /**
   * Check if instance of {@link BlockchainPublicKey}.
   *
   * @return true if instance of {@link BlockchainPublicKey}, false otherwise
   */
  public boolean isBlockchainPublicKey() {
    return state instanceof BlockchainPublicKey;
  }

  /**
   * Check if instance of {@link Signature}.
   *
   * @return true if instance of {@link Signature}, false otherwise
   */
  public boolean isSignature() {
    return state instanceof Signature;
  }

  /**
   * Check if instance of {@link BlsPublicKey}.
   *
   * @return true if instance of {@link BlsPublicKey}, false otherwise
   */
  public boolean isBlsPublicKey() {
    return state instanceof BlsPublicKey;
  }

  /**
   * Check if instance of {@link BlsSignature}.
   *
   * @return true if instance of {@link BlsSignature}, false otherwise
   */
  public boolean isBlsSignature() {
    return state instanceof BlsSignature;
  }

  private void traverseTree(Object tree, BiConsumer<Object, Object> consumeLeaf) {
    AvlTree<?, ?> t = (AvlTree<?, ?>) tree;
    traverseTree(t, consumeLeaf);
  }

  private <K extends Comparable<K>, V> void traverseTree(
      AvlTree<K, V> tree, BiConsumer<Object, Object> consumeLeaf) {
    for (K key : tree.keySet()) {
      V value = tree.getValue(key);
      consumeLeaf.accept(key, value);
    }
  }
}
