package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Hash.HashAndSize;
import com.partisiablockchain.serialization.SerializationContext.Serializer;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

final class SerializerFixedList implements Serializer<FixedList<?>> {

  private final SerializationContext context;
  private final HashAndSize hash;
  private final FixedList<?> list;
  private final StateStorage storage;
  private final Class<?> typeArgument;
  private final List<Object> shallowValues;

  SerializerFixedList(
      SerializationContext context, StateStorage storage, FixedList<?> list, Type genericType) {
    this.context = context;
    this.storage = storage;
    this.typeArgument = getListArgument(genericType);
    this.list = list;
    this.shallowValues =
        list.stream().map(o -> context.convertValue(o, typeArgument)).collect(Collectors.toList());
    int subSize =
        list.stream().mapToInt(value -> context.sizeIfSub(value, typeArgument, typeArgument)).sum();
    this.hash = Hash.createSize(this::writeListValues).withSubSize(subSize);
  }

  SerializerFixedList(
      SerializationContext context, StateStorage storage, Hash hash, Type genericType) {
    this.context = context;
    this.storage = storage;
    this.typeArgument = getListArgument(genericType);
    this.list = null;
    this.shallowValues = null;
    this.hash = hash.withZeroSize();
  }

  private Class<?> getListArgument(Type genericType) {
    ParameterizedType parameters = (ParameterizedType) genericType;
    Type actualTypeArgument = parameters.getActualTypeArguments()[0];
    return context.getActualType(actualTypeArgument);
  }

  @Override
  public HashAndSize hash() {
    return hash;
  }

  @Override
  @SuppressWarnings("unchecked")
  public FixedList<?> read() {
    Class<Object> valueType = (Class<Object>) typeArgument;
    List<Object> objects =
        storage.read(
            hash.hash(),
            stream -> {
              int length = stream.readInt();
              MaskReader maskReader =
                  new MaskReader(stream.readBytes(BitMask.bytesNeededForBits(length)));
              List<Object> read = new ArrayList<>(length);
              for (int i = 0; i < length; i++) {
                if (maskReader.next()) {
                  read.add(
                      SerializationContext.readNonNullForType(
                          stream,
                          valueType,
                          context.getTypeParameters(),
                          context.supportRecordDeserialization()));
                } else {
                  read.add(null);
                }
              }
              return read;
            });
    return FixedList.create(objects.stream().map(o -> context.readIfSub(o, valueType, valueType)));
  }

  @Override
  public void write() {
    boolean updated = storage.write(hash.hash(), this::writeListValues);
    if (updated) {
      for (Object value : list) {
        context.writeIfSub(value, typeArgument, typeArgument);
      }
    }
  }

  private void writeListValues(SafeDataOutputStream s) {
    s.writeInt(shallowValues.size());
    s.write(createBitMask());
    for (Object value : shallowValues) {
      context.writeIfNotNullForType(s, typeArgument, value);
    }
  }

  private byte[] createBitMask() {
    BitMask mask = new BitMask();
    for (Object o : list) {
      mask.write(o != null);
    }
    return mask.asBytes();
  }
}
