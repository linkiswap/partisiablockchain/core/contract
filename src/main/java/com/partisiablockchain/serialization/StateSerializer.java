package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeCollectionStream;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.FunctionUtility;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * Serializes blockchain serializable objects. This can be simple types and anything that implements
 * {@link StateSerializable}.
 */
public final class StateSerializer {

  private static final Hash EMPTY =
      Hash.fromString("0000000000000000000000000000000000000000000000000000000000000000");
  static Map<Class<?>, SerializationHandler<?>> SIMPLE_TYPES;

  static {
    HashMap<Class<?>, SerializationHandler<?>> map = new HashMap<>();
    map.put(
        Void.class,
        new SerializationHandler<>(
            FunctionUtility.noOpBiConsumer(), FunctionUtility.nullFunction()));
    map.put(
        Integer.TYPE,
        new SerializationHandler<>(
            SafeCollectionStream.primitive(SafeDataOutputStream::writeInt),
            SafeDataInputStream::readInt));
    map.put(
        Integer.class,
        new SerializationHandler<>(
            SafeCollectionStream.primitive(SafeDataOutputStream::writeInt),
            SafeDataInputStream::readInt));
    map.put(
        Boolean.TYPE,
        new SerializationHandler<>(
            SafeCollectionStream.primitive(SafeDataOutputStream::writeBoolean),
            SafeDataInputStream::readBoolean));
    map.put(
        Boolean.class,
        new SerializationHandler<>(
            SafeCollectionStream.primitive(SafeDataOutputStream::writeBoolean),
            SafeDataInputStream::readBoolean));
    map.put(
        StateBoolean.class,
        new SerializationHandler<>(
            SafeCollectionStream.primitive(
                (SafeDataOutputStream stream, StateBoolean object) ->
                    stream.writeBoolean(object.value())),
            safeDataInputStream -> new StateBoolean(safeDataInputStream.readBoolean())));
    map.put(
        Byte.TYPE,
        new SerializationHandler<>(
            SafeCollectionStream.primitive(
                (BiConsumer<SafeDataOutputStream, Byte>) SafeDataOutputStream::writeByte),
            SafeDataInputStream::readSignedByte));
    map.put(
        Byte.class,
        new SerializationHandler<>(
            SafeCollectionStream.primitive(
                (BiConsumer<SafeDataOutputStream, Byte>) SafeDataOutputStream::writeByte),
            SafeDataInputStream::readSignedByte));
    map.put(
        Long.TYPE,
        new SerializationHandler<>(
            SafeCollectionStream.primitive(SafeDataOutputStream::writeLong),
            SafeDataInputStream::readLong));
    map.put(
        Long.class,
        new SerializationHandler<>(
            SafeCollectionStream.primitive(SafeDataOutputStream::writeLong),
            SafeDataInputStream::readLong));
    map.put(
        StateLong.class,
        new SerializationHandler<>(
            SafeCollectionStream.primitive(
                (SafeDataOutputStream stream, StateLong object) ->
                    stream.writeLong(object.value())),
            safeDataInputStream -> new StateLong(safeDataInputStream.readLong())));
    map.put(
        String.class,
        new SerializationHandler<>(
            SafeCollectionStream.primitive(SafeDataOutputStream::writeString),
            SafeDataInputStream::readString));
    map.put(
        StateString.class,
        new SerializationHandler<>(
            SafeCollectionStream.primitive(
                (SafeDataOutputStream stream, StateString object) ->
                    stream.writeString(object.value())),
            safeDataInputStream -> new StateString(safeDataInputStream.readString())));
    map.put(
        UUID.class,
        new SerializationHandler<>(
            SafeCollectionStream.primitive(SafeDataOutputStream::writeUuid),
            SafeDataInputStream::readUuid));
    map.put(
        BlockchainAddress.class,
        new SerializationHandler<>(BlockchainAddress::write, BlockchainAddress::read));
    map.put(Hash.class, new SerializationHandler<>(Hash::write, Hash::read));
    map.put(
        BlockchainPublicKey.class,
        new SerializationHandler<>(BlockchainPublicKey::write, BlockchainPublicKey::read));
    map.put(Signature.class, new SerializationHandler<>(Signature::write, Signature::read));
    map.put(
        BlsPublicKey.class,
        new SerializationHandler<>(BlsPublicKey::write, BlsPublicKey::readUnsafe));
    map.put(
        BlsSignature.class,
        new SerializationHandler<>(BlsSignature::write, BlsSignature::readUnsafe));
    map.put(Unsigned256.class, new SerializationHandler<>(Unsigned256::write, Unsigned256::read));

    StateSerializer.SIMPLE_TYPES = Map.copyOf(map);
  }

  private final StateStorage storage;
  private final boolean supportRecordDeserialization;
  private final boolean freshStorage;

  /**
   * Create state serializer with storage.
   *
   * @param storage where serialized values will be saved to
   * @param supportRecordDeserialization whether {@link StateSerializable record deserialization
   *     support} should be enabled
   */
  @SuppressWarnings("WeakerAccess")
  public StateSerializer(StateStorage storage, boolean supportRecordDeserialization) {
    this(storage, supportRecordDeserialization, false);
  }

  /**
   * Create state serializer with storage.
   *
   * @param storage where serialized values will be saved to
   * @param supportRecordDeserialization whether {@link StateSerializable record deserialization
   *     support} should be enabled
   * @param freshStorage whether the provided storage is fresh, i.e. not the same as the object was
   *     read from.
   */
  public StateSerializer(
      StateStorage storage, boolean supportRecordDeserialization, boolean freshStorage) {
    this.storage = storage;
    this.supportRecordDeserialization = supportRecordDeserialization;
    this.freshStorage = freshStorage;
  }

  /**
   * Creates hash for a value.
   *
   * @param value the object to compute the hash from
   * @param parameters the type parameters
   * @return the hash
   */
  @SuppressWarnings("WeakerAccess")
  public Hash createHash(StateSerializable value, Class<?>... parameters) {
    SerializationContext serializationContext =
        new SerializationContext(storage, supportRecordDeserialization);
    serializationContext.addTypeParameters(value.getClass(), parameters);
    return serializationContext.createHash(value, value.getClass()).hash();
  }

  /**
   * Writes a value to storage.
   *
   * @param serializable the object to write
   * @param parameters the type parameters
   * @return the hash for the object
   */
  public SerializationResult write(StateSerializable serializable, Class<?>... parameters) {
    if (serializable == null) {
      return new SerializationResult(EMPTY.withZeroSize());
    } else {
      SerializationContext serializationContext =
          new SerializationContext(storage, supportRecordDeserialization, freshStorage);
      serializationContext.addTypeParameters(serializable.getClass(), parameters);
      return serializationContext.writeStateSerializable(serializable);
    }
  }

  /**
   * Create a proof that can be used to argue about the values of the supplied object.
   *
   * @param serializable the object whose values are to be proved
   * @param parameters the type parameters of the type
   * @return the constructed proof
   */
  public StateSerializableProof proof(StateSerializable serializable, Class<?>... parameters) {
    SerializationContext context = new SerializationContext(storage, supportRecordDeserialization);
    context.addTypeParameters(serializable.getClass(), parameters);
    SerializerStateSerializable<StateSerializable> serializer =
        new SerializerStateSerializable<>(context, storage, serializable);
    byte[] rootBytes = SafeDataOutputStream.serialize(serializer::writeObject);
    return StateSerializableProof.create(rootBytes, supportRecordDeserialization);
  }

  /**
   * Construct proof attesting to the existence of a specific key in a AvlTree.
   *
   * @param serializable the object containing the tree
   * @param field the name of the field containing the tree
   * @param key the key to attest existence of
   * @param parameters type parameters of the object
   * @return the constructed proof
   */
  @SuppressWarnings({"unchecked", "Immutable"})
  public AvlProof avlProof(
      StateSerializable serializable, String field, Hash key, Class<?>... parameters) {
    return ExceptionConverter.call(
        () -> {
          SerializationContext context =
              new SerializationContext(storage, supportRecordDeserialization);
          context.addTypeParameters(serializable.getClass(), parameters);
          Field declaredField = serializable.getClass().getDeclaredField(field);
          declaredField.setAccessible(true);
          AvlTree<Hash, ?> tree = (AvlTree<Hash, ?>) declaredField.get(serializable);
          SerializerAvlTree treeSerializer =
              new SerializerAvlTree(context, storage, tree, declaredField.getGenericType());
          return treeSerializer.generatePath(tree, key);
        },
        "Unable to produce proof");
  }

  /**
   * Reads a value from disc.
   *
   * @param hash the hash of the object read
   * @param clazz the class of the object to read
   * @param parameters the type parameters
   * @param <T> type of value to read
   * @return the hash for the object
   */
  public <T extends StateSerializable> T read(Hash hash, Class<T> clazz, Class<?>... parameters) {
    if (hash.equals(EMPTY)) {
      return null;
    } else {
      SerializationContext serializationContext =
          new SerializationContext(storage, supportRecordDeserialization);
      serializationContext.addTypeParameters(clazz, parameters);
      return serializationContext.read(clazz, clazz, hash);
    }
  }

  static final class SerializationHandler<T> {

    private final BiConsumer<T, SafeDataOutputStream> serialize;
    private final Function<SafeDataInputStream, T> deserialize;

    private SerializationHandler(
        BiConsumer<T, SafeDataOutputStream> serialize,
        Function<SafeDataInputStream, T> deserialize) {
      this.serialize = serialize;
      this.deserialize = deserialize;
    }

    @SuppressWarnings("unchecked")
    void write(Object o, SafeDataOutputStream stream) {
      serialize.accept((T) o, stream);
    }

    T read(SafeDataInputStream stream) {
      return deserialize.apply(stream);
    }
  }
}
