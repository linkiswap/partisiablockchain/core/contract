package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;

final class SerializerStateSerializableInline<T extends StateSerializableInline> {

  private final Class<T> type;
  private final List<SerializerStateSerializable.FieldAndValue> fieldAndValues;

  public SerializerStateSerializableInline(
      Class<T> type, List<SerializerStateSerializable.FieldAndValue> fieldAndValues) {
    this.type = type;
    this.fieldAndValues = fieldAndValues;
  }

  public static <T extends StateSerializableInline> SerializerStateSerializableInline<T> initialize(
      SafeDataInputStream stream,
      Class<T> type,
      SerializationContext.TypeParameterCache typeParameters,
      boolean supportRecordDeserialization) {
    List<SerializerStateSerializable.FieldAndValue> fieldAndValues =
        SerializerStateSerializable.readFromStream(
            type, stream, typeParameters, supportRecordDeserialization);
    return new SerializerStateSerializableInline<>(type, fieldAndValues);
  }

  static void write(SerializationContext context, SafeDataOutputStream stream, Object value) {
    SerializerStateSerializable.writeNonNull(context, stream, value);
  }

  @SuppressWarnings("unchecked")
  static <T> int subSize(SerializationContext context, Class<T> clazz, Object value) {
    return SerializerStateSerializable.subSize(context, clazz, (T) value);
  }

  @SuppressWarnings("unchecked")
  static <T> void writeSubs(SerializationContext context, Class<T> clazz, Object value) {
    SerializerStateSerializable.writeSubs(context, clazz, (T) value);
  }

  T instantiateWithSubs(SerializationContext context) {
    return SerializerStateSerializable.readSubsAndInstantiate(context, type, fieldAndValues);
  }
}
