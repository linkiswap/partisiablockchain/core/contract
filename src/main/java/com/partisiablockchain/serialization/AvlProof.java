package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.util.DataStreamLimit;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeCollectionStream;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.stream.SafeListStream;
import java.util.List;
import java.util.stream.Collectors;

/** A proof that an AvlTree contains a specific key. */
public final class AvlProof implements DataStreamSerializable {

  private static final SafeListStream<byte[]> BYTES_SERIALIZER =
      SafeListStream.create(
          DataStreamLimit::readDynamicBytes,
          SafeCollectionStream.primitive(SafeDataOutputStream::writeDynamicBytes));

  private final List<byte[]> composites;
  private final byte[] leaf;

  private final Hash rootIdentifier;
  private final Hash key;

  private AvlProof(List<byte[]> composites, byte[] leaf) {
    this.composites = composites;
    this.leaf = leaf;
    this.key = verifyLeafAndGetKey(leaf);
    this.rootIdentifier = verifyAndGetIdentifier(composites, leaf);
  }

  private static Hash verifyAndGetIdentifier(List<byte[]> composites, byte[] leaf) {
    Hash previous = Hash.create(s -> s.write(leaf));
    for (int i = composites.size() - 1; i >= 0; i--) {
      byte[] bytes = composites.get(i);
      verifyInParentComposite(bytes, previous);
      previous = Hash.create(s -> s.write(bytes));
    }
    return previous;
  }

  /**
   * Create a AvlProof with the supplied path.
   *
   * @param composites the composites in the tree from root to leaf
   * @param leaf the leaf containing the key to attest
   * @return a new AvlProof
   */
  public static AvlProof create(List<byte[]> composites, byte[] leaf) {
    return new AvlProof(
        composites.stream().map(byte[]::clone).collect(Collectors.toList()), leaf.clone());
  }

  /**
   * Get the key that this proof attests the existence of.
   *
   * @return the attested key
   */
  public Hash getKey() {
    return key;
  }

  /**
   * Get the identifier of the root of this tree.
   *
   * @return the tree root
   */
  public Hash getRootIdentifier() {
    return rootIdentifier;
  }

  private static void verifyInParentComposite(byte[] composite, Hash previous) {
    SafeDataInputStream reader = SafeDataInputStream.createFromBytes(composite);
    byte header = reader.readSignedByte();
    if (header != 1) {
      throw new IllegalStateException("Wrong header for composite. Expected 1 got " + header);
    }
    Hash leftChild = Hash.read(reader);
    Hash rightChild = Hash.read(reader);
    if (!previous.equals(leftChild) && !previous.equals(rightChild)) {
      throw new IllegalStateException("Child node was not present in supplied parent");
    }
  }

  private static Hash verifyLeafAndGetKey(byte[] leaf) {
    SafeDataInputStream reader = SafeDataInputStream.createFromBytes(leaf);
    byte header = reader.readSignedByte();
    if (header != 0) {
      throw new IllegalStateException("Wrong header for leaf. Expected 0 got " + header);
    }
    return Hash.read(reader);
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    BYTES_SERIALIZER.writeDynamic(stream, composites);
    stream.writeDynamicBytes(leaf);
  }

  /**
   * Read an AvlProof from the supplied stream.
   *
   * @param stream the stream to read from
   * @return the read proof
   */
  public static AvlProof read(SafeDataInputStream stream) {
    List<byte[]> treeBytes = DataStreamLimit.readDynamicList(stream, BYTES_SERIALIZER);
    byte[] leaf = DataStreamLimit.readDynamicBytes(stream);
    return new AvlProof(treeBytes, leaf);
  }
}
