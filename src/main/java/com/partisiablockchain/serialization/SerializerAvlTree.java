package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.ImmutableTypeParameter;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Hash.HashAndSize;
import com.partisiablockchain.serialization.SerializationContext.Serializer;
import com.partisiablockchain.tree.AvlTree;
import com.partisiablockchain.tree.TreePathVisitor;
import com.secata.stream.SafeDataOutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

final class SerializerAvlTree implements Serializer<AvlTree<?, ?>> {

  private final SerializationContext context;
  private final StateStorage storage;
  private final Type genericType;

  private final Hash hash;
  private final AvlTree<?, ?> value;

  /** Constructor for writing. */
  SerializerAvlTree(
      SerializationContext context, StateStorage storage, AvlTree<?, ?> value, Type genericType) {
    this.context = context;
    this.storage = storage;
    this.genericType = genericType;

    this.hash = null;
    this.value = value;
  }

  /** Constructor for reading. */
  SerializerAvlTree(
      SerializationContext context, StateStorage storage, Hash hash, Type genericType) {
    this.context = context;
    this.storage = storage;
    this.genericType = genericType;

    this.hash = hash;
    this.value = null;
  }

  @Override
  @SuppressWarnings("Immutable")
  public HashAndSize hash() {
    return hashTree(value);
  }

  @Override
  @SuppressWarnings("Immutable")
  public AvlTree<?, ?> read() {
    return readTree();
  }

  @Override
  @SuppressWarnings("Immutable")
  public void write() {
    writeTree(value);
  }

  public <@ImmutableTypeParameter V> AvlProof generatePath(AvlTree<Hash, V> tree, Hash key) {
    StateTreeWriter<Hash, V> writer = new StateTreeWriter<>(context, storage, genericType);
    List<byte[]> elements = new ArrayList<>();
    ProofGenerator<V> proofGenerator = new ProofGenerator<V>(context, writer, elements);
    tree.visitPath(writer, key, proofGenerator);
    return proofGenerator.getProof();
  }

  private <@ImmutableTypeParameter S extends Comparable<S>, @ImmutableTypeParameter T>
      AvlTree<S, T> readTree() {
    StateTreeReader<S, T> reader = new StateTreeReader<>(context, storage, genericType);
    return reader.readNode(hash);
  }

  private <@ImmutableTypeParameter K extends Comparable<K>, @ImmutableTypeParameter V>
      HashAndSize hashTree(AvlTree<K, V> root) {
    StateTreeWriter<K, V> writer = new StateTreeWriter<>(context, storage, genericType);
    return root.calculateHash(writer);
  }

  private <@ImmutableTypeParameter K extends Comparable<K>, @ImmutableTypeParameter V>
      void writeTree(AvlTree<K, V> root) {
    StateTreeWriter<K, V> writer = new StateTreeWriter<>(context, storage, genericType);
    if (context.isFreshStorage()) {
      root.freshWrite(writer);
    } else {
      root.write(writer);
    }
  }

  private static final class ProofGenerator<V> implements TreePathVisitor<Hash, V> {

    private final SerializationContext context;
    private final StateTreeWriter<Hash, V> writer;
    private final List<byte[]> elements;
    private byte[] leaf;

    public ProofGenerator(
        SerializationContext context, StateTreeWriter<Hash, V> writer, List<byte[]> elements) {
      this.context = context;
      this.writer = writer;
      this.elements = elements;
    }

    @Override
    public void leaf(Hash key, V value) {
      Object keyValue = context.convertValue(key, writer.keyType);
      Object valueValue = context.convertValue(value, writer.valueType);
      leaf = SafeDataOutputStream.serialize(s -> writer.leafToStream(s, keyValue, valueValue));
    }

    @Override
    public void composite(HashAndSize hash, Hash left, Hash right, int size, byte height) {
      elements.add(
          SafeDataOutputStream.serialize(
              s -> writer.compositeToStream(s, left, right, size, height, hash.size())));
    }

    public AvlProof getProof() {
      return AvlProof.create(elements, leaf);
    }
  }
}
