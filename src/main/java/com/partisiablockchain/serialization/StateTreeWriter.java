package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Hash.HashAndSize;
import com.partisiablockchain.tree.TreeWriter;
import com.secata.stream.SafeDataOutputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

final class StateTreeWriter<K extends Comparable<K>, V> implements TreeWriter<K, V> {

  static final int COMPOSITE_SIZE = 2 * Byte.BYTES + 2 * Hash.BYTES + 2 * Integer.BYTES;
  private final SerializationContext context;
  private final StateStorage storage;
  final Class<K> keyType;
  final Class<V> valueType;

  @SuppressWarnings("unchecked")
  StateTreeWriter(SerializationContext context, StateStorage storage, Type genericType) {
    this.context = context;
    this.storage = storage;
    ParameterizedType type = (ParameterizedType) genericType;
    Type[] typeArguments = type.getActualTypeArguments();
    this.keyType = (Class<K>) context.getActualType(typeArguments[0]);
    this.valueType = (Class<V>) context.getActualType(typeArguments[1]);
  }

  @Override
  public void writeEmpty(HashAndSize hash) {
    storage.write(hash.hash(), this::emptyToStream);
  }

  @Override
  public void writeLeaf(HashAndSize hash, K key, V value) {
    Object keyValue = context.convertValue(key, keyType);
    Object valueValue = context.convertValue(value, valueType);
    boolean updated = storage.write(hash.hash(), s -> leafToStream(s, keyValue, valueValue));
    if (updated) {
      context.writeIfSub(key, keyType, keyType);
      context.writeIfSub(value, valueType, valueType);
    }
  }

  @Override
  public void writeComposite(HashAndSize hash, Hash left, Hash right, int size, byte height) {
    storage.write(hash.hash(), s -> compositeToStream(s, left, right, size, height, hash.size()));
  }

  @Override
  public HashAndSize hashEmpty() {
    return Hash.createSize(this::emptyToStream);
  }

  @Override
  public HashAndSize hashLeaf(K key, V value) {
    Object keyValue = context.convertValue(key, keyType);
    Object valueValue = context.convertValue(value, valueType);
    int subSize =
        context.sizeIfSub(key, keyType, keyType) + context.sizeIfSub(value, valueType, valueType);
    return Hash.createSize(safeStream -> leafToStream(safeStream, keyValue, valueValue))
        .withSubSize(subSize);
  }

  @Override
  public HashAndSize hashComposite(HashAndSize left, HashAndSize right, int size, byte height) {
    int subSize = left.size() + right.size();
    return Hash.createSize(
            safeStream ->
                compositeToStream(
                    safeStream, left.hash(), right.hash(), size, height, subSize + COMPOSITE_SIZE))
        .withSubSize(subSize);
  }

  private void emptyToStream(SafeDataOutputStream stream) {
    stream.writeByte((byte) 2);
  }

  void compositeToStream(
      SafeDataOutputStream stream, Hash left, Hash right, int size, byte height, int storageSize) {
    stream.writeByte((byte) 1);
    left.write(stream);
    right.write(stream);
    stream.writeInt(size);
    stream.writeByte(height);
    stream.writeInt(storageSize);
  }

  void leafToStream(SafeDataOutputStream stream, Object keyValue, Object valueValue) {
    stream.writeByte((byte) 0);
    context.writeIfNotNullForType(stream, keyType, keyValue);
    context.writeIfNotNullForType(stream, valueType, valueValue);
  }
}
