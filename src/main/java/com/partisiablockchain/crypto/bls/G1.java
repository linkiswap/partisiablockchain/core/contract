package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.Hash;
import java.math.BigInteger;
import java.util.Objects;

/**
 * Defines the subgroup G1 of order r over the curve E(Fq) for E : y^2 = x^3 + 4. No considerations
 * are taken wrt. protections against timing attacks.
 */
@Immutable
public final class G1 implements EllipticCurve<G1> {

  private final Fq vx;
  private final Fq vy;
  private final Fq vz;

  /** The byte size of a compressed G1 point. */
  public static final int BYTE_SIZE = Fq.BYTES;

  private static final CurveHelper<Fq> helper =
      new CurveHelper<>(Fq::createConstant, Fq.createConstant(4));

  /**
   * G1 Generator. See https://datatracker.ietf.org/doc/draft-irtf-cfrg-pairing-friendly-curves/09/
   */
  public static final G1 G =
      new G1(
          Fq.create(
              new BigInteger(
                  "36854167537133870167810883151830777579616207957825464"
                      + "09894578378688607592378376318836054947676345821548104185464507")),
          Fq.create(
              new BigInteger(
                  "13395065449444764730204713799419212215849338759383496"
                      + "20426543736416511423956333506472724655353366534992391756441569")),
          Fq.createOne());

  /**
   * (x, y, z) -> (x/z, y/z), so any point with z = 0 is a point at infinity. The definition of the
   * set of homogenous points mandate that not all are 0, so one typically sets y = 1.
   */
  static final G1 POINT_AT_INFINITY = new G1(Fq.createZero(), Fq.createOne(), Fq.createZero());

  /**
   * Deserializes a byte array into a G1 point.
   *
   * @param bytes to be Deserialized
   * @return G1 point
   */
  public static G1 create(byte[] bytes) {
    G1 deserialized = createUnsafe(bytes);
    if (!deserialized.isPointAtInfinity()) {
      deserialized.validatePoint();
    }
    return deserialized;
  }

  /**
   * Deserializes a byte array into a G1 point without validating that the point is on the correct
   * curve or has the right order.
   *
   * @param bytes to be Deserialized
   * @return G1 point
   */
  public static G1 createUnsafe(byte[] bytes) {
    if (bytes.length == 0) {
      throw new IllegalArgumentException("Cannot deserialize 0 bytes");
    }
    boolean isCompressed = ((bytes[0] >> 7) & 1) == 1;
    int expectedLength = isCompressed ? Fq.BYTES : 2 * Fq.BYTES;

    if (bytes.length != expectedLength) {
      throw new IllegalArgumentException(
          "Expected " + expectedLength + " bytes, but got " + bytes.length);
    }

    if (isPointInfinity(bytes)) {
      return POINT_AT_INFINITY;
    }

    Fq x = createPointX(bytes);
    Fq y = createPointY(bytes, isCompressed, x);

    return new G1(x, y, Fq.createOne());
  }

  /** Validate that the point is on the correct curve and has the right order. */
  public void validatePoint() {
    if (!helper.isOnCurve(vx, vy)) {
      throw new IllegalStateException("Deserialized point is not on the curve");
    }

    if (!scalePoint(Pairing.order).equals(POINT_AT_INFINITY)) {
      throw new IllegalStateException("Deserialized point is in an invalid subgroup");
    }
  }

  private static boolean isPointInfinity(byte[] bytes) {
    boolean isPointAtInfinity = ((bytes[0] >> 6) & 1) == 1;
    if (isPointAtInfinity) {
      if (helper.validateSerializedPointAtInfinity(bytes)) {
        return true;
      } else {
        throw new IllegalStateException("Invalid serialized element");
      }
    }
    return false;
  }

  private static Fq createPointX(byte[] bytes) {
    byte[] vxBytes = new byte[Fq.BYTES];
    System.arraycopy(bytes, 0, vxBytes, 0, Fq.BYTES);
    // clear top three bits before the element is created.
    vxBytes[0] = (byte) (vxBytes[0] & 0x1F);
    return Fq.create(vxBytes);
  }

  private static Fq createPointY(byte[] bytes, boolean isCompressed, Fq x) {
    Fq y;
    if (isCompressed) {
      boolean selectGreatest = ((bytes[0] >> 5) & 1) == 1;
      y = helper.extractSecondCoordinateFromFirst(x, selectGreatest);
    } else {
      byte[] vyBytes = new byte[Fq.BYTES];
      System.arraycopy(bytes, Fq.BYTES, vyBytes, 0, Fq.BYTES);
      y = Fq.create(vyBytes);
    }
    return y;
  }

  G1(Fq vx, Fq vy, Fq vz) {
    this.vx = vx;
    this.vy = vy;
    this.vz = vz;
  }

  /**
   * Generate hash of message.
   *
   * @param message to hash
   * @return hash of message
   */
  public static G1 hash(Hash message) {
    return G1Hasher.hash(message);
  }

  Fq[] toAffine() {
    Fq vzInv = vz.invert();
    return new Fq[] {vx.multiply(vzInv), vy.multiply(vzInv)};
  }

  @Override
  public G1 addPoint(G1 other) {
    if (isPointAtInfinity()) {
      return other;
    } else if (other.isPointAtInfinity()) {
      return this;
    } else {
      CurveHelper.CoordinatePack<Fq> result =
          helper.addPoints(vx, vy, vz, other.vx, other.vy, other.vz);
      if (result == null) {
        return POINT_AT_INFINITY;
      } else {
        return new G1(result.vx, result.vy, result.vz);
      }
    }
  }

  @Override
  public G1 doublePoint() {
    if (isPointAtInfinity()) {
      return POINT_AT_INFINITY;
    } else {
      CurveHelper.CoordinatePack<Fq> result = helper.doublePoint(vx, vy, vz);
      return new G1(result.vx, result.vy, result.vz);
    }
  }

  @Override
  public G1 scalePoint(NafEncoded scalar) {
    G1 result = POINT_AT_INFINITY;
    for (NafEncoded.Value value : scalar) {
      result = result.doublePoint();
      if (value == NafEncoded.Value.NEGATIVE) {
        result = result.addPoint(this.negatePoint());
      } else if (value == NafEncoded.Value.POSITIVE) {
        result = result.addPoint(this);
      }
    }
    return result;
  }

  @Override
  public G1 negatePoint() {
    if (vy.isZero()) {
      return POINT_AT_INFINITY;
    } else {
      return new G1(vx, vy.negate(), vz);
    }
  }

  @Override
  public byte[] serialize(boolean compressPoint) {
    if (isPointAtInfinity()) {
      return helper.serializePointAtInfinity(compressPoint, Fq.BYTES);
    } else {
      Fq[] coordinates = toAffine();
      return helper.serializePoint(coordinates[0], coordinates[1], compressPoint, Fq.BYTES);
    }
  }

  @Override
  public boolean isPointAtInfinity() {
    return equals(POINT_AT_INFINITY);
  }

  @Override
  public String toString() {
    if (isPointAtInfinity()) {
      return "G1{POINT_AT_INFINITY}";
    } else {
      Fq[] coordinates = toAffine();
      return "G1{X: " + coordinates[0] + ", Y: " + coordinates[1] + "}";
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    G1 other = (G1) o;
    return helper.equals(vx, vy, vz, other.vx, other.vy, other.vz);
  }

  @Override
  public int hashCode() {
    if (isPointAtInfinity()) {
      return 0;
    } else {
      // normalize coordinates before computing hash
      Fq[] affine = toAffine();
      return Objects.hash(affine[0], affine[1]);
    }
  }
}
