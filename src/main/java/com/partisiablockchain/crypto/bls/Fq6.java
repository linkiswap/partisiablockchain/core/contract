package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.math.BigInteger;
import java.util.Objects;

/** Extension of degree 6 over Fq. Implemented as Fq6 = Fq2[Y]/(Y^3 - X + 1). */
final class Fq6 implements FiniteField<Fq6> {

  final Fq2 a0;
  final Fq2 a1;
  final Fq2 a2;

  static final int BYTE_SIZE = 3 * Fq2.BYTE_SIZE;

  private static final Fq2 FROBENIUS_CONST_1 =
      new Fq2(
          Fq.createZero(),
          Fq.create(
              new BigInteger(
                  "1a0111ea397fe699ec02408663d4de85aa0d857d89759ad4897d29650fb85f9b409427eb4f49"
                      + "fffd8bfd00000000aaac",
                  16)));

  private static final Fq2 FROBENIUS_CONST_2 =
      new Fq2(
          Fq.create(
              new BigInteger(
                  "1a0111ea397fe699ec02408663d4de85aa0d857d89759ad4897d29650fb85f9b409427eb4f49"
                      + "fffd8bfd00000000aaad",
                  16)),
          Fq.createZero());

  private static final Fq2 FROBENIUS_Fq12_HELPER =
      new Fq2(
          Fq.create(
              new BigInteger(
                  "1904d3bf02bb0667c231beb4202c0d1f0fd603fd3cbd5f4f7b2443d784bab9c4f67ea53d63e7"
                      + "813d8d0775ed92235fb8",
                  16)),
          Fq.create(
              new BigInteger(
                  "00fc3e2b36c4e03288e9e902231f9fb854a14787b6c7b36fec0c8ec971f63c5f282d5ac14d6c"
                      + "7ec22cf78a126ddc4af3",
                  16)));

  static Fq6 createZero() {
    return new Fq6(Fq2.createZero(), Fq2.createZero(), Fq2.createZero());
  }

  static Fq6 createOne() {
    return new Fq6(Fq2.createOne(), Fq2.createZero(), Fq2.createZero());
  }

  static Fq6 create(byte[] bytes) {
    if (bytes.length != Fq6.BYTE_SIZE) {
      throw new IllegalStateException(
          "Cannot create Fq6 element from " + bytes.length + " bytes. Expected " + Fq6.BYTE_SIZE);
    }
    byte[] first = new byte[Fq2.BYTE_SIZE];
    System.arraycopy(bytes, 0, first, 0, Fq2.BYTE_SIZE);
    byte[] second = new byte[Fq2.BYTE_SIZE];
    System.arraycopy(bytes, Fq2.BYTE_SIZE, second, 0, Fq2.BYTE_SIZE);
    byte[] third = new byte[Fq2.BYTE_SIZE];
    System.arraycopy(bytes, 2 * Fq2.BYTE_SIZE, third, 0, Fq2.BYTE_SIZE);
    return new Fq6(Fq2.create(first), Fq2.create(second), Fq2.create(third));
  }

  Fq6(Fq2 a0, Fq2 a1, Fq2 a2) {
    this.a0 = a0;
    this.a1 = a1;
    this.a2 = a2;
  }

  @Override
  public Fq6 add(Fq6 other) {
    return new Fq6(a0.add(other.a0), a1.add(other.a1), a2.add(other.a2));
  }

  @Override
  public Fq6 subtract(Fq6 other) {
    return new Fq6(a0.subtract(other.a0), a1.subtract(other.a1), a2.subtract(other.a2));
  }

  @Override
  public Fq6 negate() {
    return new Fq6(a0.negate(), a1.negate(), a2.negate());
  }

  @Override
  public Fq6 multiply(Fq6 other) {
    // Follows from §5 "The case of multiplication over F_{p^6}"
    // by P.Longa https://eprint.iacr.org/2022/367.pdf
    Fq otherA1011 = other.a1.a0.add(other.a1.a1);
    Fq otherS1011 = other.a1.a0.subtract(other.a1.a1);
    Fq otherA2021 = other.a2.a0.add(other.a2.a1);
    Fq otherS2021 = other.a2.a0.subtract(other.a2.a1);
    Fq a0a1Negated = this.a0.a1.negate();
    Fq a1a1Negated = this.a1.a1.negate();
    Fq a2a1Negated = this.a2.a1.negate();
    return new Fq6(
        new Fq2(
            Fq.interleavedMontgomeryMultiplication(
                new Fq[] {
                  this.a0.a0, a0a1Negated,
                  this.a1.a0, a1a1Negated,
                  this.a2.a0, a2a1Negated
                },
                new Fq[] {
                  other.a0.a0, other.a0.a1, otherS2021, otherA2021, otherS1011, otherA1011
                }),
            Fq.interleavedMontgomeryMultiplication(
                new Fq[] {this.a0.a0, this.a0.a1, this.a1.a0, this.a1.a1, this.a2.a0, this.a2.a1},
                new Fq[] {
                  other.a0.a1, other.a0.a0, otherA2021, otherS2021, otherA1011, otherS1011
                })),
        new Fq2(
            Fq.interleavedMontgomeryMultiplication(
                new Fq[] {
                  this.a0.a0, a0a1Negated,
                  this.a1.a0, a1a1Negated,
                  this.a2.a0, a2a1Negated
                },
                new Fq[] {
                  other.a1.a0, other.a1.a1, other.a0.a0, other.a0.a1, otherS2021, otherA2021
                }),
            Fq.interleavedMontgomeryMultiplication(
                new Fq[] {this.a0.a0, this.a0.a1, this.a1.a0, this.a1.a1, this.a2.a0, this.a2.a1},
                new Fq[] {
                  other.a1.a1, other.a1.a0, other.a0.a1, other.a0.a0, otherA2021, otherS2021
                })),
        new Fq2(
            Fq.interleavedMontgomeryMultiplication(
                new Fq[] {
                  this.a0.a0, a0a1Negated,
                  this.a1.a0, a1a1Negated,
                  this.a2.a0, a2a1Negated
                },
                new Fq[] {
                  other.a2.a0, other.a2.a1, other.a1.a0, other.a1.a1, other.a0.a0, other.a0.a1
                }),
            Fq.interleavedMontgomeryMultiplication(
                new Fq[] {this.a0.a0, this.a0.a1, this.a1.a0, this.a1.a1, this.a2.a0, this.a2.a1},
                new Fq[] {
                  other.a2.a1, other.a2.a0, other.a1.a1, other.a1.a0, other.a0.a1, other.a0.a0
                })));
  }

  @Override
  public Fq6 square() {
    return multiply(this);
  }

  /**
   * Sparse multiplication. Multiplies this element by the sparse Fq6 element [c0 c1 c2] where only
   * c0 and c1 are non zero
   *
   * @param c0 The value c0 set in C ∈ Fq6
   * @param c1 The value c1 set in C ∈ Fq6
   * @return the product of the inputs
   */
  public Fq6 multiplyBy01(Fq2 c0, Fq2 c1) {
    Fq otherA1011 = c1.a0.add(c1.a1);
    Fq otherS1011 = c1.a0.subtract(c1.a1);
    Fq thisA1A1Negated = this.a1.a1.negate();
    Fq thisA2A1Negated = this.a2.a1.negate();
    Fq thisA0A1negated = this.a0.a1.negate();
    return new Fq6(
        new Fq2(
            Fq.interleavedMontgomeryMultiplication(
                new Fq[] {this.a0.a0, thisA0A1negated, this.a2.a0, thisA2A1Negated},
                new Fq[] {c0.a0, c0.a1, otherS1011, otherA1011}),
            Fq.interleavedMontgomeryMultiplication(
                new Fq[] {this.a0.a0, this.a0.a1, this.a2.a0, this.a2.a1},
                new Fq[] {c0.a1, c0.a0, otherA1011, otherS1011})),
        new Fq2(
            Fq.interleavedMontgomeryMultiplication(
                new Fq[] {this.a0.a0, thisA0A1negated, this.a1.a0, thisA1A1Negated},
                new Fq[] {c1.a0, c1.a1, c0.a0, c0.a1}),
            Fq.interleavedMontgomeryMultiplication(
                new Fq[] {this.a0.a0, this.a0.a1, this.a1.a0, this.a1.a1},
                new Fq[] {c1.a1, c1.a0, c0.a1, c0.a0})),
        new Fq2(
            Fq.interleavedMontgomeryMultiplication(
                new Fq[] {this.a1.a0, thisA1A1Negated, this.a2.a0, thisA2A1Negated},
                new Fq[] {c1.a0, c1.a1, c0.a0, c0.a1}),
            Fq.interleavedMontgomeryMultiplication(
                new Fq[] {this.a1.a0, this.a1.a1, this.a2.a0, this.a2.a1},
                new Fq[] {c1.a1, c1.a0, c0.a1, c0.a0})));
  }

  /**
   * Sparse multiplication. Multiplies this element by the sparse Fq6 element [c0 c1 c2] where only
   * c1 are non zero.
   *
   * @param c1 The only value set in C ∈ Fq6
   * @return the product of the inputs
   */
  public Fq6 multiplyBy1(Fq2 c1) {
    Fq otherA1011 = c1.a0.add(c1.a1);
    Fq otherS1011 = c1.a0.subtract(c1.a1);
    return new Fq6(
        new Fq2(
            Fq.interleavedMontgomeryMultiplication(
                new Fq[] {this.a2.a0, this.a2.a1.negate()}, new Fq[] {otherS1011, otherA1011}),
            Fq.interleavedMontgomeryMultiplication(
                new Fq[] {this.a2.a0, this.a2.a1}, new Fq[] {otherA1011, otherS1011})),
        new Fq2(
            Fq.interleavedMontgomeryMultiplication(
                new Fq[] {this.a0.a0, this.a0.a1.negate()}, new Fq[] {c1.a0, c1.a1}),
            Fq.interleavedMontgomeryMultiplication(
                new Fq[] {this.a0.a0, this.a0.a1}, new Fq[] {c1.a1, c1.a0})),
        new Fq2(
            Fq.interleavedMontgomeryMultiplication(
                new Fq[] {this.a1.a0, this.a1.a1.negate()}, new Fq[] {c1.a0, c1.a1}),
            Fq.interleavedMontgomeryMultiplication(
                new Fq[] {this.a1.a0, this.a1.a1}, new Fq[] {c1.a1, c1.a0})));
  }

  @Override
  public Fq6 invert() {
    // See: https://github.com/zkcrypto/bls12_381/blob/main/src/fp6.rs#L218
    Fq2 temp0 = a1.multiply(a2).multiplyByNonResidue();
    temp0 = a0.square().subtract(temp0);

    Fq2 temp1 = a2.square().multiplyByNonResidue();
    temp1 = temp1.subtract(a0.multiply(a1));

    Fq2 temp2 = a1.square();
    temp2 = temp2.subtract(a0.multiply(a2));

    Fq2 part0 = a1.multiply(temp2);
    Fq2 part1 = a2.multiply(temp1);
    Fq2 temp = part0.add(part1).multiplyByNonResidue();
    temp = temp.add(a0.multiply(temp0));

    temp = temp.invert();
    return new Fq6(temp.multiply(temp0), temp.multiply(temp1), temp.multiply(temp2));
  }

  Fq6 multiplyByNonResidue() {
    return new Fq6(a2.multiplyByNonResidue(), a0, a1);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Fq6 fq6 = (Fq6) o;
    return a0.equals(fq6.a0) && a1.equals(fq6.a1) && a2.equals(fq6.a2);
  }

  @Override
  public int hashCode() {
    return Objects.hash(a0, a1, a2);
  }

  @Override
  public byte[] serialize() {
    byte[] bytes = new byte[Fq6.BYTE_SIZE];
    System.arraycopy(a0.serialize(), 0, bytes, 0, Fq2.BYTE_SIZE);
    System.arraycopy(a1.serialize(), 0, bytes, Fq2.BYTE_SIZE, Fq2.BYTE_SIZE);
    System.arraycopy(a2.serialize(), 0, bytes, 2 * Fq2.BYTE_SIZE, Fq2.BYTE_SIZE);
    return bytes;
  }

  @Override
  public boolean isZero() {
    return a0.isZero() && a1.isZero() && a2.isZero();
  }

  @Override
  public int compare(Fq6 other) {
    int c0 = a0.compare(other.a0);
    int c0square = c0 * c0;
    int c1 = a1.compare(other.a1);
    int c1square = c1 * c1;
    int c2 = a2.compare(other.a2);
    // - if c0 != 0, then c0^2 == 1 and so the two last terms disappear.
    // - if c0 == 0, but c1 != 0, then the first and last term disappear, and
    //     c0 + (1 - c0^2)·c1 + (1 - c0^2)·(1 - c1^2)·c2
    //        = 0 + 1·c1 + 1·0·c2
    //        = c1
    //   is returned.
    // - if c0 == 0, c1 == 0, but c2 != 0, then
    //     c0 + (1 - c0^2)·c1 + (1 - c0^2)·(1 - c1^2)·(c2 - c1)
    //        = 0 + (1 - 0)·0 + (1 - 0)·(1 - 0)·c2
    //        = 0 + 0 + 1^2·c2 = c2
    //  is returned.
    return c0 + (1 - c0square) * c1 + (1 - c0square) * (1 - c1square) * c2;
  }

  @Override
  public String toString() {
    return "Fq6{" + a0 + " + " + a1 + " * Y + " + a2 + " * Y^2}";
  }

  @Override
  public Fq6 sqrt() {
    throw new UnsupportedOperationException("Sqrt in Fq6 not supported");
  }

  Fq6 frobenius() {
    Fq2 c0 = a0.frobenius();
    Fq2 c1 = a1.frobenius();
    Fq2 c2 = a2.frobenius();
    // these constants come from
    // https://github.com/paulmillr/noble-bls12-381/blob/master/math.js#L483
    // but can also be computed manually by computing powers of the irreducible element in Fq2.
    c1 = c1.multiply(FROBENIUS_CONST_1);
    c2 = c2.multiply(FROBENIUS_CONST_2);
    return new Fq6(c0, c1, c2);
  }

  Fq6 frobeniusFq12Helper() {
    return new Fq6(
        a0.multiply(FROBENIUS_Fq12_HELPER),
        a1.multiply(FROBENIUS_Fq12_HELPER),
        a2.multiply(FROBENIUS_Fq12_HELPER));
  }
}
