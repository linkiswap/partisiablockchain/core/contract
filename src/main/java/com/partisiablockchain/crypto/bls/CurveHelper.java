package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.function.Function;

/**
 * Helper class for adding, doubling and checking equality of points in projective coordinates.
 *
 * @param <F> type of the field over which the curve is defined.
 */
final class CurveHelper<F extends FiniteField<F> & FieldWithMultiply3B<F>> {

  private final Function<Integer, F> constantProvider;
  private final F curveParamB;
  private final F curveParamA;

  CurveHelper(Function<Integer, F> constantProvider, F b, F a) {
    this.constantProvider = constantProvider;
    this.curveParamB = b;
    this.curveParamA = a;
  }

  CurveHelper(Function<Integer, F> constantProvider, F b) {
    this(constantProvider, b, constantProvider.apply(0));
  }

  /**
   * Computes R = P + Q where P, Q are not equal to the point at infinity.
   *
   * @param x1 x value of the point P
   * @param y1 y value of the point P
   * @param z1 z value of the point P
   * @param x2 x value of the point Q
   * @param y2 y value of the point Q
   * @param z2 z value of the point Q
   * @return R = P + Q or null if R is the point at infinity.
   */
  CoordinatePack<F> addPoints(F x1, F y1, F z1, F x2, F y2, F z2) {
    // https://en.wikibooks.org/wiki/Cryptography/Prime_Curve/Standard_Projective_Coordinates#Point_Addition_(12M_+_2S)
    // u1 = y2·z1
    F u1 = y2.multiply(z1);
    // u2 = y1·z2
    F u2 = y1.multiply(z2);
    // v1 = x2·z1
    F v1 = x2.multiply(z1);
    // v2 = x1·z2
    F v2 = x1.multiply(z2);
    if (v1.equals(v2)) {
      if (u1.equals(u2)) {
        return doublePoint(x1, y1, z1);
      } else {
        return null;
      }
    } else {
      // u = u1 - u2
      F u = u1.subtract(u2);
      // v = v1 - v2
      F v = v1.subtract(v2);
      // w = z1·z2
      F w = z1.multiply(z2);
      F a0 = u.square().multiply(w);
      F vvSquare = v.square();
      F vvCube = vvSquare.multiply(v);
      F vvSquareTimesV2 = vvSquare.multiply(v2);
      F a1 = vvSquareTimesV2.multiply(constantProvider.apply(2));
      // a = w·u^2 - v^3 - 2·v^2·v2
      F a = a0.subtract(vvCube).subtract(a1);
      F t0 = u.multiply(vvSquareTimesV2.subtract(a));
      F t1 = vvCube.multiply(u2);
      // new_x = v·a  (note a is _not_ the curve parameter a here)
      F xx = v.multiply(a);
      // new_y = u·(v^2·v2 - a) - v^3·u2
      F yy = t0.subtract(t1);
      // new_z = v^3·w
      F zz = vvCube.multiply(w);
      return new CoordinatePack<>(xx, yy, zz);
    }
  }

  /**
   * Computes R = 2·P (i.e., a point doubling) where P is not the point at infinity.
   *
   * @param x value x of P
   * @param y value y of P
   * @param z value z of P
   * @return R = 2·P or null if R is the point at infinity.
   */
  CoordinatePack<F> doublePoint(F x, F y, F z) {
    // https://eprint.iacr.org/2015/1060.pdf algorithm 9.
    F t0 = y.square();
    F z3 = t0.add(t0);
    z3 = z3.add(z3);
    z3 = z3.add(z3);
    F t1 = y.multiply(z);
    F t2 = z.square();
    t2 = t2.multiplyBy3B();
    final F t3 = t2.multiply(z3);
    z3 = t1.multiply(z3);
    t1 = t2.add(t2);
    F y3 = t0.add(t2);
    t2 = t1.add(t2);
    t0 = t0.subtract(t2);
    y3 = t0.multiply(y3);
    y3 = t3.add(y3);
    t1 = x.multiply(y);
    F x3 = t0.multiply(t1);
    x3 = x3.add(x3);
    return new CoordinatePack<>(x3, y3, z3);
  }

  /**
   * Checks if two projective points P, Q are equal.
   *
   * @param x1 x value of the point P
   * @param y1 y value of the point P
   * @param z1 z value of the point P
   * @param x2 x value of the point Q
   * @param y2 y value of the point Q
   * @param z2 z value of the point Q
   * @return true if P and Q encode the same point and false otherwise.
   */
  boolean equals(F x1, F y1, F z1, F x2, F y2, F z2) {
    // checks that (x1, y1, z1) and (x2, y2, z2) encodes the same point. Note that
    // (x1/z1, y1/z1) = (x2/z2, y2/z2) is the same as (x1·z2, y1·z2) = (x2·z1, y2·z1).
    // The latter is cheaper to compute.
    return x1.multiply(z2).equals(x2.multiply(z1)) && y1.multiply(z2).equals(y2.multiply(z1));
  }

  /**
   * Checks if an affine point given by (x, y) is on the curve y^2 = x^3 + b.
   *
   * @param x the x coordinate of the point to check
   * @param y the y coordinate of the point to check
   * @return true if (x, y) satisfies the equation y^2 = x^3 + b and false otherwise.
   */
  boolean isOnCurve(F x, F y) {
    // Check if the coordinates (x, y) encode a valid point on the curve y^2 = x^3 + a·x + b.
    F lhs = y.square();
    F rhs = x.square().multiply(x).add(curveParamB).add(curveParamA.multiply(x));
    return lhs.equals(rhs);
  }

  /**
   * Serialize a point (x, y), not equal to point at infinity, to a byte string. Serialization
   * happens in the same manner as in ethereum and zcash: I.e., a point is serialized as (flags, x,
   * y) where - flags is 3 bits cf, if, pf with - cf == 1 if the point is compressed - if == 1 if
   * the serialized point is the point at infinity (so always 0 here) - pf == 1 if cf == 1 and if y
   * is the larger of y and -y. - x is the x coordinate - y is either empty if cf == 1 or the y
   * coordinate.
   *
   * @param x the x coordinate of the point to serialize
   * @param y the y coordinate of the point to serialize
   * @param compressPoint whether or not to compress the point
   * @param elementSize size of a finite field element
   * @return a serialized point.
   */
  byte[] serializePoint(F x, F y, boolean compressPoint, int elementSize) {
    byte[] serialized;
    if (compressPoint) {
      serialized = new byte[elementSize];
      System.arraycopy(x.serialize(), 0, serialized, 0, elementSize);
      F negate = y.negate();
      byte greatest = (byte) (isGreater(negate, y) ? 0 : 1);
      serialized[0] = (byte) (serialized[0] | (0x80 | (greatest << 5)));
    } else {
      serialized = new byte[2 * elementSize];
      System.arraycopy(x.serialize(), 0, serialized, 0, elementSize);
      System.arraycopy(y.serialize(), 0, serialized, elementSize, elementSize);
    }
    return serialized;
  }

  /**
   * Serialize a point at infinity. This method returns an all zero byte string of size either
   * <code>elementSize</code> or <code>2 * elementSize</code>, depending on whether compression is
   * enabled. The first two most significant bits of the returned byte array are either 11 or 01,
   * where the MSB is 1 if <code>compressPoint</code> is true.
   *
   * @param compressPoint whether to compress the point
   * @param elementSize size of a finite field in bytes
   * @return a serialized element representing the point at infinity.
   */
  byte[] serializePointAtInfinity(boolean compressPoint, int elementSize) {
    byte[] bytes = new byte[compressPoint ? elementSize : 2 * elementSize];
    bytes[0] = (byte) (bytes[0] | ((compressPoint ? 0x80 : 0) | 0x40));
    return bytes;
  }

  /**
   * Tests if the passed bytes correctly serialize the point at infinity.
   *
   * @param bytes the serialized point at infinity
   * @return true if <code>bytes</code> serialize the point at infinity and false otherwise.
   */
  boolean validateSerializedPointAtInfinity(byte[] bytes) {
    boolean allGood = true;
    for (int i = 1; i < bytes.length; i++) {
      allGood &= bytes[i] == 0;
    }
    return allGood && (bytes[0] & 0x3F) == 0;
  }

  /**
   * Extracts the y coordinate, given the x coordinate. This is done by computing {y, -y} = sqrt(x^3
   * + b) where b is the constant b of the curve parameter (we assume that we're using this for BLS
   * style curves, where a--the other parameter--is always 0).
   *
   * @param first the first coordinate, i.e., the x coordinate
   * @param selectGreatest whether to select the larger of the y and -y
   * @return the y coordinate.
   */
  F extractSecondCoordinateFromFirst(F first, boolean selectGreatest) {
    F cubePlusFour = first.square().multiply(first).add(curveParamB);
    F vyCandidate = cubePlusFour.sqrt();
    F negate = vyCandidate.negate();
    F smallest;
    F largest;
    if (isGreater(negate, vyCandidate)) {
      smallest = vyCandidate;
      largest = negate;
    } else {
      smallest = negate;
      largest = vyCandidate;
    }
    return selectGreatest ? largest : smallest;
  }

  /**
   * Return true iff x is lexicographically larger than y.
   *
   * @param x the first argument
   * @param y the second argument
   * @return true if x > y and false otherwise.
   */
  boolean isGreater(F x, F y) {
    return x.compare(y) > 0;
  }

  static final class CoordinatePack<F> {

    F vx;
    F vy;
    F vz;

    CoordinatePack(F vx, F vy, F vz) {
      this.vx = vx;
      this.vy = vy;
      this.vz = vz;
    }
  }
}
