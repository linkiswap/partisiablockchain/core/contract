package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.Objects;

final class Gt {

  private final Fq12 value;

  Gt(Fq12 value) {
    if (value.isZero()) {
      throw new IllegalArgumentException("0 is not a member of this group");
    }
    this.value = value;
  }

  static Gt createOne() {
    return new Gt(Fq12.createOne());
  }

  Gt multiply(Gt other) {
    return new Gt(value.multiply(other.value));
  }

  Gt square() {
    return new Gt(value.square());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Gt gt = (Gt) o;
    return value.equals(gt.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(value);
  }

  @Override
  public String toString() {
    return "Gt{" + value + '}';
  }

  Gt exp(NafEncoded exponent) {
    Gt result = Gt.createOne();
    Gt inverted = new Gt(this.value.invert());
    for (NafEncoded.Value value : exponent) {
      result = result.square();
      if (value == NafEncoded.Value.NEGATIVE) {
        result = result.multiply(inverted);
      } else if (value == NafEncoded.Value.POSITIVE) {
        result = result.multiply(this);
      }
    }
    return result;
  }
}
