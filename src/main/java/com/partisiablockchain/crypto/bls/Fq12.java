package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.Objects;

/** Extension of degree 12 over Fq. Implemented as Fq12 = Fq6[Z]/(Z^2 - Y) */
final class Fq12 implements FiniteField<Fq12> {

  final Fq6 a0;
  final Fq6 a1;

  static final int BYTE_SIZE = 2 * Fq6.BYTE_SIZE;

  static Fq12 createZero() {
    return new Fq12(Fq6.createZero(), Fq6.createZero());
  }

  static Fq12 createOne() {
    return new Fq12(Fq6.createOne(), Fq6.createZero());
  }

  public Fq12 conjugate() {
    return new Fq12(a0, a1.negate());
  }

  static Fq12 create(byte[] bytes) {
    if (bytes.length != Fq12.BYTE_SIZE) {
      throw new IllegalStateException(
          "Could not create Fq12 element from "
              + bytes.length
              + " bytes. Expected "
              + Fq12.BYTE_SIZE);
    }
    byte[] first = new byte[Fq6.BYTE_SIZE];
    System.arraycopy(bytes, 0, first, 0, Fq6.BYTE_SIZE);
    byte[] second = new byte[Fq6.BYTE_SIZE];
    System.arraycopy(bytes, Fq6.BYTE_SIZE, second, 0, Fq6.BYTE_SIZE);
    return new Fq12(Fq6.create(first), Fq6.create(second));
  }

  Fq12(Fq6 a0, Fq6 a1) {
    this.a0 = a0;
    this.a1 = a1;
  }

  /**
   * Performs a sparse multiplication and adaption of algorithm 2,
   * https://eprint.iacr.org/2017/1174.pdf. Multiplies this element by the sparse Fq12 element [c0
   * c1 c2 c3 c4 c5 c6] where only c0, c1, c4 are non zero
   *
   * @param c0 The value c0 set in C ∈ Fq12
   * @param c1 The value c1 set in C ∈ Fq12
   * @param c4 The value c4 set in C ∈ Fq12
   * @return the product of the inputs
   */
  public Fq12 mulBy014(Fq2 c0, Fq2 c1, Fq2 c4) {
    Fq6 aa = a0.multiplyBy01(c0, c1);
    Fq6 bb = a1.multiplyBy1(c4);
    Fq2 o = c1.add(c4);
    Fq6 cc1 = a1.add(a0);
    cc1 = cc1.multiplyBy01(c0, o);
    cc1 = cc1.subtract(aa).subtract(bb);
    Fq6 c00 = bb;
    c00 = c00.multiplyByNonResidue();
    c00 = c00.add(aa);
    return new Fq12(c00, cc1);
  }

  @Override
  public Fq12 add(Fq12 other) {
    return new Fq12(a0.add(other.a0), a1.add(other.a1));
  }

  @Override
  public Fq12 subtract(Fq12 other) {
    return new Fq12(a0.subtract(other.a0), a1.subtract(other.a1));
  }

  @Override
  public Fq12 negate() {
    return new Fq12(a0.negate(), a1.negate());
  }

  @Override
  public Fq12 multiply(Fq12 other) {
    // (a + b * Z) * (c + d * Z) =
    // a*c + b*d * Y + ((a+b) * (c + d) - a*c - b*d) * Z
    Fq6 a0b0 = a0.multiply(other.a0);
    Fq6 a1b1 = a1.multiply(other.a1);
    Fq6 b0b1 = other.a0.add(other.a1);

    Fq6 c1 = a0.add(a1);
    c1 = c1.multiply(b0b1);
    c1 = c1.subtract(a0b0);
    c1 = c1.subtract(a1b1);
    a1b1 = a1b1.multiplyByNonResidue();
    Fq6 c0 = a0b0.add(a1b1);
    return new Fq12(c0, c1);
  }

  @Override
  public Fq12 square() {
    return multiply(this);
  }

  @Override
  public Fq12 invert() {
    // Similar trick to Fq2.
    Fq6 a0a0 = a0.square();
    Fq6 a1a1 = a1.square().multiplyByNonResidue();
    Fq6 div = a0a0.subtract(a1a1).invert();
    return new Fq12(a0.multiply(div), a1.negate().multiply(div));
  }

  @Override
  public byte[] serialize() {
    byte[] bytes = new byte[Fq12.BYTE_SIZE];
    System.arraycopy(a0.serialize(), 0, bytes, 0, Fq6.BYTE_SIZE);
    System.arraycopy(a1.serialize(), 0, bytes, Fq6.BYTE_SIZE, Fq6.BYTE_SIZE);
    return bytes;
  }

  @Override
  public boolean isZero() {
    return a0.isZero() && a1.isZero();
  }

  @Override
  public int compare(Fq12 other) {
    int c0 = a0.compare(other.a0);
    int c1 = a1.compare(other.a1);
    return c0 + (1 - c0 * c0) * c1;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Fq12 fq12 = (Fq12) o;
    return a0.equals(fq12.a0) && a1.equals(fq12.a1);
  }

  @Override
  public int hashCode() {
    return Objects.hash(a0, a1);
  }

  @Override
  public String toString() {
    return "Fq12{" + a0 + " + " + a1 + " * Z}";
  }

  @Override
  public Fq12 sqrt() {
    throw new UnsupportedOperationException("Sqrt in Fq12 not supported");
  }

  Fq12 frobenius() {
    Fq6 c0 = a0.frobenius();
    Fq6 c1 = a1.frobenius();
    return new Fq12(c0, c1.frobeniusFq12Helper());
  }
}
