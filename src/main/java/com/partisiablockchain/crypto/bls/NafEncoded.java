package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import java.math.BigInteger;
import java.util.Iterator;

/**
 * Encodes a BigInteger into Non-adjacent form. This means that each non-zero value cannot be
 * adjacent.
 */
@Immutable
public final class NafEncoded implements Iterable<NafEncoded.Value> {

  @SuppressWarnings("Immutable")
  private final Value[] value;

  private NafEncoded(Value[] value) {
    this.value = value;
  }

  /**
   * Converts a bit string from the Big Integer into NAF.
   *
   * @param bigInteger bits to be encoded
   * @return Naf encoding
   */
  public static NafEncoded create(BigInteger bigInteger) {
    return new NafEncoded(convertToNaf(bigInteger));
  }

  /**
   * Converts a bit string into NAF. It uses the algorithm
   * http://www.hyperelliptic.org/tanja/conf/summerschool06/slides/scalar_multiplication.pdf slide
   * 9.
   *
   * @param bigInteger Number to be converted into NAF.
   * @return NAF of parameter given.
   */
  private static Value[] convertToNaf(BigInteger bigInteger) {
    int bitLength = bigInteger.bitLength();
    Value[] result = new Value[bigInteger.bitLength() + 1];
    int c = 0;
    for (int i = 0; i <= bitLength; i++) {
      int ci = (c + returnBit(bigInteger, i) + returnBit(bigInteger, i + 1)) / 2;
      int ri = c + returnBit(bigInteger, i) - 2 * ci;
      if (ri == -1) {
        result[bitLength - i] = Value.NEGATIVE;
      } else if (ri == 1) {
        result[bitLength - i] = Value.POSITIVE;
      } else {
        result[bitLength - i] = Value.ZERO;
      }
      c = ci;
    }
    return result;
  }

  private static int returnBit(BigInteger bitToGet, int placement) {
    return bitToGet.testBit(placement) ? 1 : 0;
  }

  @Override
  public Iterator<Value> iterator() {
    return new Iterator<>() {

      private int index = 0;
      private final int length = value.length;

      @Override
      public boolean hasNext() {
        return index < length;
      }

      @Override
      public Value next() {
        return value[index++];
      }
    };
  }

  enum Value {
    ZERO,
    POSITIVE,
    NEGATIVE
  }
}
