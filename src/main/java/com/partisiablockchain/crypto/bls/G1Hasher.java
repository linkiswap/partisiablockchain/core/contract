package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.tools.coverage.ExceptionConverter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

/** Helper for hashing bit strings to G1. */
final class G1Hasher {

  private static final byte[] DOMAIN_SEPARATOR =
      new byte[] {
        'P', 'a', 'r', 't', 'i', 's', 'i', 'a', 'b', 'l', 'o', 'c', 'k', 'c', 'h', 'a', 'i', 'n'
      };

  private static final int L = 64;

  static final Fq swuA =
      Fq.create(
          new BigInteger(
              "144698a3b8e9433d693a02c96d4982b0ea985383ee66a8d8e8981aef"
                  + "d881ac98936f8da0e0f97f5cf428082d584c1d",
              16));
  static final Fq swuB =
      Fq.create(
          new BigInteger(
              "12e2908d11688030018b12e8753eee3b2016c1f0f24f4070a0b9c14f"
                  + "cef35ef55a23215a316ceaa5d1cc48e98e172be0",
              16));
  private static final Fq swuZ = Fq.createConstant(11);

  private static final NafEncoded BLS_X_ONE = NafEncoded.create(Pairing.BLS_X.add(BigInteger.ONE));

  private G1Hasher() {}

  /**
   * This implements the hash_to_curve(message) function from
   * https://datatracker.ietf.org/doc/html/draft-irtf-cfrg-hash-to-curve-11#section-3.
   *
   * @param message the message to be hashed
   * @return a point on G1 of order r.
   */
  //
  static G1 hash(Hash message) {
    Fq[] u = hashToField(message);
    G1 q0 = mapToCurve(u[0]);
    G1 q1 = mapToCurve(u[1]);
    G1 p = q0.addPoint(q1);
    // The isogeny map is an isomorphism, so we can apply it after the above addition instead of at
    // the end of mapToCurve, saving us one application.
    Fq[] pp = p.toAffine();
    Fq[] t = isoMap(pp, G1Hasher::evaluate);
    return clearCofactor(t);
  }

  private static byte[] sha256(byte[] message) {
    MessageDigest digest =
        ExceptionConverter.call(
            () -> MessageDigest.getInstance("SHA-256"), "SHA-256 not supported");
    digest.update(message);
    return digest.digest();
  }

  // https://datatracker.ietf.org/doc/html/draft-irtf-cfrg-hash-to-curve-11#section-5.3
  static Fq[] hashToField(Hash message) {
    int lenInBytes = 2 * L;
    byte[] uniformBytes = expandMessage(message, lenInBytes);
    Fq[] result = new Fq[2];
    for (int i = 0; i < 2; i++) {
      byte[] elementBytes = new byte[L];
      System.arraycopy(uniformBytes, i * L, elementBytes, 0, L);
      result[i] = Fq.create(elementBytes);
    }
    return result;
  }

  // https://datatracker.ietf.org/doc/html/draft-irtf-cfrg-hash-to-curve-11#section-5.4.1
  static byte[] expandMessage(Hash message, int outputSize) {
    // first = sha256(0 .. 0 || message || outputSize || 0 || DOMAIN_SEPARATOR)
    byte[] first = computeFirstHash(message, outputSize);
    List<byte[]> outputBytes = new ArrayList<>();
    outputBytes.add(first);

    int dstSize = DOMAIN_SEPARATOR.length;
    byte[] msg = new byte[first.length + 1 + dstSize];
    System.arraycopy(first, 0, msg, 0, first.length);
    msg[first.length] = (byte) 1;
    System.arraycopy(DOMAIN_SEPARATOR, 0, msg, first.length + 1, dstSize);
    // sha256(first || 1 || DOMAIN_SEPARATOR)
    outputBytes.add(sha256(msg));

    int ell = (int) Math.ceil(outputSize / 32.0);
    for (int i = 2; i < ell; i++) {
      msg = new byte[32 + 1 + dstSize];
      System.arraycopy(xor(first, outputBytes.get(i - 1)), 0, msg, 0, 32);
      msg[32] = (byte) i;
      System.arraycopy(DOMAIN_SEPARATOR, 0, msg, 33, dstSize);
      // sha256(xor(first, b(i-1)) || i || DOMAIN_SEPARATOR)
      outputBytes.add(sha256(msg));
    }
    return concatenateBytes(outputBytes);
  }

  private static byte[] computeFirstHash(Hash message, int outputSize) {
    byte[] outputSizeBytes = encodeSize(outputSize);
    byte[] messageBytes = message.getBytes();
    int dstSize = DOMAIN_SEPARATOR.length;
    byte[] msg = new byte[64 + messageBytes.length + 2 + 1 + dstSize];
    System.arraycopy(messageBytes, 0, msg, 64, messageBytes.length);
    System.arraycopy(outputSizeBytes, 0, msg, 64 + messageBytes.length, 2);
    System.arraycopy(DOMAIN_SEPARATOR, 0, msg, 64 + messageBytes.length + 2 + 1, dstSize);
    return sha256(msg);
  }

  static byte[] encodeSize(int size) {
    byte[] outputSizeBytes = new byte[2];
    outputSizeBytes[0] = (byte) ((size >> 8) & 0xFF);
    outputSizeBytes[1] = (byte) (size & 0xFF);
    return outputSizeBytes;
  }

  private static byte[] concatenateBytes(List<byte[]> bytes) {
    byte[] result = new byte[bytes.stream().map(b -> b.length).reduce(0, Integer::sum)];
    int offset = 0;
    for (byte[] b : bytes) {
      System.arraycopy(b, 0, result, offset, b.length);
      offset += b.length;
    }
    return result;
  }

  private static byte[] xor(byte[] a, byte[] b) {
    // is only ever used to xor two sha256 hashes, so no need to assert that lengths are equal.
    byte[] c = new byte[a.length];
    for (int i = 0; i < a.length; i++) {
      c[i] = (byte) (a[i] ^ b[i]);
    }
    return c;
  }

  // https://datatracker.ietf.org/doc/html/draft-irtf-cfrg-hash-to-curve-11#section-6.6.3
  static G1 mapToCurve(Fq element) {
    Fq[] isoPoint = mapToCurveSimpleSwu(element);
    return new G1(isoPoint[0], isoPoint[1], Fq.createOne());
  }

  // Because A = 0 for the BLS curve (which is y^2 = x^3 + 4), we instead map our element to the
  // curve y^2 = x^3 + swuA·x + swuB.
  // See: https://datatracker.ietf.org/doc/html/draft-irtf-cfrg-hash-to-curve-11#section-6.6.2 and
  // https://datatracker.ietf.org/doc/html/draft-irtf-cfrg-hash-to-curve-11#section-6.6.3
  static Fq[] mapToCurveSimpleSwu(Fq u) {
    Fq uu = u.square();
    // tv1 = (Z^2·u^4 + Z·u^2)
    Fq tv1 = inv0(swuZ.square().multiply(uu.square()).add(swuZ.multiply(uu)));
    // x1 = (-B / A)·(1 + tv1)
    Fq x1 = swuB.negate().multiply(swuA.invert()).multiply(Fq.createConstant(1).add(tv1));
    if (tv1.isZero()) {
      // x1 = B / (Z·A)
      x1 = swuB.multiply(swuZ.multiply(swuA).invert());
    }
    // gx1 = x1^3 + A·x1 + B
    Fq gx1 = x1.multiply(x1.square()).add(swuA.multiply(x1)).add(swuB);
    // x2 = Z·u^2·x1
    Fq x2 = swuZ.multiply(uu).multiply(x1);
    // gx2 = x2^3 + A·x2 + B
    Fq gx2 = x2.multiply(x2.square()).add(swuA.multiply(x2)).add(swuB);
    Fq xx;
    Fq yy;
    if (gx1.isSquare()) {
      xx = x1;
      yy = gx1.sqrt();
    } else {
      xx = x2;
      yy = gx2.sqrt();
    }
    if (u.sign() != yy.sign()) {
      yy = yy.negate();
    }
    return new Fq[] {xx, yy};
  }

  private static Fq inv0(Fq x) {
    if (x.isZero()) {
      return Fq.createZero();
    } else {
      return x.invert();
    }
  }

  // allow testing of exceptional cases of isoMap.
  interface IsoMapHelper {

    Fq evaluate(Fq base, List<Fq> coefficients);
  }

  // given a point (x', y') on the alternative curve E (see comment on mapToCurveSimpleSwu), we
  // need to map this back to E : y^2 = x^3 + 4.
  static Fq[] isoMap(Fq[] isoPoint, IsoMapHelper helper) {
    Fq xx = isoPoint[0];
    Fq xxNum = helper.evaluate(xx, IsogenyConstants.G1_X_NUM);
    Fq xxDen = helper.evaluate(xx, IsogenyConstants.G1_X_DEN);
    Fq yyNum = helper.evaluate(xx, IsogenyConstants.G1_Y_NUM);
    Fq yyDen = helper.evaluate(xx, IsogenyConstants.G1_Y_DEN);
    if (yyDen.isZero() || xxDen.isZero()) {
      return null;
    }
    return new Fq[] {
      // x_num / x_den
      xxNum.multiply(xxDen.invert()),
      // y' · y_num / y_den
      isoPoint[1].multiply(yyNum).multiply(yyDen.invert())
    };
  }

  static Fq evaluate(Fq base, List<Fq> coefficients) {
    Fq result = coefficients.get(coefficients.size() - 1);
    for (int i = coefficients.size() - 2; i >= 0; i--) {
      result = result.multiply(base).add(coefficients.get(i));
    }
    return result;
  }

  static G1 clearCofactor(Fq[] affinePoint) {
    if (affinePoint == null) {
      return G1.POINT_AT_INFINITY;
    } else {
      // it suffices to return (x + 1)·P in order to move P into the right subgroup. See
      // section 5 of https://eprint.iacr.org/2019/403.pdf. Note that z = -BLS_X, and therefore
      // that (1 - z) == (BLS_X + 1).
      G1 point = new G1(affinePoint[0], affinePoint[1], Fq.createOne());
      return point.scalePoint(BLS_X_ONE);
    }
  }
}
