package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import java.math.BigInteger;
import java.util.Objects;

/**
 * Extension of degree 2 over Fq. This field is defined as Fq[X]/(X^2 + 1) and so elements in Fq2
 * are tuples (a, b) that define the polynomial a + b·X.
 */
@Immutable
final class Fq2 implements FiniteField<Fq2>, FieldWithMultiply3B<Fq2> {

  final Fq a0;
  final Fq a1;

  /** Byte length of Fq2. */
  public static final int BYTE_SIZE = 2 * Fq.BYTES;

  static final Fq2 FOUR_I_1 = new Fq2(Fq.createConstant(4), Fq.createConstant(4));

  Fq2(Fq a0, Fq a1) {
    this.a0 = a0;
    this.a1 = a1;
  }

  /**
   * Multiplies a Fq element and a Fq2 element.
   *
   * @param value Fq element
   * @return this * value
   */
  Fq2 multiplyWithFq(Fq value) {
    return this.multiply(Fq2.createConstant(value));
  }

  @Override
  public Fq2 multiplyBy3B() {
    Fq threeB = Fq.createConstant(12);
    return new Fq2(
        Fq.interleavedMontgomeryMultiplication(
            new Fq[] {a0, a1.negate()}, new Fq[] {threeB, threeB}),
        Fq.interleavedMontgomeryMultiplication(new Fq[] {a0, a1}, new Fq[] {threeB, threeB}));
  }

  static Fq2 createConstant(Fq a0) {
    return new Fq2(a0, Fq.createZero());
  }

  static Fq2 createZero() {
    return new Fq2(Fq.createZero(), Fq.createZero());
  }

  static Fq2 createOne() {
    return new Fq2(Fq.createOne(), Fq.createZero());
  }

  static Fq2 create(byte[] bytes) {
    if (bytes.length != Fq2.BYTE_SIZE) {
      throw new IllegalStateException(
          "Cannot create Fq2 element from " + bytes.length + " bytes. Expected " + Fq2.BYTE_SIZE);
    }
    byte[] first = new byte[Fq.BYTES];
    byte[] second = new byte[Fq.BYTES];
    System.arraycopy(bytes, 0, first, 0, bytes.length / 2);
    System.arraycopy(bytes, Fq.BYTES, second, 0, bytes.length / 2);
    return new Fq2(Fq.create(first), Fq.create(second));
  }

  @Override
  public Fq2 add(Fq2 other) {
    return new Fq2(a0.add(other.a0), a1.add(other.a1));
  }

  @Override
  public Fq2 subtract(Fq2 other) {
    return new Fq2(a0.subtract(other.a0), a1.subtract(other.a1));
  }

  @Override
  public Fq2 negate() {
    return new Fq2(a0.negate(), a1.negate());
  }

  @Override
  public Fq2 multiply(Fq2 other) {
    //  (a + bX)·(c + dX) = ac + (ad + bc)·X + bd·X^2
    //                    = ac - bd + (ad + bc)·X
    return new Fq2(
        Fq.interleavedMontgomeryMultiplication(
            new Fq[] {a0, a1.negate()}, new Fq[] {other.a0, other.a1}),
        Fq.interleavedMontgomeryMultiplication(new Fq[] {a0, a1}, new Fq[] {other.a1, other.a0}));
  }

  @Override
  public Fq2 square() {
    return multiply(this);
  }

  Fq2 multiplyByNonResidue() {
    // Multiply an Fq2 element by X + 1. Notice that
    //  (a + bX)·(X + 1) = a + aX + bX + bX^2
    //                   = a - b + (a + b)X
    return new Fq2(a0.subtract(a1), a0.add(a1));
  }

  @Override
  public Fq2 invert() {
    // See: https://github.com/zkcrypto/bls12_381/blob/main/src/fp2.rs#L305.
    //
    // In a nutshell, the inverse of (a + bX) is (ad - bd·X) where d = 1/(a^2 + b^2) since
    //  (a + bX)·((a - bX)/(a^2 + b^2)) = ((a + bX)(a - bX))/(a^2 + b^2)
    //                                  = (a^2 + b^2)/(a^2 + b^2)
    //                                  = 1.
    Fq asqr = a0.square();
    Fq bsqr = a1.square();
    Fq delt = asqr.add(bsqr).invert();
    return new Fq2(a0.multiply(delt), a1.multiply(delt).negate());
  }

  @Override
  public byte[] serialize() {
    byte[] bytes = new byte[Fq2.BYTE_SIZE];
    System.arraycopy(a0.serialize(), 0, bytes, 0, Fq.BYTES);
    System.arraycopy(a1.serialize(), 0, bytes, Fq.BYTES, Fq.BYTES);
    return bytes;
  }

  /**
   * Test whether this element is the 0 element of the field.
   *
   * @return true if this is zero, and false otherwise.
   */
  @Override
  public boolean isZero() {
    return a0.isZero() && a1.isZero();
  }

  /**
   * Performs a lexicographic comparison of two field elements. Tries to return this value in
   * constant time manner.
   *
   * @param other the other element
   * @return -1 if this is smaller than other, 0 if they're equal and 1 otherwise.
   */
  @Override
  public int compare(Fq2 other) {
    int c0 = a0.compare(other.a0);
    int c1 = a1.compare(other.a1);
    // if c0 == 0, then return c1. Otherwise return c0.
    return c0 + (1 - c0 * c0) * c1;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Fq2 fq2 = (Fq2) o;
    return a0.equals(fq2.a0) && a1.equals(fq2.a1);
  }

  @Override
  public int hashCode() {
    return Objects.hash(a0, a1);
  }

  @Override
  public String toString() {
    return "Fq2{" + a0 + " + " + a1 + " * X}";
  }

  @Override
  public Fq2 sqrt() {
    // Algorithm 9, https://eprint.iacr.org/2012/685.pdf
    // b1 = this^((p - 3)/4)
    Fq2 b1 = pow(Fq.PRIME_BIGINTEGER.subtract(BigInteger.valueOf(3)).divide(BigInteger.valueOf(4)));
    // \alpha = this·b1^2
    Fq2 alpha = multiply(b1.square());
    // b0 = \alpha·\alpha^p
    Fq2 b0 = alpha.pow(Fq.PRIME_BIGINTEGER).multiply(alpha);
    Fq2 minusOne = new Fq2(Fq.createConstant(-1), Fq.createZero());
    if (b0.equals(minusOne)) {
      throw new IllegalArgumentException("Value is not a square");
    }
    b1 = multiply(b1);
    if (alpha.equals(minusOne)) {
      // b1·i where i s.t. i·i = 1
      return b1.multiply(new Fq2(Fq.createZero(), Fq.createOne()));
    } else {
      // c = (\alpha + 1)^((p - 1)/2)
      Fq2 c = alpha.add(createOne());
      c = c.pow(Fq.PRIME_BIGINTEGER.subtract(BigInteger.ONE).divide(BigInteger.TWO));
      return b1.multiply(c);
    }
  }

  Fq2 pow(BigInteger exponent) {
    Fq2 result = createOne();
    int numberOfBits = exponent.bitLength();
    for (int i = numberOfBits; i >= 0; i--) {
      result = result.square();
      if (exponent.testBit(i)) {
        result = multiply(result);
      }
    }
    return result;
  }

  Fq2 frobenius() {
    return new Fq2(a0, a1.negate());
  }
}
