package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

interface EllipticCurve<C extends EllipticCurve<C>> {

  /**
   * Add this point to another.
   *
   * @param other the other point
   * @return the sum of this point and other.
   */
  C addPoint(C other);

  /**
   * Perform a point doubling.
   *
   * @return 2 * this.
   */
  C doublePoint();

  /**
   * Perform a scalar multiplication in the curve.
   *
   * @param scalar the scalar
   * @return the scaled point.
   */
  C scalePoint(NafEncoded scalar);

  /**
   * Negates this point.
   *
   * @return the negated point.
   */
  C negatePoint();

  /**
   * Serialize this point into a string of bytes in the same manner as is done in zcash and
   * ethereum.
   *
   * @param compressPoint whether to compress the point or not
   * @return a serialized point.
   */
  byte[] serialize(boolean compressPoint);

  /**
   * Test if this is a point at infinity.
   *
   * @return true if this is the point at infinity and false otherwise.
   */
  boolean isPointAtInfinity();
}
