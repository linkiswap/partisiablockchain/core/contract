package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import java.math.BigInteger;
import java.util.Objects;

/** Elliptic curve. E'(Fq2) with E' : y^2 = x^3 + 4(1 + i). */
@Immutable
public final class G2 implements EllipticCurve<G2> {

  private final Fq2 vx;
  private final Fq2 vy;
  private final Fq2 vz;

  /** The byte size of a compressed G2 point. */
  public static final int BYTE_SIZE = Fq2.BYTE_SIZE;

  private static final CurveHelper<Fq2> helper =
      new CurveHelper<>(v -> Fq2.createConstant(Fq.createConstant(v)), Fq2.FOUR_I_1);

  /** Points at infinity. */
  private static final G2 POINT_AT_INFINITY =
      new G2(Fq2.createZero(), Fq2.createOne(), Fq2.createZero());

  /** Inner G. */
  public static final G2 G =
      new G2(
          new Fq2(
              Fq.create(
                  new BigInteger(
                      "024aa2b2f08f0a91260805272dc51051c6e47ad4fa403b02b4510b647ae3d1770ba"
                          + "c0326a805bbefd48056c8c121bdb8",
                      16)),
              Fq.create(
                  new BigInteger(
                      "13e02b6052719f607dacd3a088274f65596bd0d09920b61ab5da61"
                          + "bbdc7f5049334cf11213945d57e5ac7d055d042b7e",
                      16))),
          new Fq2(
              Fq.create(
                  new BigInteger(
                      "0ce5d527727d6e118cc9cdc6da2e351aadfd9baa8cbdd3a76d429a695160d12c923ac9"
                          + "cc3baca289e193548608b82801",
                      16)),
              Fq.create(
                  new BigInteger(
                      "0606c4a02ea734cc32acd2b02bc28b99cb3e287e85a763af267492ab572e99ab3f370d275"
                          + "cec1da1aaa9075ff05f79be",
                      16))),
          Fq2.createOne());

  /**
   * Deserializes a byte array into a G2 point.
   *
   * @param bytes to be Deserialized
   * @return G2 point
   */
  public static G2 create(byte[] bytes) {
    G2 deserialized = createUnsafe(bytes);
    if (!deserialized.isPointAtInfinity()) {
      deserialized.validatePoint();
    }
    return deserialized;
  }

  /**
   * Deserializes a byte array into a G2 point without validating that the point is on the correct
   * curve or has the right order.
   *
   * @param bytes to be Deserialized
   * @return G2 point
   */
  public static G2 createUnsafe(byte[] bytes) {
    if (bytes.length == 0) {
      throw new IllegalArgumentException("Cannot deserialize 0 bytes");
    }
    boolean isCompressed = ((bytes[0] >> 7) & 1) == 1;
    int expectedLength = isCompressed ? Fq2.BYTE_SIZE : 2 * Fq2.BYTE_SIZE;

    if (bytes.length != expectedLength) {
      throw new IllegalArgumentException(
          "Expected " + expectedLength + " bytes, but got " + bytes.length);
    }

    if (isPointInfinity(bytes)) {
      return POINT_AT_INFINITY;
    }

    Fq2 x = createPointX(bytes);
    Fq2 y = createPointY(bytes, isCompressed, x);

    return new G2(x, y, Fq2.createOne());
  }

  private static Fq2 createPointX(byte[] bytes) {
    byte[] vxBytes = new byte[Fq2.BYTE_SIZE];
    System.arraycopy(bytes, 0, vxBytes, 0, Fq2.BYTE_SIZE);
    // clear top three bits before the element is created.
    vxBytes[0] = (byte) (vxBytes[0] & 0x1F);
    return Fq2.create(vxBytes);
  }

  private static Fq2 createPointY(byte[] bytes, boolean isCompressed, Fq2 x) {
    Fq2 y;
    if (isCompressed) {
      boolean selectGreatest = ((bytes[0] >> 5) & 1) == 1;
      y = helper.extractSecondCoordinateFromFirst(x, selectGreatest);
    } else {
      byte[] vyBytes = new byte[Fq2.BYTE_SIZE];
      System.arraycopy(bytes, Fq2.BYTE_SIZE, vyBytes, 0, Fq2.BYTE_SIZE);
      y = Fq2.create(vyBytes);
    }
    return y;
  }

  private static boolean isPointInfinity(byte[] bytes) {
    boolean isPointAtInfinity = ((bytes[0] >> 6) & 1) == 1;
    if (isPointAtInfinity) {
      if (helper.validateSerializedPointAtInfinity(bytes)) {
        return true;
      } else {
        throw new IllegalStateException("Invalid serialized element");
      }
    }
    return false;
  }

  G2(Fq2 vx, Fq2 vy, Fq2 vz) {
    this.vx = vx;
    this.vy = vy;
    this.vz = vz;
  }

  /** Validate that the point is on the correct curve and has the right order. */
  public void validatePoint() {
    if (!helper.isOnCurve(vx, vy)) {
      throw new IllegalStateException("Deserialized point is not on the curve");
    }

    if (!scalePoint(Pairing.order).equals(POINT_AT_INFINITY)) {
      throw new IllegalStateException("Deserialized point is in an invalid subgroup");
    }
  }

  @Override
  public G2 addPoint(G2 other) {
    if (isPointAtInfinity()) {
      return other;
    } else if (other.isPointAtInfinity()) {
      return this;
    } else {
      CurveHelper.CoordinatePack<Fq2> result =
          helper.addPoints(vx, vy, vz, other.vx, other.vy, other.vz);
      if (result == null) {
        return POINT_AT_INFINITY;
      } else {
        return new G2(result.vx, result.vy, result.vz);
      }
    }
  }

  @Override
  public G2 doublePoint() {
    if (isPointAtInfinity()) {
      return POINT_AT_INFINITY;
    } else {
      CurveHelper.CoordinatePack<Fq2> result = helper.doublePoint(vx, vy, vz);
      return new G2(result.vx, result.vy, result.vz);
    }
  }

  @Override
  public G2 scalePoint(NafEncoded naf) {
    G2 result = POINT_AT_INFINITY;
    for (NafEncoded.Value value : naf) {
      result = result.doublePoint();
      if (value == NafEncoded.Value.NEGATIVE) {
        result = result.addPoint(this.negatePoint());
      } else if (value == NafEncoded.Value.POSITIVE) {
        result = result.addPoint(this);
      }
    }
    return result;
  }

  @Override
  public G2 negatePoint() {
    if (vy.equals(Fq2.createZero())) {
      return POINT_AT_INFINITY;
    } else {
      return new G2(vx, vy.negate(), vz);
    }
  }

  @Override
  public byte[] serialize(boolean compressPoint) {
    if (isPointAtInfinity()) {
      return helper.serializePointAtInfinity(compressPoint, Fq2.BYTE_SIZE);
    } else {
      Fq2[] coordinates = toAffine();
      return helper.serializePoint(coordinates[0], coordinates[1], compressPoint, Fq2.BYTE_SIZE);
    }
  }

  Fq2[] toAffine() {
    Fq2 vzInv = vz.invert();
    return new Fq2[] {vx.multiply(vzInv), vy.multiply(vzInv)};
  }

  @Override
  public boolean isPointAtInfinity() {
    return equals(POINT_AT_INFINITY);
  }

  @Override
  public String toString() {
    if (isPointAtInfinity()) {
      return "G2{POINT_AT_INFINITY}";
    } else {
      Fq2[] coordinates = toAffine();
      return "G2{X: " + coordinates[0] + ", Y: " + coordinates[1] + "}";
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    G2 other = (G2) o;
    return helper.equals(vx, vy, vz, other.vx, other.vy, other.vz);
  }

  @Override
  public int hashCode() {
    if (isPointAtInfinity()) {
      return 0;
    } else {
      // normalize before hashing.
      Fq2[] affine = toAffine();
      return Objects.hash(affine[0], affine[1]);
    }
  }
}
