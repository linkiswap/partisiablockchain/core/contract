package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.secata.util.NumericConversion;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Fq elements is implemented as a custom 406 bit number. The number is stored as an array of 7
 * longs with the smallest index being the least significant bits. Each index contains 58 bit. A
 * serialized Fq is 48 bytes in big endian. Fq is an implementation of arithmetic modulo the
 * BLS12-381 base field prime.
 */
@Immutable
final class Fq implements FiniteField<Fq>, FieldWithMultiply3B<Fq> {

  private static final int NUMBER_OF_LIMBS = 7;
  /** Byte size of an Fq element. */
  public static final int BYTES = 48;

  private static final int LIMB_SIZE = 58;
  private static final int ADDITIONAL_BITS = Long.SIZE - LIMB_SIZE;

  private static final long LIMB_MASK = (1L << LIMB_SIZE) - 1;

  static final BigInteger PRIME_BIGINTEGER =
      new BigInteger(
          "1a0111ea397fe69a4b1ba7b6434bacd764774b84f385"
              + "12bf6730d2a0f6b0f6241eabfffeb153ffffb9feffffffffaaab",
          16);

  /** INV = -p^{-1} mod 2^58. Note it is first limb in FACTOR. */
  private static final long INV = 140737475470229501L;

  /**
   * Factor = (reducer * reducer^-1 - 1) / n in internal representation, with reducer being 1 *
   * 2^REDUCER_BITS. Used to calculate value * factor mod 2^406 where value comes from multiply.
   */
  private static final long[] FACTOR = {
    140737475470229501L,
    195595106123053602L,
    211401512351728262L,
    16390194033875900L,
    207843772558077130L,
    264233694228432319L,
    279920322751631471L,
  };

  /** Prime in the internal representation. */
  static final long[] PRIME =
      new long[] {
        143833713099123371L,
        216172422762594286L,
        83896495553790442L,
        149689799186160835L,
        163057217235613515L,
        171129804685765101L,
        6980443811L
      };

  /**
   * SQRT_EXP = (p + 1)/4 in internal representation. Used for calculating square roots in the field
   */
  private static final long[] SQRT_EXP =
      new long[] {
        180073616350636715L,
        198158293766504443L,
        237146906002231418L,
        253595231910324016L,
        112821898346831314L,
        258955233285225083L,
        1745110952L,
      };

  /**
   * IS_SQUARE_EXP = (p - 1)/2 in internal representation. Used for checking if a number is a square
   */
  private static final long[] IS_SQUARE_EXP =
      new long[] {
        71916856549561685L,
        108086211381297143L,
        186063435852751093L,
        218960087668936289L,
        225643796693662629L,
        229680090418738422L,
        3490221905L,
      };

  /** INVERSE_EXP = Prime - 2 in the internal representation. Used to calculate the inverse. */
  private static final long[] INVERSE_EXP =
      new long[] {
        143833713099123369L,
        216172422762594286L,
        83896495553790442L,
        149689799186160835L,
        163057217235613515L,
        171129804685765101L,
        6980443811L,
      };

  private static final long[] ZERO = new long[NUMBER_OF_LIMBS];

  /** The number one in Montgomery in internal representation. */
  private static final long[] ONE =
      new long[] {
        104709593340967812L,
        138265342342498545L,
        49741797901776404L,
        260939257840249917L,
        238652850196049085L,
        65361077346142448L,
        5152162575L
      };

  /** Reducer^2 in internal, with reducer being 1 * 2^REDUCER_BITS. */
  private static final long[] R2 = {
    145867405174699950L,
    82274687238640616L,
    156792236337409234L,
    77058955434583515L,
    255137176314340249L,
    22261605452455100L,
    6956673579L
  };

  @SuppressWarnings("Immutable")
  private final long[] value;

  /**
   * Create a new field element equal to 0.
   *
   * @return 0 in the field.
   */
  static Fq createZero() {
    return new Fq(ZERO);
  }

  /**
   * Create a new field element equal to 1.
   *
   * @return 1 in the field.
   */
  static Fq createOne() {
    return new Fq(ONE);
  }

  /**
   * Create a new field element from an integer.
   *
   * @param value the value to encode.
   * @return the field element equivalent to the input.
   */
  static Fq createConstant(long value) {
    if (value <= -1) {
      return create384(Fq.PRIME_BIGINTEGER.subtract(BigInteger.valueOf(-value)).toByteArray());
    } else {
      return create384(BigInteger.valueOf(value).toByteArray());
    }
  }

  /**
   * Computes a sum-of-products mod p using Algorithm 2 from <a
   * href="https://eprint.iacr.org/2022/367.pdf">paper</a> with radix <code>r = 2^{58}</code> and
   * <code>B = 1</code>.
   *
   * @param a Fq's to compute
   * @param b Fq's to compute
   * @return A sum of the products of the inputs modulo p.
   */
  public static Fq interleavedMontgomeryMultiplication(Fq[] a, Fq[] b) {
    long[] value = new long[NUMBER_OF_LIMBS];
    long[] u = new long[NUMBER_OF_LIMBS + 1];
    for (int j = 0; j < NUMBER_OF_LIMBS; j++) {
      System.arraycopy(value, 0, u, 0, value.length);
      u[NUMBER_OF_LIMBS] = 0;
      for (int i = 0; i < a.length; i++) {
        long carry = 0;
        long[] mul;
        for (int k = 0; k < NUMBER_OF_LIMBS; k++) {
          mul = multiplyLongsAndAdd(a[i].value[j], b[i].value[k], u[k], carry);
          carry = mul[0];
          u[k] = mul[1];
        }
        long addK6 = u[NUMBER_OF_LIMBS] + carry;
        u[NUMBER_OF_LIMBS] = addK6;
      }
      long k = (u[0] * INV) & LIMB_MASK;
      long[] mul = multiplyLongsAndAdd(k, PRIME[0], u[0], 0);
      long carry = mul[0];
      for (int g = 1; g < NUMBER_OF_LIMBS; g++) {
        mul = multiplyLongsAndAdd(k, PRIME[g], u[g], carry);
        carry = mul[0];
        value[g - 1] = mul[1];
      }
      long addK6 = u[NUMBER_OF_LIMBS] + carry;
      value[NUMBER_OF_LIMBS - 1] = addK6;
    }
    if (compareValues(value, PRIME) >= 0) {
      return new Fq(sub(value, PRIME));
    }
    return new Fq(value);
  }

  /**
   * Converts an array on 7 longs in Montgomery out of Montgomery but still in the internal
   * representation, e.g. 58 bit limbs.
   *
   * @param limbs array in Montgomery and internal representation
   * @return array not in Montgomery and in internal representation
   */
  static long[] convertOutLimbs(long[] limbs) {
    byte[] returnBytes = new byte[BYTES];
    long[] correctLimbs = convertOutOfSmallLimbs(convertOutOfMontgomery(limbs));
    for (int i = 0; i < correctLimbs.length; i++) {
      NumericConversion.longToBytes(
          correctLimbs[i], returnBytes, (correctLimbs.length - i - 1) * 8);
    }
    return createWithoutConversion(returnBytes);
  }

  static byte[] padToElementByteSize(byte[] bytesToPad) {
    ByteBuffer buffer =
        ByteBuffer.allocate(BYTES)
            .position(BYTES - bytesToPad.length); // offset to get correct amount of leading zeros
    return buffer.put(bytesToPad).array();
  }

  /**
   * Converts bytes into a Fq element that is not in Montgomery.
   *
   * @param bytes to create the Fq element
   * @return array in internal representation
   */
  private static long[] createWithoutConversion(byte[] bytes) {
    bytes = padToElementByteSize(bytes);
    long[] arr = new long[NUMBER_OF_LIMBS];
    for (int i = 0; i < NUMBER_OF_LIMBS - 1; i++) {
      arr[i] = NumericConversion.longFromBytes(bytes, (NUMBER_OF_LIMBS - i - 2) * 8);
    }
    return convertIntoSmallLimbs(arr);
  }

  public static Fq create(BigInteger value) {
    if (value.bitLength() < 384) {
      return create384(value.toByteArray());
    } else {
      return create384(value.mod(Fq.PRIME_BIGINTEGER).toByteArray());
    }
  }

  /**
   * Create a new field element from a byte representation.
   *
   * @param bytes a serialized field element
   * @return a new field element.
   */
  public static Fq create(byte[] bytes) {
    if (bytes.length < 49) {
      return create384(bytes);
    } else {
      return create(new BigInteger(1, bytes));
    }
  }

  /**
   * An element 'a' is in Montgomery form as: aR mod p. Therefore, multiplying with a * R^2 / R =
   * aR.
   *
   * @param value value to convert into Montgomery form
   * @return value in Montgomery form
   */
  private static Fq convertIntoMontgomery(long[] value) {
    return interleavedMontgomeryMultiplication(new Fq[] {new Fq(value)}, new Fq[] {new Fq(R2)});
  }

  private static Fq create384(byte[] bytes) {
    byte[] paddedBytes = padToElementByteSize(bytes);
    long[] arr = new long[NUMBER_OF_LIMBS];
    for (int i = 0; i < NUMBER_OF_LIMBS - 1; i++) {
      arr[i] = NumericConversion.longFromBytes(paddedBytes, (NUMBER_OF_LIMBS - i - 2) * 8);
    }
    return convertIntoMontgomery(convertIntoSmallLimbs(arr));
  }

  /**
   * Converts limbs into 58 bit limbs.
   *
   * @param arr array to be corrected into internal representation
   * @return array in internal representation
   */
  static long[] convertIntoSmallLimbs(long[] arr) {
    long[] result = new long[NUMBER_OF_LIMBS];
    long lostBits = 0;
    for (int i = 0; i < NUMBER_OF_LIMBS; i++) {
      int offset = ADDITIONAL_BITS * i;
      result[i] = (lostBits | (arr[i] << offset)) & LIMB_MASK;
      lostBits = arr[i] >>> (LIMB_SIZE - offset);
    }
    return result;
  }

  /**
   * Converts limbs into 64 bit limbs.
   *
   * @param arr array to be corrected out of internal representation
   * @return 64 bit limbs
   */
  private static long[] convertOutOfSmallLimbs(long[] arr) {
    long[] result = Arrays.copyOf(arr, NUMBER_OF_LIMBS - 1);
    long lostBits;
    for (int i = 0; i < result.length - 1; i++) {
      int offset = ADDITIONAL_BITS * i;
      lostBits = arr[i + 1] << (LIMB_SIZE - offset);
      result[i] |= lostBits;
      result[i + 1] >>>= offset + ADDITIONAL_BITS;
    }
    result[5] |= arr[6] << (LIMB_SIZE - (ADDITIONAL_BITS * 5));
    return result;
  }

  /**
   * Creates a new Fq element from a big integer v that's assumed to be within the right range. The
   * input needs to be in Montgomery form.
   *
   * @param value the big integer
   * @return a new Fq element.
   */
  static Fq createUnsafe(BigInteger value) {
    long[] arr = new long[NUMBER_OF_LIMBS];
    byte[] bytes = padToElementByteSize(value.toByteArray());
    for (int i = 0; i < NUMBER_OF_LIMBS - 1; i++) {
      arr[i] = NumericConversion.longFromBytes(bytes, (NUMBER_OF_LIMBS - i - 2) * 8);
    }
    return new Fq(convertIntoSmallLimbs(arr));
  }

  private Fq(long[] value) {
    this.value = value;
  }

  @Override
  public Fq multiplyBy3B() {
    return this.multiply(Fq.createConstant(12));
  }

  @Override
  public Fq add(Fq other) {
    long[] sum = add(this.value, other.value);
    if (compareValues(sum, PRIME) >= 0) {
      return new Fq(sub(sum, PRIME));
    } else {
      return new Fq(sum);
    }
  }

  /**
   * Adding 406 bit values in the internal representation.
   *
   * @param left left array to add
   * @param right right array to add
   * @return left + right
   */
  static long[] add(long[] left, long[] right) {
    long[] result = new long[left.length];
    long difference = 0;
    for (int i = 0; i < left.length; i++) {
      difference = left[i] + right[i] + (difference >>> LIMB_SIZE);
      result[i] = difference & LIMB_MASK;
    }
    return result;
  }

  @Override
  public Fq subtract(Fq other) {
    long[] diff = sub(this.value, other.value);
    if (diff[NUMBER_OF_LIMBS - 1] < 0) {
      diff[NUMBER_OF_LIMBS - 1] &= LIMB_MASK;
      return new Fq(add(diff, PRIME));
    }
    diff[NUMBER_OF_LIMBS - 1] &= LIMB_MASK;
    return new Fq(diff);
  }

  /**
   * Subtracting 406 bit values in the internal representation.
   *
   * @param left left array to subtract
   * @param right right array to subtract
   * @return left - right
   */
  private static long[] sub(long[] left, long[] right) {
    long[] result = new long[NUMBER_OF_LIMBS];
    long difference = 0;
    for (int i = 0; i < NUMBER_OF_LIMBS - 1; i++) {
      difference = left[i] - right[i] + (difference >> LIMB_SIZE);
      result[i] = difference & LIMB_MASK;
    }
    difference = left[NUMBER_OF_LIMBS - 1] - right[NUMBER_OF_LIMBS - 1] + (difference >> LIMB_SIZE);
    result[NUMBER_OF_LIMBS - 1] = difference;
    return result;
  }

  @Override
  public Fq negate() {
    if (compareValues(this.value, ZERO) == 0) {
      return new Fq(ZERO);
    }
    return new Fq(sub(PRIME, this.value));
  }

  @Override
  public Fq multiply(Fq other) {
    return interleavedMontgomeryMultiplication(new Fq[] {this}, new Fq[] {other});
  }

  /**
   * Multiplies 406 bit values in the internal representation. Uses the Standard Algorithm from:
   * https://en.wikipedia.org/wiki/Multiplication_algorithm#Example.
   *
   * @param left value to multiply
   * @param right value to multiply
   * @return left * right
   */
  static long[] multiply(long[] left, long[] right) {
    long[] result = new long[NUMBER_OF_LIMBS * 2];
    for (int i = 0; i < NUMBER_OF_LIMBS; i++) {
      long carry = 0;
      for (int j = 0; j < NUMBER_OF_LIMBS; j++) {
        long[] ms = multiplyLongsAndAdd(left[i], right[j], carry, result[i + j]);
        carry = ms[0];
        result[i + j] = ms[1];
      }
      result[i + NUMBER_OF_LIMBS] = carry;
    }
    return result;
  }

  /**
   * Multiplies left and right and returns result mod R, with R begin 2^406. Algorithm is the same
   * as multiply, however the variable h makes sure that we do not multiply unnecessary as we are
   * only interested in the first 7 limbs This is also why we do not have result[i +
   * NUMBER_OF_LIMBS] = carry after the first loop.
   *
   * @param left element to multiply
   * @param right element to multiply
   * @return left * right mod 2^406
   */
  private static long[] multiplyAndModReduce(long[] left, long[] right) {
    long[] result = new long[NUMBER_OF_LIMBS];
    for (int i = 0; i <= NUMBER_OF_LIMBS - 1; i++) {
      long carry = 0;
      for (int j = 0; j < NUMBER_OF_LIMBS - i; j++) {
        long[] ms = multiplyLongsAndAdd(left[i], right[j], carry, result[i + j]);
        carry = ms[0];
        result[i + j] = ms[1];
      }
    }
    return result;
  }

  private static long[] multiplyLongsAndAdd(long left, long right, long carry, long overflow) {
    long[] result = new long[2];
    long lowerPart = left * right;
    long highPart = Math.multiplyHigh(left, right);
    long lowerTooMuch = lowerPart >>> LIMB_SIZE;
    long lowerPartRes = (lowerPart & LIMB_MASK) + carry + overflow;
    result[1] = lowerPartRes & LIMB_MASK;
    result[0] = (highPart << ADDITIONAL_BITS) + lowerTooMuch + (lowerPartRes >>> LIMB_SIZE);
    return result;
  }

  /**
   * Returns the upper 7 limbs of a 14 limb value. This is the same as shifting the 14 limb value by
   * 406 bits.
   *
   * @param value a 14 limb array
   * @return a new 7 limb array
   */
  private static long[] shiftRight406(long[] value) {
    return Arrays.copyOfRange(value, 7, 2 * NUMBER_OF_LIMBS);
  }

  /**
   * Reduces a 14 limb value to 7 limb value in Montgomery form. Reduce does the following: temp =
   * value * factor mod R. reduced2 = value + temp * PRIME. Its a fact that reduced2 is a multiple
   * of r. Result = reduced2 / R. Since R is 2^406 it is just a right shift of 406. Result is
   * between 0 <= result < 2* PRIME, so a simple if else is enough to perform modulo by PRIME.
   *
   * @param value 14 long limbs
   * @return 7 long limbs
   */
  static long[] reduce(long[] value) {
    long[] temp = multiplyAndModReduce(value, FACTOR);
    long[] reduced1 = multiply(temp, PRIME);
    long[] reduced2 = add(value, reduced1);
    long[] result = shiftRight406(reduced2);
    return compareValues(result, PRIME) >= 0 ? sub(result, PRIME) : result;
  }

  /**
   * Compares two 406 bit internal values.
   *
   * @param left value to compare with
   * @param right value to compare with
   * @return -1, 0 or 1 as left is numerically less than, equal to or greater than right
   */
  private static int compareValues(long[] left, long[] right) {
    for (int i = NUMBER_OF_LIMBS - 1; i >= 0; i--) {
      long res = left[i] - right[i];
      if (res < 0) {
        return -1;
      } else if (res > 0) {
        return 1;
      }
    }
    return 0;
  }

  @Override
  public Fq square() {
    return multiply(this);
  }

  @Override
  public Fq invert() {
    if (isZero()) {
      throw new IllegalArgumentException("0 is not invertible in Fq");
    } else {
      return modExp(INVERSE_EXP);
    }
  }

  @Override
  public byte[] serialize() {
    long[] returnValue = convertOutOfSmallLimbs(convertOutOfMontgomery(this.value));
    byte[] returnBytes = new byte[BYTES];
    for (int i = 0; i < returnValue.length; i++) {
      NumericConversion.longToBytes(returnValue[i], returnBytes, (returnValue.length - i - 1) * 8);
    }
    return returnBytes;
  }

  /**
   * Convert an element aR mod p out of Montgomery by doing a reduction, by doing the following
   * operation, aR / R mod p = a.
   *
   * @param value to convert of Montgomery
   * @return value out of Montgomery
   */
  private static long[] convertOutOfMontgomery(long[] value) {
    return reduce(expand(value));
  }

  /**
   * Expands the value into double number of limbs with the new part being 0.
   *
   * @param value to expand
   * @return expanded value.
   */
  private static long[] expand(long[] value) {
    return new long[] {
      value[0], value[1], value[2], value[3], value[4], value[5], value[6], 0, 0, 0, 0, 0, 0, 0
    };
  }

  @Override
  public boolean isZero() {
    return Arrays.equals(this.value, ZERO);
  }

  @Override
  public boolean equals(Object other) {
    if (this == other) {
      return true;
    }
    if (other == null || getClass() != other.getClass()) {
      return false;
    }
    Fq element = (Fq) other;
    return Arrays.equals(this.value, element.value);
  }

  @Override
  public int hashCode() {
    return Arrays.hashCode(value);
  }

  @Override
  public String toString() {
    return new BigInteger(serialize()).toString();
  }

  @Override
  public int compare(Fq other) {
    return compareValues(convertOutLimbs(value), convertOutLimbs(other.value));
  }

  @Override
  public Fq sqrt() {
    // if value is a square, its square-root is value^((p + 1)/4) since PRIME == 3 mod 4.
    if (!isSquare()) {
      throw new IllegalArgumentException("Fq value is not a square");
    }
    // sqrt(x) = x^((p + 1)/4)
    return modExp(SQRT_EXP);
  }

  boolean isSquare() {
    // x is a square if x^((p - 1)/2) == 1.
    return modExp(IS_SQUARE_EXP).compare(Fq.createOne()) == 0;
  }

  Fq modExp(long[] exponent) {
    int bits = bitLength(exponent);
    Fq result = Fq.createOne();
    for (int i = 0; i < bits; i++) {
      result = interleavedMontgomeryMultiplication(new Fq[] {result}, new Fq[] {result});
      if (testBit(exponent, bits - i - 1)) {
        result = interleavedMontgomeryMultiplication(new Fq[] {result}, new Fq[] {this});
      }
    }
    return result;
  }

  private static int firstNonZeroLimb(long[] val) {
    for (int i = NUMBER_OF_LIMBS - 1; i >= 1; i--) {
      if (val[i] != 0) {
        return i;
      }
    }
    return 0;
  }

  static int bitLength(long[] val) {
    int lengthOfNumber = firstNonZeroLimb(val);
    return lengthOfNumber * LIMB_SIZE + bitLengthForLong(val[lengthOfNumber]);
  }

  private static int bitLengthForLong(long n) {
    return Long.SIZE - Long.numberOfLeadingZeros(n);
  }

  boolean sign() {
    // Must be converted out of Montgomery form before computing the sign.
    long[] limbs = convertOutLimbs(this.value);
    return testBit(limbs, 0);
  }

  private boolean testBit(long[] val, int n) {
    return (val[n / LIMB_SIZE] >>> (n % LIMB_SIZE) & 1) != 0;
  }
}
