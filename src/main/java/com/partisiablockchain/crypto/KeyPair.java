package com.partisiablockchain.crypto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.crypto.Curve.CURVE;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Objects;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.signers.ECDSASigner;
import org.bouncycastle.crypto.signers.HMacDSAKCalculator;
import org.bouncycastle.crypto.util.DigestFactory;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.math.ec.FixedPointCombMultiplier;

/** Class for elliptic curve signature related functions. */
public final class KeyPair implements SignatureProvider {

  private final BlockchainPublicKey publicKey;
  private final BigInteger privateKey;

  /**
   * Construct a new key pair for the supplied private key.
   *
   * @param privateKey the private key to sign with
   */
  public KeyPair(BigInteger privateKey) {
    if (privateKey.bitLength() > CURVE.getN().bitLength()) {
      privateKey = privateKey.mod(CURVE.getN());
    }
    this.privateKey = privateKey;
    ECPoint ecPoint = new FixedPointCombMultiplier().multiply(CURVE.getG(), privateKey);
    this.publicKey = new BlockchainPublicKey(ecPoint);
  }

  /** Construct a new KeyPair with a randomly generated private key. */
  public KeyPair() {
    this(new BigInteger(Curve.CURVE.getN().bitLength(), new SecureRandom()));
  }

  static BigInteger normalizeS(BigInteger s) {
    if (s.compareTo(Curve.HALF_CURVE_ORDER) > 0) {
      s = CURVE.getN().subtract(s);
    }
    return s;
  }

  static Signature findRecoveryId(
      Hash message, BigInteger r, BigInteger s, BlockchainPublicKey thisKey) {
    for (byte i = 0; i < 4; i++) {
      Signature signature = new Signature(i, r, s);
      BlockchainPublicKey blockchainPublicKey = signature.recoverPublicKey(message);
      if (Objects.equals(blockchainPublicKey, thisKey)) {
        return signature;
      }
    }
    // This should not happen
    return null;
  }

  @Override
  public Signature sign(Hash messageHash) {
    ECDSASigner ecdsaSigner = new ECDSASigner(new HMacDSAKCalculator(DigestFactory.createSHA256()));
    ECPrivateKeyParameters privKey = new ECPrivateKeyParameters(privateKey, CURVE);
    ecdsaSigner.init(true, privKey);
    BigInteger[] bigIntegers = ecdsaSigner.generateSignature(messageHash.getBytes());
    BigInteger r = bigIntegers[0];
    BigInteger s = bigIntegers[1];
    s = normalizeS(s);

    return findRecoveryId(messageHash, r, s, publicKey);
  }

  /**
   * Get the public key.
   *
   * @return public key
   */
  public BlockchainPublicKey getPublic() {
    return publicKey;
  }

  /**
   * Get the private key.
   *
   * @return private key
   */
  @SuppressWarnings("WeakerAccess")
  public BigInteger getPrivateKey() {
    return privateKey;
  }
}
