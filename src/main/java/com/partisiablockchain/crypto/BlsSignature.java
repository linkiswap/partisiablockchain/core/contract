package com.partisiablockchain.crypto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.bls.G1;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.Objects;

/** A BLS signature which can be aggregated with other signatures. */
@Immutable
public final class BlsSignature implements DataStreamSerializable, StateSerializable {

  private final G1 signatureValue;

  /**
   * Read BLS signature from stream. Assumes signatures are always written as compressed points.
   *
   * @param stream to read from
   * @return read BLS signature
   */
  public static BlsSignature read(SafeDataInputStream stream) {
    return new BlsSignature(stream.readBytes(G1.BYTE_SIZE));
  }

  /**
   * Read BLS signature from stream. Assumes signatures are always written as compressed points.
   *
   * @param stream to read from
   * @return read BLS signature
   */
  public static BlsSignature readUnsafe(SafeDataInputStream stream) {
    G1 point = G1.createUnsafe(stream.readBytes(G1.BYTE_SIZE));
    return new BlsSignature(point);
  }

  /**
   * Creates signature from bytes. Checks if its a valid signature.
   *
   * @param signatureBytes bytes to create signature from
   */
  public BlsSignature(byte[] signatureBytes) {
    G1 signatureValue = G1.create(signatureBytes);
    if (signatureValue.isPointAtInfinity()) {
      throw new IllegalArgumentException("Signature is zero");
    }
    this.signatureValue = signatureValue;
  }

  /**
   * Create a BLS signature.
   *
   * @param signatureValue value of signature
   */
  BlsSignature(G1 signatureValue) {
    this.signatureValue = signatureValue;
  }

  /**
   * Add a new signature to this one. If <code>sig0</code> is the current signature, which is valid
   * for the public key <code>pk0</code>, and <code>signature</code> is the signature that this
   * method is called with, and where <code>signature</code> is valid for the public key <code>pk
   * </code> then the return value is a signature <code>sig1</code> such that <code>sig1</code> is
   * valid for <code>pk.addPublicKey(pk0)</code>.
   *
   * @param signature the new signature
   * @return a new aggregate signature.
   */
  public BlsSignature addSignature(BlsSignature signature) {
    return new BlsSignature(signatureValue.addPoint(signature.getSignatureValue()));
  }

  /**
   * Get the signature value.
   *
   * @return signature value
   */
  public G1 getSignatureValue() {
    return signatureValue;
  }

  /**
   * Writes the object to a {@link SafeDataOutputStream}.
   *
   * @param stream the destination stream for this object
   */
  @Override
  public void write(SafeDataOutputStream stream) {
    stream.write(signatureValue.serialize(true));
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BlsSignature that = (BlsSignature) o;
    return signatureValue.equals(that.signatureValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(signatureValue);
  }
}
