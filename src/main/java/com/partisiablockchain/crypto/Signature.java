package com.partisiablockchain.crypto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.crypto.Curve.CURVE;

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import java.util.Objects;
import org.bouncycastle.asn1.x9.X9IntegerConverter;
import org.bouncycastle.math.ec.ECAlgorithms;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.math.ec.custom.sec.SecP256K1Curve;
import org.bouncycastle.util.encoders.Hex;

/**
 * Signature is a sign of a hash based on a private key.
 *
 * <p>This file includes derived source code from the third-party library EthereumJ
 * (https://github.com/ethereum/ethereumj).
 *
 * <p>The derived source code is used under the Apache 2.0 license.
 *
 * <p>The function {@link #recoverPublicKey(Hash)}, which implements signature recovery as specified
 * in SEC1v2 section 4.1.6 is derived from the function recoverFromSignature() in the specific file
 * and version linked here
 * https://github.com/ethereum/ethereumj/blob/348ae7617a6063bd074049ef228d26f1f66c2590/ethereumj-core/src/main/java/org/ethereum/crypto/ECKey.java
 *
 * <p>The following changes has been made: 1. Renamed function 2. Modified which classes are used
 * from other libraries. 3. Modified function entry and exit to fit the purposes of the Partisia
 * Blockchain Software
 */
@Immutable
public final class Signature implements DataStreamSerializable, StateSerializable {

  private final BigInteger valueR;
  private final BigInteger valueS;
  private final int recoveryId;

  /**
   * Construct a new signature with the supplied values.
   *
   * @param recoveryId the id used to recover public key when verifying signature
   * @param valueR r value in signature
   * @param valueS s value in signature
   */
  public Signature(int recoveryId, BigInteger valueR, BigInteger valueS) {
    if (recoveryId < 0 || recoveryId > 3) {
      throw new IllegalArgumentException("Invalid recovery id: " + recoveryId);
    }
    check(valueR.signum() >= 0, "r must be positive");
    check(valueS.signum() >= 0, "s must be positive");
    this.valueR = valueR;
    this.valueS = valueS;
    this.recoveryId = recoveryId;
  }

  /**
   * Recover the sender of the signature for the given message hash.
   *
   * @param messageHash the signed message
   * @return the recovered sender or null if signature is invalid
   */
  @SuppressWarnings("WeakerAccess")
  public BlockchainAddress recoverSender(Hash messageHash) {
    BlockchainPublicKey publicKey = recoverPublicKey(messageHash);
    if (publicKey == null) {
      return null;
    } else {
      return publicKey.createAddress();
    }
  }

  /**
   * Given the components of a signature and a selector value, recover and return the public key
   * that generated the signature according to the algorithm in SEC1v2 section 4.1.6.
   *
   * <p>The recId is an index from 0 to 3 which indicates which of the 4 possible keys is the
   * correct one. Because the key recovery operation yields multiple potential keys, the correct key
   * must either be stored alongside the signature, or you must be willing to try each recId in turn
   * until you find one that outputs the key you are expecting.
   *
   * <p>If this method returns null it means recovery was not possible and recId should be iterated.
   *
   * <p>Given the above two points, a correct usage of this method is inside a for loop from 0 to 3,
   * and if the output is null OR a key that is not the one you expect, you try again with the next
   * recId.
   *
   * @param messageHash Hash of the data that was signed.
   * @return 65-byte encoded public key
   */
  public BlockchainPublicKey recoverPublicKey(Hash messageHash) {
    final int recId = getRecoveryId();
    final BigInteger r = getR();
    final BigInteger s = getS();
    if (messageHash == null) {
      throw new IllegalArgumentException("messageHash must not be null");
    }
    // 1.0 For j from 0 to h   (h == recId here and the loop is outside this function)
    //   1.1 Let x = r + jn
    // Curve order.
    BigInteger n = CURVE.getN();
    BigInteger i = BigInteger.valueOf((long) recId / 2);
    BigInteger x = r.add(i.multiply(n));
    //   1.2. Convert the integer x to an octet string X of length mlen using the conversion routine
    //        specified in Section 2.3.7, where mlen = ?(log2 p)/8? or mlen = ?m/8?.
    //   1.3. Convert the octet string (16 set binary digits)||X to an elliptic curve point R using
    //        the conversion routine specified in Section 2.3.4. If this conversion routine outputs
    //        �invalid�, then do another iteration of Step 1.
    //
    // More concisely, what these points mean is to use X as a compressed public key.
    SecP256K1Curve curve = (SecP256K1Curve) CURVE.getCurve();
    // Bouncy Castle is not consistent about the letter it uses for the prime.
    BigInteger prime = curve.getQ();
    if (x.compareTo(prime) >= 0) {
      // Cannot have point co-ordinates larger than this as everything takes place modulo Q.
      return null;
    }
    // Compressed keys require you to know an extra bit of data about the y-coord as there are two
    // possibilities. So it's encoded in the recId.
    ECPoint pointR = decompressKey(x, (recId & 1) == 1);
    //   1.4. If nR != point at infinity, then do another iteration of Step 1
    //        (callers responsibility).
    //   NOTE: Since R i a valid point and the used curve is of prime order the below will always
    //         be the case. Hence it has been removed.
    // if (!R.multiply(n).isInfinity()) {
    //   return null;
    // }
    //   1.5. Compute e from M using Steps 2 and 3 of ECDSA signature verification.
    BigInteger e = new BigInteger(1, messageHash.getBytes());
    //   1.6. For k from 1 to 2 do the following.(loop is outside this function via iterating recId)
    //   1.6.1. Compute a candidate public key as:
    //               Q = mi(r) * (sR - eG)
    //
    // Where mi(x) is the modular multiplicative inverse. We transform this into the following:
    //               Q = (mi(r) * s ** R) + (mi(r) * -e ** G)
    // Where -e is the modular additive inverse of e, that is z such that z + e = 0 (mod n). In the
    // above equation ** is point multiplication and + is point addition (the EC group operator).
    //
    // We can find the additive inverse by subtracting e from zero then taking the mod. For example
    // the additive inverse of 3 modulo 11 is 8 because 3 + 8 mod 11 = 0, and -3 mod 11 = 8.
    BigInteger inverseE = BigInteger.ZERO.subtract(e).mod(n);
    BigInteger inverseR = r.modInverse(n);
    BigInteger firstProduct = inverseR.multiply(s).mod(n);
    BigInteger secondProduct = inverseR.multiply(inverseE).mod(n);
    ECPoint ecPoint =
        ECAlgorithms.sumOfTwoMultiplies(CURVE.getG(), secondProduct, pointR, firstProduct);
    return new BlockchainPublicKey(ecPoint);
  }

  private ECPoint decompressKey(BigInteger coordinate, boolean sign) {
    X9IntegerConverter x9 = new X9IntegerConverter();
    byte[] compEnc = x9.integerToBytes(coordinate, 1 + x9.getByteLength(CURVE.getCurve()));
    compEnc[0] = (byte) (sign ? 0x03 : 0x02);
    return CURVE.getCurve().decodePoint(compEnc);
  }

  /**
   * Get the r value in signature.
   *
   * @return r value in signature
   */
  @SuppressWarnings("WeakerAccess")
  public BigInteger getR() {
    return valueR;
  }

  /**
   * Get the s value in signature.
   *
   * @return s value in signature
   */
  public BigInteger getS() {
    return valueS;
  }

  /**
   * Get the id used to recover public key when verifying signature.
   *
   * @return id used to recover public key when verifying signature
   */
  @SuppressWarnings("WeakerAccess")
  public int getRecoveryId() {
    return recoveryId;
  }

  private void check(boolean valid, String message) {
    if (!valid) {
      throw new IllegalArgumentException(message);
    }
  }

  /**
   * Write the signature as a hex string.
   *
   * @return the hex encoded signature
   */
  public String writeAsString() {
    byte[] bytes = new byte[65];
    bytes[0] = (byte) recoveryId;
    System.arraycopy(zeroPad(valueR), 0, bytes, 1, 32);
    System.arraycopy(zeroPad(valueS), 0, bytes, 33, 32);
    return Hex.toHexString(bytes);
  }

  /**
   * Reads a signature from a string representation. Consistent with writeAsString.
   *
   * @param string the hex encoded signature
   * @return the decoded signature
   */
  public static Signature fromString(String string) {
    byte[] bytes = Hex.decode(string);
    return new Signature(
        bytes[0], new BigInteger(1, bytes, 1, 32), new BigInteger(1, bytes, 33, 32));
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.writeByte(recoveryId);
    stream.write(zeroPad(valueR));
    stream.write(zeroPad(valueS));
  }

  /**
   * Reads a signature from the supplied stream.
   *
   * @param stream the stream to read from
   * @return the read signature
   */
  public static Signature read(SafeDataInputStream stream) {
    byte recoveryId = stream.readSignedByte();
    return new Signature(recoveryId, readBigInteger(stream), readBigInteger(stream));
  }

  private static BigInteger readBigInteger(SafeDataInputStream stream) {
    return new BigInteger(1, stream.readBytes(32));
  }

  private byte[] zeroPad(BigInteger value) {
    byte[] bytes = value.toByteArray();
    int offset;
    if (bytes[0] == 0) {
      // The byte is only included to contain sign
      offset = 1;
    } else {
      offset = 0;
    }

    int bytesToWrite = bytes.length - offset;
    int destination = 32 - bytesToWrite;
    byte[] result = new byte[32];
    System.arraycopy(bytes, offset, result, destination, bytesToWrite);
    return result;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Signature signature = (Signature) o;
    return recoveryId == signature.recoveryId
        && Objects.equals(valueR, signature.valueR)
        && Objects.equals(valueS, signature.valueS);
  }

  @Override
  public int hashCode() {
    return Objects.hash(valueR, valueS, recoveryId);
  }
}
