package com.partisiablockchain.crypto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.stream.SafeFixedListStream;
import com.secata.stream.SafeListStream;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.function.Consumer;
import org.bouncycastle.util.encoders.Hex;

/** Is the result of a hashing process, a list of bytes passed through a stream. */
@Immutable
public final class Hash implements DataStreamSerializable, Comparable<Hash>, StateSerializable {

  /** Byte length of a hash. */
  @SuppressWarnings("WeakerAccess")
  public static final int BYTES = 32;

  /** Utility for serializing and deserializing a list of hashes. */
  @SuppressWarnings("WeakerAccess")
  public static final SafeListStream<Hash> LIST_SERIALIZER =
      SafeListStream.create(Hash::read, Hash::write);

  /** Utility for serializing and deserializing a fixed list of hashes. */
  public static final SafeFixedListStream<Hash> FIXED_LIST_SERIALIZER =
      SafeFixedListStream.create(Hash::read, Hash::write);

  @SuppressWarnings("Immutable")
  private final byte[] value;

  private Hash(byte[] value) {
    if (value.length != BYTES) {
      String template = "Too few bytes, expected %d but was %d";
      throw new IllegalArgumentException(String.format(template, BYTES, value.length));
    }
    this.value = value;
  }

  /**
   * Create hash from stream supplier.
   *
   * @param serializable supplies stream
   * @return created hash
   */
  public static Hash create(Consumer<SafeDataOutputStream> serializable) {
    return createSize(serializable).hash();
  }

  /**
   * Create a hash based on the supplied stream consumer.
   *
   * @param serializable the payload being hashed.
   * @return a {@link HashAndSize} object.
   */
  public static HashAndSize createSize(Consumer<SafeDataOutputStream> serializable) {
    HashStream hashStream = new HashStream();
    SafeDataOutputStream stream = new SafeDataOutputStream(hashStream);
    serializable.accept(stream);
    return new HashAndSize(hashStream.getHash(), stream.size());
  }

  /**
   * Create hash and size from this and no size.
   *
   * @return hash and size
   */
  public HashAndSize withZeroSize() {
    return withSize(0);
  }

  /**
   * Create hash and size from this and provided size.
   *
   * @param size size of data on disk
   * @return hash and size
   */
  public HashAndSize withSize(int size) {
    return new HashAndSize(this, size);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Hash hash = (Hash) o;
    return Arrays.equals(value, hash.value);
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.write(value);
  }

  @Override
  public int hashCode() {
    return Arrays.hashCode(value);
  }

  @Override
  public String toString() {
    return Hex.toHexString(value);
  }

  /**
   * Parse the given hex to a hash. Consistent with toString.
   *
   * @param hash the hex encoded hash
   * @return the decoded hash
   */
  public static Hash fromString(String hash) {
    byte[] decode = Hex.decode(hash);
    return new Hash(decode);
  }

  /**
   * Reads a hash from the specified stream.
   *
   * @param dataInputStream the stream to read from
   * @return the read hash
   */
  public static Hash read(SafeDataInputStream dataInputStream) {
    return new Hash(dataInputStream.readBytes(32));
  }

  /**
   * Get the value as bytes.
   *
   * @return value as bytes
   */
  public byte[] getBytes() {
    return value.clone();
  }

  @Override
  public int compareTo(Hash o) {
    return Arrays.compare(this.value, o.value);
  }

  private static final class HashStream extends OutputStream {

    private final MessageDigest digest;

    /** Create a new hash stream. */
    HashStream() {
      this.digest =
          ExceptionConverter.call(
              () -> MessageDigest.getInstance("SHA-256"), "Unable to get SHA256");
    }

    @Override
    public void write(byte[] b, int off, int len) {
      digest.update(b, off, len);
    }

    @Override
    public void write(int b) {
      digest.update((byte) b);
    }

    /**
     * Get the hash.
     *
     * @return hash
     */
    public Hash getHash() {
      return new Hash(digest.digest());
    }
  }

  /** Holds a combination of a hash and the corresponding size on disc. */
  public static final class HashAndSize {

    private final Hash hash;
    private final int size;

    private HashAndSize(Hash hash, int size) {
      this.hash = hash;
      this.size = size;
    }

    /**
     * Create next hash and size with additional size.
     *
     * @param subSize additional size
     * @return next hash and size with additional size
     */
    public HashAndSize withSubSize(int subSize) {
      return new HashAndSize(hash, size + subSize);
    }

    /**
     * Get the hash.
     *
     * @return hash
     */
    public Hash hash() {
      return hash;
    }

    /**
     * Get the size on disk.
     *
     * @return size on disk
     */
    public int size() {
      return size;
    }
  }
}
