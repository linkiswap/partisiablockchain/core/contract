package com.partisiablockchain.crypto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.stream.SafeListStream;
import java.math.BigInteger;
import java.util.Objects;
import org.bouncycastle.crypto.agreement.ECDHCBasicAgreement;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.util.encoders.Base64;

/**
 * Wraps an Elliptic Curve Point (ECPoint) into something serializable and hides the ECPoint
 * implementation.
 */
@Immutable
public final class BlockchainPublicKey implements StateSerializable {

  /** Utility for serializing and deserializing lists of points on curve. */
  public static final SafeListStream<BlockchainPublicKey> LIST_SERIALIZER =
      SafeListStream.create(
          stream -> BlockchainPublicKey.fromEncodedEcPoint(stream.readBytes(33)),
          BlockchainPublicKey::write);

  @SuppressWarnings("Immutable")
  private final ECPoint ecPoint;

  /**
   * Returns a wrapper of an ECPoint.
   *
   * @param ecPoint The ECPoint which is to be wrapped
   */
  BlockchainPublicKey(ECPoint ecPoint) {
    this.ecPoint = ecPoint;
  }

  /**
   * Decodes a serialized point on the elliptic curve and creates a blockchain public key.
   *
   * @param bytes the serialized point
   * @return the point on the elliptic curve corresponding to the supplied bytes.
   */
  public static BlockchainPublicKey fromEncodedEcPoint(byte[] bytes) {
    return new BlockchainPublicKey(Curve.CURVE.getCurve().decodePoint(bytes));
  }

  /**
   * Read a BlockchainPublicKey over the stream.
   *
   * @param stream data stream
   * @return deserialized public key
   */
  public static BlockchainPublicKey read(SafeDataInputStream stream) {
    return BlockchainPublicKey.fromEncodedEcPoint(stream.readBytes(33));
  }

  /**
   * Serialize a BlockchainPublicKey over the stream.
   *
   * @param stream data stream
   */
  public void write(SafeDataOutputStream stream) {
    stream.write(asBytes());
  }

  /**
   * Get a byte encoded public key.
   *
   * @return the byte encoded public key
   */
  public byte[] asBytes() {
    return Objects.requireNonNull(ecPoint).getEncoded(true);
  }

  /**
   * Derive ECDHC shared secret from this public key and a private key.
   *
   * @param privateKey another party's private key
   * @return the derived shared secret
   */
  public Hash deriveSharedKey(BigInteger privateKey) {
    BigInteger shared = calculateAgreement(privateKey);
    return Hash.create(h -> h.write(shared.toByteArray()));
  }

  /**
   * Derive ECDHC shared secret from this public key, a private key, and some extra data.
   *
   * @param privateKey another party's private key
   * @param extraData some extra data
   * @return the derived shared secret
   */
  public Hash deriveSharedKey(BigInteger privateKey, byte[] extraData) {
    BigInteger shared = calculateAgreement(privateKey);
    return Hash.create(
        h -> {
          h.write(shared.toByteArray());
          h.write(extraData);
        });
  }

  private BigInteger calculateAgreement(BigInteger privateKey) {
    ECPrivateKeyParameters key = new ECPrivateKeyParameters(privateKey, Curve.CURVE);
    ECPublicKeyParameters keyParameters = new ECPublicKeyParameters(ecPoint, Curve.CURVE);
    ECDHCBasicAgreement agreement = new ECDHCBasicAgreement();
    agreement.init(key);
    return agreement.calculateAgreement(keyParameters);
  }

  /**
   * Get the uncompressed bytes of the elliptic curve point.
   *
   * @return elliptic curve point in uncompressed byte form.
   */
  public byte[] getEcPointBytes() {
    return ecPoint.getEncoded(false);
  }

  /**
   * Create an address from a public key.
   *
   * @return a blockchain address corresponding to the public key
   */
  public BlockchainAddress createAddress() {
    return BlockchainAddress.fromHash(
        BlockchainAddress.Type.ACCOUNT, Hash.create(s -> s.write(ecPoint.getEncoded(false))));
  }

  @Override
  public boolean equals(Object other) {
    if (other == null) {
      return false;
    }
    if (this.getClass() != other.getClass()) {
      return false;
    }
    return Objects.requireNonNull(this.ecPoint).equals(((BlockchainPublicKey) other).ecPoint);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.ecPoint);
  }

  @Override
  public String toString() {
    return Base64.toBase64String(this.asBytes());
  }
}
