package com.partisiablockchain.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;
import java.util.Objects;

/**
 * The group of events after a call to a binder.
 *
 * @param <EventT> the type of events.
 */
public final class BinderEventGroup<EventT extends BinderEvent> implements CallResult {

  /** The events being sent in this group. */
  private final List<EventT> events;
  /** The callback RPC that will be sent when all events complete. null if there is no callback. */
  private final byte[] callbackRpc;
  /** The cost allocated to the callback. null if there is no callback. */
  private final Long callbackCost;
  /** True if the callback cost should be paid by the spawning contract. */
  private final boolean callbackCostFromContract;

  /**
   * Creates a new event group without callback information.
   *
   * @param events the events in this group
   */
  public BinderEventGroup(List<EventT> events) {
    this.callbackRpc = null;
    this.callbackCost = null;
    this.events = events;
    this.callbackCostFromContract = false;
  }

  /**
   * Creates a new event group with callback.
   *
   * @param callback the callback rpc
   * @param callbackCost the callback cost
   * @param callbackCostFromContract the cost will be paid by the current contract
   * @param events the events in this group
   */
  public BinderEventGroup(
      byte[] callback, long callbackCost, boolean callbackCostFromContract, List<EventT> events) {
    this.callbackRpc = Objects.requireNonNull(callback);
    this.callbackCostFromContract = callbackCostFromContract;
    this.callbackCost = callbackCost;
    this.events = events;
  }

  /**
   * Gets the callback information for this group.
   *
   * <p>null if empty
   *
   * @return the callback
   */
  public byte[] getCallbackRpc() {
    return callbackRpc;
  }

  /**
   * Gets the callback cost for this group.
   *
   * <p>null if empty
   *
   * @return the callback cost
   */
  public Long getCallbackCost() {
    return callbackCost;
  }

  /**
   * Get whether the callback cost will be paid by the current contract.
   *
   * @return true if the callback cost should be paid by the spawning contract.
   */
  public boolean isCallbackCostFromContract() {
    return callbackCostFromContract;
  }

  /**
   * The interactions that this group contains.
   *
   * @return the list of executable events
   */
  public List<EventT> getEvents() {
    return events;
  }
}
