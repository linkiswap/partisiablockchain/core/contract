package com.partisiablockchain.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.serialization.StateSerializable;

/**
 * Binder contracts binds the actual, deployed contracts to the blockchain.
 *
 * <p>These contracts serves as the glue between the blockchain and the programmed contract.
 *
 * <p>A binder is deployed together with the actual contract, and therefore PBC allows different
 * versions of binders and thereby changes in the API for the contracts, since the binders are
 * runtime loaded and bridge the API differences.
 *
 * <p>Binders are intended to deliverer as a part of PBC.
 *
 * <p>For ZK contracts, all the shared orchestration happens in the binder
 *
 * @param <StateT> the contract state type
 * @param <ContextT> the type of context for the contract call
 * @param <EventT> the type of events that this binder creates
 */
public interface BinderContract<
    StateT extends StateSerializable, ContextT, EventT extends BinderEvent> {

  /**
   * Gets the class for the contract state.
   *
   * @return the class
   */
  Class<StateT> getStateClass();

  /**
   * Execute a creation of a contract.
   *
   * @param context contract context
   * @param rpc invocation data for creation
   * @return initial contract state
   */
  BinderResult<StateT, EventT> create(ContextT context, byte[] rpc);

  /**
   * Execute an invocation on contract.
   *
   * @param context contract context
   * @param state contract state
   * @param rpc invocation data for invocation
   * @return next contract state
   */
  BinderResult<StateT, EventT> invoke(ContextT context, StateT state, byte[] rpc);

  /**
   * Execute a callback for contract.
   *
   * @param context contract context
   * @param state contract state
   * @param callbackContext call back context
   * @param rpc invocation data for callback
   * @return next contract state
   */
  BinderResult<StateT, EventT> callback(
      ContextT context, StateT state, CallbackContext callbackContext, byte[] rpc);
}
