package com.partisiablockchain.binder.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderEvent;

/** Deploys a contract. */
public final class BinderDeploy implements BinderEvent {

  /** Target contract address. */
  public final BlockchainAddress contract;
  /** Binder jar. */
  public final byte[] binderJar;
  /** Contract jar. */
  public final byte[] contractJar;
  /** Abi. */
  public final byte[] abi;
  /** Rpc for init. */
  public final byte[] rpc;
  /** Sends as the original sender. */
  public final boolean originalSender;
  /** Effective cost. */
  public final long effectiveCost;

  /**
   * Creates a deploy event.
   *
   * @param originalSender send as the original sender
   * @param effectiveCost the cost that this event has been assigned
   * @param contract address to deploy
   * @param binderJar Binder jar
   * @param contractJar Contract jar
   * @param abi abi
   * @param rpc init rpc
   */
  public BinderDeploy(
      BlockchainAddress contract,
      byte[] binderJar,
      byte[] contractJar,
      byte[] abi,
      byte[] rpc,
      boolean originalSender,
      long effectiveCost) {
    this.contract = contract;
    this.binderJar = binderJar;
    this.contractJar = contractJar;
    this.abi = abi;
    this.rpc = rpc;
    this.originalSender = originalSender;
    this.effectiveCost = effectiveCost;
  }
}
