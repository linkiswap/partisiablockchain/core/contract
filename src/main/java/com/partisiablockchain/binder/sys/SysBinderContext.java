package com.partisiablockchain.binder.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderContext;
import com.partisiablockchain.contract.sys.Governance;
import com.partisiablockchain.contract.sys.PluginInteractionCreator;
import com.partisiablockchain.fee.InfrastructurePayer;
import com.partisiablockchain.fee.ServicePayer;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.tools.immutable.FixedList;

/**
 * Contract context for a system binder contract.
 *
 * @see SysBinderContract
 */
public interface SysBinderContext extends ServicePayer, InfrastructurePayer, BinderContext {

  /**
   * Get the global state of the plugin responsible for handling accounts.
   *
   * @return the currently active accounts plugin's global state
   */
  StateSerializable getGlobalAccountPluginState();

  /**
   * Get the interaction creator for the currently active account plugin.
   *
   * @return the interaction creator
   */
  PluginInteractionCreator getAccountPluginInteractions();

  /**
   * Get the global state of the plugin responsible for handling consensus.
   *
   * @return the currently active consensus plugin's global state
   */
  StateSerializable getGlobalConsensusPluginState();

  /**
   * Get the interaction creator for the currently active consensus plugin.
   *
   * @return the interaction creator
   */
  PluginInteractionCreator getConsensusPluginInteractions();

  /**
   * Gets the overall governance state for the blockchain.
   *
   * @return the governance object
   */
  Governance getGovernance();

  /**
   * Retrieve a feature flag from the blockchain.
   *
   * @param key the key of the feature to fetch
   * @return the set feature value or null is not set
   */
  String getFeature(String key);

  /**
   * Register the fees to be paid out to BYOC oracle nodes.
   *
   * @param amount the amount to be paid out
   * @param symbol the symbol of the coin
   * @param nodes the oracle nodes
   */
  void registerDeductedByocFees(
      Unsigned256 amount, String symbol, FixedList<BlockchainAddress> nodes);
}
