package com.partisiablockchain.binder.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;

/** The definition of the system events. */
public interface SystemInteraction extends BinderEvent {

  /** Creates an account on the blockchain. */
  final class CreateAccount implements SystemInteraction {

    /** address. */
    public final BlockchainAddress address;

    /**
     * Creates an account on the blockchain.
     *
     * @param address the address to create
     */
    public CreateAccount(BlockchainAddress address) {
      this.address = address;
    }
  }

  /** Adds a shard to the list of active shards. */
  final class CreateShard implements SystemInteraction {

    /** shardId. */
    public final String shardId;

    /**
     * Adds a shard to the list of active shards.
     *
     * @param shardId id of the shard
     */
    public CreateShard(String shardId) {
      this.shardId = shardId;
    }
  }

  /** Removes a shard from the list of active shards. */
  final class RemoveShard implements SystemInteraction {

    /** shardId. */
    public final String shardId;

    /**
     * Removes a shard from the list of active shards.
     *
     * @param shardId id of the shard
     */
    public RemoveShard(String shardId) {
      this.shardId = shardId;
    }
  }

  /** Update the local state of the plugin responsible for handling accounts. */
  final class UpdateLocalAccountPluginState implements SystemInteraction {

    /** update. */
    public final LocalPluginStateUpdate update;

    /**
     * Update the local state of the plugin responsible for handling accounts.
     *
     * @param update the update to execute
     */
    public UpdateLocalAccountPluginState(LocalPluginStateUpdate update) {
      this.update = update;
    }
  }

  /** Update the context free state of the account plugin on a named shard. */
  final class UpdateContextFreeAccountPluginState implements SystemInteraction {

    /** shardId. */
    public final String shardId;
    /** rpc. */
    public final byte[] rpc;

    /**
     * Update the context free state of the account plugin on a named shard.
     *
     * @param shardId the shard on which to update the plugin
     * @param rpc rpc for local state update
     */
    public UpdateContextFreeAccountPluginState(String shardId, byte[] rpc) {
      this.shardId = shardId;
      this.rpc = rpc;
    }
  }

  /**
   * Update the context free state of the account plugin on a named shard.
   *
   * <p>/** Update the global state of the plugin responsible for handling accounts.
   */
  final class UpdateGlobalAccountPluginState implements SystemInteraction {

    /** update. */
    public final GlobalPluginStateUpdate update;

    /**
     * Update the global state of the plugin responsible for handling accounts.
     *
     * @param update the update to execute
     */
    public UpdateGlobalAccountPluginState(GlobalPluginStateUpdate update) {
      this.update = update;
    }
  }

  /** Update the account plugin to the code in the supplied jar using the supplied migration rpc. */
  final class UpdateAccountPlugin implements SystemInteraction {

    /** pluginJar. */
    public final byte[] pluginJar;
    /** rpc. */
    public final byte[] rpc;

    /**
     * Update the account plugin to the code in the supplied jar using the supplied migration rpc.
     *
     * @param pluginJar jar containing the actual plugin
     * @param rpc rpc for global state migration
     */
    public UpdateAccountPlugin(byte[] pluginJar, byte[] rpc) {
      this.pluginJar = pluginJar;
      this.rpc = rpc;
    }
  }

  /** Update the local state of the plugin responsible for handling consensus. */
  final class UpdateLocalConsensusPluginState implements SystemInteraction {

    /** update. */
    public final LocalPluginStateUpdate update;

    /**
     * Update the local state of the plugin responsible for handling consensus.
     *
     * @param update the update to execute
     */
    public UpdateLocalConsensusPluginState(LocalPluginStateUpdate update) {
      this.update = update;
    }
  }

  /** Update the global state of the plugin responsible for handling consensus. */
  final class UpdateGlobalConsensusPluginState implements SystemInteraction {

    /** update. */
    public final GlobalPluginStateUpdate update;

    /**
     * Update the global state of the plugin responsible for handling consensus.
     *
     * @param update the update to execute
     */
    public UpdateGlobalConsensusPluginState(GlobalPluginStateUpdate update) {
      this.update = update;
    }
  }

  /**
   * Update the consensus plugin to the code in the supplied jar using the supplied migration rpc.
   */
  final class UpdateConsensusPlugin implements SystemInteraction {

    /** pluginJar. */
    public final byte[] pluginJar;
    /** rpc. */
    public final byte[] rpc;

    /**
     * Update the consensus plugin to the code in the supplied jar using the supplied migration rpc.
     *
     * @param pluginJar jar containing the actual plugin
     * @param rpc rpc for global state migration
     */
    public UpdateConsensusPlugin(byte[] pluginJar, byte[] rpc) {
      this.pluginJar = pluginJar;
      this.rpc = rpc;
    }
  }

  /** Update the routing plugin to the code in the supplied jar using the supplied migration rpc. */
  final class UpdateRoutingPlugin implements SystemInteraction {

    /** pluginJar. */
    public final byte[] pluginJar;
    /** rpc. */
    public final byte[] rpc;

    /**
     * Update the routing plugin to the code in the supplied jar using the supplied migration rpc.
     *
     * @param pluginJar jar containing the actual plugin
     * @param rpc rpc for global state migration
     */
    public UpdateRoutingPlugin(byte[] pluginJar, byte[] rpc) {
      this.pluginJar = pluginJar;
      this.rpc = rpc;
    }
  }

  /** Forcibly remove an existing contract. */
  final class RemoveContract implements SystemInteraction {

    /** contract. */
    public final BlockchainAddress contract;

    /**
     * Forcibly remove an existing contract.
     *
     * @param contract the contract to remove
     */
    public RemoveContract(BlockchainAddress contract) {
      this.contract = contract;
    }
  }

  /** Identify if a contract/account exists on the blockchain. */
  final class Exists implements SystemInteraction {

    /** address. */
    public final BlockchainAddress address;

    /**
     * Identify if a contract/account exists on the blockchain.
     *
     * @param address the address of the contract or account
     */
    public Exists(BlockchainAddress address) {
      this.address = address;
    }
  }

  /** Set a feature flag in the blockchain. */
  final class SetFeature implements SystemInteraction {

    /** key. */
    public final String key;
    /** value. */
    public final String value;

    /**
     * Set a feature flag in the blockchain.
     *
     * @param key the key of the feature
     * @param value the value of the feature
     */
    public SetFeature(String key, String value) {
      this.key = key;
      this.value = value;
    }
  }

  /** Upgrade a system contract. */
  final class UpgradeSystemContract implements SystemInteraction {

    /** JAR of the contract. */
    public final byte[] contractJar;
    /** JAR of the binder. */
    public final byte[] binderJar;
    /** RPC for the upgrade. */
    public final byte[] rpc;
    /** ABI of the contract. */
    public final byte[] abi;
    /** Address of the contract. */
    public final BlockchainAddress contractAddress;

    /**
     * Upgrade a system contract.
     *
     * @param contractJar the contract.
     * @param binderJar the binder.
     * @param rpc the invocation.
     * @param abi the invocation.
     * @param contractAddress the address of the contract.
     */
    public UpgradeSystemContract(
        byte[] contractJar,
        byte[] binderJar,
        byte[] rpc,
        byte[] abi,
        BlockchainAddress contractAddress) {
      this.contractJar = contractJar;
      this.binderJar = binderJar;
      this.rpc = rpc;
      this.abi = abi;
      this.contractAddress = contractAddress;
    }
  }
}
