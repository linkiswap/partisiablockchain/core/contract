package com.partisiablockchain.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** After a call to a smart contract and a result is produced, this object holds the result. */
public final class CallReturnValue implements CallResult {
  /** The callback RPC that will be sent when all events complete. null if there is no callback. */
  private final byte[] result;

  /**
   * Creates a new call return value.
   *
   * @param result the actual result.
   */
  public CallReturnValue(byte[] result) {
    this.result = result;
  }

  /**
   * Gets the result as bytes.
   *
   * @return the bytes.
   */
  public byte[] getResult() {
    return result;
  }
}
