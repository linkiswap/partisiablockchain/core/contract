package com.partisiablockchain.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;

/** Interacts with a contract. */
public final class BinderInteraction implements BinderEvent {

  /** Target contract address. */
  public final BlockchainAddress contract;
  /** Rpc for the contract. */
  public final byte[] rpc;
  /** Sends as the original sender. */
  public final boolean originalSender;
  /** Effective cost. */
  public final long effectiveCost;
  /** True if the cost should be paid by the spawning contract. */
  public final boolean costFromContract;

  /**
   * Handles an interaction event.
   *
   * @param contract Contract address
   * @param rpc Invocation data
   * @param originalSender send as the original sender
   * @param effectiveCost the cost that this event has been assigned
   * @param costFromContract the cost will be paid by the current contract
   */
  public BinderInteraction(
      BlockchainAddress contract,
      byte[] rpc,
      boolean originalSender,
      long effectiveCost,
      boolean costFromContract) {
    this.contract = contract;
    this.rpc = rpc;
    this.originalSender = originalSender;
    this.effectiveCost = effectiveCost;
    this.costFromContract = costFromContract;
  }
}
