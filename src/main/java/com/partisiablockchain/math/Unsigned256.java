package com.partisiablockchain.math;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.util.NumericConversion;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Arrays;

/** Custom implementation of unsigned 256 integer. */
@Immutable
public final class Unsigned256 implements DataStreamSerializable, Comparable<Unsigned256> {

  @SuppressWarnings("Immutable")
  private final long[] value;

  static final int BYTE_SIZE = 32;
  private static final int NUMBER_OF_LIMBS = 4;

  /** The constant 0. */
  public static final Unsigned256 ZERO = new Unsigned256(new long[NUMBER_OF_LIMBS]);
  /** The constant 1. */
  public static final Unsigned256 ONE = new Unsigned256(new long[] {1, 0, 0, 0});
  /** The constant 10. */
  public static final Unsigned256 TEN = new Unsigned256(new long[] {10, 0, 0, 0});

  private Unsigned256(long[] value) {
    this.value = value;
  }

  /**
   * Creates an unsigned 256 integer from a long.
   *
   * @param value long value to wrap
   * @return unsigned 256 integer
   */
  public static Unsigned256 create(long value) {
    if (value < 0) {
      throw new ArithmeticException("Unable to create a negative unsigned number");
    }
    return new Unsigned256(new long[] {value, 0, 0, 0});
  }

  /**
   * Creates an unsigned 256 integer from a base 10 number.
   *
   * @param value as base 10 number
   * @return unsigned 256 integer
   */
  public static Unsigned256 create(String value) {
    return create(new BigInteger(value, 10));
  }

  /**
   * Creates an unsigned 256 integer from a BigInteger.
   *
   * @param value BigInteger to create a 256 unsigned integer
   * @return unsigned 256 integer
   */
  public static Unsigned256 create(BigInteger value) {
    if (value.signum() == -1) {
      throw new ArithmeticException("Unable to create a negative unsigned number");
    }
    return create(value.toByteArray());
  }

  /**
   * Creates an unsigned 256 integer from an array of bytes.
   *
   * @param bytes to be converted into number
   * @return unsigned 256 integer
   */
  public static Unsigned256 create(byte[] bytes) {
    long[] limbs = new long[NUMBER_OF_LIMBS];
    byte[] createBytes = padToElementByteSize(bytes);
    for (int i = 0; i < NUMBER_OF_LIMBS; i++) {
      limbs[i] = NumericConversion.longFromBytes(createBytes, (NUMBER_OF_LIMBS - i - 1) * 8);
    }
    return new Unsigned256(limbs);
  }

  /**
   * Add two unsigned 256 integers.
   *
   * @param other the other element
   * @return the sum of this and other.
   */
  public Unsigned256 add(Unsigned256 other) {
    long[] result = new long[NUMBER_OF_LIMBS];
    long difference = 0;
    for (int i = 0; i < NUMBER_OF_LIMBS; i++) {
      long[] add = add64(this.value[i], other.value[i], difference);
      result[i] = add[0];
      difference = add[1];
    }

    if (difference != 0) {
      throw new ArithmeticException("An overflow occurred");
    }
    return new Unsigned256(result);
  }

  /**
   * Subtracts two unsigned 256 integers.
   *
   * @param other the other element
   * @return the difference of this and other.
   */
  public Unsigned256 subtract(Unsigned256 other) {
    long[] result = new long[NUMBER_OF_LIMBS];
    long borrow = 0;
    for (int i = 0; i < NUMBER_OF_LIMBS; i++) {
      long[] ss = sub64(this.value[i], other.value[i], borrow);
      result[i] = ss[0];
      borrow = ss[1];
    }
    if (borrow != 0) {
      throw new ArithmeticException("An underflow occurred");
    }
    return new Unsigned256(result);
  }

  /**
   * Increments the unsigned integer by one.
   *
   * @return this + 1;
   */
  public Unsigned256 increment() {
    return this.add(ONE);
  }

  /**
   * Decrements the unsigned integer by one.
   *
   * @return this - 1;
   */
  public Unsigned256 decrement() {
    return this.subtract(ONE);
  }

  /**
   * Multiply two unsigned 256 integers.
   *
   * @param other the other element
   * @return the product of this and other.
   */
  public Unsigned256 multiply(Unsigned256 other) {
    long[] product = new long[NUMBER_OF_LIMBS];
    int leftLen = nonZeroLength(this.value);
    int rightLen = nonZeroLength(other.value);
    for (int i = 0; i < leftLen; i++) {
      long carry = 0;
      long[] ms;
      long leftValue = this.value[i];
      for (int j = 0; j < rightLen; j++) {
        if (i + j < NUMBER_OF_LIMBS) {
          ms = multiplyLongsAndAdd(leftValue, other.value[j], carry, product[i + j]);
          product[i + j] = ms[1];
          carry = ms[0];
        } else {
          throw new ArithmeticException("An overflow occurred");
        }
      }
      if (i + rightLen < NUMBER_OF_LIMBS) {
        product[i + rightLen] = carry;
      } else if (carry != 0) {
        throw new ArithmeticException("An overflow occurred");
      }
    }

    return new Unsigned256(product);
  }

  /**
   * Multiplies two longs and adds a carry to the lower part. Returns the lower part of the
   * multiplication on index 1, and high part on 0
   *
   * @param left value to multiply
   * @param right value to multiply
   * @param carry carry to add
   * @return [highPart, lowPart]
   */
  private static long[] multiplyLongsAndAdd(long left, long right, long carry, long overflow) {
    long[] product = new long[2];
    long lowerPart = left * right;
    long highPart = multiplyHigh(left, right);
    long[] add64 = add64(lowerPart, overflow, 0);
    long[] add642 = add64(add64[0], carry, 0);
    highPart += add64[1];
    highPart += add642[1];
    product[0] = highPart;
    product[1] = add642[0];
    return product;
  }

  /**
   * Converts this Unsigned256 to a long, checking for lost information. If the value of this
   * Unsigned256 is out of the range of the long type, then an ArithmeticException is thrown.
   *
   * @return this Unsigned256 converted to a long
   */
  public long longValueExact() {
    if (nonZeroLength(this.value) > 1 || this.value[0] < 0) {
      throw new ArithmeticException("Unsigned256 out of long range");
    }
    return this.value[0];
  }

  /**
   * Remainder of the two unsigned 256 integers.
   *
   * @param other the other element
   * @return the remainder of this / other.
   */
  public Unsigned256 remainder(Unsigned256 other) {
    if (other.compareTo(ZERO) == 0) {
      throw new ArithmeticException("Division by zero");
    }
    int cmp = compareValues(this.value, other.value);
    if (cmp < 0) {
      return this;
    }
    if (cmp == 0) {
      return ZERO;
    }
    int m = nonZeroLength(this.value);
    int n = nonZeroLength(other.value);
    int s = Long.numberOfLeadingZeros(other.value[n - 1]);
    long[][] normalizedValues = normalize(this.value, other.value, m, n, s);
    long[] un = normalizedValues[0];
    long[] vn = normalizedValues[1];
    if (n == 1) {
      long[] remInter = divideOneWord(un, vn[0], m)[1];
      long[] rem = new long[] {remInter[0] >>> s, 0, 0, 0};
      return new Unsigned256(rem);
    }
    long[] remInter = divideMultiWord(un, vn, m + 1, n)[1];
    long[] rem = new long[NUMBER_OF_LIMBS];
    if (s > 0) {
      for (int i = 0; i <= n - 2; i++) {
        rem[i] = (remInter[i] >>> s) | (remInter[i + 1] << (Long.SIZE - s));
      }
      rem[n - 1] = remInter[n - 1] >>> s;
    } else {
      rem = Arrays.copyOf(remInter, NUMBER_OF_LIMBS);
    }

    return new Unsigned256(rem);
  }

  /**
   * Divide two unsigned 256 integers.
   *
   * @param other the other element
   * @return the division of this and other.
   */
  public Unsigned256 divide(Unsigned256 other) {
    if (other.compareTo(ZERO) == 0) {
      throw new ArithmeticException("Division by zero");
    }
    int cmp = compareValues(this.value, other.value);
    if (cmp < 0) {
      return ZERO;
    }
    if (cmp == 0) {
      return ONE;
    }
    int m = nonZeroLength(this.value);
    int n = nonZeroLength(other.value);
    int s = Long.numberOfLeadingZeros(other.value[n - 1]);
    long[][] normalizedValues = normalize(this.value, other.value, m, n, s);
    long[] un = normalizedValues[0];
    long[] vn = normalizedValues[1];
    if (n == 1) {
      return new Unsigned256(divideOneWord(un, vn[0], m)[0]);
    }
    return new Unsigned256(divideMultiWord(un, vn, m + 1, n)[0]);
  }

  private long[][] normalize(long[] u, long[] v, int m, int n, int s) {
    long[] un;
    long[] vn;
    if (s > 0) {
      vn = new long[n];
      for (int i = n - 1; i > 0; i--) {
        vn[i] = (v[i] << s) | (v[i - 1] >>> (64 - s));
      }
      vn[0] = v[0] << s;

      un = new long[m + 1];
      un[m] = u[m - 1] >>> (64 - s);
      for (int i = m - 1; i > 0; i--) {
        un[i] = (u[i] << s) | (u[i - 1] >>> (64 - s));
      }
      un[0] = u[0] << s;
    } else {
      un = Arrays.copyOfRange(this.value, 0, m + 1);
      vn = Arrays.copyOfRange(v, 0, n);
    }
    return new long[][] {un, vn};
  }

  /**
   * Adds two longs and a carry. Returns the result in index 0 and carry in index 1. The carry must
   * be a value of 0 or 1.
   *
   * @param x 64 bit integer
   * @param y 64 bit integer
   * @param carry a value 0 or 1.
   * @return [x + y + carry, newCarry]
   */
  static long[] add64(long x, long y, long carry) {
    long sum = x + y + carry;
    // The sum will overflow if both top bits are set (x & y) or if one of them
    // is (x | y), and a carry from the lower place happened. If such a carry
    // happens, the top bit will be 1 + 0 + 1 = 0 (&^ sum).
    long carryOut = ((x & y) | ((x | y) & ~sum)) >>> 63;
    return new long[] {sum, carryOut};
  }

  /**
   * Division by one word divisor. Uses the principle of school division:
   * https://en.wikipedia.org/wiki/Long_division. Also uses the algorithm from
   * https://gmplib.org/~tege/division-paper.pdf to calculate 128 bit / 64 bit division.
   *
   * @param un normalized dividend
   * @param d normalized divisor
   * @param m length of dividend
   * @return [quotient, remainder]
   */
  private long[][] divideOneWord(long[] un, long d, int m) {
    long[] result = new long[NUMBER_OF_LIMBS];
    long reciprocal = reciprocal2By1(d);
    long rem = un[m];
    for (int i = m - 1; i >= 0; i--) {
      long[] interRes = udivrem2by1(rem, un[i], d, reciprocal);
      result[i] = interRes[0];
      rem = interRes[1];
    }
    return new long[][] {result, new long[] {rem}};
  }

  /**
   * Divides a 128 bit with a 64 bit integer as per https://gmplib.org/~tege/division-paper.pdf
   * algorithm 4.
   *
   * @param uh the high 64 bit
   * @param ul the low 64 bit
   * @param d the 64 bit divisor
   * @param reciprocal reciprocal of divisor
   * @return [quotient, remainder]
   */
  static long[] udivrem2by1(long uh, long ul, long d, long reciprocal) {
    long qh = multiplyHigh(reciprocal, uh);
    long ql = reciprocal * uh;
    long[] addRes = add64(ql, ul, 0);
    qh = add64(qh, uh, addRes[1])[0];
    ql = addRes[0];
    qh++;
    long r = ul - qh * d;
    if (Long.compareUnsigned(r, ql) > 0) {
      qh--;
      r += d;
    }
    if (Long.compareUnsigned(r, d) >= 0) {
      qh++;
      r -= d;
    }
    return new long[] {qh, r};
  }

  /**
   * Returns as a long the most significant 64 bits of the 128-bit product of two 64-bit factors.
   * Follows from Hacker's Delight section 8.3
   *
   * @param x the first value
   * @param y the second value
   * @return the result
   */
  static long multiplyHigh(long x, long y) {
    long p = Math.multiplyHigh(x, y);
    long t1 = (x >> 63) & y;
    long t2 = (y >> 63) & x;
    return p + t1 + t2;
  }

  /**
   * Calculates the reciprocal as by the following paper:
   * https://gmplib.org/~tege/division-paper.pdf.
   *
   * @param d value to calculate reciprocal
   * @return the result
   */
  static long reciprocal2By1(long d) {
    return divide2By1(~d, ~0, d)[1];
  }

  /**
   * Implements a modified Knuths division algorithm. The modification is the usage of
   * https://gmplib.org/~tege/division-paper.pdf in hopes to get a speedup.
   *
   * @param un normalized dividend
   * @param vn normalized divisor
   * @param m length of dividend
   * @param n length of divisor
   * @return [quotient, remainder]
   */
  private long[][] divideMultiWord(long[] un, long[] vn, int m, int n) {
    long[] result = new long[NUMBER_OF_LIMBS];

    long dh = vn[n - 1];
    long dl = vn[n - 2];
    long reciprocal = reciprocal2By1(dh);

    for (int j = m - n - 1; j >= 0; j--) {
      long u2 = un[j + n];
      long u1 = un[j + n - 1];
      long u0 = un[j + n - 2];

      long qhat;
      long rhat;
      if (Long.compareUnsigned(u2, dh) >= 0) {
        // Divisor overflow
        qhat = -1;
      } else {
        long[] inter = udivrem2by1(u2, u1, dh, reciprocal);
        qhat = inter[0];
        rhat = inter[1];
        long ph = multiplyHigh(qhat, dl);
        long pl = qhat * dl;
        if (Long.compareUnsigned(ph, rhat) > 0) {
          qhat--;
        } else if (ph == rhat && Long.compareUnsigned(pl, u0) > 0) {
          qhat--;
        }
      }

      long borrow = subToMul(un, vn, qhat, j);
      un[j + n] = u2 - borrow;
      if (Long.compareUnsigned(u2, borrow) < 0) {
        qhat--;
        un[j + n] += addTo(un, vn, j);
      }
      result[j] = qhat;
    }
    return new long[][] {result, un};
  }

  /**
   * Calculates un[j + i] - qhat * vn[i] for i=0 upto n.
   *
   * @param un normalized dividend
   * @param vn normalized divisor
   * @param qhat multiplier
   * @param j offset
   * @return Borrow for last index.
   */
  private long subToMul(long[] un, long[] vn, long qhat, int j) {
    long borrow = 0;
    for (int i = 0; i < vn.length; i++) {
      long[] ms = sub64(un[i + j], borrow, 0);
      long ph = multiplyHigh(vn[i], qhat);
      long pl = vn[i] * qhat;
      long[] ss = sub64(ms[0], pl, 0);
      un[i + j] = ss[0];
      borrow = ms[1] + ss[1] + ph;
    }
    return borrow;
  }

  /**
   * Sub64 returns the difference of x, y and borrow: diff = x - y - borrow. The borrow input must
   * be 0 or 1; The borrowOut output is guaranteed to be 0 or 1.
   *
   * @param x first value
   * @param y second value
   * @param borrow value between 0 and 1
   * @return [x - y - borrow, newBorrow]
   */
  static long[] sub64(long x, long y, long borrow) {
    long diff = x - y - borrow;
    // The difference will underflow if the top bit of x is not set and the top
    // bit of y is set (^x & y) or if they are the same (^(x ^ y)) and a borrow
    // from the lower place happens. If that borrow happens, the result will be
    // 1 - 1 - 1 = 0 - 0 - 1 = 1 (& diff).
    long borrowOut = ((~x & y) | (~(x ^ y) & diff)) >>> 63;
    return new long[] {diff, borrowOut};
  }

  /**
   * Adds back the divisor into the dividend.
   *
   * @param un normalized dividend
   * @param vn normalized divisor
   * @param j offset
   * @return carry for last addition
   */
  static long addTo(long[] un, long[] vn, int j) {
    long carry = 0;
    for (int i = 0; i < vn.length; i++) {
      long[] ms = add64(un[j + i], vn[i], carry);
      un[j + i] = ms[0];
      carry = ms[1];
    }
    return carry;
  }

  /**
   * Divides 128 bit by 64 bit. The divisor to needs be normalized. Furthermore it assumes the most
   * significant bit is set in v and not set in u1. This means that we can optimize and remove q1/q0
   * >= base as its impossible. The loop will loop at most 2 times. This is because 2x vn1 >= base.
   *
   * @param u1 Upper 64 bit
   * @param u0 lower 64 bit
   * @param v divisor
   * @return [rem, quo]
   */
  static long[] divide2By1(long u1, long u0, long v) {
    final long base = 1L << Integer.SIZE;
    final long baseMask = base - 1;
    long vn1 = v >>> Integer.SIZE;

    long q1 = Long.divideUnsigned(u1, vn1);
    long rhat = Long.remainderUnsigned(u1, vn1);

    long un1 = u0 >>> Integer.SIZE;
    long vn0 = v & baseMask;
    long whileCheck = q1 * vn0;

    while (Long.compareUnsigned(whileCheck, base * rhat + un1) > 0) {
      q1--;
      rhat += vn1;
      if (rhat >> 32 >= 1) {
        break;
      }
    }

    // Multiply and subtract.
    long un21 = u1 * base + un1 - q1 * v;
    long un0 = u0 & baseMask;
    long q0 = Long.divideUnsigned(un21, vn1);

    rhat = Long.remainderUnsigned(un21, vn1);

    whileCheck = q0 * vn0;
    while (Long.compareUnsigned(whileCheck, base * rhat + un0) > 0) {
      q0--;
      rhat += vn1;
      if (rhat >> 32 >= 1) {
        break;
      }
    }

    return new long[] {un21 * base + un0 - q0 * v, q1 * base + q0};
  }

  /**
   * Converts value array in internal representation into an array of bytes.
   *
   * @return a serialized unsigned 256 integer
   */
  public byte[] serialize() {
    byte[] bytes = new byte[BYTE_SIZE];
    for (int i = 0; i < NUMBER_OF_LIMBS; i++) {
      NumericConversion.longToBytes(this.value[i], bytes, (NUMBER_OF_LIMBS - i - 1) * 8);
    }
    return bytes;
  }

  static byte[] padToElementByteSize(byte[] bytes) {
    if (bytes.length > BYTE_SIZE && bytes[0] != 0) {
      throw new IllegalStateException(
          "Cannot create 256 bit number with " + bytes.length * Byte.SIZE + " bits");
    } else if (bytes.length == BYTE_SIZE + 1) {
      return Arrays.copyOfRange(bytes, 1, BYTE_SIZE + 1);
    } else {
      ByteBuffer buffer =
          ByteBuffer.allocate(BYTE_SIZE)
              .position(BYTE_SIZE - bytes.length); // offset to get correct amount of leading zeros
      return buffer.put(bytes).array();
    }
  }

  private static int nonZeroLength(long[] array) {
    for (int i = NUMBER_OF_LIMBS - 1; i >= 0; i--) {
      if (array[i] != 0) {
        return i + 1;
      }
    }
    return 0;
  }

  private static int compareValues(long[] left, long[] right) {
    long res;
    for (int i = NUMBER_OF_LIMBS - 1; i >= 0; i--) {
      res = Long.compareUnsigned(left[i], right[i]);
      if (res > 0) {
        return 1;
      } else if (res < 0) {
        return -1;
      }
    }
    return 0;
  }

  @Override
  public String toString() {
    return new BigInteger(1, serialize()).toString(10);
  }

  @Override
  public boolean equals(Object other) {
    if (this == other) {
      return true;
    }
    if (other == null || getClass() != other.getClass()) {
      return false;
    }
    Unsigned256 element = (Unsigned256) other;
    return Arrays.equals(this.value, element.value);
  }

  @Override
  public int hashCode() {
    return Arrays.hashCode(value);
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.write(serialize());
  }

  /**
   * Reads an unsigned 256 integer from the stream.
   *
   * @param stream to read from
   * @return read unsigned 256 integer
   */
  public static Unsigned256 read(SafeDataInputStream stream) {
    return create(stream.readBytes(BYTE_SIZE));
  }

  @Override
  public int compareTo(Unsigned256 o) {
    return compareValues(this.value, o.value);
  }
}
