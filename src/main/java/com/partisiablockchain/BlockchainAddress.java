package com.partisiablockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.stream.SafeListStream;
import java.util.Arrays;
import java.util.StringJoiner;
import org.bouncycastle.util.encoders.Hex;

/**
 * Represent an address on the PBC blockchain. Is derived from a hash or a EcPoint private key.
 * There are different types, one for persons/account - the rest are different contract types.
 * Accounts are created from private keys, contract from hashes.
 */
@Immutable
public final class BlockchainAddress
    implements DataStreamSerializable, Comparable<BlockchainAddress>, StateSerializable {

  /** Utility for serializing and deserializing a list of blockchain addresses. */
  public static final SafeListStream<BlockchainAddress> LIST_SERIALIZER =
      SafeListStream.create(BlockchainAddress::read, BlockchainAddress::write);

  private final Type type;

  @SuppressWarnings("Immutable")
  private final byte[] identifier;

  /**
   * Construct a new account.
   *
   * @param type the type of the address.
   * @param identifier the 20 bytes identifying this account
   */
  private BlockchainAddress(Type type, byte[] identifier) {
    this.type = type;
    this.identifier = identifier;
  }

  /**
   * Create an address from a hash. The last 20 bytes of the hash is used as the identifier.
   *
   * @param type the type of account to create
   * @param addressHash the hash to use as identifier
   * @return a new blockchain address
   */
  public static BlockchainAddress fromHash(Type type, Hash addressHash) {
    return new BlockchainAddress(type, hashToIdentifier(addressHash));
  }

  /**
   * Create an address from a hex string.
   *
   * @param encodedAddress the encoded address
   * @return the deconded address
   */
  public static BlockchainAddress fromString(String encodedAddress) {
    byte[] decode = Hex.decode(encodedAddress);
    Type type = Type.values()[Byte.toUnsignedInt(decode[0])];
    byte[] identifier = new byte[20];
    System.arraycopy(decode, 1, identifier, 0, 20);
    return new BlockchainAddress(type, identifier);
  }

  private static byte[] hashToIdentifier(Hash hash) {
    return Arrays.copyOfRange(hash.getBytes(), 12, 32);
  }

  /**
   * Read blockchain address from stream.
   *
   * @param stream to read from
   * @return read blockchain address
   */
  public static BlockchainAddress read(SafeDataInputStream stream) {
    return stream.readEnum(Type.values()).read(stream);
  }

  /**
   * Get the identifier.
   *
   * @return identifier
   */
  public byte[] getIdentifier() {
    return identifier.clone();
  }

  /**
   * Get the type of address.
   *
   * @return type of address
   */
  public Type getType() {
    return type;
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    type.write(stream);
    stream.write(identifier);
  }

  /**
   * Write the blockchain address as string.
   *
   * @return blockchain address as string
   */
  public String writeAsString() {
    return Hex.toHexString(new byte[] {(byte) type.ordinal()}) + Hex.toHexString(identifier);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BlockchainAddress that = (BlockchainAddress) o;
    return type == that.type && Arrays.equals(identifier, that.identifier);
  }

  @Override
  public int hashCode() {
    return Arrays.deepHashCode(new Object[] {type, identifier});
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", BlockchainAddress.class.getSimpleName() + "[", "]")
        .add("identifier=" + writeAsString())
        .toString();
  }

  @Override
  public int compareTo(BlockchainAddress that) {
    int typeCompare = type.compareTo(that.type);
    if (typeCompare != 0) {
      return typeCompare;
    } else {
      return Arrays.compare(identifier, that.identifier);
    }
  }

  /**
   * Types of address for a blockchain address. This type is written as the first byte in the
   * address.
   */
  public enum Type implements DataStreamSerializable {
    /** External users that have an account on the blockchain. */
    ACCOUNT,
    /** System contracts are the public part of the operating system on the blockchain. */
    CONTRACT_SYSTEM,
    /** Public contracts are regular smart contract on the blockchain. */
    CONTRACT_PUBLIC,
    /**
     * Zero knowledge contracts are the more complicated contracts that are similar to public
     * contracts, but have confidential state as well.
     */
    CONTRACT_ZK,
    /**
     * Governance contracts are a special type of system contract that is deemed necessary for the
     * system to run and therefore are (mostly) free to interact with.
     */
    CONTRACT_GOV;

    @Override
    public void write(SafeDataOutputStream stream) {
      stream.writeEnum(this);
    }

    private BlockchainAddress read(SafeDataInputStream stream) {
      byte[] data = stream.readBytes(20);
      return new BlockchainAddress(this, data);
    }
  }
}
