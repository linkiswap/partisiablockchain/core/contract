package com.partisiablockchain.tree;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.google.errorprone.annotations.ImmutableTypeParameter;
import com.partisiablockchain.crypto.Hash.HashAndSize;
import java.util.Set;
import java.util.function.Supplier;

/**
 * A leaf in the Avl tree.
 *
 * @param <K> the type of keys
 * @param <V> the type of values to associate with keys
 */
@Immutable
public final class AvlLeaf<
        @ImmutableTypeParameter K extends Comparable<K>, @ImmutableTypeParameter V>
    extends AvlTree<K, V> {

  @SuppressWarnings("Immutable")
  private final K key;

  @SuppressWarnings("Immutable")
  private final Supplier<V> lazyValue;

  private final V value;

  @SuppressWarnings("Immutable")
  private transient HashAndSize hash;

  /**
   * Construct a new AvlLeaf when read from storage.
   *
   * @param key the key of the leaf
   * @param value the value of the leaf
   */
  public AvlLeaf(K key, V value) {
    this.key = key;
    this.value = value;
    this.lazyValue = null;
  }

  /**
   * Construct a new Lazy AvlLeaf.
   *
   * @param key the key of the leaf
   * @param lazyValue a supplier for the value
   */
  public AvlLeaf(K key, Supplier<V> lazyValue) {
    this.key = key;
    this.value = null;
    this.lazyValue = lazyValue;
  }

  @Override
  public V getValue(K key) {
    if (this.key.compareTo(key) == 0) {
      return getValue();
    } else {
      return null;
    }
  }

  private V getValue() {
    if (lazyValue != null) {
      return lazyValue.get();
    } else {
      return value;
    }
  }

  @Override
  public AvlTree<K, V> remove(K key) {
    if (this.key.compareTo(key) == 0) {
      return new EmptyTree<>();
    } else {
      return this;
    }
  }

  @Override
  public AvlTree<K, V> set(K key, V value) {
    int compare = this.key.compareTo(key);
    if (compare > 0) {
      return new AvlComposite<>(new AvlLeaf<>(key, value), this);
    } else if (compare < 0) {
      return new AvlComposite<>(this, new AvlLeaf<>(key, value));
    } else {
      return new AvlLeaf<>(key, value);
    }
  }

  @Override
  K maxKeyInTree() {
    return key;
  }

  @Override
  byte height() {
    return 0;
  }

  @Override
  public int size() {
    return 1;
  }

  @Override
  int balance() {
    return 0;
  }

  @Override
  K getKey() {
    return key;
  }

  @Override
  public void write(TreeWriter<K, V> writer) {
    if (value != null) {
      writer.writeLeaf(calculateHash(writer), key, value);
    }
  }

  @Override
  public void freshWrite(TreeWriter<K, V> writer) {
    writer.writeLeaf(calculateHash(writer), key, getValue());
  }

  @Override
  public HashAndSize calculateHash(TreeWriter<K, V> treeWriter) {
    if (hash == null) {
      hash = treeWriter.hashLeaf(key, getValue());
    }
    return hash;
  }

  @Override
  public void keySet(Set<K> keys) {
    keys.add(key);
  }

  @Override
  public void visitPath(TreeWriter<K, V> writer, K key, TreePathVisitor<K, V> pathVisitor) {
    if (key.equals(this.key)) {
      pathVisitor.leaf(key, getValue());
    } else {
      throw new IllegalStateException("Key " + key + " does not exist");
    }
  }
}
