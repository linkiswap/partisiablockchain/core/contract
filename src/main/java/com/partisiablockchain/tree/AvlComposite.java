package com.partisiablockchain.tree;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.google.errorprone.annotations.ImmutableTypeParameter;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Hash.HashAndSize;
import java.util.Objects;
import java.util.Set;

/**
 * A node in the avl tree.
 *
 * @param <K> the type of keys
 * @param <V> the type of values to associate with keys
 */
@Immutable
public final class AvlComposite<
        @ImmutableTypeParameter K extends Comparable<K>, @ImmutableTypeParameter V>
    extends AvlTree<K, V> {

  @SuppressWarnings("Immutable")
  private K maxKeyInTree;

  private final byte height;
  private final int size;

  private final LazyNode<K, V> leftNode;
  private final LazyNode<K, V> rightNode;

  @SuppressWarnings("Immutable")
  private HashAndSize hash;

  @SuppressWarnings("Immutable")
  private TreeReader<K, V> reader;

  /**
   * Create a new AvlComposite.
   *
   * @param leftNode the left node in the tree
   * @param rightNode the right node in the tree
   */
  public AvlComposite(AvlTree<K, V> leftNode, AvlTree<K, V> rightNode) {
    this.maxKeyInTree = rightNode.maxKeyInTree();
    this.height = (byte) (Math.max(leftNode.height(), rightNode.height()) + 1);
    this.size = leftNode.size() + rightNode.size();
    this.leftNode = new LazyNode<>(leftNode);
    this.rightNode = new LazyNode<>(rightNode);
  }

  /**
   * Construct a new lazy AvlComposite.
   *
   * @param reader the reader used to lazy load nodes
   * @param identifier the identifier of this composite node
   * @param left the identifier my left child
   * @param right the identifier of my right child
   * @param size the size of the tree rooted at this node
   * @param height the height of the tree rooted at this node
   */
  public AvlComposite(
      TreeReader<K, V> reader,
      HashAndSize identifier,
      Hash left,
      Hash right,
      int size,
      byte height) {
    this.reader = reader;
    this.height = height;
    this.size = size;
    this.leftNode = new LazyNode<>(left);
    this.rightNode = new LazyNode<>(right);
    this.hash = identifier;
  }

  @Override
  K getKey() {
    return getLeft().maxKeyInTree();
  }

  @Override
  public void write(TreeWriter<K, V> writer) {
    HashAndSize hash = calculateHash(writer);
    writer.writeComposite(hash, leftNode.hash, rightNode.hash, size, height);
    leftNode.write(writer);
    rightNode.write(writer);
  }

  @Override
  public void freshWrite(TreeWriter<K, V> writer) {
    HashAndSize hash = calculateHash(writer);
    writer.writeComposite(hash, leftNode.hash, rightNode.hash, size, height);
    getLeft().freshWrite(writer);
    getRight().freshWrite(writer);
  }

  @Override
  public HashAndSize calculateHash(TreeWriter<K, V> treeWriter) {
    if (hash == null) {
      HashAndSize left = leftNode.calculateHash(reader, treeWriter);
      HashAndSize right = rightNode.calculateHash(reader, treeWriter);
      this.hash = treeWriter.hashComposite(left, right, size, height);
    }
    return hash;
  }

  @Override
  public void visitPath(TreeWriter<K, V> writer, K key, TreePathVisitor<K, V> pathVisitor) {
    HashAndSize hash = calculateHash(writer);
    pathVisitor.composite(hash, leftNode.hash, rightNode.hash, size, height);
    nextNode(key).visitPath(writer, key, pathVisitor);
  }

  @Override
  public void keySet(Set<K> keys) {
    getLeft().keySet(keys);
    getRight().keySet(keys);
  }

  AvlTree<K, V> getLeft() {
    return leftNode.getValue(reader);
  }

  AvlTree<K, V> getRight() {
    return rightNode.getValue(reader);
  }

  @Override
  int balance() {
    return getLeft().height() - getRight().height();
  }

  @Override
  K maxKeyInTree() {
    if (maxKeyInTree == null) {
      maxKeyInTree = getRight().maxKeyInTree();
    }
    return maxKeyInTree;
  }

  @Override
  byte height() {
    return height;
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public V getValue(K key) {
    return nextNode(key).getValue(key);
  }

  private AvlTree<K, V> nextNode(K key) {
    int compare = this.getKey().compareTo(key);
    AvlTree<K, V> node;
    if (compare >= 0) {
      node = getLeft();
    } else {
      node = getRight();
    }
    return node;
  }

  @Override
  public AvlTree<K, V> remove(K key) {
    int compare = this.getKey().compareTo(key);
    AvlTree<K, V> newNode;
    if (compare >= 0) {
      AvlTree<K, V> node = getLeft().remove(key);
      if (node instanceof EmptyTree) {
        newNode = getRight();
      } else {
        newNode = new AvlComposite<>(node, getRight());
      }
    } else {
      AvlTree<K, V> node = getRight().remove(key);
      if (node instanceof EmptyTree) {
        newNode = getLeft();
      } else {
        newNode = new AvlComposite<>(getLeft(), node);
      }
    }
    return newNode instanceof AvlComposite ? ((AvlComposite<K, V>) newNode).rebalance() : newNode;
  }

  @Override
  public AvlTree<K, V> set(K key, V value) {
    int compare = this.getKey().compareTo(key);
    AvlComposite<K, V> newNode;
    if (compare >= 0) {
      newNode = new AvlComposite<>(getLeft().set(key, value), getRight());
    } else {
      newNode = new AvlComposite<>(getLeft(), getRight().set(key, value));
    }
    return newNode.rebalance();
  }

  private AvlTree<K, V> rebalance() {
    int balance = this.balance();
    if (balance > 1) {
      if (this.getLeft().balance() >= 0) {
        return this.rotateRight();
      } else {
        AvlComposite<K, V> left = (AvlComposite<K, V>) this.getLeft();

        return new AvlComposite<>(left.rotateLeft(), this.getRight()).rotateRight();
      }
    } else if (balance < -1) {
      if (this.getRight().balance() <= 0) {
        return this.rotateLeft();
      } else {
        AvlComposite<K, V> right = (AvlComposite<K, V>) this.getRight();

        return new AvlComposite<>(this.getLeft(), right.rotateRight()).rotateLeft();
      }
    } else {
      return this;
    }
  }

  private AvlTree<K, V> rotateRight() {
    AvlComposite<K, V> left = (AvlComposite<K, V>) getLeft();
    return new AvlComposite<>(left.getLeft(), new AvlComposite<>(left.getRight(), getRight()));
  }

  private AvlTree<K, V> rotateLeft() {
    AvlComposite<K, V> right = (AvlComposite<K, V>) getRight();
    return new AvlComposite<>(new AvlComposite<>(getLeft(), right.getLeft()), right.getRight());
  }

  @Immutable
  static final class LazyNode<K extends Comparable<K>, V> {

    @SuppressWarnings("Immutable")
    private boolean dirty;

    @SuppressWarnings("Immutable")
    private Hash hash;

    @SuppressWarnings("Immutable")
    private AvlTree<K, V> value;

    LazyNode(Hash hash) {
      this.hash = Objects.requireNonNull(hash);
      this.dirty = false;
    }

    LazyNode(AvlTree<K, V> value) {
      this.dirty = true;
      this.value = value;
    }

    AvlTree<K, V> getValue(TreeReader<K, V> reader) {
      if (value == null) {
        this.value = reader.readNode(hash);
      }
      return value;
    }

    public HashAndSize calculateHash(TreeReader<K, V> reader, TreeWriter<K, V> treeWriter) {
      HashAndSize hashAndSize = getValue(reader).calculateHash(treeWriter);
      this.hash = hashAndSize.hash();
      return hashAndSize;
    }

    public void write(TreeWriter<K, V> writer) {
      if (dirty) {
        value.write(writer);
        dirty = false;
      }
    }
  }
}
