package com.partisiablockchain.tree;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;

/**
 * Visitor used to visit all nodes in an AvlTree.
 *
 * @param <K> the type of keys in the tree
 * @param <V> the type of values in the tree
 */
public interface TreePathVisitor<K extends Comparable<K>, V> {

  /**
   * Visit a leaf node.
   *
   * @param key key of node
   * @param value value of node
   */
  void leaf(K key, V value);

  /**
   * Visit composite node.
   *
   * @param hash hash and size for node
   * @param left hash of left child
   * @param right hash of right child
   * @param size size of node
   * @param height distance from root in tree
   */
  void composite(Hash.HashAndSize hash, Hash left, Hash right, int size, byte height);
}
