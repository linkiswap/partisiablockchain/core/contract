package com.partisiablockchain.tree;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.ImmutableTypeParameter;
import com.partisiablockchain.crypto.Hash.HashAndSize;
import java.util.Set;

/**
 * Represents an empty tree, that is neither a leaf or a node.
 *
 * @param <K> the type of keys
 * @param <V> the type of values to associate with keys
 */
public final class EmptyTree<
        @ImmutableTypeParameter K extends Comparable<K>, @ImmutableTypeParameter V>
    extends AvlTree<K, V> {

  @Override
  public V getValue(K key) {
    return null;
  }

  @Override
  public AvlTree<K, V> remove(K key) {
    return this;
  }

  @Override
  public AvlTree<K, V> set(K key, V value) {
    return new AvlLeaf<>(key, value);
  }

  @Override
  K maxKeyInTree() {
    return null;
  }

  @Override
  byte height() {
    return 0;
  }

  @Override
  public int size() {
    return 0;
  }

  @Override
  int balance() {
    return 0;
  }

  @Override
  K getKey() {
    return null;
  }

  @Override
  public void write(TreeWriter<K, V> writer) {
    writer.writeEmpty(calculateHash(writer));
  }

  @Override
  public void freshWrite(TreeWriter<K, V> writer) {
    write(writer);
  }

  @Override
  public HashAndSize calculateHash(TreeWriter<K, V> treeWriter) {
    return treeWriter.hashEmpty();
  }

  @Override
  public void keySet(Set<K> keys) {}

  @Override
  public void visitPath(TreeWriter<K, V> writer, K key, TreePathVisitor<K, V> pathVisitor) {
    throw new IllegalStateException("Key " + key + " does not exist");
  }
}
