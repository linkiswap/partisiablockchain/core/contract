package com.partisiablockchain.tree;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.CheckReturnValue;
import com.google.errorprone.annotations.Immutable;
import com.google.errorprone.annotations.ImmutableTypeParameter;
import com.partisiablockchain.crypto.Hash.HashAndSize;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * An immutable sorted map from keys to values, implemented as a balanced avl tree.
 *
 * @param <K> the type of keys
 * @param <V> the type of values to associate with keys
 */
@Immutable
@CheckReturnValue
public abstract class AvlTree<
    @ImmutableTypeParameter K extends Comparable<K>, @ImmutableTypeParameter V> {

  /**
   * Construct a new empty AvlTree.
   *
   * @param <K> key type
   * @param <V> value type
   * @return new empty tree
   */
  public static <@ImmutableTypeParameter K extends Comparable<K>, @ImmutableTypeParameter V>
      AvlTree<K, V> create() {
    return new EmptyTree<>();
  }

  /**
   * Construct a new AvlTree populated with the values from the supplied map.
   *
   * @param <K> key type
   * @param <V> value type
   * @param initial the initial key value pairs for the tree
   * @return new populated tree
   */
  public static <@ImmutableTypeParameter K extends Comparable<K>, @ImmutableTypeParameter V>
      AvlTree<K, V> create(Map<K, V> initial) {
    List<Map.Entry<K, V>> sortedEntries =
        initial.entrySet().stream().sorted(Map.Entry.comparingByKey()).collect(Collectors.toList());
    AvlTree<K, V> tree = new EmptyTree<>();
    for (Map.Entry<K, V> entry : sortedEntries) {
      tree = tree.set(entry.getKey(), entry.getValue());
    }
    return tree;
  }

  /**
   * Associates the specified value with the specified key.
   *
   * @param key key with which the specified value is to be associated
   * @param value value to be associated with the specified key
   * @return new tree with key and associated value
   */
  public abstract AvlTree<K, V> set(K key, V value);

  /**
   * Removes the mapping for a key from this tree if it is present.
   *
   * @param key key whose mapping is to be removed from the tree
   * @return new tree without key and associated value
   */
  public abstract AvlTree<K, V> remove(K key);

  /**
   * The number of key value pairs in the tree.
   *
   * @return the number of key value pairs
   */
  public abstract int size();

  /**
   * Retrieve the value associated with the given key if any.
   *
   * @param key key whose mapping is to be return
   * @return the associated value or null if none found
   */
  public abstract V getValue(K key);

  abstract K maxKeyInTree();

  abstract byte height();

  abstract int balance();

  abstract K getKey();

  /**
   * Write a tree.
   *
   * @param writer writes a tree
   */
  public abstract void write(TreeWriter<K, V> writer);

  /**
   * Write a tree to a fresh storage.
   *
   * @param writer writes a tree
   */
  public abstract void freshWrite(TreeWriter<K, V> writer);

  /**
   * Calculate hash of root node for a tree.
   *
   * @param treeWriter writes an avl tree
   * @return hash of root node
   */
  public abstract HashAndSize calculateHash(TreeWriter<K, V> treeWriter);

  abstract void keySet(Set<K> keys);

  /**
   * Get a set of all keys in this tree.
   *
   * @return a set containing all keys of this tree
   */
  public Set<K> keySet() {
    Set<K> keys = new LinkedHashSet<>();
    keySet(keys);
    return keys;
  }

  /**
   * Get a list of all values in this tree.
   *
   * @return a list containing all values contained in this tree
   */
  public List<V> values() {
    return keySet().stream().map(this::getValue).collect(Collectors.toList());
  }

  /**
   * Checks if tree contains given key.
   *
   * @param key given
   * @return true if tree contains key
   */
  public boolean containsKey(K key) {
    return getValue(key) != null;
  }

  /**
   * Visit a path in the tree.
   *
   * @param writer can write an avl tree or sub-tree
   * @param key next node to visit
   * @param pathVisitor visitor to visit nodes with
   */
  public abstract void visitPath(TreeWriter<K, V> writer, K key, TreePathVisitor<K, V> pathVisitor);
}
