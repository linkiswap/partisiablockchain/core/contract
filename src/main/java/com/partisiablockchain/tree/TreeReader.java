package com.partisiablockchain.tree;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Hash.HashAndSize;
import com.secata.stream.SafeDataInputStream;
import java.util.function.Supplier;

/**
 * Reads the avl tree from a hash, nodes and leaves can be read lazy.
 *
 * @param <K> the type of keys
 * @param <V> the type of values to associate with keys
 */
public interface TreeReader<K extends Comparable<K>, V> {

  /**
   * read an empty node from storage.
   *
   * @param hash the identifier to store the node under
   * @return AvlNode read from hash
   */
  AvlTree<K, V> readNode(Hash hash);

  /**
   * read a leaf node from storage.
   *
   * @param hash the identifier to store the node under
   * @param stream source to read from
   * @return AvlNode read from hash
   */
  Supplier<AvlTree<K, V>> readLeaf(HashAndSize hash, SafeDataInputStream stream);

  /**
   * read a composite node from storage.
   *
   * @param hash the identifier to store the node under
   * @param stream source to read from
   * @return AvlNode read from hash
   */
  Supplier<AvlTree<K, V>> readComposite(Hash hash, SafeDataInputStream stream);
}
