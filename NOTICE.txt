Partisia Blockchain https://partisiablockchain.com/
Web 3.0 Public Blockchain built for trust, transparency, privacy, and speed of light finalization across all platforms.

Copyright (C) 2021 Partisia Blockchain Foundation

This program includes derived source code from the third-party library EthereumJ (https://github.com/ethereum/ethereumj).
The derived source code is used under the Apache 2.0 license, and contains a notice and states the changes made in the source file containing the derived function.
See /src/main/java/com/partisiablockchain/crypto/Signature.java
